# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0] - 2022-08-03
### Added
- Added 'CHANGELOG.md'

### Changed
- Last version for 'Simsoft' name

### Removed
- [NaughtyAttributes] Removed UPM package dependency

## [0.2.0] - 2021-12-27
### Added
- Added 'Monetization Ads' abstractions to implement custom ad networks. Currently, we have 'AdMob Monetization' implementation.
- New extensions for Unity API
- [Odin] Added UPM package as dependency
- [NaughtyAttributes] Added UPM package as dependency

### Changed
- UPM Package URL: from 'https://bitbucket.org/simofun/simsoft.unity.git?path=/src/Simsoft.Unity/Assets/Simsoft' to 'https://bitbucket.org/simofun/simsoft.unity.git?path=/src/Simsoft.Unity/Assets/Package'
- Following package layout convention followed by official Unity packages
- Renamed and refactored all singletons which have more abstractions.
- Renamed package root folder name from 'Assets/Simsoft' to 'Assets/Package'
- [Coffee UI Effect] UPM package is now optional with 'SIM_COFFEE_UIEFFECTS' define
- [GameAnalytics] UPM package is now optional with 'SIM_GAME_ANALYTICS' define

### Removed
- So, because of renaming singletons; removed deprecated singletons which have [ObsoleteAttribute](https://docs.microsoft.com/dotnet/api/system.obsoleteattribute).

## [0.1.0] - 2021-09-13
### Added
- Abstractions for 'Analytics'
- Assembly definition files for Runtime and Editor side
- Auto save system for Simsoft Unity player prefs
- Extensions for pure C#
- Extensions for Unity API
- Level system with manager scripts. LevelInfo and UserInfo player prefs
- Simsoft Unity prefs implementation
- Singletons for global and scene based Monobehaviour's
- Singleton for Custom Asset(ScriptableObject)
- Utilities for pure C#
- Utilities for Unity API
- [Bayhaksam.Unity.Logging] Logging for Release and Development Builds
- [Coffee UI Effect] Decorate your UI with effects
- [GameAnalytics] 'Analytics' abstractions implementation
- [RandomUserApi] utilities
- [Text Mesh Pro] Extensions

[Unreleased]: https://bitbucket.org/simofun/simofun.unity/branches/compare/HEAD..v0.2.0
[0.3.0]: https://bitbucket.org/simofun/simofun.unity/branches/compare/v0.3.0..v0.2.0
[0.2.0]: https://bitbucket.org/simofun/simofun.unity/branches/compare/v0.2.0..v0.1.0
[0.1.0]: https://bitbucket.org/simofun/simofun.unity/src/v0.1.0
[Coffee UI Effect]: https://github.com/mob-sakai/UIEffect.git
[GameAnalytics]: https://github.com/GameAnalytics/GA-SDK-UNITY.git
[Odin]: https://odininspector.com
[NaughtyAttributes]: https://github.com/dbrizov/NaughtyAttributes.git
[RandomUserApi]: https://randomuser.me
[Text Mesh Pro]: https://docs.unity3d.com/Manual/com.unity.textmeshpro.html
