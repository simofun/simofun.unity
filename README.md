# Simofun Unity Library

UPM Package Url: https://bitbucket.org/simofun/simofun.unity.git?path=/src/Simofun.Unity/Assets/Package

**[Changelog](CHANGELOG.md)**

**Third Party Deps**

* [GameAnalytics (Optional - SIM_GAME_ANALYTICS)](https://github.com/GameAnalytics/GA-SDK-UNITY.git)
* [UIEffect (Optional - SIM_COFFEE_UIEFFECTS)](https://github.com/mob-sakai/UIEffect.git)
* [UniTask](https://github.com/Cysharp/UniTask.git?path=src/UniTask/Assets/Plugins/UniTask)
