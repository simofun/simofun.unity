﻿#if SIM_COFFEE_UIEFFECTS
//-----------------------------------------------------------------------
// <copyright file="UIEffectMenuItems.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.CoffeeUIEffects.Unity.Editor
{
	using Coffee.UIEffects;
	using System;
	using System.Linq;
	using UnityEditor;
	using UnityEditor.Events;
	using UnityEngine;
	using UnityEngine.Events;
	using UnityEngine.EventSystems;
	using UnityEngine.UI;

	public static class UIEffectMenuItems
	{
		#region Fields
		const string rootMenuItemPath = "Tools/Coffee UI Effect/";

		const string shadowOutline8SoftOutlinePointerEnterExitUpMenuItemPath =
			"Shadow - Outline8 - Soft Outline Pointer Enter-Exit-Up";
		#endregion

		#region Methods
		[MenuItem(rootMenuItemPath + shadowOutline8SoftOutlinePointerEnterExitUpMenuItemPath)]
		static void ShadowOutline8SoftOutlinePointerEnterExitUp()
		{
			var obj = Selection.activeGameObject;

			// Add 'UIEffect' component if not exits
			var uiEffectComp = obj.GetComponent<UIEffect>() ?? obj.AddComponent<UIEffect>();
			uiEffectComp.effectMode = EffectMode.None;
			uiEffectComp.colorMode = ColorMode.Fill;
			uiEffectComp.colorFactor = 0;
			uiEffectComp.blurMode = BlurMode.DetailBlur;
			uiEffectComp.blurFactor = 0;
			uiEffectComp.advancedBlur = true;
			uiEffectComp.enabled = false;

			// Add 'UIShadow' component if not exits
			var uiShadowComp = obj.GetComponent<UIShadow>() ?? obj.AddComponent<UIShadow>();
			uiShadowComp.style = ShadowStyle.Outline8;
			uiShadowComp.effectDistance = new Vector2(5f, -5f);
			uiShadowComp.effectColor = new Color(105f / 255f, 1f, 105f / 255f, 128f / 255f);
			uiShadowComp.useGraphicAlpha = true;
			uiShadowComp.blurFactor = 1;
			uiShadowComp.enabled = false;

			// Add 'EventTrigger' component if not exits
			var eventTriggerComp = obj.GetComponent<EventTrigger>() ?? obj.AddComponent<EventTrigger>();

			// Add 'PointerEnter' trigger and callbacks to 'EventTrigger' component if not exits
			var pointerEnterTrigger = eventTriggerComp.triggers
				.FirstOrDefault((t) => t.eventID == EventTriggerType.PointerEnter);
			if (pointerEnterTrigger == null)
			{
				pointerEnterTrigger = new EventTrigger.Entry { eventID = EventTriggerType.PointerEnter };
				eventTriggerComp.triggers.Add(pointerEnterTrigger);
			}

			UnityEventTools.AddBoolPersistentListener(
				pointerEnterTrigger.callback,
				Delegate.CreateDelegate(typeof(UnityAction<bool>), uiEffectComp, "set_enabled") as UnityAction<bool>,
				true);
			UnityEventTools.AddBoolPersistentListener(
				pointerEnterTrigger.callback,
				Delegate.CreateDelegate(typeof(UnityAction<bool>), uiShadowComp, "set_enabled") as UnityAction<bool>,
				true);

			// Add 'PointerExit' trigger and callbacks to 'EventTrigger' component if not exits
			var pointerExitTrigger = eventTriggerComp.triggers
				.FirstOrDefault((t) => t.eventID == EventTriggerType.PointerExit);
			if (pointerExitTrigger == null)
			{
				pointerExitTrigger = new EventTrigger.Entry { eventID = EventTriggerType.PointerExit };
				eventTriggerComp.triggers.Add(pointerExitTrigger);
			}

			UnityEventTools.AddBoolPersistentListener(
				pointerExitTrigger.callback,
				Delegate.CreateDelegate(typeof(UnityAction<bool>), uiEffectComp, "set_enabled") as UnityAction<bool>,
				false);
			UnityEventTools.AddBoolPersistentListener(
				pointerExitTrigger.callback,
				Delegate.CreateDelegate(typeof(UnityAction<bool>), uiShadowComp, "set_enabled") as UnityAction<bool>,
				false);

			// Add 'PointerUp' trigger and callbacks to 'EventTrigger' component if not exits
			var pointerUpTrigger = eventTriggerComp.triggers
				.FirstOrDefault((t) => t.eventID == EventTriggerType.PointerUp);
			if (pointerUpTrigger == null)
			{
				pointerUpTrigger = new EventTrigger.Entry { eventID = EventTriggerType.PointerUp };
				eventTriggerComp.triggers.Add(pointerUpTrigger);
			}

			UnityEventTools.AddBoolPersistentListener(
				pointerUpTrigger.callback,
				Delegate.CreateDelegate(typeof(UnityAction<bool>), uiEffectComp, "set_enabled") as UnityAction<bool>,
				false);
			UnityEventTools.AddBoolPersistentListener(
				pointerUpTrigger.callback,
				Delegate.CreateDelegate(typeof(UnityAction<bool>), uiShadowComp, "set_enabled") as UnityAction<bool>,
				false);

			Undo.RegisterCompleteObjectUndo(obj, shadowOutline8SoftOutlinePointerEnterExitUpMenuItemPath);
		}

		[MenuItem(rootMenuItemPath + shadowOutline8SoftOutlinePointerEnterExitUpMenuItemPath, true)]
		static bool ShadowOutline8SoftOutlinePointerEnterExitUpValidation() =>
			Selection.activeGameObject != null && Selection.activeGameObject.GetComponent<Graphic>() != null;
		#endregion
	}
}
#endif
