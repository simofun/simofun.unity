//-----------------------------------------------------------------------
// <copyright file="SimOdinWatchWindow.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Odin.Unity.Editor.ThirdParty.OdinWatchWindow
{
    using Sirenix.OdinInspector.Editor;
    using Sirenix.Utilities;
    using Sirenix.Utilities.Editor;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEditor;
    using UnityEngine;

    public partial class SimOdinWatchWindow : OdinEditorWindow
    {
        #region Unity Fields
        [SerializeField]
        List<SimTreeValuesHolder> properties = new List<SimTreeValuesHolder>();

        [SerializeField]
        float labelWidth = 200;
        #endregion

        #region Static Fields
        static SimOdinWatchWindow Instance;
        #endregion

        #region Fields
        bool repaintSheduled;

        bool showSettings;
        #endregion

        #region Unity Static Methods
        [MenuItem("Tools/Odin Watch Window")]
        public static void ShowMenu()
        {
            Instance = GetWindow<SimOdinWatchWindow>();
            Instance.Show();
        }

        [InitializeOnLoadMethod]
        private static void OnPropertyContextMenu()
        {
            EditorApplication.contextualPropertyMenu += (menu, property) =>
            {
                property = property.Copy();
                menu.AddItem(new GUIContent("Watch"), false, () =>
                {
                    ShowMenu();
                    PropertyTree tree =
                        PropertyTree.Create(
                            new SerializedObject(property.serializedObject.targetObject));

                    SimTreeValuesHolder holder =
                        Instance.properties.
                            FirstOrDefault(o =>
                            o.Tree.WeakTargets[0] == property.serializedObject.targetObject);

                    if (holder == null)
                    {
                        holder = new SimTreeValuesHolder(tree);
                        Instance.properties.Add(holder);
                        tree.OnPropertyValueChanged += TreeOnOnPropertyValueChanged;
                    }
                    holder.ValuePaths.Add(property.propertyPath);

                });
            };
        }
        #endregion

        #region Unity Methods
        void OnDisable()
        {
            string json = EditorJsonUtility.ToJson(this);
            EditorPrefs.SetString("OWW_props", json);
        }

        protected override void OnEnable()
        {
            this.labelWidth = EditorPrefs.GetFloat("OWW_labelWidth", 200);
            string json = EditorPrefs.GetString("OWW_props", "");
            EditorJsonUtility.FromJsonOverwrite(json, this);
            EditorApplication.playModeStateChanged -= this.PlayModeStateChanged;
            EditorApplication.playModeStateChanged += this.PlayModeStateChanged;
            wantsMouseMove = true;

            for (int i = 0; i < this.properties.Count; i++)
            {
                SimTreeValuesHolder holder = this.properties[i];
                if (!holder.CheckRefresh())
                {
                    this.properties.RemoveAt(i--);
                }
            }
        }

        protected override void OnGUI()
        {
            this.repaintSheduled = false;
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Clear"))
            {
                this.properties.Clear();
            }

            Rect settingsRect = GUILayoutUtility.GetRect(
                24,
                24,
                GUILayout.ExpandWidth(false)).AlignLeft(20).AlignCenterY(20);

            if (SirenixEditorGUI.IconButton(
                settingsRect,
                this.showSettings
                ? EditorIcons.SettingsCog.Inactive
                : EditorIcons.SettingsCog.Active, "Settings"))
            {
                this.showSettings = !this.showSettings;
            }

            GUILayout.EndHorizontal();

            if (this.showSettings)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Space(40);
                GUI.changed = false;
                Rect rect = GUILayoutUtility
                    .GetRect(
                        1,
                        EditorGUIUtility.singleLineHeight,
                        GUILayout.ExpandWidth(true));

                labelWidth = GUI.HorizontalSlider(rect, labelWidth, rect.xMin, rect.xMax);

                if (GUI.changed)
                {
                    EditorPrefs.SetFloat("OWW_labelWidth", labelWidth);
                }

                EditorGUILayout.LabelField("Label Width", GUILayout.Width(70));
                GUILayout.EndHorizontal();
            }

            GUILayout.Space(5);
            bool first = true;

            if (this.properties.Count == 0)
            {
                EditorGUILayout.LabelField(
                    "Right-click any property in an Inspector and select 'Watch' to make it show up here.",
                    SirenixGUIStyles.MultiLineCenteredLabel);
            }

            GUIHelper.PushLabelWidth(labelWidth - 30);

            for (int i = 0; i < this.properties.Count; i++)
            {
                SimTreeValuesHolder holder = this.properties[i];
                holder.CheckRefresh();
                if (!first)
                {
                    GUILayout.Space(5);
                }

                first = false;

                Rect titleRect = SirenixEditorGUI.BeginBox("      " + holder.Tree.TargetType.Name);

                titleRect = titleRect.AlignTop(21);
                if (holder.ParentObject != null)
                {
                    Rect alignRight = titleRect.AlignRight(200).AlignCenterY(16).AlignLeft(180);
                    GUIHelper.PushGUIEnabled(false);
                    SirenixEditorFields.UnityObjectField(alignRight, holder.ParentObject, typeof(GameObject), true);
                    GUIHelper.PopGUIEnabled();
                }

                if (SirenixEditorGUI.IconButton(titleRect.AlignRight(20).AlignCenterY(18), EditorIcons.X))
                {
                    this.properties.RemoveAt(i--);
                }

                Rect titleDragDropRect = titleRect.AlignLeft(30).AlignCenter(20, 20);
                EditorIcons.List.Draw(titleDragDropRect);

                SimTreeValuesHolder treedragdrop =
                    (SimTreeValuesHolder)DragAndDropUtilities.
                        DragAndDropZone(
                            titleDragDropRect,
                            holder,
                            typeof(SimTreeValuesHolder),
                            false,
                            false);

                if (treedragdrop != holder)
                {
                    int treeDragDropIndex = this.properties.IndexOf(treedragdrop);
                    this.Swap(this.properties, treeDragDropIndex, i);
                }

                if (holder.Tree.UnitySerializedObject?.targetObject == null)
                {
                    EditorGUILayout.LabelField(
                        $"This component is no longer valid in the current context (loaded different scene?)",
                        SirenixGUIStyles.MultiLineLabel);
                }
                else
                {
                    InspectorUtilities.BeginDrawPropertyTree(holder.Tree, true);
                    for (int index = 0; index < holder.ValuePaths.Count; index++)
                    {
                        string path = holder.ValuePaths[index];
                        GUILayout.BeginHorizontal();

                        Rect rect1 = GUILayoutUtility.
                            GetRect(
                                EditorGUIUtility.singleLineHeight + 5,
                                EditorGUIUtility.singleLineHeight + 3,
                                GUILayout.ExpandWidth(false)).AlignRight(EditorGUIUtility.singleLineHeight + 2);

                        EditorIcons.List.Draw(rect1);

                        SimValueDragDropHolder dragdrop =
                            (SimValueDragDropHolder)DragAndDropUtilities.
                                DragAndDropZone(
                                rect1,
                                new SimValueDragDropHolder(holder, index),
                                typeof(SimValueDragDropHolder),
                                false,
                                false);

                        if (dragdrop.TreeValuesHolder == holder && dragdrop.Index != index)
                        {
                            string ptemp = holder.ValuePaths[index];
                            holder.ValuePaths[index] = holder.ValuePaths[dragdrop.Index];
                            holder.ValuePaths[dragdrop.Index] = ptemp;
                        }

                        InspectorProperty propertyAtPath = holder.Tree.GetPropertyAtPath(path);
                        if (propertyAtPath == null)
                            propertyAtPath = holder.Tree.GetPropertyAtUnityPath(path);
                        if (propertyAtPath != null)
                        {
                            propertyAtPath.Draw();
                        }
                        else
                            EditorGUILayout.LabelField($"Could not find property ({path})");

                        if (SirenixEditorGUI.IconButton(EditorIcons.X))
                        {
                            holder.ValuePaths.RemoveAt(index--);
                            if (holder.ValuePaths.Count == 0)
                                this.properties.RemoveAt(i--);
                        }

                        GUILayout.Space(3);
                        GUILayout.EndHorizontal();
                    }

                    InspectorUtilities.EndDrawPropertyTree(holder.Tree);
                }

                SirenixEditorGUI.EndBox();
            }

            GUIHelper.PopLabelWidth();
        }
        #endregion

        #region Methods
        void Swap<T>(IList<T> list, int indexA, int indexB)
        {
            T tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
        }

        void OnInspectorUpdate()
        {
            Repaint();
        }

        #region Event Handlers
        void PlayModeStateChanged(PlayModeStateChange obj)
        {
            foreach (SimTreeValuesHolder holder in this.properties)
            {
                holder.CheckRefresh();
            }
        }

        #region Static Event Handlers
        static void TreeOnOnPropertyValueChanged(InspectorProperty property, int selectionindex)
        {
            if (!Instance.repaintSheduled)
                Instance.Repaint();
            Instance.repaintSheduled = true;
        }
        #endregion
        #endregion
        #endregion

        #region Public Static Methods
        public static void AddWatch(InspectorProperty property)
        {
            ShowMenu();
            PropertyTree tree = PropertyTree.Create(
                property.Tree.WeakTargets,
                new SerializedObject(property.Tree.UnitySerializedObject.targetObject));

            SimTreeValuesHolder holder = Instance.properties.
                FirstOrDefault(o =>
                    o.Tree.WeakTargets[0] == property.Tree.WeakTargets[0]);

            if (holder == null)
            {
                holder = new SimTreeValuesHolder(tree);
                Instance.properties.Add(holder);
                tree.OnPropertyValueChanged += TreeOnOnPropertyValueChanged;
            }

            holder.ValuePaths.Add(property.Path);
        }
        #endregion
    }
}
