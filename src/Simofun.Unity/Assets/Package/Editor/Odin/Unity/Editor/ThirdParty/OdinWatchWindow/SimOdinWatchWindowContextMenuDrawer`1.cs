﻿//-----------------------------------------------------------------------
// <copyright file="SimOdinWatchWindowContextMenuDrawer`1.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Odin.Unity.Editor.ThirdParty.OdinWatchWindow
{
    using Sirenix.OdinInspector.Editor;
    using UnityEditor;
    using UnityEngine;

    [DrawerPriority(100, 0, 0)]
    public class SimOdinWatchWindowContextMenuDrawer<T> : OdinValueDrawer<T>, IDefinesGenericMenuItems
    {
        #region Public Methods
        public void PopulateGenericMenu(InspectorProperty property, GenericMenu genericMenu)
        {
            genericMenu.AddItem(new GUIContent("Watch"), false, () => SimOdinWatchWindow.AddWatch(property));
        }
        #endregion

        #region Methods
        protected override void DrawPropertyLayout(GUIContent label)
        {
            this.CallNextDrawer(label);
        }
        #endregion
    }
}
