﻿//-----------------------------------------------------------------------
// <copyright file="SimOdinWatchWindow.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Odin.Unity.Editor.ThirdParty.OdinWatchWindow
{
    using Sirenix.OdinInspector.Editor;
    using System;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;
    using Object = UnityEngine.Object;

    public partial class SimOdinWatchWindow
    {
        [Serializable]
        private class SimTreeValuesHolder
        {
            #region Public Fields
            [NonSerialized]
            public PropertyTree Tree;

            [NonSerialized]
            public Object Target;

            [NonSerialized]
            public Object ParentObject;

            public int InstanceID;

            public List<string> ValuePaths = new List<string>();
            #endregion

            #region Constructors
            public SimTreeValuesHolder(PropertyTree tree)
            {
                this.Tree = tree;
                this.Target = (Object)tree.WeakTargets[0];
                this.InstanceID = this.Target.GetInstanceID();
                this.GetParentObject();
            }
            #endregion

            #region Methods
            void GetParentObject()
            {
                this.ParentObject = (this.Target as MonoBehaviour);
                if (this.ParentObject == null)
                    this.ParentObject = (this.Target as Component);
                if (this.ParentObject == null)
                    this.ParentObject = this.Target as ScriptableObject;
            }
            #endregion

            #region Public Methods
            public bool CheckRefresh()
            {
                if (this.Tree == null || this.Tree.SecretRootProperty == null || this.Target == null)
                {
                    this.Target = EditorUtility.InstanceIDToObject(this.InstanceID);
                    if (this.Target == null)
                        return false;
                    this.GetParentObject();
                    this.Tree = Sirenix.OdinInspector.Editor.PropertyTree.Create(
                        new List<Object> { this.Target },
                        this.ParentObject != null
                        ? new SerializedObject(this.ParentObject)
                        : null);
                }
                return true;
            }
            #endregion
        }
    }
}
