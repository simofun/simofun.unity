﻿//-----------------------------------------------------------------------
// <copyright file="SimOdinWatchWindow.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Odin.Unity.Editor.ThirdParty.OdinWatchWindow
{
    public partial class SimOdinWatchWindow
    {
        private struct SimValueDragDropHolder
        {
            #region Public Fields
            public SimTreeValuesHolder TreeValuesHolder;

            public int Index;
            #endregion

            #region Constructors
            public SimValueDragDropHolder(SimTreeValuesHolder treeValuesHolder, int index)
            {
                this.TreeValuesHolder = treeValuesHolder;
                this.Index = index;
            }
            #endregion
        }
    }
}
