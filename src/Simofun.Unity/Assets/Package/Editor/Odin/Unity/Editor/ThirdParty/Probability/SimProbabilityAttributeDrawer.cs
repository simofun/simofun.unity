﻿//-----------------------------------------------------------------------
// <copyright file="SimProbabilityAttributeDrawer.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Odin.Unity.Editor.ThirdParty.Probability
{
    using Simofun.Odin.Unity.ThirdParty.Probability;
    using Sirenix.OdinInspector.Editor;
    using Sirenix.Utilities;
    using Sirenix.Utilities.Editor;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    public sealed class SimProbabilityAttributeDrawer : OdinAttributeDrawer<SimProbabilityAttribute, float[]>
    {
        #region Enum
        public enum State { None, Down, Up, Drag, Out }
        #endregion

        #region Fields
        string errorMessage;

        InspectorPropertyValueGetter<object[]> baseItemDataGetter;

        State state;

        Vector2 selectSize = new Vector2(30, 30);

        List<float> selectValues = new List<float>();

        List<Rect> ranges = new List<Rect>();

        int selectId = -1;

        Color[] colors;
        #endregion

        #region Constructors
        protected override void Initialize()
        {
            if (this.Attribute.DataMember != null)
            {
                this.baseItemDataGetter =
                    new InspectorPropertyValueGetter<object[]>(this.Property, this.Attribute.DataMember);
                if (this.errorMessage != null)
                {
                    this.errorMessage = this.baseItemDataGetter.ErrorMessage;
                }
            }

            if (this.baseItemDataGetter.GetValue() != null &&
                this.baseItemDataGetter.GetValue().Length != 0)
            {
                this.ValueEntry.SmartValue =
                    this.SetData(baseItemDataGetter.GetValue(), ref this.selectValues, ValueEntry.SmartValue);
            }


            for (int i = 0; i < ColorPaletteManager.Instance.ColorPalettes.Count; i++)
            {
                if (ColorPaletteManager.Instance.ColorPalettes[i].Name == Attribute.ColorName)
                {
                    colors = ColorPaletteManager.Instance.ColorPalettes[i].Colors.ToArray();
                    break;
                }
            }
        }
        #endregion

        #region Unity Methods
        protected override void DrawPropertyLayout(GUIContent label)
        {
            this.DrawAll(this.baseItemDataGetter);
        }
        #endregion

        #region Methods
        float[] SetData<T>(T[] datas, ref List<float> select, float[] value)
        {

            if (datas == null || datas.Length == 0)
            {
                select.Clear();
                this.ranges.Clear();
                return new float[0];

            }

            if (datas.Length == 1)
            {
                select.Clear();
                this.ranges.Clear();
                for (int i = 0; i < datas.Length; i++)
                {
                    this.ranges.Add(new Rect());
                }
                return new float[0];

            }

            //datas改變大小
            if (datas.Length - 1 != value.Length)
            {
                select.Clear();
                for (int i = 0; i < datas.Length; i++)
                {
                    if (i != datas.Length - 1)
                    {
                        select.Add(1 / (float)datas.Length * (i + 1));
                    }
                }
                this.ranges.Clear();
                for (int i = 0; i < datas.Length; i++)
                {
                    this.ranges.Add(new Rect());
                }


            }
            else if (datas.Length - 1 != select.Count)
            {
                //重新算區塊
                select.Clear();
                for (int i = 0; i < datas.Length; i++)
                {
                    if (i != datas.Length - 1)
                    {
                        select.Add(value[i]);
                    }
                }
                this.ranges.Clear();
                for (int i = 0; i < datas.Length; i++)
                {
                    this.ranges.Add(new Rect());
                }

            }
            return select.ToArray();
        }

        void DrawAll<T>(InspectorPropertyValueGetter<T[]> inspectorPropertyValueGetter)
        {
            if (inspectorPropertyValueGetter.GetValue() != null &&
                inspectorPropertyValueGetter.GetValue().Length != 0)
            {
                Rect rect = EditorGUILayout.GetControlRect(false, 40);
                this.DrawRange(rect, this.baseItemDataGetter);
                this.DrawSelect(rect, this.baseItemDataGetter);
            }
        }

        void DrawRange<T>(Rect rect, InspectorPropertyValueGetter<T[]> inspectorPropertyValueGetter)
        {

            if (inspectorPropertyValueGetter.GetValue() != null &&
                inspectorPropertyValueGetter.GetValue().Length != 0)
            {
                SirenixEditorGUI.DrawSolidRect(rect, new Color(0.7f, 0.7f, 0.7f, 1f));
                GUIStyle style = new GUIStyle();
                style.alignment = TextAnchor.UpperCenter;
                GUIStyle percentageStyle = new GUIStyle();
                percentageStyle.alignment = TextAnchor.LowerCenter;

                this.ValueEntry.SmartValue =
                    this.SetData(
                        inspectorPropertyValueGetter.GetValue(),
                        ref this.selectValues, ValueEntry.SmartValue);

                T[] datas = inspectorPropertyValueGetter.GetValue();
                GameObject[] go = datas as GameObject[];


                for (int i = 0; i < this.ranges.Count; i++)
                {
                    if (this.ranges.Count == 1)
                    {
                        this.ranges[i] = rect.SetXMin(rect.xMin).SetXMax(rect.xMax);
                        SirenixEditorGUI.DrawSolidRect(this.ranges[i], this.colors[i]);
                    }
                    else
                    {
                        if (i == 0)
                        {
                            this.ranges[i] =
                                rect.SetXMin(rect.xMin)
                                .SetXMax(rect.width * this.selectValues[i] + (this.selectSize.x / 2));
                            SirenixEditorGUI.DrawSolidRect(this.ranges[i], this.colors[i]);
                        }
                        else if (i == this.ranges.Count - 1)
                        {
                            this.ranges[i] = rect.SetXMin(
                                rect.width * this.selectValues[i - 1] + (this.selectSize.x / 2));
                            SirenixEditorGUI.DrawSolidRect(this.ranges[i], this.colors[i]);
                        }
                        else
                        {
                            this.ranges[i] = rect.SetXMin(
                                rect.width * this.selectValues[i - 1] + (this.selectSize.x / 2))
                                .SetXMax(rect.width * this.selectValues[i] + (this.selectSize.x / 2));
                            SirenixEditorGUI.DrawSolidRect(this.ranges[i], this.colors[i]);
                        }
                        SirenixEditorGUI.DrawSolidRect(this.ranges[i], this.colors[i]);
                    }

                    GUIHelper.PushColor(Color.black);
                    if (inspectorPropertyValueGetter.GetValue()[i] != null)
                    {
                        if (go != null)
                        {
                            if (go[i] != null)
                            {
                                GUI.Label(this.ranges[i].AlignCenterX(this.ranges[i].width / 2), go[i].name, style);
                            }
                        }
                        else
                        {
                            GUI.Label(this.ranges[i].
                                AlignCenterX(this.ranges[i].width / 2),
                                inspectorPropertyValueGetter.GetValue()[i].ToString(), style);
                        }
                    }
                    float percentage = (this.ranges[i].width / rect.width) * 100;

                    GUI.Label(this.ranges[i]
                        .AlignCenterX(this.ranges[i].width / 2),
                        Mathf.Round(percentage) + "%", percentageStyle);
                    GUIHelper.PopColor();
                }


            }
        }
        void DrawSelect<T>(Rect rect, InspectorPropertyValueGetter<T[]> inspectorPropertyValueGetter)
        {
            if (inspectorPropertyValueGetter.GetValue() != null &&
                inspectorPropertyValueGetter.GetValue().Length != 0)
            {
                Event currentEvent = Event.current;
                Rect[] selectPoint = new Rect[this.selectValues.Count];
                for (int i = 0; i < selectPoint.Length; i++)
                {
                    selectPoint[i] =
                        new Rect(rect.width * this.selectValues[i],
                        rect.y,
                        this.selectSize.x,
                        this.selectSize.y);

                    GUI.Label(selectPoint[i], EditorIcons.Eject.Raw);
                }
                //判定點到第幾個Select
                for (int i = 0; i < selectPoint.Length; i++)
                {
                    if (selectPoint[i].Contains(currentEvent.mousePosition))
                    {
                        if (currentEvent.type == EventType.MouseDown && currentEvent.button == 0)
                        {

                            ChangeState(State.Down);
                            this.selectId = i;
                        }
                    }

                }
                if (currentEvent.type == EventType.MouseUp && currentEvent.button == 0)
                {
                    ChangeState(State.Up);
                    this.selectId = -1;
                }
                ///////////////////Mouse Event/////////////////////
                switch (this.state)
                {
                    case State.Down:
                        if (currentEvent.type == EventType.MouseDrag && currentEvent.button == 0)
                        {
                            ChangeState(State.Drag);
                        }
                        break;
                    case State.Up:
                        break;
                    case State.Drag:

                        if (Event.current.type != EventType.Repaint) return;
                        float value = currentEvent.mousePosition.x;
                        value = Mathf.Clamp(value, rect.xMin, rect.xMax);
                        value = (value - rect.xMin) / (rect.xMax - rect.xMin);

                        if (this.selectValues.Count == 1)
                        {
                            this.selectValues[this.selectId] = value;
                        }
                        else
                        {
                            if (this.selectId == 0)
                            {
                                value = Mathf.Clamp(value, 0, this.selectValues[this.selectId + 1]);
                                this.selectValues[this.selectId] = value;
                            }
                            else if (this.selectId == selectPoint.Length - 1)
                            {
                                value = Mathf.Clamp(value, this.selectValues[this.selectId - 1], 1);
                                this.selectValues[this.selectId] = value;
                            }
                            else
                            {
                                value = Mathf.Clamp(value,
                                    this.selectValues[this.selectId - 1],
                                    this.selectValues[this.selectId + 1]);
                                this.selectValues[this.selectId] = value;
                            }
                        }
                        break;
                    case State.Out:
                        break;
                    default:
                        break;
                }

            }
        }

        void ChangeState(State state) => this.state = state;
        #endregion
    }
}
