//-----------------------------------------------------------------------
// <copyright file="SimBuildPreProcessor.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Editor.Build
{
	using UnityEditor;
	using UnityEditor.Build;
	using UnityEditor.Build.Reporting;

	public class SimBuildPreProcessor : IPreprocessBuildWithReport
	{
		#region Fields
		const string keySimofun = "simofun";

		const string keySimsoft = "simsoft";
		#endregion

		#region Properties
		public int callbackOrder { get; }
		#endregion

		#region Public Methods
		public void OnPreprocessBuild(BuildReport report)
		{
			if (!PlayerSettings.Android.useCustomKeystore)
			{
				return;
			}

			var keystoreName = PlayerSettings.Android.keystoreName;
			if (keystoreName == keySimofun + ".keystore")
			{
				PlayerSettings.Android.keystorePass = keySimofun;
				PlayerSettings.Android.keyaliasPass = keySimofun;

				return;
			}

			if (keystoreName == keySimsoft + ".keystore")
			{
				PlayerSettings.Android.keystorePass = keySimsoft;
				PlayerSettings.Android.keyaliasPass = keySimsoft;
			}
		}
		#endregion
	}
}
