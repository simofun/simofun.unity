//-----------------------------------------------------------------------
// <copyright file="SimConfigResetButton.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Editor.Data.Configuration.CustomAsset
{
	using Simofun.Unity.Data.Configuration.CustomAsset.Utils;
	using UnityEditor;
	using UnityEngine;
	using UnityToolbarExtender;

	[InitializeOnLoad]
	public static class SimConfigResetButton
	{
		#region Constructors
		static SimConfigResetButton()
		{
			ToolbarExtender.RightToolbarGUI.Add(HandleOnToolbarGUI);
		}
		#endregion

		#region Methods
		#region Event Handlers
		static void HandleOnToolbarGUI()
		{
			var config = SimCustomAssetConfigUtils.LoadServiceConfig();
			if (config == null)
			{
				return;
			}

			GUILayout.FlexibleSpace();
			
			if (GUILayout.Button("Hard Reset"))
			{
				SimCustomAssetConfigServiceMenuItems.HardReset();
			}
		}
		#endregion
		#endregion
	}
}
