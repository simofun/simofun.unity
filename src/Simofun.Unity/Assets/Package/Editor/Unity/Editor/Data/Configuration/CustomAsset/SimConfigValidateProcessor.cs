//-----------------------------------------------------------------------
// <copyright file="SimConfigValidateProcessor.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Editor.Data.Configuration.CustomAsset
{
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using Simofun.Unity.Editor.Data.Configuration.CustomAsset.Utils;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEditor;

	public static class SimConfigValidateProcessor
	{
		#region Static Fields
		readonly static List<SimConfigValidationTracker> configList = new List<SimConfigValidationTracker>();
		#endregion

		#region Constructors
		static SimConfigValidateProcessor()
		{
			RegisterEventHandlers();
		}
		#endregion

		#region Public Static Methods
		public static void Stop(SimCustomAssetConfigBase config)
		{
			var tracker = configList.FirstOrDefault(tracker => tracker.Config == config);
			if (tracker == null)
			{
				return;
			}

			tracker.Dispose();
			configList.Remove(tracker);
		}

		public static void Validate(SimCustomAssetConfigBase config)
		{
			if (configList.Any(tracker => tracker.Config == config))
			{
				return;
			}

			var tracker = new SimConfigValidationTracker(config);
			configList.Add(tracker);
		}
		#endregion

		#region Methods
		#region Event Handlers
		#region Registration
		static void RegisterEventHandlers()
		{
			Selection.selectionChanged -= HandleOnSelectionChange;
			Selection.selectionChanged += HandleOnSelectionChange;
		}
		#endregion

		static void HandleOnSelectionChange()
		{
			ValidateSelected();
		}
		#endregion

		static void Clear()
		{
			foreach (var config in configList)
			{
				config.Dispose();
			}

			configList.Clear();
		}

		static void TrackAll()
		{
			foreach (var config in SimCustomAssetConfigEditorUtils.LoadAll<SimCustomAssetConfigBase>())
			{
				Stop(config);
				Validate(config);
			}
		}

		static void ValidateSelected()
		{
			var selectedConfigs = SimCustomAssetConfigEditorUtils.LoadSelected<SimCustomAssetConfigBase>();
			Clear();

			foreach (var config in selectedConfigs)
			{
				Validate(config);
			}
		}
		#endregion
	}
}
