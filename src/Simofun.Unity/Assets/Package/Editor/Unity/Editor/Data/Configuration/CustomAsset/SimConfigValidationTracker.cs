//-----------------------------------------------------------------------
// <copyright file="SimConfigValidationTracker.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Editor.Data.Configuration.CustomAsset
{
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using Simofun.Unity.Editor.Data.Configuration.CustomAsset.Utils;
	using System;

	public class SimConfigValidationTracker : IDisposable
	{
		#region Fields
		SimCustomAssetConfigBase config;
		#endregion

		#region Properties
		public SimCustomAssetConfigBase Config => this.config;
		#endregion

		#region Constructors
		public SimConfigValidationTracker(SimCustomAssetConfigBase config)
		{
			if (config == null)
			{
				return;
			}

			this.config = config;
			this.RegisterEventHandlers();
		}
		#endregion

		#region Public Methods
		public void Dispose() => this.UnRegisterEventHandlers();
		#endregion

		#region Methods
		#region Event Handlers
		#region Registration
		void RegisterEventHandlers()
		{
			if (this.Config == null)
			{
				return;
			}

			this.Config.OnEditorValidation += this.HandleOnValidation;
		}

		void UnRegisterEventHandlers()
		{
			if (this.Config == null)
			{
				return;
			}

			this.Config.OnEditorValidation -= this.HandleOnValidation;
		}
		#endregion

		void HandleOnValidation(SimCustomAssetConfigBase obj) => SimCustomAssetConfigEditorUtils.CheckDefaults(obj);
		#endregion
		#endregion
	}
}
