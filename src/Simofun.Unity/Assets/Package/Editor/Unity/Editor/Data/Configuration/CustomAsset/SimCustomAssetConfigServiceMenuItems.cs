﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigServiceMenuItems.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Editor.Data.Configuration.CustomAsset
{
	using Simofun.Unity.Data.Configuration.CustomAsset.Utils;
	using Simofun.Utils;
	using System.IO;
	using UnityEditor;
	using UnityEngine;

	public static class SimCustomAssetConfigServiceMenuItems
	{
		#region Methods
		[MenuItem(SimMenuItems.Path_ + nameof(Unity) + SimMenuItems.Seperator + nameof(Data) + SimMenuItems.Seperator
			+ nameof(Configuration) + SimMenuItems.Seperator + "Delete All Save")]
		public static void DeleteAllSave()
		{
			var config = SimCustomAssetConfigUtils.LoadServiceConfig();
			if (config == null)
			{
				return;
			}

			foreach (var cfg in SimCustomAssetConfigUtils.GetConfigs())
			{
				File.Delete(config.GetFilePath(cfg)); // Delete config
			}
		}

		[MenuItem(SimMenuItems.Path_ + nameof(Unity) + SimMenuItems.Seperator + nameof(Data) + SimMenuItems.Seperator
			+ nameof(Configuration) + SimMenuItems.Seperator + "Reset Configs")]
		public static void ResetConfigs()
		{
			var config = SimCustomAssetConfigUtils.LoadServiceConfig();
			if (config == null)
			{
				return;
			}

			foreach (var cfg in SimCustomAssetConfigUtils.GetConfigs())
			{
				cfg.Reset(); // Reset config
			}
		}

		[MenuItem(SimMenuItems.Path_ + nameof(Unity) + SimMenuItems.Seperator + nameof(Data) + SimMenuItems.Seperator
			+ nameof(Configuration) + SimMenuItems.Seperator + "Hard Reset")]
		public static void HardReset()
		{
			// [Performance] Equals to calling 'DeleteAllSave()' and 'ResetConfigs()' methods sequentially.
			var config = SimCustomAssetConfigUtils.LoadServiceConfig();
			if (config == null)
			{
				PlayerPrefs.DeleteAll();

				return;
			}

			PlayerPrefs.DeleteAll();

			foreach (var cfg in SimCustomAssetConfigUtils.GetConfigs())
			{
				SimFileUtil.DeleteFileIfExist(config.GetFilePath(cfg)); // Delete config
				
				cfg.Reset(); // Reset config
			}
		}
		#endregion
	}
}
