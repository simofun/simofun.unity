﻿//-----------------------------------------------------------------------
// <copyright file="DesignVariableInfo.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Hasan Emre Tonguç</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Editor.DesignVariables.Attributes
{
	using Simofun.Unity.DesignVariables.Attributes;
	using System.Reflection;

	public struct DesignVariableInfo
	{
		#region Fields
		readonly MemberInfo member;

		readonly string displayName;
		#endregion

		#region Properties
		public MemberInfo Member => this.member;

		public string DisplayName => this.displayName;
		#endregion

		#region Constructors
		public DesignVariableInfo(DesignVariableAttribute attribute, MemberInfo member)
		{
			var name = attribute.DisplayName;
			if (string.IsNullOrEmpty(name))
			{
				name = char.ToUpper(member.Name[0]) + member.Name.Substring(1);
			}

			this.displayName = name;
			this.member = member;
		}
		#endregion
	}
}
