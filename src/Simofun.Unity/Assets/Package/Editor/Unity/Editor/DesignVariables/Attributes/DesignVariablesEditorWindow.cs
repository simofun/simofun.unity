//-----------------------------------------------------------------------
// <copyright file="DesignVariablesEditorWindow.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tongu�</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Editor.DesignVariables.Attributes
{
	using Simofun.Unity.DesignVariables.Attributes;
	using Simofun.Utils;
	using Sirenix.Utilities.Editor;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Reflection;
	using UnityEditor;
	using UnityEngine;

	public class DesignVariablesEditorWindow : SimEditorWindowBase
	{
		#region Fields
		readonly BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;

		readonly Dictionary<Type, List<DesignVariableInfo>> typeExposedMember =
			new Dictionary<Type, List<DesignVariableInfo>>();

		readonly Dictionary<Type, UnityEngine.Object[]> typeInstances = new Dictionary<Type, UnityEngine.Object[]>();

		Vector2 scrollPos;
		#endregion

		#region Public Static Methods
		[MenuItem("Simofun/Editor/Window/Design Variables", priority = 11)]
		public static DesignVariablesEditorWindow Open()
		{
			var window = CreateWindow<DesignVariablesEditorWindow>("Design Variables");
			window.titleContent.image = EditorGUIUtility.IconContent("CustomTool@2x").image;
			window.Show();

			return window;
		}
		#endregion

		#region Unity Methods
	#region Editor
	/// <inheritdoc />
	protected override void OnGUI()
		{
			if (this.typeInstances == null)
			{
				return;
			}

			foreach (var typeInstance in this.typeInstances)
			{
				var designVariableInfos = this.typeExposedMember[typeInstance.Key];
				if (designVariableInfos == null || designVariableInfos.Count <= 0)
				{
					continue;
				}

				this.scrollPos = EditorGUILayout.BeginScrollView(this.scrollPos);

				foreach (var instance in typeInstance.Value)
				{
					this.DrawInstance(designVariableInfos, instance);
				}

				EditorGUILayout.EndScrollView();
			}
		}

		/// <inheritdoc />
		protected virtual void OnHierarchyChange()
		{
			if (this.typeInstances == null)
			{
				return;
			}

			for (var i = 0; i < this.typeInstances.Count; i++)
			{
				var typeInstance = this.typeInstances.ElementAt(i);
				this.typeInstances[typeInstance.Key] = FindObjectsByType(typeInstance.Key, FindObjectsSortMode.None);
			}
		}
		#endregion

		/// <inheritdoc />
		protected override void OnEnable()
		{
			base.OnEnable();

			foreach (var type in SimTypeUtil.GetAllTypes().Cast<Type>())
			{
				var designVariableInfos = new List<DesignVariableInfo>();

				foreach (var member in type.GetMembers(this.flags))
				{
					if (member.CustomAttributes.Count() < 1)
					{
						continue;
					}

					var attribute = member.GetCustomAttribute<DesignVariableAttribute>();
					if (attribute == null)
					{
						continue;
					}

					designVariableInfos.Add(new DesignVariableInfo(attribute, member));
				}

				if (designVariableInfos.Count > 0)
				{
					this.typeExposedMember.Add(type, designVariableInfos);
					this.typeInstances.Add(type, FindObjectsByType(type, FindObjectsSortMode.None));
				}
			}
		}
		#endregion

		#region Methods
		bool CanDrawPrefabButton(UnityEngine.Object instance, out GameObject obj)
		{
			obj = null;

			return !this.IsPrefab(instance, out obj) ? false : PrefabUtility.HasPrefabInstanceAnyOverrides(obj, false);
		}

		bool IsPrefab(UnityEngine.Object instance, out GameObject obj)
		{
			obj = null;
			var prefabObject = PrefabUtility.GetOutermostPrefabInstanceRoot(instance);
			if (prefabObject == null)
			{
				return false;
			}

			var status = PrefabUtility.GetPrefabInstanceStatus(prefabObject);
			if (status != PrefabInstanceStatus.Connected)
			{
				return false;
			}

			obj = prefabObject;

			return true;
		}

		void DrawFields(
			List<DesignVariableInfo> designVariableInfos,
			SerializedObject serializedObject,
			SerializedProperty[] serializedProperties)
		{
			for (var i = 0; i < designVariableInfos.Count; i++)
			{
				serializedProperties[i] = serializedObject.FindProperty(designVariableInfos[i].Member.Name);

				EditorGUILayout.PropertyField(serializedProperties[i]);
			}

			serializedObject.ApplyModifiedProperties();
		}

		void DrawHeader(UnityEngine.Object instance)
		{
			SirenixEditorGUI.BeginToolbarBoxHeader();

			this.DrawSelectObjectButton(instance);
			this.DrawOverridePrefabButton(instance);
			this.DrawRevertPrefabButton(instance);

			SirenixEditorGUI.EndToolbarBoxHeader();
		}

		void DrawInstance(List<DesignVariableInfo> designVariableInfos, UnityEngine.Object instance)
		{
			var serializedObject = new SerializedObject(instance);
			var serializedProperties = new SerializedProperty[designVariableInfos.Count];

			SirenixEditorGUI.BeginBox(instance.name, true);

			this.DrawHeader(instance);
			this.DrawFields(designVariableInfos, serializedObject, serializedProperties);

			SirenixEditorGUI.EndBox();
		}

		void DrawOverridePrefabButton(UnityEngine.Object instance)
		{
			if (!this.CanDrawPrefabButton(instance, out var prefabObject))
			{
				return;
			}

			var isClicked = SirenixEditorGUI.ToolbarButton("Override Prefab", false);
			if (!isClicked)
			{
				return;
			}

			PrefabUtility.ApplyPrefabInstance(prefabObject, InteractionMode.UserAction);
			AssetDatabase.SaveAssets();
		}

		void DrawRevertPrefabButton(UnityEngine.Object instance)
		{
			if (!this.CanDrawPrefabButton(instance, out var prefabObject))
			{
				return;
			}

			var isClicked = SirenixEditorGUI.ToolbarButton("Revert Prefab", false);
			if (!isClicked)
			{
				return;
			}

			PrefabUtility.RevertPrefabInstance(prefabObject, InteractionMode.UserAction);
			AssetDatabase.SaveAssets();
		}

		void DrawSelectObjectButton(UnityEngine.Object instance)
		{
			var isClicked = SirenixEditorGUI.ToolbarButton("Select object in hierarchy", false);
			if (!isClicked)
			{
				return;
			}

			this.OnSelectObjectButtonClicked(instance);
		}

		void OnSelectObjectButtonClicked(UnityEngine.Object instance) =>
			Selection.SetActiveObjectWithContext(instance, instance);
		#endregion
	}
}
