//-----------------------------------------------------------------------
// <copyright file="SimUpmPromiseClient.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Editor.PackageManager
{
	using System;
	using System.Collections.Generic;
	using UnityEditor;
	using UnityEditor.PackageManager;
	using UnityEditor.PackageManager.Requests;
	using UnityEngine;
	using UpmPackageInfo = UnityEditor.PackageManager.PackageInfo;

	public class SimUpmPromiseClient
	{
		#region Public Methods
		public static void Add(string identifier, Action<UpmPackageInfo> onSuccess, Action<Error> onFail = null) =>
			new Task<AddRequest>().Execute(
				() => Client.Add(identifier),
				(r) => onSuccess?.Invoke(r.Result),
				onFail);

		public static void GetAll(Action<IEnumerable<UpmPackageInfo>> onSuccess, Action<Error> onFail = null) =>
			new Task<ListRequest>().Execute(
				() => Client.List(),
				(r) => onSuccess?.Invoke(r.Result),
				onFail);

		public static void Remove(string packageName, Action onSuccess, Action<Error> onFail = null) =>
			new Task<RemoveRequest>().Execute(
				() => Client.Remove(packageName),
				(r) => onSuccess?.Invoke(),
				onFail);
		#endregion

		#region Nested Types
		public class Task<TRequest> where TRequest : Request
		{
			#region Fields
			Action<Error> onFail;

			Action<TRequest> onSuccess;

			TRequest req;
			#endregion

			#region Public Methods
			public void Execute(Func<TRequest> onRequest, Action<TRequest> onSuccess, Action<Error> onFail)
			{
				if (this.req != null)
				{
					EditorApplication.update -= this.WaitForResponse;
				}

				this.onSuccess = onSuccess;
				this.onFail = onFail;
				this.req = onRequest();
				EditorApplication.update += this.WaitForResponse;
			}
			#endregion

			#region Methods
			void WaitForResponse()
			{
				if (!this.req.IsCompleted)
				{
					return;
				}

				EditorApplication.update -= this.WaitForResponse;

				if (this.req.Status == StatusCode.Success)
				{
					this.onSuccess?.Invoke(this.req);
				}
				else if (this.req.Status >= StatusCode.Failure)
				{
					if (this.onFail != null)
					{
						this.onFail.Invoke(this.req.Error);
					}
					else
					{
						Debug.LogError(this.req.Error.message);
					}
				}

				this.onFail = null;
				this.onSuccess = null;
				this.req = null;
			}
			#endregion
		}
		#endregion
	}
}
