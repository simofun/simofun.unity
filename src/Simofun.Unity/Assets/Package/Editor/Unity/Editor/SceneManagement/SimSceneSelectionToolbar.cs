//-----------------------------------------------------------------------
// <copyright file="SimSceneSelectionToolbar.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Editor.SceneManagement
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEditor;
	using UnityEditor.SceneManagement;
	using UnityEngine;
	using UnityEngine.SceneManagement;
	using UnityToolbarExtender;

	[InitializeOnLoad]
	public static class SimSceneSelectionToolbar
	{
		#region Fields
		static int selectedIndex;

		static List<SceneInfo> scenes;

		static SceneInfo sceneOpened;

		static string[] displayedOptions;
		#endregion

		#region Constructors
		static SimSceneSelectionToolbar()
		{
			LoadFromPlayerPrefs();
			ToolbarExtender.LeftToolbarGUI.Add(HandleOnToolbarGUI);
			EditorSceneManager.sceneOpened += HandleOnSceneOpened;
		}
		#endregion

		#region Methods
		static string GetPref(string name) => EditorPrefs.GetString($"{Application.productName}_{name}");

		static void AddScene(SceneInfo scene)
		{
			if (scene == null)
			{
				return;
			}

			if (scenes.Any(s => s.Path == scene.Path))
			{
				RemoveScene(scene);
			}

			scenes.Add(scene);
			selectedIndex = scenes.Count;
			SetOpenedScene(scene);
			RefreshDisplayedOptions();
			SaveToPlayerPrefs();
		}

		static void HandleOnToolbarGUI()
		{
			GUILayout.FlexibleSpace();

			selectedIndex = EditorGUILayout.Popup(selectedIndex, displayedOptions, GUILayout.MaxWidth(125));

			GUI.enabled = selectedIndex == 0;
			if (GUILayout.Button("+"))
			{
				AddScene(sceneOpened);
			}

			GUI.enabled = selectedIndex > 0;
			if (GUILayout.Button("-"))
			{
				RemoveScene(sceneOpened);
			}

			GUI.enabled = true;
			if (GUI.changed && selectedIndex > 0 && scenes.Count > selectedIndex - 1)
			{
				EditorSceneManager.OpenScene(scenes[selectedIndex - 1].Path);
			}
		}

		static void HandleOnSceneOpened(Scene scene, OpenSceneMode mode) => SetOpenedScene(scene);

		static void LoadFromPlayerPrefs()
		{
			var serialized = GetPref(nameof(SimSceneSelectionToolbar) + ".Scenes");

			scenes = serialized.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
				.Select(s => new SceneInfo(s))
				.ToList();

			if (scenes == null)
			{
				scenes = new List<SceneInfo>();
			}

			serialized = GetPref(nameof(SimSceneSelectionToolbar) + ".LatestOpenedScene");

			if (!string.IsNullOrEmpty(serialized))
			{
				SetOpenedScene(new SceneInfo(serialized));
			}

			RefreshDisplayedOptions();
		}

		static void RefreshDisplayedOptions()
		{
			displayedOptions = new string[scenes.Count + 1];
			displayedOptions[0] = "Scene Selection";

			for (var i = 0; i < scenes.Count; i++)
			{
				displayedOptions[i + 1] = scenes[i].Name;
			}
		}

		static void RemoveScene(SceneInfo scene)
		{
			scenes.Remove(scene);
			selectedIndex = 0;
			RefreshDisplayedOptions();
			SaveToPlayerPrefs();
		}

		static void SaveToPlayerPrefs(bool onlyLatestOpenedScene = false)
		{
			if (!onlyLatestOpenedScene)
			{
				SetPref(
					nameof(SimSceneSelectionToolbar) + ".Scenes",
					string.Join(";", scenes.Where(s => !string.IsNullOrEmpty(s.Path)).Select(s => s.Path)));
			}

			if (sceneOpened != null)
			{
				SetPref(nameof(SimSceneSelectionToolbar) + ".LatestOpenedScene", sceneOpened.Path);
			}
		}

		static void SetOpenedScene(Scene scene) => SetOpenedScene(new SceneInfo(scene));

		static void SetOpenedScene(SceneInfo scene)
		{
			if (scene == null || string.IsNullOrEmpty(scene.Path))
			{
				return;
			}

			for (var i = 0; i < scenes.Count; i++)
			{
				if (scenes[i].Path != scene.Path)
				{
					continue;
				}

				sceneOpened = scenes[i];
				selectedIndex = i + 1;
				SaveToPlayerPrefs(true);

				return;
			}

			sceneOpened = scene;
			selectedIndex = 0;
			SaveToPlayerPrefs(true);
		}

		static void SetPref(string name, string value) =>
			EditorPrefs.SetString($"{Application.productName}_{name}", value);
		#endregion

		#region Nested Types
		[Serializable]
		class SceneInfo
		{
			#region Unity Fields
			public string Name;

			public string Path;
			#endregion

			#region Constructors
			public SceneInfo()
			{
			}

			public SceneInfo(Scene scene)
			{
				this.Name = scene.name;
				this.Path = scene.path;
			}

			public SceneInfo(string path)
			{
				this.Name = System.IO.Path.GetFileNameWithoutExtension(path);
				this.Path = path;
			}
			#endregion
		}
		#endregion
	}
}
