//-----------------------------------------------------------------------
// <copyright file="SimContextMenuItems.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Editor
{
	using UnityEditor;
	using UnityEngine;
	using UnityEngine.UI;

	public class SimContextMenuItems : MonoBehaviour
	{
		#region Methods
		[MenuItem("CONTEXT/Button/Invoke 'OnClick'")]
		static void InvokeOnClick(MenuCommand command) => ((Button)command.context).onClick.Invoke();
		#endregion
	}
}
