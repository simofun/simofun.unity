#if UNITY_2021_2_OR_NEWER
//-----------------------------------------------------------------------
// <copyright file="SimPackageIconSetterEditor.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Editor
{
	using UnityEditor;
	using UnityEngine;

	public class SimPackageIconSetterEditor : MonoBehaviour
	{
		#region Fields
		const string menuItemPath = "Simofun/Editor/Add 'Package' Icon";
		#endregion

		#region Methods
		[MenuItem(menuItemPath)]
		static void AddSimIcon()
		{
			var selectedObjects = Selection.objects;
			if (selectedObjects.Length < 1)
			{
				return;
			}

			var icon = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/Package/Res/Editor/PackageLogo.png", typeof(Texture2D));

			foreach (var selectedObject in selectedObjects)
			{
				var importer = AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(selectedObject)) as MonoImporter;
				if (importer == null)
				{
					continue;
				}

				importer.SetIcon(icon);
				importer.SaveAndReimport();
			}
		}

		[MenuItem(menuItemPath, true)]
		static bool AddSimIconValidation()
		{
			var selectedObjects = Selection.objects;
			if (selectedObjects.Length < 1)
			{
				return false;
			}

			foreach (var selectedObject in selectedObjects)
			{
				var importer = AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(selectedObject)) as MonoImporter;
				if (importer != null)
				{
					return true;
				}
			}

			return false;
		}
		#endregion
	}
}
#endif
