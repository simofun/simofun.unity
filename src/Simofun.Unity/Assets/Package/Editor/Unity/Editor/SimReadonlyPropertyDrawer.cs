﻿//-----------------------------------------------------------------------
// <copyright file="SimReadonlyPropertyDrawer.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Editor
{
	using UnityEditor;
	using UnityEngine;

	/// <summary>
	/// This class contain custom drawer for Readonly attribute.
	/// </summary>
	[CustomPropertyDrawer(typeof(SimReadonlyAttribute))]
	public class SimReadonlyPropertyDrawer : PropertyDrawer
	{
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label) =>
			EditorGUI.GetPropertyHeight(property, label, true);

		/// <summary>
		/// Unity method for drawing GUI in Editor
		/// </summary>
		/// <param name="position">Position.</param>
		/// <param name="property">Property.</param>
		/// <param name="label">Label.</param>
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			var isGuiEnabled = GUI.enabled; // Saving previous GUI enabled value
			
			GUI.enabled = false; // Disabling edit for property

			EditorGUI.PropertyField(position, property, label, true); // Drawing Property

			GUI.enabled = isGuiEnabled; // Setting previous GUI enabled value
		}
	}
}
