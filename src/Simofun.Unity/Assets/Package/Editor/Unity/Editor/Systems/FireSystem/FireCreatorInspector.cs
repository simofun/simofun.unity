﻿//-----------------------------------------------------------------------
// <copyright file="FireCreatorInspector.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

using UniRx;

namespace Simofun.Unity.Editor.Systems.FireSystem
{
	using Simofun.Unity.Systems.FireSystem;
	using Simofun.Unity.Systems.FireSystem.Serialization;
	using Simofun.Unity.Systems.FireSystem.Serialization.Json;
	using System;
	using UnityEditor;
	using UnityEngine;
	using UnityEngine.SceneManagement;

	[CustomEditor(typeof(SimFireCreator))]
	public class FireCreatorInspector : Editor
	{
		#region Fields
		bool isShowingMessageBox;

		IDisposable timerDisposable;

		ISimGridInfoFileInfo fileInfo;

		string fileName;
		#endregion

		#region Protected Properties
		protected SimFireCreator Target { get => (SimFireCreator)this.target; }
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void OnDisable() => this.timerDisposable?.Dispose();
		#endregion

		#region Public Methods
		#region Editor
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			if (GUILayout.Button("Save Grid Info"))
			{
				new SimGridInfoJsonWriter(this.GetFileInfo()).Write(this.Target.Grid);

				EditorUtility.RevealInFinder(this.fileInfo.GetFullPath());

				this.timerDisposable?.Dispose();
				this.timerDisposable = Observable.Timer(TimeSpan.FromSeconds(10f))
					.Subscribe(_ => this.isShowingMessageBox = false);
				this.isShowingMessageBox = true;
			}

			if (this.isShowingMessageBox)
			{
				GUI.backgroundColor = Color.green;
				EditorGUILayout.HelpBox(
					$"GridInfo saved to {this.fileInfo.GetFullDirectoryPath()} as {this.fileName}",
					MessageType.Info);
				GUI.backgroundColor = Color.white;
			}
		}
		#endregion
		#endregion

		#region Methods
		ISimGridInfoFileInfo GetFileInfo() => this.fileInfo = new SimPersistentGridInfoJsonFileInfo(this.GetFileName());

		string GetFileName() =>
			this.fileName = new SimGridInfoFileNameGenerator().Generate(SceneManager.GetActiveScene().name);
		#endregion
	}
}
