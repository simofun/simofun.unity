//-----------------------------------------------------------------------
// <copyright file="SimGistImporter.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Editor.VersionControlSystem.Server.GitHub.Gist
{
	using System;
	using System.IO;
	using System.Linq;
	using System.Net;
	using System.Text.RegularExpressions;
	using UnityEditor;
	using UnityEngine;

	[InitializeOnLoad]
	public static class SimGistImporter
	{
		#region Fields
		static readonly Regex getDescriptionRegex = new Regex(
			@"\<title\>(?<description>.+)\</title\>",
			RegexOptions.IgnoreCase | RegexOptions.Compiled);

		static readonly Regex getFileUrlRegex = new Regex(
			"href=\"(?<url>.+/raw/[a-z0-9\\./\\-]+)\"",
			RegexOptions.IgnoreCase | RegexOptions.Compiled);

		static readonly Regex getGistUrlInfoRegex = new Regex(
			"https://gist.github.com/(?<owner>.+)/(?<gistId>[a-z0-9]+)",
			RegexOptions.IgnoreCase | RegexOptions.Compiled);

		const string GistsFolder = "ThirdParty/Gists";
		#endregion

		#region Methods
		[MenuItem("Simofun/Editor/Version Control System/Server/GitHub/Gist/Import Gist", true)]
		static bool ValidateImportGist() => getGistUrlInfoRegex.IsMatch(EditorGUIUtility.systemCopyBuffer);

		static void DownloadFile(WebClient client, string url, string destinationFolder, string filename)
		{
			var fileContent = client.DownloadString(url);

			// Is an editor script?
			if (fileContent.IndexOf("using UnityEditor;", StringComparison.OrdinalIgnoreCase) > -1)
			{
				destinationFolder = Path.Combine(destinationFolder, "Editor");
			}

			Directory.CreateDirectory(destinationFolder);
			File.WriteAllText(Path.Combine(destinationFolder, filename), fileContent);
		}

		[MenuItem("Simofun/Editor/Version Control System/Server/GitHub/Gist/Import Gist")]
		static void ImportGist()
		{
			var gistUrl = EditorGUIUtility.systemCopyBuffer;

			try
			{
				using (var client = new WebClient())
				{
					var gistPageContent = client.DownloadString(gistUrl);

					var filesMatches = getFileUrlRegex.Matches(gistPageContent); // Extract the files raw urls.

					if (filesMatches.Count > 0)
					{
						var infoMatch = getGistUrlInfoRegex.Match(gistUrl).Groups;
						var gistOwner = infoMatch["owner"].Value;
						var gistId = infoMatch["gistId"].Value;
						var rawUrls = filesMatches.OfType<Match>()
										.Select(m => $"https://gist.github.com{m.Groups["url"].Value}")
										.OrderByDescending(u => u.EndsWith(".cs", StringComparison.OrdinalIgnoreCase))
										.ToArray();

						var destFolder = Path.Combine(
							Application.dataPath,
							GistsFolder,
							gistOwner,
							$"{Path.GetFileNameWithoutExtension(rawUrls.First())} ({gistId})");

						// Downloads and write the Gist files.
						for (var i = 0; i < rawUrls.Length; i++)
						{
							var rawUrl = rawUrls[i];
							var filename = Path.GetFileName(rawUrl);

							EditorUtility.DisplayProgressBar(
								"Importing Gist...",
								filename,
								i / (float)filesMatches.Count);
							DownloadFile(client, rawUrl, destFolder, filename);
						}

						EditorUtility.ClearProgressBar();
						WriteReadme(gistUrl, gistPageContent, destFolder);
					}
					else
					{
						Debug.LogWarning($"No files found for Gist '{gistUrl}'.");
					}
				}
			}
			finally
			{
				EditorUtility.ClearProgressBar();
			}
		}

		static void WriteReadme(string gistUrl, string gistPageContent, string folder)
		{
			var description = getDescriptionRegex.Match(gistPageContent).Groups["description"].Value;
			var readmePath = Path.Combine(folder, "README.txt");
			File.WriteAllText(
				readmePath,
				$"{description}\n\nUrl: {gistUrl}\nDate: {DateTime.Now:dd/MM/yyyy HH:mm}\n\nImported using Gist Importer (http://diegogiacomelli.com.br/unitytips-gist-importer).");

			// Selects the readme file on project window.
			AssetDatabase.Refresh();
			var readmeAsset = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(
				$"Assets{readmePath.Replace(Application.dataPath, string.Empty)}");
			Selection.activeObject = readmeAsset;
			EditorGUIUtility.PingObject(readmeAsset);
		}
		#endregion
	}
}
