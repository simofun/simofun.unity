﻿//-----------------------------------------------------------------------
// <copyright file="SimEditorWindowBase.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Editor
{
	using Sirenix.OdinInspector.Editor;

	public abstract class SimEditorWindowBase : OdinEditorWindow
	{
	}
}
