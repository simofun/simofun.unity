﻿//-----------------------------------------------------------------------
// <copyright file="SimListEditorWindow.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Editor
{
	using Newtonsoft.Json;
	using System.Collections.Generic;
	using System.Linq;
	using System.Reflection;
	using UnityEditor;
	using UnityEngine;

	public class SimListEditorWindow : SimEditorWindowBase
	{
		#region Fields
		readonly List<EditorInfo> editors = new List<EditorInfo>();

		readonly List<string> displayOptions = new List<string>();

		bool isInitialized;

		int targetTypeIndex;

		string targetTypeAsString;

		Vector2 scrollPos;
		#endregion

		#region Properties
		public List<EditorInfo> Editors => this.editors;
		#endregion

		#region Public Static Methods
		[MenuItem("Simofun/Editor/Window/List Editor Windows", priority = 0)]
		public static SimListEditorWindow Open()
		{
			var window = GetWindow<SimListEditorWindow>("List Editors");
			window.titleContent.image = EditorGUIUtility.IconContent("d_winbtn_win_restore").image;
			window.Show();

			return window;
		}
		#endregion

		#region Unity Methods
		#region GUI Methods
		protected virtual void OnGUI()
		{
			if (!this.isInitialized)
			{
				EditorGUILayout.HelpBox(
					$"Not found any editor window which inherits from '{typeof(SimEditorWindowBase)}' type",
					MessageType.Info);

				return;
			}

			this.scrollPos = EditorGUILayout.BeginScrollView(this.scrollPos);

			var index = this.targetTypeIndex;
			this.targetTypeIndex = EditorGUILayout.Popup(index, this.displayOptions.ToArray());
			if (this.targetTypeIndex != index)
			{
				this.targetTypeAsString = this.displayOptions[this.targetTypeIndex];
				this.Refresh();
			}

			foreach (var editor in this.Editors)
			{
				BeginVerticalBoxGroupLayout();
				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{
						editor.IsFoldout = EditorGUILayout.Foldout(editor.IsFoldout, editor.Type.Name, true);
						EditorGUILayout.Space();
						if (GUILayout.Button(
								EditorGUIUtility.IconContent("Grid.BoxTool"),
								GUILayout.MaxWidth(30)))
						{
							GetWindow(editor.Type);
						}

						if (GUILayout.Button(
								EditorGUIUtility.IconContent("animationvisibilitytoggleon"),
								GUILayout.MaxWidth(30)))
						{
							var script = FindScriptsByType(editor.Type, true).FirstOrDefault();
							if (script != null)
							{
								EditorGUIUtility.PingObject(script);
							}
						}
					}
					EditorGUILayout.EndHorizontal();

					if (editor.IsFoldout)
					{
						editor.Draw();
					}
				}
				EditorGUILayout.EndVertical();
				EndVerticalBoxGroupLayout();
			}

			EditorGUILayout.EndScrollView();
		}
		#endregion

		protected virtual void OnEnable()
		{
			var settings = this.GetSettings();
			this.targetTypeAsString = settings.TargetTypeAsString;

			this.Initialize();

			this.Editors.ForEach(e =>
				e.IsFoldout = settings.Editors.FirstOrDefault(se => se.Type == e.Type)?.IsFoldout ?? false);

			if (!string.IsNullOrEmpty(this.targetTypeAsString))
			{
				var index = this.displayOptions.FindIndex(d => d == this.targetTypeAsString);
				if (index != -1)
				{
					this.targetTypeIndex = index;
				}
			}
		}

		protected virtual void OnDisable()
		{
			if (this.isInitialized)
			{
				this.SaveSettings();
			}

			this.Dispose();
		}
		#endregion

		#region Public Methods
		public SimListEditorWindowSettings GetSettings()
		{
			var settings = EditorPrefs.GetString(nameof(SimListEditorWindowSettings), string.Empty);
			if (string.IsNullOrEmpty(settings))
			{
				return new SimListEditorWindowSettings();
			}

			try
			{
				return JsonConvert.DeserializeObject<SimListEditorWindowSettings>(settings);
			}
			catch (System.Exception)
			{
				return new SimListEditorWindowSettings();
			}
			finally
			{
				EditorPrefs.DeleteKey(nameof(SimListEditorWindowSettings));
			}
		}

		public void Dispose()
		{
			this.displayOptions.Clear();
			this.Editors.Clear();
			this.scrollPos = Vector3.zero;
		}

		public void Initialize()
		{
			var types = GetChildrenTypes(typeof(SimEditorWindowBase)).ToList();
			types.Remove(typeof(SimListEditorWindow));
			if (types.Count < 1)
			{
				this.isInitialized = false;

				return;
			}

			foreach (var type in types.OrderBy(t => t.FullName))
			{
				this.displayOptions.Add(type.FullName);
			}

			if (string.IsNullOrEmpty(this.targetTypeAsString))
			{
				this.targetTypeAsString = this.displayOptions.First();
			}

			foreach (var type in GetChildrenTypesAndSelf(GetTypeByFullName(this.targetTypeAsString))
				.OrderBy(t => t.Name))
			{
				this.Editors.Add(new EditorInfo { Type = type });
			}

			this.isInitialized = true;
		}

		public void Refresh()
		{
			this.Dispose();
			this.Initialize();
		}

		public void SaveSettings()
		{
			if (string.IsNullOrEmpty(this.targetTypeAsString) || this.Editors.Count < 1)
			{
				return;
			}

			EditorPrefs.SetString(
				nameof(SimListEditorWindowSettings),
				JsonConvert.SerializeObject(
					new SimListEditorWindowSettings
					{
						Editors = this.Editors,
						TargetTypeAsString = this.targetTypeAsString
					}));
		}
		#endregion

		#region Static Methods
		static IEnumerable<System.Type> GetChildrenTypesAndSelf(System.Type type)
		{
			foreach (var item in GetChildrenTypesAndSelf(type, System.AppDomain.CurrentDomain))
			{
				yield return item;
			}
		}

		static IEnumerable<System.Type> GetChildrenTypesAndSelf(System.Type type, System.AppDomain domain)
		{
			var query = (from domainAssembly in domain.GetAssemblies()
						 from assemblyType in domainAssembly.GetTypes()
						 where type.IsAssignableFrom(assemblyType)
						 select assemblyType);
			foreach (var item in query)
			{
				yield return item;
			}
		}

		static IEnumerable<System.Type> GetChildrenTypes(System.Type type)
		{
			foreach (var item in GetChildrenTypes(type, System.AppDomain.CurrentDomain))
			{
				yield return item;
			}
		}

		static IEnumerable<System.Type> GetChildrenTypes(System.Type type, System.AppDomain domain)
		{
			var query = (from domainAssembly in domain.GetAssemblies()
						 from assemblyType in domainAssembly.GetTypes()
						 where type.IsAssignableFrom(assemblyType) && assemblyType != type
						 select assemblyType);
			foreach (var item in query)
			{
				yield return item;
			}
		}

		static IEnumerable<Object> FindScriptsByType(System.Type type) =>
			FindScriptsByType(type, false);

		static IEnumerable<Object> FindScriptsByType(System.Type type, bool isIncludeSubclasses)
		{
			var guids = AssetDatabase.FindAssets("t:MonoScript");
			for (var i = 0; i < guids.Length; i++)
			{
				var assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
				var script = AssetDatabase.LoadAssetAtPath<MonoScript>(assetPath);
				if (script == null)
				{
					continue;
				}

				var scriptType = script.GetClass();
				if (scriptType == null)
				{
					continue;
				}

				if (scriptType == type || (isIncludeSubclasses && scriptType.IsSubclassOf(type)))
				{
					yield return script;
				}
			}
		}

		static System.Type GetTypeByFullName(string fullName) =>
			System.AppDomain.CurrentDomain.GetAssemblies()
				.SelectMany(a => a.GetTypes())
				.FirstOrDefault(t => t.FullName == fullName);

		static System.Type GetTypeByFullName(string fullName, System.AppDomain domain) =>
			domain.GetAssemblies().SelectMany(a => a.GetTypes()).FirstOrDefault(t => t.FullName == fullName);

		public static void BeginVerticalBoxGroupLayout(string label = "")
		{
			EditorGUILayout.BeginVertical(GUI.skin.box);
			if (!string.IsNullOrEmpty(label))
			{
				EditorGUILayout.LabelField(label, EditorStyles.boldLabel);
			}
		}

		public static void EndVerticalBoxGroupLayout() => EditorGUILayout.EndVertical();
		#endregion

		#region Nested Types
		[System.Serializable]
		public class SimListEditorWindowSettings
		{
			public string TargetTypeAsString { get; set; }

			public List<EditorInfo> Editors { get; set; } = new List<EditorInfo>();
		}

		[System.Serializable]
		public class EditorInfo
		{
			#region Fields
			Editor editor;
			#endregion

			#region Properties
			public bool IsFoldout { get; set; }

			[JsonIgnore]
			public Editor Editor
			{
				get
				{
					if (this.editor != null)
					{
						return this.editor;
					}

					return this.editor = Editor.CreateEditor(CreateInstance(this.Type));
				}

				protected set => this.editor = value;
			}

			public System.Type Type { get; set; }
			#endregion

			#region Public Methods
			public void Draw()
			{
				var method = this.Type.GetMethod(
					"OnGUI",
					BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
				if (method != null)
				{
					method.Invoke(this.Editor.target, new object[0]);
				}
			}
			#endregion
		}
		#endregion
	}
}
