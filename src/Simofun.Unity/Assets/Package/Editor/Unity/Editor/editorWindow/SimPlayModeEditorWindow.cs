﻿//-----------------------------------------------------------------------
// <copyright file="SimPlayModeEditorWindow.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Editor
{
	using UnityEditor;
	using UnityEngine;

	public class SimPlayModeEditorWindow : SimEditorWindowBase
	{
		#region Fields
		readonly Color blueColor = new Color(114f / 255f, 178f / 255f, 238f / 255f, 1f);
		#endregion

		#region Public Static Methods
		[MenuItem("Simofun/Editor/Window/Play Mode Editor Window", priority = 12)]
		public static SimPlayModeEditorWindow Open()
		{
			var window = GetWindow<SimPlayModeEditorWindow>("App Editor Window");
			window.titleContent.image = EditorGUIUtility.IconContent("preAudioPlayOn").image;
			window.Show();

			return window;
		}
		#endregion

		#region Unity Methods
		#region GUI Methods
		protected virtual void OnGUI()
		{
			var guiColor = GUI.backgroundColor;
			{
				var isStep = false;
				EditorGUILayout.BeginHorizontal();
				{
					GUI.backgroundColor = EditorApplication.isPlaying ? this.blueColor : guiColor;
					if (GUILayout.Button(EditorGUIUtility.IconContent("d_PlayButton@2x"), GUILayout.MinHeight(64)))
					{
						EditorApplication.isPlaying = !EditorApplication.isPlaying;
					}

					GUI.backgroundColor = EditorApplication.isPaused ? this.blueColor : guiColor;
					if (GUILayout.Button(EditorGUIUtility.IconContent("d_PauseButton@2x"), GUILayout.MinHeight(64)))
					{
						EditorApplication.isPaused = !EditorApplication.isPaused;
					}

					GUI.backgroundColor = guiColor;

					if (GUILayout.Button(EditorGUIUtility.IconContent("d_StepButton@2x"), GUILayout.MinHeight(64)))
					{
						isStep = true;
					}
				}
				EditorGUILayout.EndHorizontal();

				if (isStep)
				{
					EditorApplication.Step();
				}
			}
			GUI.backgroundColor = guiColor;
		}
		#endregion
		#endregion
	}
}
