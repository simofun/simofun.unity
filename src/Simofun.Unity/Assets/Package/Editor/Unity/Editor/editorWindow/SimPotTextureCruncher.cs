#if UNITY_2021_2_OR_NEWER
//-----------------------------------------------------------------------
// <copyright file="SimPotTextureCruncher.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Editor
{
	using System.Linq;
	using UnityEditor;
	using UnityEngine;

	public class SimPotTextureCruncher : SimEditorWindowBase
	{
		#region Fields
		const string defaultMessage = "All Textures";

		readonly int[] textureSizes = new int[] {
			32,
			64,
			128,
			256,
			512,
			1024,
			2048,
			4096,
			8192,
			16384,
		};

		bool isEnabledX2 = true;

		int compressionQuality = 50;

		string message = defaultMessage;
		#endregion

		#region Public Static Methods
		[MenuItem("Simofun/Editor/Window/POT Texture Cruncher Editor Window", priority = 13)]
		public static SimPotTextureCruncher Open()
		{
			var window = GetWindow<SimPotTextureCruncher>("POT Texture Cruncher");
			window.titleContent.image = EditorGUIUtility.IconContent("PreTextureArrayFirstSlice").image;
			window.Show();

			return window;
		}
		#endregion

		#region Unity Methods
		#region Editor
		/// <inheritdoc />
		protected virtual void OnGUI()
		{
			this.message = Selection.objects.Length < 1
				? defaultMessage
				: "Selected " + Selection.objects.Length + " assets";

			EditorGUILayout.HelpBox(this.message, MessageType.Info);

			this.isEnabledX2 = EditorGUILayout.Toggle("Is Enabled X2", this.isEnabledX2);
			this.compressionQuality = EditorGUILayout.IntSlider(
				"Compressor Quality:", this.compressionQuality, 0, 100);

			if (GUILayout.Button("Start"))
			{
				this.CrunchTextures();
			}
		}

		/// <inheritdoc />
		protected virtual void OnInspectorUpdate() => this.Repaint();
		#endregion
		#endregion

		#region Methods
		void CrunchTextures()
		{
			var textureImporters = Selection.objects.Length > 0
				? Selection.objects
					.Select(o => AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(o)) as TextureImporter)
				: AssetDatabase.FindAssets("t:texture", null)
					.Select(p => AssetImporter.GetAtPath(AssetDatabase.GUIDToAssetPath(p)) as TextureImporter);

			textureImporters = textureImporters.Where(t => t != null)
				.Where(t => !t.crunchedCompression || t.compressionQuality != this.compressionQuality)
				.Where(t =>
				{
					t.GetSourceTextureWidthAndHeight(out int width, out int height);

					return this.textureSizes.Contains(width) && this.textureSizes.Contains(height);
				});

			foreach (var textureImporter in textureImporters)
			{
				textureImporter.compressionQuality = this.compressionQuality;
				textureImporter.crunchedCompression = true;
				this.FixTextureSize(textureImporter);
				AssetDatabase.ImportAsset(textureImporter.assetPath);
			}
		}

		void FixTextureSize(TextureImporter importer)
		{
			importer.GetSourceTextureWidthAndHeight(out int width, out int height);
			var max = Mathf.Max(width, height);
			var size = 1024;
			for (var i = 0; i < this.textureSizes.Length; i++)
			{
				if (this.textureSizes[i] >= max)
				{
					size = this.textureSizes[i];

					break;
				}
			}

			importer.maxTextureSize = Mathf.Max(size * (this.isEnabledX2 ? 2 : 1), this.textureSizes.Min());
		}
		#endregion
	}
}
#endif
