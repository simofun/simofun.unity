//-----------------------------------------------------------------------
// <copyright file="SimResourceChecker.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

// Resource Checker
// (c) 2012 Simon Oliver / HandCircus / hello@handcircus.com
// (c) 2015 Brice Clocher / Mangatome / hello@mangatome.net
// Public domain, do with whatever you like, commercial or not
// This comes with no warranty, use at your own risk!
// https://github.com/handcircus/Unity-Resource-Checker

namespace Simofun.Unity.Editor
{
	using System.Collections.Generic;
	using System.Linq;
	using System.Reflection;
	using UnityEditor;
	using UnityEngine;
	using UnityEngine.SceneManagement;
	using UnityEngine.UI;

	public class SimResourceCheckerEditorWindow : SimEditorWindowBase
	{
		#region Fields
		readonly float ThumbnailHeight = 40;

		readonly float ThumbnailWidth = 40;

		readonly List<MaterialDetails> ActiveMaterials = new();

		readonly List<MeshDetails> ActiveMeshDetails = new();

		readonly List<MissingGraphic> MissingObjects = new();

		bool collectedInPlayingMode;

		bool ctrlPressed = false;

		bool IncludeDisabledObjects = true;

		bool IncludeGuiElements = true;

		bool IncludeLightmapTextures = true;

		bool IncludeScriptReferences = true;

		bool IncludeSpriteAnimations = true;

		bool isThingsMissing = false;

		Color defColor;

		InspectType ActiveInspectType = InspectType.Textures;

		int TotalMeshVertices = 0;

		int TotalTextureMemory = 0;

		List<TextureDetails> ActiveTextures = new();

		string[] inspectToolbarStrings = { "Textures", "Materials", "Meshes" };

		string[] inspectToolbarStrings2 = { "Textures", "Materials", "Meshes", "Missing" };

		Vector2 materialListScrollPos = new(0, 0);

		Vector2 meshListScrollPos = new(0, 0);

		Vector2 missingListScrollPos = new(0, 0);

		Vector2 textureListScrollPos = new(0, 0);
		#endregion

		#region Public Static Methods
		[MenuItem("Simofun/Editor/Window/Resource Checker Editor Window", priority = 14)]
		public static SimResourceCheckerEditorWindow Open()
		{
			var window = GetWindowWithRect<SimResourceCheckerEditorWindow>(
				new Rect(20, 20, 820, 612), false, "Resources Checker");
			window.titleContent.image = EditorGUIUtility.IconContent("CustomTool@2x").image;
			window.Show();
			window.CheckResources();

			return window;
		}
		#endregion

		#region Unity Methods
		#region Editor
		/// <inheritdoc />
		protected virtual void OnGUI()
		{
			this.DrawOptionsGroup();
			this.RemoveDestroyedResources();
			this.DrawButtonSelection();
			this.ctrlPressed = Event.current.control || Event.current.command;

			switch (this.ActiveInspectType)
			{
				case InspectType.Textures:
					{
						ListTextures();

						break;
					}
				case InspectType.Materials:
					{
						ListMaterials();

						break;
					}
				case InspectType.Meshes:
					{
						ListMeshes();

						break;
					}
				case InspectType.Missing:
					{
						ListMissing();

						break;
					}
			}

			if (this.isThingsMissing)
			{
				EditorGUILayout.HelpBox("Some GameObjects are missing graphical elements.", MessageType.Error);
			}
		}
		#endregion
		#endregion

		#region Static Methods
		static GameObject[] GetAllRootGameObjects()
		{
#if !UNITY_5_3_OR_NEWER
			return UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects().ToArray();
#else
			List<GameObject> allGo = new List<GameObject>();

			for (int sceneIdx = 0; sceneIdx < UnityEngine.SceneManagement.SceneManager.sceneCount; ++sceneIdx)
			{
				allGo.AddRange(UnityEngine.SceneManagement.SceneManager.GetSceneAt(sceneIdx).GetRootGameObjects().ToArray());
			}

			allGo.AddRange(GetDontDestroyOnLoadRoots());
			return allGo.ToArray();
#endif
		}

		/// <summary>
		/// Sort by RenderQueue
		/// </summary>
		static int MaterialSorter(MaterialDetails first, MaterialDetails second)
		{
			var firstIsNull = first.material == null;
			var secondIsNull = second.material == null;

			if (firstIsNull && secondIsNull) return 0;

			if (firstIsNull) return int.MaxValue;

			if (secondIsNull) return int.MinValue;

			return first.material.renderQueue - second.material.renderQueue;
		}

		static List<GameObject> GetDontDestroyOnLoadRoots()
		{
			List<GameObject> objs = new List<GameObject>();

			if (Application.isPlaying)
			{
				GameObject temp = null;

				try
				{
					temp = new GameObject();
					DontDestroyOnLoad(temp);
					UnityEngine.SceneManagement.Scene dontDestryScene = temp.scene;
					DestroyImmediate(temp);
					temp = null;

					if (dontDestryScene.IsValid())
					{
						objs = dontDestryScene.GetRootGameObjects().ToList();
					}
				}
				catch (System.Exception e)
				{
					Debug.LogException(e);
					return null;
				}
				finally
				{
					if (temp != null)
						DestroyImmediate(temp);
				}
			}

			return objs;
		}
		#endregion

		#region Methods
		int CalculateTextureSizeBytes(Texture tTexture)
		{
			var tWidth = tTexture.width;
			var tHeight = tTexture.height;

			if (tTexture is Texture2D)
			{
				var tTex2D = tTexture as Texture2D;
				var bitsPerPixel = this.GetBitsPerPixel(tTex2D.format);
				var mipMapCount = tTex2D.mipmapCount;
				var mipLevel = 1;
				var tSize = 0;

				while (mipLevel <= mipMapCount)
				{
					tSize += tWidth * tHeight * bitsPerPixel / 8;
					tWidth /= 2;
					tHeight /= 2;
					mipLevel++;
				}

				return tSize;
			}

			if (tTexture is Texture2DArray)
			{
				var tTex2D = tTexture as Texture2DArray;
				var bitsPerPixel = this.GetBitsPerPixel(tTex2D.format);
				var mipMapCount = 10;
				var mipLevel = 1;
				var tSize = 0;

				while (mipLevel <= mipMapCount)
				{
					tSize += tWidth * tHeight * bitsPerPixel / 8;
					tWidth = tWidth / 2;
					tHeight = tHeight / 2;
					mipLevel++;
				}

				return tSize * (tTex2D).depth;
			}

			if (tTexture is Cubemap)
			{
				var bitsPerPixel = this.GetBitsPerPixel((tTexture as Cubemap).format);

				return tWidth * tHeight * 6 * bitsPerPixel / 8;
			}

			return 0;
		}

		int GetBitsPerPixel(TextureFormat format)
		{
			switch (format)
			{
				case TextureFormat.Alpha8:      // Alpha-only texture format.
					{
						return 8;
					}
				case TextureFormat.ARGB4444:    // A 16 bits/pixel texture format. Texture stores color with an alpha channel.
					{
						return 16;
					}
				case TextureFormat.RGBA4444:    // A 16 bits/pixel texture format.
					{
						return 16;
					}
				case TextureFormat.RGB24:       // A color texture format.
					{
						return 24;
					}
				case TextureFormat.RGBA32:      // Color with an alpha channel texture format.
					{
						return 32;
					}
				case TextureFormat.ARGB32:      // Color with an alpha channel texture format.
					{
						return 32;
					}
				case TextureFormat.RGB565:      // A 16 bit color texture format.
					{
						return 16;
					}
				case TextureFormat.DXT1:        // Compressed color texture format.
					{
						return 4;
					}
				case TextureFormat.DXT5:        // Compressed color with alpha channel texture format.
					{
						return 8;
					}
				case TextureFormat.PVRTC_RGB2:  // PowerVR (iOS) 2 bits/pixel compressed color texture format.
					{
						return 2;
					}
				case TextureFormat.PVRTC_RGBA2: // PowerVR (iOS) 2 bits/pixel compressed with alpha channel texture format
					{
						return 2;
					}
				case TextureFormat.PVRTC_RGB4:  // PowerVR (iOS) 4 bits/pixel compressed color texture format.
					{
						return 4;
					}
				case TextureFormat.PVRTC_RGBA4: // PowerVR (iOS) 4 bits/pixel compressed with alpha channel texture format
					{
						return 4;
					}
				case TextureFormat.ETC_RGB4:    // ETC (GLES2.0) 4 bits/pixel compressed RGB texture format.
					{
						return 4;
					}
				case TextureFormat.ETC2_RGBA8:
					{
						return 8;
					}
				case TextureFormat.EAC_R:
					{
						return 4;
					}
				case TextureFormat.BGRA32:      // Format returned by iPhone camera
					{
						return 32;
					}
			}

			return 0;
		}

		MaterialDetails FindMaterialDetails(Material tMaterial)
		{
			foreach (var tMaterialDetails in this.ActiveMaterials)
			{
				if (tMaterialDetails.material == tMaterial)
				{
					return tMaterialDetails;
				}
			}

			return null;
		}

		MeshDetails FindMeshDetails(Mesh tMesh)
		{
			foreach (var tMeshDetails in this.ActiveMeshDetails)
			{
				if (tMeshDetails.mesh == tMesh)
				{
					return tMeshDetails;
				}
			}

			return null;
		}

		string ShaderShortName(string value, int maxLength)
		{
			if (value.Contains("Shader Graphs/"))
			{
				value = value.Replace("Shader Graphs/", "");
			}
			else if (value.Contains("Universal Render Pipeline/"))
			{
				value = value.Replace("Universal Render Pipeline/", "");
			}

			if (value.Length > maxLength)
			{
				value = $"{value.Substring(0, maxLength)}...";
			}

			return value;
		}

		string ShortName(string value, int maxLength)
		{
			var length = value.Length;
			if (length > maxLength)
			{
				value = $"{value.Substring(0, maxLength)}...";
			}

			return value;
		}

		string FormatSizeString(int memSizeKB)
		{
			if (memSizeKB < 1024)
			{
				return $"{memSizeKB} Kb";
			}

			var memSizeMB = memSizeKB / 1024.0f;

			return $"{memSizeMB.ToString("0.00")} Mb";
		}

		T[] FindObjects<T>() where T : Object
		{
			if (!IncludeDisabledObjects)
			{
				return FindObjectsByType<T>(FindObjectsSortMode.None);
			}

			var meshfilters = new List<T>();
			foreach (var go in GetAllRootGameObjects())
			{
				foreach (var tr in go.GetComponentsInChildren<Transform>(true).ToArray())
				{
					if (tr.TryGetComponent<T>(out var comp))
					{
						meshfilters.Add(comp);
					}
				}
			}

			return meshfilters.ToArray();
		}

		TextureDetails FindTextureDetails(Texture tTexture)
		{
			foreach (var tTextureDetails in this.ActiveTextures)
			{
				if (tTextureDetails.texture == tTexture)
				{
					return tTextureDetails;
				}
			}

			return null;
		}

		TextureDetails GetTextureDetail(Texture tTexture)
		{
			var tTextureDetails = this.FindTextureDetails(tTexture);
			if (tTextureDetails != null)
			{
				return tTextureDetails;
			}

			tTextureDetails = new TextureDetails
			{
				isCubeMap = tTexture is Cubemap,
				texture = tTexture
			};

			var memSize = this.CalculateTextureSizeBytes(tTexture);
			var tFormat = TextureFormat.RGBA32;
			var tMipMapCount = 1;

			if (tTexture is Texture2D)
			{
				tFormat = (tTexture as Texture2D).format;
				tMipMapCount = (tTexture as Texture2D).mipmapCount;
			}

			if (tTexture is Cubemap)
			{
				tFormat = (tTexture as Cubemap).format;
				memSize = 8 * tTexture.height * tTexture.width;
			}

			if (tTexture is Texture2DArray)
			{
				tFormat = (tTexture as Texture2DArray).format;
				tMipMapCount = 10;
			}

			tTextureDetails.memSizeKB = memSize / 1024;
			tTextureDetails.format = tFormat;
			tTextureDetails.mipMapCount = tMipMapCount;

			return tTextureDetails;
		}

		TextureDetails GetTextureDetail(Texture tTexture, Animator animator)
		{
			var tTextureDetails = this.GetTextureDetail(tTexture);
			tTextureDetails.FoundInAnimators.Add(animator);

			return tTextureDetails;
		}

		TextureDetails GetTextureDetail(Texture tTexture, Button button)
		{
			var tTextureDetails = this.GetTextureDetail(tTexture);
			if (!tTextureDetails.FoundInButtons.Contains(button))
			{
				tTextureDetails.FoundInButtons.Add(button);
			}

			return tTextureDetails;
		}

		TextureDetails GetTextureDetail(Texture tTexture, Graphic graphic)
		{
			var tTextureDetails = this.GetTextureDetail(tTexture);
			tTextureDetails.FoundInGraphics.Add(graphic);

			return tTextureDetails;
		}

		TextureDetails GetTextureDetail(Texture tTexture, Material tMaterial, MaterialDetails tMaterialDetails)
		{
			var tTextureDetails = this.GetTextureDetail(tTexture);
			tTextureDetails.FoundInMaterials.Add(tMaterial);

			foreach (var renderer in tMaterialDetails.FoundInRenderers)
			{
				if (!tTextureDetails.FoundInRenderers.Contains(renderer))
				{
					tTextureDetails.FoundInRenderers.Add(renderer);
				}
			}

			foreach (var graphic in tMaterialDetails.FoundInGraphics)
			{
				if (!tTextureDetails.FoundInGraphics.Contains(graphic))
				{
					tTextureDetails.FoundInGraphics.Add(graphic);
				}
			}

			return tTextureDetails;
		}

		TextureDetails GetTextureDetail(Texture tTexture, MonoBehaviour script)
		{
			var tTextureDetails = this.GetTextureDetail(tTexture);
			tTextureDetails.FoundInScripts.Add(script);

			return tTextureDetails;
		}

		TextureDetails GetTextureDetail(Texture tTexture, Renderer renderer)
		{
			var tTextureDetails = this.GetTextureDetail(tTexture);
			tTextureDetails.FoundInRenderers.Add(renderer);

			return tTextureDetails;
		}

		void CheckResources()
		{
			this.ActiveTextures.Clear();
			this.ActiveMaterials.Clear();
			this.ActiveMeshDetails.Clear();
			this.MissingObjects.Clear();
			this.isThingsMissing = false;
			var renderers = FindObjects<Renderer>();
			var skyMat = new MaterialDetails
			{
				isSky = true,
				material = RenderSettings.skybox
			};
			this.ActiveMaterials.Add(skyMat);

			foreach (var renderer in renderers)
			{
				foreach (var material in renderer.sharedMaterials)
				{
					var tMaterialDetails = this.FindMaterialDetails(material);
					if (tMaterialDetails == null)
					{
						tMaterialDetails = new MaterialDetails
						{
							material = material
						};
						this.ActiveMaterials.Add(tMaterialDetails);
					}

					tMaterialDetails.FoundInRenderers.Add(renderer);
				}

				if (renderer is SpriteRenderer)
				{
					var tSpriteRenderer = (SpriteRenderer)renderer;
					if (tSpriteRenderer.sprite != null)
					{
						var tSpriteTextureDetail = GetTextureDetail(tSpriteRenderer.sprite.texture, renderer);

						if (!this.ActiveTextures.Contains(tSpriteTextureDetail))
						{
							this.ActiveTextures.Add(tSpriteTextureDetail);
						}
					}
					else if (tSpriteRenderer.sprite == null)
					{
						var tMissing = new MissingGraphic
						{
							name = tSpriteRenderer.transform.name,
							Object = tSpriteRenderer.transform,
							type = "sprite"
						};
						this.MissingObjects.Add(tMissing);
						this.isThingsMissing = true;
					}
				}
			}

			if (this.IncludeLightmapTextures)
			{
				// Unity lightmaps
				foreach (var lightmapData in LightmapSettings.lightmaps)
				{
					if (lightmapData.lightmapColor != null)
					{
						var textureDetail = this.GetTextureDetail(lightmapData.lightmapColor);
						if (!this.ActiveTextures.Contains(textureDetail))
						{
							this.ActiveTextures.Add(textureDetail);
						}
					}

					if (lightmapData.lightmapDir != null)
					{
						var textureDetail = this.GetTextureDetail(lightmapData.lightmapColor);
						if (!this.ActiveTextures.Contains(textureDetail))
						{
							this.ActiveTextures.Add(textureDetail);
						}
					}

					if (lightmapData.shadowMask != null)
					{
						var textureDetail = this.GetTextureDetail(lightmapData.shadowMask);
						if (!this.ActiveTextures.Contains(textureDetail))
						{
							this.ActiveTextures.Add(textureDetail);
						}
					}
				}
			}

			if (this.IncludeGuiElements)
			{
				foreach (var graphic in FindObjects<Graphic>())
				{
					if (graphic.mainTexture)
					{
						var tSpriteTextureDetail = this.GetTextureDetail(graphic.mainTexture, graphic);
						if (!this.ActiveTextures.Contains(tSpriteTextureDetail))
						{
							this.ActiveTextures.Add(tSpriteTextureDetail);
						}
					}

					if (graphic.materialForRendering)
					{
						var tMaterialDetails = this.FindMaterialDetails(graphic.materialForRendering);
						if (tMaterialDetails == null)
						{
							tMaterialDetails = new MaterialDetails
							{
								isgui = true,
								material = graphic.materialForRendering
							};
							this.ActiveMaterials.Add(tMaterialDetails);
						}

						tMaterialDetails.FoundInGraphics.Add(graphic);
					}
				}

				foreach (var button in FindObjects<Button>())
				{
					this.CheckButtonSpriteState(button, button.spriteState.disabledSprite);
					this.CheckButtonSpriteState(button, button.spriteState.highlightedSprite);
					this.CheckButtonSpriteState(button, button.spriteState.pressedSprite);
				}
			}

			foreach (var tMaterialDetails in this.ActiveMaterials)
			{
				var tMaterial = tMaterialDetails.material;
				if (tMaterial != null)
				{
					var dependencies = EditorUtility.CollectDependencies(new Object[] { tMaterial });
					foreach (Object obj in dependencies)
					{
						if (obj is not Texture)
						{
							continue;
						}

						var tTexture = obj as Texture;
						var tTextureDetail = this.GetTextureDetail(tTexture, tMaterial, tMaterialDetails);
						tTextureDetail.isSky = tMaterialDetails.isSky;
						tTextureDetail.instance = tMaterialDetails.instance;
						tTextureDetail.isgui = tMaterialDetails.isgui;
						this.ActiveTextures.Add(tTextureDetail);
					}

					// If the texture was downloaded, it won't be included in the editor dependencies
					if (tMaterial.HasProperty("_MainTex")
							&& tMaterial.mainTexture != null && !dependencies.Contains(tMaterial.mainTexture))
					{
						this.ActiveTextures.Add(
							this.GetTextureDetail(tMaterial.mainTexture, tMaterial, tMaterialDetails));
					}
				}
			}

			foreach (MeshFilter tMeshFilter in FindObjects<MeshFilter>())
			{
				var tMesh = tMeshFilter.sharedMesh;
				if (tMesh != null)
				{
					var tMeshDetails = this.FindMeshDetails(tMesh);
					if (tMeshDetails == null)
					{
						tMeshDetails = new MeshDetails
						{
							mesh = tMesh
						};
						this.ActiveMeshDetails.Add(tMeshDetails);
					}

					tMeshDetails.FoundInMeshFilters.Add(tMeshFilter);

					if (GameObjectUtility.AreStaticEditorFlagsSet(
							tMeshFilter.gameObject, StaticEditorFlags.BatchingStatic))
					{
						tMeshDetails.StaticBatchingEnabled.Add(tMeshFilter.gameObject);
					}

				}
				else if (tMesh == null && tMeshFilter.transform.GetComponent("TextContainer") == null)
				{
					var tMissing = new MissingGraphic
					{
						name = tMeshFilter.transform.name,
						Object = tMeshFilter.transform,
						type = "mesh"
					};
					this.MissingObjects.Add(tMissing);
					this.isThingsMissing = true;
				}

				var meshRenderrer = tMeshFilter.transform.GetComponent<MeshRenderer>();

				if (meshRenderrer == null || meshRenderrer.sharedMaterial == null)
				{
					var tMissing = new MissingGraphic
					{
						name = tMeshFilter.transform.name,
						Object = tMeshFilter.transform,
						type = "material"
					};
					this.MissingObjects.Add(tMissing);
					this.isThingsMissing = true;
				}
			}

			foreach (var tSkinnedMeshRenderer in FindObjects<SkinnedMeshRenderer>())
			{
				var tMesh = tSkinnedMeshRenderer.sharedMesh;
				if (tMesh != null)
				{
					var tMeshDetails = this.FindMeshDetails(tMesh);
					if (tMeshDetails == null)
					{
						tMeshDetails = new MeshDetails
						{
							mesh = tMesh
						};
						this.ActiveMeshDetails.Add(tMeshDetails);
					}

					tMeshDetails.FoundInSkinnedMeshRenderer.Add(tSkinnedMeshRenderer);
				}
				else if (tMesh == null)
				{
					var tMissing = new MissingGraphic
					{
						name = tSkinnedMeshRenderer.transform.name,
						Object = tSkinnedMeshRenderer.transform,
						type = "mesh"
					};
					this.MissingObjects.Add(tMissing);
					this.isThingsMissing = true;
				}
				
				if (tSkinnedMeshRenderer.sharedMaterial == null)
				{
					var tMissing = new MissingGraphic
					{
						name = tSkinnedMeshRenderer.transform.name,
						Object = tSkinnedMeshRenderer.transform,
						type = "material"
					};
					this.MissingObjects.Add(tMissing);
					this.isThingsMissing = true;
				}
			}
			
			// Check if any LOD groups have no renderers
			foreach (var group in this.FindObjects<LODGroup>())
			{
				var lods = group.GetLODs();
				for (int i = 0, l = lods.Length; i < l; i++)
				{
					if (lods[i].renderers.Length != 0)
					{
						continue;
					}
					
					var tMissing = new MissingGraphic
					{
						name = group.transform.name,
						Object = group.transform,
						type = "lod"
					};
					this.MissingObjects.Add(tMissing);
					this.isThingsMissing = true;
				}
			}

			if (this.IncludeSpriteAnimations)
			{
				foreach (var anim in FindObjects<Animator>())
				{
#if UNITY_4
					var ac = anim.runtimeAnimatorController as UnityEditorInternal.AnimatorController;
#elif UNITY_5_3_OR_NEWER
					var ac = anim.runtimeAnimatorController as UnityEditor.Animations.AnimatorController;
#endif
					// Skip animators without layers, this can happen if they don't have an animator controller.
					if (!ac || ac.layers == null || ac.layers.Length == 0)
					{
						continue;
					}

					for (var x = 0; x < anim.layerCount; x++)
					{
						var sm = ac.layers[x].stateMachine;
						var cnt = sm.states.Length;

						for (var i = 0; i < cnt; i++)
						{
							var state = sm.states[i].state;
							var m = state.motion;
							if (m == null)
							{
								continue;
							}

							var clip = m as AnimationClip;
							if (clip == null)
							{
								continue;
							}

							foreach (var ecb in AnimationUtility.GetObjectReferenceCurveBindings(clip))
							{
								if (ecb.propertyName != "m_Sprite")
								{
									continue;
								}

								foreach (var keyframe in AnimationUtility.GetObjectReferenceCurve(clip, ecb))
								{
									var tSprite = keyframe.value as Sprite;
									if (tSprite == null)
									{
										continue;
									}

									var tTextureDetail = this.GetTextureDetail(tSprite.texture, anim);
									if (!this.ActiveTextures.Contains(tTextureDetail))
									{
										this.ActiveTextures.Add(tTextureDetail);
									}
								}
							}
						}
					}

				}
			}

			if (this.IncludeScriptReferences)
			{
				foreach (var script in FindObjects<MonoBehaviour>())
				{
					var flags = BindingFlags.Public | BindingFlags.Instance; // Only public non-static fields are bound to by Unity.
					foreach (var field in script.GetType().GetFields(flags))
					{
						var fieldType = field.FieldType;
						if (fieldType == typeof(Sprite))
						{
							var tSprite = field.GetValue(script) as Sprite;
							if (tSprite != null)
							{
								var tSpriteTextureDetail = this.GetTextureDetail(tSprite.texture, script);
								if (!this.ActiveTextures.Contains(tSpriteTextureDetail))
								{
									this.ActiveTextures.Add(tSpriteTextureDetail);
								}
							}
						}

						if (fieldType == typeof(Mesh))
						{
							var tMesh = field.GetValue(script) as Mesh;
							if (tMesh != null)
							{
								var tMeshDetails = this.FindMeshDetails(tMesh);
								if (tMeshDetails == null)
								{
									tMeshDetails = new MeshDetails
									{
										instance = true,
										mesh = tMesh
									};
									this.ActiveMeshDetails.Add(tMeshDetails);
								}
							}
						}

						if (fieldType == typeof(Material))
						{
							var tMaterial = field.GetValue(script) as Material;
							if (tMaterial != null)
							{
								var tMatDetails = this.FindMaterialDetails(tMaterial);
								if (tMatDetails == null)
								{
									tMatDetails = new MaterialDetails
									{
										instance = true,
										material = tMaterial
									};

									if (!this.ActiveMaterials.Contains(tMatDetails))
									{
										this.ActiveMaterials.Add(tMatDetails);
									}
								}

								if (tMaterial.mainTexture)
								{
									var tSpriteTextureDetail = this.GetTextureDetail(tMaterial.mainTexture);
									if (!this.ActiveTextures.Contains(tSpriteTextureDetail))
									{
										this.ActiveTextures.Add(tSpriteTextureDetail);
									}
								}

								foreach (var obj in EditorUtility.CollectDependencies(new Object[] { tMaterial }))
								{
									if (obj is not Texture)
									{
										continue;
									}

									var tTexture = obj as Texture;
									var tTextureDetail = this.GetTextureDetail(tTexture, tMaterial, tMatDetails);
									if (!this.ActiveTextures.Contains(tTextureDetail))
									{
										this.ActiveTextures.Add(tTextureDetail);
									}
								}
							}
						}
					}
				}
			}

			this.TotalTextureMemory = 0;
			foreach (TextureDetails tTextureDetails in this.ActiveTextures)
			{
				this.TotalTextureMemory += tTextureDetails.memSizeKB;
			}

			this.TotalMeshVertices = 0;
			foreach (MeshDetails tMeshDetails in this.ActiveMeshDetails)
			{
				this.TotalMeshVertices += tMeshDetails.mesh.vertexCount;
			}

			// Sort by size, descending
			this.ActiveTextures.Sort(
				delegate (TextureDetails details1, TextureDetails details2)
				{
					return details2.memSizeKB - details1.memSizeKB;
				});
			this.ActiveTextures = this.ActiveTextures.Distinct().ToList();
			this.ActiveMeshDetails.Sort(
				delegate (MeshDetails details1, MeshDetails details2)
				{
					return details2.mesh.vertexCount - details1.mesh.vertexCount;
				});
			this.collectedInPlayingMode = Application.isPlaying;

			this.ActiveMaterials.Sort(MaterialSorter); // Sort by render queue
		}

		void CheckButtonSpriteState(Button button, Sprite sprite)
		{
			if (sprite == null)
			{
				return;
			}

			var texture = sprite.texture;
			var tButtonTextureDetail = this.GetTextureDetail(texture, button);
			if (!this.ActiveTextures.Contains(tButtonTextureDetail))
			{
				this.ActiveTextures.Add(tButtonTextureDetail);
			}
		}

		void DrawOptionsGroup()
		{
			EditorGUILayout.LabelField(new GUIContent("Filters"), "box", GUILayout.ExpandWidth(true));

			using (var hGroup0 = new GUILayout.HorizontalScope("box", GUILayout.Height(98)))
			{
				using (var vGroup0 = new GUILayout.VerticalScope("box", GUILayout.ExpandWidth(true)))
				{
					this.defColor = GUI.color;

					this.IncludeDisabledObjects = GUILayout.Toggle(
						this.IncludeDisabledObjects, "Include disabled objects");
					this.IncludeSpriteAnimations = GUILayout.Toggle(
						this.IncludeSpriteAnimations, "Look in sprite animations");

					GUI.color = new Color(0.8f, 0.8f, 1.0f, 1.0f);
					this.IncludeScriptReferences = GUILayout.Toggle(
						this.IncludeScriptReferences, "Look in behavior fields");

					GUI.color = new Color(1.0f, 0.95f, 0.8f, 1.0f);
					this.IncludeGuiElements = GUILayout.Toggle(this.IncludeGuiElements, "Look in GUI elements");
					this.IncludeLightmapTextures = GUILayout.Toggle(
						this.IncludeLightmapTextures, "Look in Lightmap textures");

					GUI.color = this.defColor;
				}

				using (var vGroup1 = new GUILayout.VerticalScope(GUILayout.Width(100)))
				{
					if (GUILayout.Button("Calculate", GUILayout.ExpandHeight(true)))
					{
						this.CheckResources();
					}

					if (GUILayout.Button("CleanUp", GUILayout.ExpandHeight(true)))
					{
						Resources.UnloadUnusedAssets();
					}

				}
			}
		}

		void DrawButtonSelection()
		{
			EditorGUILayout.LabelField(
				new GUIContent($"Current objects within scene -{SceneManager.GetActiveScene().name}-"),
				"box",
				GUILayout.ExpandWidth(true));

			using (var hGroup = new GUILayout.HorizontalScope())
			{
				EditorGUILayout.LabelField(
					new GUIContent($"Textures {this.ActiveTextures.Count} - {FormatSizeString(TotalTextureMemory)}"),
					"box",
					GUILayout.ExpandWidth(true));
				EditorGUILayout.LabelField(
					new GUIContent($"Materials {this.ActiveMaterials.Count}"),
					"box",
					GUILayout.ExpandWidth(true));
				EditorGUILayout.LabelField(
					new GUIContent($"Meshes {this.ActiveMeshDetails.Count} - {this.TotalMeshVertices} verts"),
					"box",
					GUILayout.ExpandWidth(true));
			}

			if (this.isThingsMissing)
			{
				this.ActiveInspectType = (InspectType)GUILayout.Toolbar(
					(int)this.ActiveInspectType, this.inspectToolbarStrings2);

				return;
			}

			this.ActiveInspectType = (InspectType)GUILayout.Toolbar(
				(int)this.ActiveInspectType, this.inspectToolbarStrings);
		}

		void RemoveDestroyedResources()
		{
			if (this.collectedInPlayingMode != Application.isPlaying)
			{
				this.ActiveTextures.Clear();
				this.ActiveMaterials.Clear();
				this.ActiveMeshDetails.Clear();
				this.MissingObjects.Clear();
				this.isThingsMissing = false;
				this.collectedInPlayingMode = Application.isPlaying;
			}

			this.ActiveTextures.RemoveAll(x => !x.texture);

			this.ActiveTextures.ForEach(delegate (TextureDetails obj)
			{
				obj.FoundInAnimators.RemoveAll(x => !x);
				obj.FoundInMaterials.RemoveAll(x => !x);
				obj.FoundInRenderers.RemoveAll(x => !x);
				obj.FoundInScripts.RemoveAll(x => !x);
				obj.FoundInGraphics.RemoveAll(x => !x);
			});

			this.ActiveMaterials.RemoveAll(x => !x.material);

			this.ActiveMaterials.ForEach(delegate (MaterialDetails obj)
			{
				obj.FoundInRenderers.RemoveAll(x => !x);
				obj.FoundInGraphics.RemoveAll(x => !x);
			});

			this.ActiveMeshDetails.RemoveAll(x => !x.mesh);

			this.ActiveMeshDetails.ForEach(delegate (MeshDetails obj)
			{
				obj.FoundInMeshFilters.RemoveAll(x => !x);
				obj.FoundInSkinnedMeshRenderer.RemoveAll(x => !x);
				obj.StaticBatchingEnabled.RemoveAll(x => !x);
			});

			this.TotalTextureMemory = 0;
			foreach (var tTextureDetails in this.ActiveTextures)
			{
				this.TotalTextureMemory += tTextureDetails.memSizeKB;
			}

			this.TotalMeshVertices = 0;
			foreach (var tMeshDetails in this.ActiveMeshDetails)
			{
				this.TotalMeshVertices += tMeshDetails.mesh.vertexCount;
			}
		}

		void SelectObject(Object selectedObject, bool isAppend)
		{
			if (!isAppend)
			{
				Selection.activeObject = selectedObject;

				return;
			}

			var currentSelection = new List<Object>(Selection.objects);

			// Allow toggle selection
			if (currentSelection.Contains(selectedObject))
			{
				currentSelection.Remove(selectedObject);
			}
			else
			{
				currentSelection.Add(selectedObject);
			}

			Selection.objects = currentSelection.ToArray();
		}

		void SelectObjects(List<Object> selectedObjects, bool append)
		{
			if (!append)
			{
				Selection.objects = selectedObjects.ToArray();

				return;
			}

			var currentSelection = new List<Object>(Selection.objects);
			currentSelection.AddRange(selectedObjects);
			Selection.objects = currentSelection.ToArray();
		}

		void ListTextures()
		{
			using (var sGroup = new GUILayout.ScrollViewScope(
									this.textureListScrollPos,
									false,
									false,
									"horizontalScrollbar",
									"verticalScrollbar",
									"box",
									GUILayout.ExpandWidth(true),
									GUILayout.ExpandHeight(true)))
			{
				this.textureListScrollPos = sGroup.scrollPosition;
				var line = 0;

				foreach (var tDetails in this.ActiveTextures)
				{
					GUI.color = line % 2 == 0 ? Color.cyan : Color.white;

					using (var hGroup0 = new GUILayout.HorizontalScope("box", GUILayout.Height(this.ThumbnailHeight)))
					{
						GUI.color = Color.white;
						var tex = tDetails.texture;

						if ((tDetails.texture is Texture2DArray) || (tDetails.texture is Cubemap))
						{
							tex = AssetPreview.GetMiniThumbnail(tDetails.texture);
						}

						GUILayout.Box(
							tex,
							GUILayout.Width(this.ThumbnailWidth),
							GUILayout.Height(this.ThumbnailHeight));

						if (tDetails.instance)
						{
							GUI.color = new Color(0.8f, 0.8f, this.defColor.b, 1.0f);
						}

						if (tDetails.isgui)
						{
							GUI.color = new Color(this.defColor.r, 0.95f, 0.8f, 1.0f);
						}

						if (tDetails.isSky)
						{
							GUI.color = new Color(0.9f, this.defColor.g, this.defColor.b, 1.0f);
						}

						if (GUILayout.Button(
								this.ShortName(tDetails.texture.name, 14),
								GUILayout.Width(150),
								GUILayout.Height(ThumbnailHeight)))
						{
							this.SelectObject(tDetails.texture, this.ctrlPressed);
						}

						GUI.color = this.defColor;
						var sizeLabel = $"{tDetails.texture.width}x{tDetails.texture.height}";

						if (tDetails.isCubeMap)
						{
							sizeLabel += " x6";
						}

						if (tDetails.texture is Texture2DArray tex2d)
						{
							sizeLabel += $"[]\n {tex2d.depth} depths";
						}

						sizeLabel += $" - {tDetails.mipMapCount} mip\n {this.FormatSizeString(tDetails.memSizeKB)} - {tDetails.format}";
						EditorGUILayout.LabelField(
							new GUIContent(sizeLabel),
							GUILayout.Width(this.ThumbnailWidth * 4),
							GUILayout.Height(this.ThumbnailHeight));

						if (GUILayout.Button(
								$"{tDetails.FoundInMaterials.Count}\nMaterial(s)",
								GUILayout.Width(120),
								GUILayout.Height(this.ThumbnailHeight)))
						{
							this.SelectObjects(tDetails.FoundInMaterials, this.ctrlPressed);
						}

						var foundObjects = new HashSet<Object>();
						tDetails.FoundInRenderers.ForEach(f => foundObjects.Add((f as Component).gameObject));
						tDetails.FoundInAnimators.ForEach(f => foundObjects.Add((f as Component).gameObject));
						tDetails.FoundInGraphics.ForEach(f => foundObjects.Add((f as Component).gameObject));
						tDetails.FoundInButtons.ForEach(f => foundObjects.Add((f as Component).gameObject));
						tDetails.FoundInScripts.ForEach(f => foundObjects.Add((f as Component).gameObject));

						if (GUILayout.Button(
								$"Used by {foundObjects.Count}\nEscene Object(s)",
								GUILayout.Height(this.ThumbnailHeight)))
						{
							this.SelectObjects(new List<Object>(foundObjects), this.ctrlPressed);
						}
					}

					line++;
				}

				GUI.color = Color.white;

				if (this.ActiveTextures.Count > 0)
				{
					using (var hGroup = new GUILayout.HorizontalScope("box", GUILayout.ExpandWidth(true)))
					{
						if (GUILayout.Button("Select \n All", GUILayout.Width(this.ThumbnailWidth * 2)))
						{
							var allTextures = new List<Object>();
							this.ActiveTextures.ForEach(d => allTextures.Add(d.texture));

							this.SelectObjects(allTextures, ctrlPressed);
						}
					}
				}
			}
		}

		void ListMaterials()
		{
			using (var sGroup = new GUILayout.ScrollViewScope(
									this.materialListScrollPos,
									false,
									false,
									"horizontalScrollbar",
									"verticalScrollbar",
									"box",
									GUILayout.ExpandWidth(true),
									GUILayout.ExpandHeight(true)))
			{
				this.materialListScrollPos = sGroup.scrollPosition;
				var line = 0;

				foreach (var tDetails in this.ActiveMaterials)
				{
					GUI.color = line % 2 == 0 ? Color.cyan : Color.white;

					if (tDetails.material != null)
					{
						using (var hGroup = new GUILayout.HorizontalScope(
													"box",
													GUILayout.Height(this.ThumbnailHeight)))
						{
							GUI.color = Color.white;
							GUILayout.Box(
								AssetPreview.GetAssetPreview(tDetails.material),
								GUILayout.Width(this.ThumbnailWidth),
								GUILayout.Height(this.ThumbnailHeight));

							if (tDetails.instance)
							{
								GUI.color = new Color(0.8f, 0.8f, this.defColor.b, 1.0f);
							}

							if (tDetails.isgui)
							{
								GUI.color = new Color(this.defColor.r, 0.95f, 0.8f, 1.0f);
							}

							if (tDetails.isSky)
							{
								GUI.color = new Color(0.9f, this.defColor.g, this.defColor.b, 1.0f);
							}

							if (GUILayout.Button(
									this.ShortName(tDetails.material.name, 14),
									GUILayout.Width(150),
									GUILayout.Height(this.ThumbnailHeight)))
							{
								this.SelectObject(tDetails.material, this.ctrlPressed);
							}

							GUI.color = this.defColor;
							string shaderLabel = tDetails.material.shader != null ? tDetails.material.shader.name : "No Shader";
							EditorGUILayout.LabelField(new GUIContent(ShaderShortName(shaderLabel, 22)), GUILayout.Width(ThumbnailWidth * 4), GUILayout.Height(ThumbnailHeight));

							if (GUILayout.Button(
									$"In Renderer: {tDetails.FoundInRenderers.Count} / In Graphic: {tDetails.FoundInGraphics.Count}\nEscene Object(s)",
									GUILayout.Height(this.ThumbnailWidth)))
							{
								var foundObjects = new List<Object>();
								tDetails.FoundInRenderers.ForEach(f => foundObjects.Add(f.gameObject));
								tDetails.FoundInGraphics.ForEach(f => foundObjects.Add(f.gameObject));

								this.SelectObjects(foundObjects, this.ctrlPressed);
							}

							using (var vGroup = new GUILayout.VerticalScope(GUILayout.Width(this.ThumbnailWidth)))
							{
								EditorGUILayout.Space(8);
								var queue = tDetails.material.renderQueue;
								EditorGUI.BeginChangeCheck();
								queue = EditorGUILayout.DelayedIntField(queue, GUILayout.Width(40));
								EditorGUILayout.Space(8);

								if (EditorGUI.EndChangeCheck())
								{
									tDetails.material.renderQueue = queue;
									this.ActiveMaterials.Sort(MaterialSorter);
									GUIUtility.ExitGUI();
									hGroup.Dispose();

									break;
								}
							}
						}
					}

					line++;
				}
			}
		}

		void ListMeshes()
		{
			using (var sGroup = new GUILayout.ScrollViewScope(
									this.meshListScrollPos,
									false,
									false,
									"horizontalScrollbar",
									"verticalScrollbar",
									"box",
									GUILayout.ExpandWidth(true),
									GUILayout.ExpandHeight(true)))
			{
				this.meshListScrollPos = sGroup.scrollPosition;
				var line = 0;

				foreach (var tDetails in this.ActiveMeshDetails)
				{
					GUI.color = line % 2 == 0 ? Color.cyan : Color.white;

					if (tDetails.mesh != null)
					{
						using (var hGroup = new GUILayout.HorizontalScope("box"))
						{
							GUI.color = Color.white;
							string name = tDetails.mesh.name;

							if (name == null || !name.Any())
							{
								name = tDetails.FoundInMeshFilters[0].gameObject.name;
							}

							if (tDetails.instance)
							{
								GUI.color = new Color(0.8f, 0.8f, this.defColor.b, 1.0f);
							}

							if (GUILayout.Button(ShortName(name, 14), GUILayout.Width(150)))
							{
								this.SelectObject(tDetails.mesh, this.ctrlPressed);
							}

							GUI.color = this.defColor;
							var sizeLabel = $"{tDetails.mesh.vertexCount} Verts";
							GUILayout.Label(sizeLabel, GUILayout.Width(120));

							if (GUILayout.Button($"{tDetails.FoundInMeshFilters.Count} GO", GUILayout.Width(80)))
							{
								var foundObjects = new List<Object>();
								tDetails.FoundInMeshFilters.ForEach(f => foundObjects.Add(f.gameObject));

								this.SelectObjects(foundObjects, this.ctrlPressed);
							}

							if (tDetails.FoundInSkinnedMeshRenderer.Count > 0)
							{
								if (GUILayout.Button($"{tDetails.FoundInSkinnedMeshRenderer.Count} Skinned Mesh GO", GUILayout.Width(200)))
								{
									var foundObjects = new List<Object>();
									tDetails.FoundInSkinnedMeshRenderer.ForEach(f => foundObjects.Add(f.gameObject));

									this.SelectObjects(foundObjects, this.ctrlPressed);
								}
							}
							else
							{
								GUI.color = new Color(this.defColor.r, this.defColor.g, this.defColor.b, 0.5f);
								EditorGUILayout.LabelField(new GUIContent("0 Skinned Mesh"), GUILayout.Width(200));
								GUI.color = this.defColor;
							}

							if (tDetails.StaticBatchingEnabled.Count > 0)
							{
								if (GUILayout.Button($"{tDetails.StaticBatchingEnabled.Count} Static Batching", GUILayout.ExpandWidth(true)))
								{
									var foundObjects = new List<Object>();
									tDetails.StaticBatchingEnabled.ForEach(f => foundObjects.Add(f));

									this.SelectObjects(foundObjects, this.ctrlPressed);
								}
							}
							else
							{
								GUI.color = new Color(this.defColor.r, this.defColor.g, this.defColor.b, 0.5f);
								EditorGUILayout.LabelField(new GUIContent("0 Static Batching"), GUILayout.ExpandWidth(true));
								GUI.color = this.defColor;
							}
						}
					}

					line++;
				}
			}
		}

		void ListMissing()
		{
			this.missingListScrollPos = EditorGUILayout.BeginScrollView(this.missingListScrollPos);
			using (var sGroup = new GUILayout.ScrollViewScope(
									this.missingListScrollPos,
									false,
									false,
									"horizontalScrollbar",
									"verticalScrollbar",
									GUILayout.ExpandWidth(true),
									GUILayout.ExpandHeight(true)))
			{
				this.missingListScrollPos = sGroup.scrollPosition;
				var line = 0;

				foreach (MissingGraphic dMissing in MissingObjects)
				{
					GUI.color = line % 2 == 0 ? Color.cyan : Color.white;
					
					using (var hGroup = new GUILayout.HorizontalScope("box"))
					{
						GUI.color = Color.white;

						if (GUILayout.Button(this.ShortName(dMissing.name, 20), GUILayout.Width(200)))
						{
							this.SelectObject(dMissing.Object, this.ctrlPressed);
						}

						GUILayout.Label("Missing ", GUILayout.Width(60));

						switch (dMissing.type)
						{
							case "lod":
								{
									GUI.color = new Color(defColor.r, defColor.b, 0.8f, 1.0f);
									
									break;
								}
							case "mesh":
								{
									GUI.color = new Color(0.8f, 0.8f, this.defColor.b, 1.0f);

									break;
								}
							case "sprite":
								{
									GUI.color = new Color(this.defColor.r, 0.8f, 0.8f, 1.0f);
									
									break;
								}
							case "material":
								{
									GUI.color = new Color(0.8f, this.defColor.g, 0.8f, 1.0f);
									
									break;
								}
						}

						GUILayout.Label(dMissing.type);
						GUI.color = this.defColor;
					}

					line++;
				}
			}
		}
		#endregion

		#region Nested Types
		enum InspectType
		{
			Textures,

			Materials,

			Meshes,

			Missing
		}
		#endregion
	}

	public class MaterialDetails
	{
		#region Public Fields
		public bool instance;

		public bool isgui;

		public bool isSky;

		public List<Graphic> FoundInGraphics = new();

		public List<Renderer> FoundInRenderers = new();

		public Material material;
		#endregion

		#region Constructors
		public MaterialDetails()
		{
			instance = false;
			isgui = false;
			isSky = false;
		}
		#endregion
	}

	public class MeshDetails
	{
		#region Public Fields
		public bool instance;

		public List<GameObject> StaticBatchingEnabled = new();

		public List<MeshFilter> FoundInMeshFilters = new();

		public List<SkinnedMeshRenderer> FoundInSkinnedMeshRenderer = new();

		public Mesh mesh;
		#endregion

		#region Constructors
		public MeshDetails()
		{
			instance = false;
		}
		#endregion
	}

	public class MissingGraphic
	{
		#region Public Fields
		public string name;

		public string type;

		public Transform Object;
		#endregion
	}

	public class TextureDetails : System.IEquatable<TextureDetails>
	{
		#region Public Fields
		public bool instance;

		public bool isCubeMap;

		public bool isgui;

		public bool isSky;

		public int memSizeKB;

		public int mipMapCount;

		public List<Object> FoundInAnimators = new();

		public List<Object> FoundInButtons = new();

		public List<Object> FoundInGraphics = new();

		public List<Object> FoundInMaterials = new();

		public List<Object> FoundInRenderers = new();

		public List<Object> FoundInScripts = new();

		public Texture texture;

		public TextureFormat format;
		#endregion

		#region Constructors
		public TextureDetails()
		{
		}
		#endregion

		#region Public Methods
		public override bool Equals(object obj) => this.Equals(obj as TextureDetails);

		public bool Equals(TextureDetails other) =>
			this.texture != null
				&& other.texture != null
				&& this.texture.GetNativeTexturePtr() == other.texture.GetNativeTexturePtr();

		public override int GetHashCode() => (int)this.texture.GetNativeTexturePtr();
		#endregion
	}
}
