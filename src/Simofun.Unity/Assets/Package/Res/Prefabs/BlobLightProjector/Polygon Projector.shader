// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Projector' with 'unity_Projector'

// Upgrade NOTE: replaced '_Projector' with '_Projector'

// Upgrade NOTE: replaced '_Projector' with '_Projector'

// Upgrade NOTE: replaced '_Projector' with '_Projector'

Shader "Projector/Polygon Projector" {
   Properties {
      _Color ("Main Color", Color) = (1,0.5,0.5,1) 
      _ShadowTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	  _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	  _Alpha ("Alpha", Range(0,1)) = 0.5
   }
   SubShader {
   Tags { 
			"RenderType"="Opaque" 
			"Queue" = "Geometry+450"
	  }
      Pass {      
         Blend SrcAlpha OneMinusSrcAlpha 
		 ZWrite On
         Fog { Color (1, 1, 1) }
         AlphaTest Greater 0
         ColorMask RGB
		 Offset -1, -1
         CGPROGRAM

#pragma vertex vert
#pragma target 3.0
#pragma fragment frag
#pragma fragmentoption ARB_fog_exp2
#pragma fragmentoption ARB_precision_hint_fastest
#include "UnityCG.cginc"

struct v2f
{
    float4 pos : SV_POSITION;
    float4 uv_Main     : TEXCOORD0;
};

 

sampler2D _ShadowTex;
sampler2D _MainTex;
float4 _Color;
float4x4 unity_Projector;
float _Alpha;
float4 _MainTex_ST; 

v2f vert(appdata_tan v)
{
    v2f o;
    o.pos = UnityObjectToClipPos (v.vertex);
    o.uv_Main = mul (unity_Projector, v.vertex);
    return o;
}

 

half4 frag (v2f i) : COLOR
{
    float4 ua;
	ua = i.uv_Main;
	ua.x = ua.x * _MainTex_ST.x;
	ua.y = ua.y * _MainTex_ST.y;
	half4 mtex = tex2Dproj(_MainTex, UNITY_PROJ_COORD(ua));
	half4 otex = tex2Dproj(_MainTex, UNITY_PROJ_COORD(i.uv_Main));
    half4 tex = tex2Dproj(_ShadowTex, UNITY_PROJ_COORD(i.uv_Main));
		
	clip((mtex.a) > 0.05f ? 1 : -1);
	//_Color = tex;
	tex.a = tex.a * _Alpha;
	return tex * _Color;
	
}

ENDCG
      }
   }  
}