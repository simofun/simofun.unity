// Shader created with Shader Forge v1.38 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:3,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:2865,x:32902,y:32531,varname:node_2865,prsc:2|diff-1859-OUT,spec-358-OUT,gloss-1813-OUT,normal-5964-RGB;n:type:ShaderForge.SFN_Multiply,id:6343,x:32058,y:32595,varname:node_6343,prsc:2|A-7736-RGB,B-6665-RGB;n:type:ShaderForge.SFN_Color,id:6665,x:31845,y:32764,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Tex2d,id:7736,x:31878,y:32434,ptovrint:True,ptlb:Base Color,ptin:_MainTex,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:b66bceaf0cc0ace4e9bdc92f14bba709,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:5964,x:32083,y:32784,ptovrint:True,ptlb:Normal Map,ptin:_BumpMap,varname:_BumpMap,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Slider,id:358,x:32302,y:32949,ptovrint:False,ptlb:Metallic,ptin:_Metallic,varname:node_358,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:1813,x:32314,y:33066,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:_Metallic_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.8,max:1;n:type:ShaderForge.SFN_Tex2d,id:8028,x:31382,y:32507,ptovrint:False,ptlb:NoiseMap,ptin:_NoiseMap,varname:node_8028,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:acacf540130588a49a8ef9b305783d3a,ntxv:0,isnm:False|UVIN-8240-OUT;n:type:ShaderForge.SFN_Slider,id:4686,x:31092,y:32098,ptovrint:False,ptlb:BurnAmount,ptin:_BurnAmount,varname:node_4686,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Power,id:6593,x:32092,y:32143,varname:node_6593,prsc:2|VAL-5041-OUT,EXP-8028-RGB;n:type:ShaderForge.SFN_Subtract,id:4120,x:32268,y:32244,varname:node_4120,prsc:2|A-6593-OUT,B-6343-OUT;n:type:ShaderForge.SFN_Multiply,id:1859,x:32669,y:32485,varname:node_1859,prsc:2|A-4846-OUT,B-6343-OUT;n:type:ShaderForge.SFN_Step,id:5041,x:31883,y:32048,varname:node_5041,prsc:2|A-4686-OUT,B-7608-OUT;n:type:ShaderForge.SFN_Power,id:7608,x:31700,y:32157,varname:node_7608,prsc:2|VAL-8573-OUT,EXP-6653-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6653,x:31272,y:32230,ptovrint:False,ptlb:BurnNoisePower,ptin:_BurnNoisePower,varname:node_6653,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Add,id:4328,x:32465,y:32339,varname:node_4328,prsc:2|A-4120-OUT,B-6048-OUT;n:type:ShaderForge.SFN_Step,id:1677,x:31935,y:32263,varname:node_1677,prsc:2|A-4686-OUT,B-8028-G;n:type:ShaderForge.SFN_Multiply,id:6048,x:32282,y:32417,varname:node_6048,prsc:2|A-1677-OUT,B-6343-OUT;n:type:ShaderForge.SFN_Time,id:6841,x:31265,y:31910,varname:node_6841,prsc:2;n:type:ShaderForge.SFN_Fmod,id:1738,x:31700,y:31918,varname:node_1738,prsc:2|A-6841-TSL,B-97-OUT;n:type:ShaderForge.SFN_Vector1,id:97,x:31514,y:32009,varname:node_97,prsc:2,v1:0.5;n:type:ShaderForge.SFN_FragmentPosition,id:3187,x:30519,y:31846,varname:node_3187,prsc:2;n:type:ShaderForge.SFN_Append,id:137,x:30838,y:31809,varname:node_137,prsc:2|A-3187-Y,B-3187-Z;n:type:ShaderForge.SFN_Append,id:9877,x:30838,y:31962,varname:node_9877,prsc:2|A-3187-X,B-3187-Y;n:type:ShaderForge.SFN_Append,id:6092,x:30838,y:32108,varname:node_6092,prsc:2|A-3187-X,B-3187-Z;n:type:ShaderForge.SFN_NormalVector,id:7591,x:30526,y:31569,prsc:2,pt:False;n:type:ShaderForge.SFN_ComponentMask,id:9970,x:30764,y:31487,varname:node_9970,prsc:2,cc1:0,cc2:1,cc3:2,cc4:-1|IN-7591-OUT;n:type:ShaderForge.SFN_Abs,id:2878,x:31003,y:31343,varname:node_2878,prsc:2|IN-9970-R;n:type:ShaderForge.SFN_Abs,id:5307,x:31003,y:31487,varname:node_5307,prsc:2|IN-9970-B;n:type:ShaderForge.SFN_Abs,id:8991,x:31003,y:31638,varname:node_8991,prsc:2|IN-9970-G;n:type:ShaderForge.SFN_Vector1,id:8724,x:31003,y:31264,varname:node_8724,prsc:2,v1:0.5;n:type:ShaderForge.SFN_If,id:286,x:31293,y:31239,varname:node_286,prsc:2|A-2878-OUT,B-8724-OUT,GT-137-OUT,EQ-7429-OUT,LT-7429-OUT;n:type:ShaderForge.SFN_If,id:7429,x:31289,y:31395,varname:node_7429,prsc:2|A-5307-OUT,B-8724-OUT,GT-9877-OUT,EQ-8269-OUT,LT-8269-OUT;n:type:ShaderForge.SFN_If,id:8269,x:31312,y:31611,varname:node_8269,prsc:2|A-8991-OUT,B-8724-OUT,GT-6092-OUT,EQ-6092-OUT,LT-6092-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8692,x:31092,y:32330,ptovrint:False,ptlb:NoiseTile,ptin:_NoiseTile,varname:node_8692,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.15;n:type:ShaderForge.SFN_Multiply,id:8240,x:31204,y:32450,varname:node_8240,prsc:2|A-286-OUT,B-8692-OUT;n:type:ShaderForge.SFN_RemapRange,id:8573,x:31471,y:32269,varname:node_8573,prsc:2,frmn:0,frmx:1,tomn:0.05,tomx:1|IN-8028-R;n:type:ShaderForge.SFN_Tex2d,id:83,x:32424,y:32061,ptovrint:False,ptlb:LastFilter,ptin:_LastFilter,varname:node_83,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-8240-OUT;n:type:ShaderForge.SFN_Multiply,id:6333,x:32633,y:32206,varname:node_6333,prsc:2|A-83-RGB,B-4686-OUT;n:type:ShaderForge.SFN_Add,id:4846,x:32727,y:32343,varname:node_4846,prsc:2|A-6333-OUT,B-4328-OUT;proporder:5964-6665-7736-358-1813-8028-4686-6653-8692-83;pass:END;sub:END;*/

Shader "Shader Forge/BurnShader" {
    Properties {
        _BumpMap ("Normal Map", 2D) = "bump" {}
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Base Color", 2D) = "white" {}
        _Metallic ("Metallic", Range(0, 1)) = 0
        _Gloss ("Gloss", Range(0, 1)) = 0.8
        _NoiseMap ("NoiseMap", 2D) = "white" {}
        _BurnAmount ("BurnAmount", Range(0, 1)) = 0
        _BurnNoisePower ("BurnNoisePower", Float ) = 2
        _NoiseTile ("NoiseTile", Float ) = 0.15
        _LastFilter ("LastFilter", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            uniform float _Metallic;
            uniform float _Gloss;
            uniform sampler2D _NoiseMap; uniform float4 _NoiseMap_ST;
            uniform float _BurnAmount;
            uniform float _BurnNoisePower;
            uniform float _NoiseTile;
            uniform sampler2D _LastFilter; uniform float4 _LastFilter_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD10;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #elif UNITY_SHOULD_SAMPLE_SH
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(i.uv0, _BumpMap)));
                float3 normalLocal = _BumpMap_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                UNITY_LIGHT_ATTENUATION(attenuation,i, i.posWorld.xyz);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = _Gloss;
                float perceptualRoughness = 1.0 - _Gloss;
                float roughness = perceptualRoughness * perceptualRoughness;
                float specPow = exp2( gloss * 10.0 + 1.0 );
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
                #if UNITY_SPECCUBE_BLENDING || UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMin[0] = unity_SpecCube0_BoxMin;
                    d.boxMin[1] = unity_SpecCube1_BoxMin;
                #endif
                #if UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMax[0] = unity_SpecCube0_BoxMax;
                    d.boxMax[1] = unity_SpecCube1_BoxMax;
                    d.probePosition[0] = unity_SpecCube0_ProbePosition;
                    d.probePosition[1] = unity_SpecCube1_ProbePosition;
                #endif
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float LdotH = saturate(dot(lightDirection, halfDirection));
                float3 specularColor = _Metallic;
                float specularMonochrome;
                float3 node_9970 = i.normalDir.rgb;
                float node_8724 = 0.5;
                float node_286_if_leA = step(abs(node_9970.r),node_8724);
                float node_286_if_leB = step(node_8724,abs(node_9970.r));
                float node_7429_if_leA = step(abs(node_9970.b),node_8724);
                float node_7429_if_leB = step(node_8724,abs(node_9970.b));
                float node_8269_if_leA = step(abs(node_9970.g),node_8724);
                float node_8269_if_leB = step(node_8724,abs(node_9970.g));
                float2 node_6092 = float2(i.posWorld.r,i.posWorld.b);
                float2 node_8269 = lerp((node_8269_if_leA*node_6092)+(node_8269_if_leB*node_6092),node_6092,node_8269_if_leA*node_8269_if_leB);
                float2 node_7429 = lerp((node_7429_if_leA*node_8269)+(node_7429_if_leB*float2(i.posWorld.r,i.posWorld.g)),node_8269,node_7429_if_leA*node_7429_if_leB);
                float2 node_8240 = (lerp((node_286_if_leA*node_7429)+(node_286_if_leB*float2(i.posWorld.g,i.posWorld.b)),node_7429,node_286_if_leA*node_286_if_leB)*_NoiseTile);
                float4 _LastFilter_var = tex2D(_LastFilter,TRANSFORM_TEX(node_8240, _LastFilter));
                float4 _NoiseMap_var = tex2D(_NoiseMap,TRANSFORM_TEX(node_8240, _NoiseMap));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 node_6343 = (_MainTex_var.rgb*_Color.rgb);
                float3 diffuseColor = (((_LastFilter_var.rgb*_BurnAmount)+((pow(step(_BurnAmount,pow((_NoiseMap_var.r*0.95+0.05),_BurnNoisePower)),_NoiseMap_var.rgb)-node_6343)+(step(_BurnAmount,_NoiseMap_var.g)*node_6343)))*node_6343); // Need this for specular when using metallic
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, specularColor, specularColor, specularMonochrome );
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = abs(dot( normalDirection, viewDirection ));
                float NdotH = saturate(dot( normalDirection, halfDirection ));
                float VdotH = saturate(dot( viewDirection, halfDirection ));
                float visTerm = SmithJointGGXVisibilityTerm( NdotL, NdotV, roughness );
                float normTerm = GGXTerm(NdotH, roughness);
                float specularPBL = (visTerm*normTerm) * UNITY_PI;
                #ifdef UNITY_COLORSPACE_GAMMA
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                #endif
                specularPBL = max(0, specularPBL * NdotL);
                #if defined(_SPECULARHIGHLIGHTS_OFF)
                    specularPBL = 0.0;
                #endif
                half surfaceReduction;
                #ifdef UNITY_COLORSPACE_GAMMA
                    surfaceReduction = 1.0-0.28*roughness*perceptualRoughness;
                #else
                    surfaceReduction = 1.0/(roughness*roughness + 1.0);
                #endif
                specularPBL *= any(specularColor) ? 1.0 : 0.0;
                float3 directSpecular = attenColor*specularPBL*FresnelTerm(specularColor, LdotH);
                half grazingTerm = saturate( gloss + specularMonochrome );
                float3 indirectSpecular = (gi.indirect.specular);
                indirectSpecular *= FresnelLerp (specularColor, grazingTerm, NdotV);
                indirectSpecular *= surfaceReduction;
                float3 specular = (directSpecular + indirectSpecular);
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float nlPow5 = Pow5(1-NdotL);
                float nvPow5 = Pow5(1-NdotV);
                float3 directDiffuse = ((1 +(fd90 - 1)*nlPow5) * (1 + (fd90 - 1)*nvPow5) * NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += gi.indirect.diffuse;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            uniform float _Metallic;
            uniform float _Gloss;
            uniform sampler2D _NoiseMap; uniform float4 _NoiseMap_ST;
            uniform float _BurnAmount;
            uniform float _BurnNoisePower;
            uniform float _NoiseTile;
            uniform sampler2D _LastFilter; uniform float4 _LastFilter_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(i.uv0, _BumpMap)));
                float3 normalLocal = _BumpMap_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                UNITY_LIGHT_ATTENUATION(attenuation,i, i.posWorld.xyz);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = _Gloss;
                float perceptualRoughness = 1.0 - _Gloss;
                float roughness = perceptualRoughness * perceptualRoughness;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float LdotH = saturate(dot(lightDirection, halfDirection));
                float3 specularColor = _Metallic;
                float specularMonochrome;
                float3 node_9970 = i.normalDir.rgb;
                float node_8724 = 0.5;
                float node_286_if_leA = step(abs(node_9970.r),node_8724);
                float node_286_if_leB = step(node_8724,abs(node_9970.r));
                float node_7429_if_leA = step(abs(node_9970.b),node_8724);
                float node_7429_if_leB = step(node_8724,abs(node_9970.b));
                float node_8269_if_leA = step(abs(node_9970.g),node_8724);
                float node_8269_if_leB = step(node_8724,abs(node_9970.g));
                float2 node_6092 = float2(i.posWorld.r,i.posWorld.b);
                float2 node_8269 = lerp((node_8269_if_leA*node_6092)+(node_8269_if_leB*node_6092),node_6092,node_8269_if_leA*node_8269_if_leB);
                float2 node_7429 = lerp((node_7429_if_leA*node_8269)+(node_7429_if_leB*float2(i.posWorld.r,i.posWorld.g)),node_8269,node_7429_if_leA*node_7429_if_leB);
                float2 node_8240 = (lerp((node_286_if_leA*node_7429)+(node_286_if_leB*float2(i.posWorld.g,i.posWorld.b)),node_7429,node_286_if_leA*node_286_if_leB)*_NoiseTile);
                float4 _LastFilter_var = tex2D(_LastFilter,TRANSFORM_TEX(node_8240, _LastFilter));
                float4 _NoiseMap_var = tex2D(_NoiseMap,TRANSFORM_TEX(node_8240, _NoiseMap));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 node_6343 = (_MainTex_var.rgb*_Color.rgb);
                float3 diffuseColor = (((_LastFilter_var.rgb*_BurnAmount)+((pow(step(_BurnAmount,pow((_NoiseMap_var.r*0.95+0.05),_BurnNoisePower)),_NoiseMap_var.rgb)-node_6343)+(step(_BurnAmount,_NoiseMap_var.g)*node_6343)))*node_6343); // Need this for specular when using metallic
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, specularColor, specularColor, specularMonochrome );
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = abs(dot( normalDirection, viewDirection ));
                float NdotH = saturate(dot( normalDirection, halfDirection ));
                float VdotH = saturate(dot( viewDirection, halfDirection ));
                float visTerm = SmithJointGGXVisibilityTerm( NdotL, NdotV, roughness );
                float normTerm = GGXTerm(NdotH, roughness);
                float specularPBL = (visTerm*normTerm) * UNITY_PI;
                #ifdef UNITY_COLORSPACE_GAMMA
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                #endif
                specularPBL = max(0, specularPBL * NdotL);
                #if defined(_SPECULARHIGHLIGHTS_OFF)
                    specularPBL = 0.0;
                #endif
                specularPBL *= any(specularColor) ? 1.0 : 0.0;
                float3 directSpecular = attenColor*specularPBL*FresnelTerm(specularColor, LdotH);
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float nlPow5 = Pow5(1-NdotL);
                float nvPow5 = Pow5(1-NdotV);
                float3 directDiffuse = ((1 +(fd90 - 1)*nlPow5) * (1 + (fd90 - 1)*nvPow5) * NdotL) * attenColor;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Metallic;
            uniform float _Gloss;
            uniform sampler2D _NoiseMap; uniform float4 _NoiseMap_ST;
            uniform float _BurnAmount;
            uniform float _BurnNoisePower;
            uniform float _NoiseTile;
            uniform sampler2D _LastFilter; uniform float4 _LastFilter_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                o.Emission = 0;
                
                float3 node_9970 = i.normalDir.rgb;
                float node_8724 = 0.5;
                float node_286_if_leA = step(abs(node_9970.r),node_8724);
                float node_286_if_leB = step(node_8724,abs(node_9970.r));
                float node_7429_if_leA = step(abs(node_9970.b),node_8724);
                float node_7429_if_leB = step(node_8724,abs(node_9970.b));
                float node_8269_if_leA = step(abs(node_9970.g),node_8724);
                float node_8269_if_leB = step(node_8724,abs(node_9970.g));
                float2 node_6092 = float2(i.posWorld.r,i.posWorld.b);
                float2 node_8269 = lerp((node_8269_if_leA*node_6092)+(node_8269_if_leB*node_6092),node_6092,node_8269_if_leA*node_8269_if_leB);
                float2 node_7429 = lerp((node_7429_if_leA*node_8269)+(node_7429_if_leB*float2(i.posWorld.r,i.posWorld.g)),node_8269,node_7429_if_leA*node_7429_if_leB);
                float2 node_8240 = (lerp((node_286_if_leA*node_7429)+(node_286_if_leB*float2(i.posWorld.g,i.posWorld.b)),node_7429,node_286_if_leA*node_286_if_leB)*_NoiseTile);
                float4 _LastFilter_var = tex2D(_LastFilter,TRANSFORM_TEX(node_8240, _LastFilter));
                float4 _NoiseMap_var = tex2D(_NoiseMap,TRANSFORM_TEX(node_8240, _NoiseMap));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 node_6343 = (_MainTex_var.rgb*_Color.rgb);
                float3 diffColor = (((_LastFilter_var.rgb*_BurnAmount)+((pow(step(_BurnAmount,pow((_NoiseMap_var.r*0.95+0.05),_BurnNoisePower)),_NoiseMap_var.rgb)-node_6343)+(step(_BurnAmount,_NoiseMap_var.g)*node_6343)))*node_6343);
                float specularMonochrome;
                float3 specColor;
                diffColor = DiffuseAndSpecularFromMetallic( diffColor, _Metallic, specColor, specularMonochrome );
                float roughness = 1.0 - _Gloss;
                o.Albedo = diffColor + specColor * roughness * roughness * 0.5;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
