#if SIM_ANIMANCER
//-----------------------------------------------------------------------
// <copyright file="SimSimpleAnimancer.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Omer Furkan Demir</author>
//-----------------------------------------------------------------------

using Animancer;

namespace Simofun.Animancer.Unity
{
	using Sirenix.OdinInspector;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;

	public class SimSimpleAnimancer : MonoBehaviour
	{
		#region Unity Properties
		[field: SerializeField, Title(nameof(SimSimpleAnimancer), "Settings")]
		public AnimancerComponent Animancer { get; protected set; }

		[field: SerializeField]
		protected float FadeDuration { get; set; } = 0.1f;

		[field: SerializeField, Space]
		protected List<AnimationClip> Clips { get; set; } = new();

		[field: SerializeField]
		protected bool IsPlayOnAwake { get; set; }

		[field: SerializeField]
		protected bool IsPlayOnStart { get; set; }
		#endregion

		#region Properties
		public AnimancerState CurrentState { get; protected set; }
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Awake()
		{
			if (this.IsPlayOnAwake)
			{
				this.PlayOnStartup();
			}
		}

		/// <inheritdoc />
		protected virtual void Start()
		{
			if (!this.IsPlayOnAwake && this.IsPlayOnStart)
			{
				this.PlayOnStartup();
			}
		}
		#endregion

		#region Public Methods
		public void PlayAnimation(AnimationClip clip) =>
			this.CurrentState = this.Animancer.Play(clip, this.FadeDuration);

		public void PlayAnimation(string clipName)
		{
			var clip = this.GetAnimationClipByName(clipName);
			if (clip == null)
			{
				return;
			}

			this.Animancer.Stop();
			this.CurrentState = this.Animancer.Play(clip, this.FadeDuration);
		}

		public void PlayMixer(LinearMixerTransition transition) =>
			this.CurrentState = this.Animancer.Play(transition, this.FadeDuration);

		public void PlayMixer(LinearMixerTransition transition, float speed)
		{
			this.CurrentState = this.Animancer.Play(transition, this.FadeDuration);
			this.CurrentState.Speed = speed;
		}
		#endregion

		#region Methods
		AnimationClip GetAnimationClipByName(string clipName) =>
			this.Clips.FirstOrDefault(c => c.name.Equals(clipName));

		void PlayOnStartup() => this.PlayAnimation(this.Clips[0].name);
		#endregion
	}
}
#endif
