//-----------------------------------------------------------------------
// <copyright file="SimUniTaskUtil.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.CysharpUniTask.Unity.Utils
{
	using Cysharp.Threading.Tasks;
	using System.Threading;

	/// <summary>
	/// Utility class for <see cref="UniTask"/>.
	/// </summary>
	public static class SimUniTaskUtil
	{
		#region Public Methods
		public static bool ExecuteOrDelay(
			System.Action execute,
			float seconds,
			PlayerLoopTiming timing = PlayerLoopTiming.Update,
			CancellationToken cancellationToken = default)
		{
			if (seconds <= 0)
			{
				execute();

				return true;
			}

			UniTask.Create(async () =>
			{
				await UniTask.Delay(
					System.TimeSpan.FromSeconds(seconds), delayTiming: timing, cancellationToken: cancellationToken);

				execute();
			});

			return false;
		}

		public static bool ExecuteOrDelay(
			System.Action execute,
			System.TimeSpan timeSpan,
			PlayerLoopTiming timing = PlayerLoopTiming.Update,
			CancellationToken cancellationToken = default)
		{
			if (timeSpan.TotalMilliseconds <= 0)
			{
				execute();

				return true;
			}

			UniTask.Create(async () =>
			{
				await UniTask.Delay(timeSpan, delayTiming: timing, cancellationToken: cancellationToken);

				execute();
			});

			return false;
		}

		public static bool ExecuteOrWaitUntil(
			System.Func<bool> predicate,
			System.Action execute,
			PlayerLoopTiming timing = PlayerLoopTiming.Update,
			CancellationToken cancellationToken = default)
		{
			if (predicate())
			{
				execute();

				return true;
			}

			UniTask.Create(async () =>
			{
				await UniTask.WaitUntil(predicate, timing, cancellationToken);

				execute();
			});

			return false;
		}
		#endregion
	}
}
