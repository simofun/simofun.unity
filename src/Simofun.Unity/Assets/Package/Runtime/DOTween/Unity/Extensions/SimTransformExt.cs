﻿//-----------------------------------------------------------------------
// <copyright file="SimTransformExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.DOTween.Unity.Extensions
{
	using DG.Tweening;
	using UnityEngine;

	public static class SimTransformExt
	{
		public static Tweener DoPunchScale(
			this Transform src,
			System.Action onCompleteAction = null,
			float duration = 0.25f,
			float scaleMultiplier = 0.1f,
			Ease ease = Ease.InOutBounce)
		{
			var localScale = src.localScale;

			var tween = src.DOPunchScale(src.localScale * scaleMultiplier, duration).SetEase(ease);
			tween.onComplete += () =>
			{
				src.localScale = localScale;
				onCompleteAction?.Invoke();
			};

			return tween;
		}
	}
}
