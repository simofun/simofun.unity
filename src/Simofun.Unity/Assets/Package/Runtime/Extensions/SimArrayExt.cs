//-----------------------------------------------------------------------
// <copyright file="SimArrayExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tonguc</author>
//-----------------------------------------------------------------------

namespace Simofun.Extensions
{
	using System.Collections.Generic;
	using UnityEngine;

	public static class SimArrayExt
	{
		public static T GetRandom<T>(this T[] array) => array[Random.Range(0, array.Length)];

		public static T GetRandom<T>(this IReadOnlyList<T> list) => list[Random.Range(0, list.Count)];
	}
}
