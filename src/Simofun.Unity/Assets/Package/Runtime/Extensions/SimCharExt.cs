﻿//-----------------------------------------------------------------------
// <copyright file="SimCharExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Extensions
{
	/// <summary>
	/// Extensions for <see cref="char"/> class.
	/// </summary>
	public static class SimCharExt
	{
		public static bool IsNotWhitespace(this char source) => source != ' ';

		public static bool IsWhitespace(this char source) => source == ' ';
	}
}
