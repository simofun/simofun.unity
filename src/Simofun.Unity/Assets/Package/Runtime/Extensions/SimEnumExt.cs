﻿//-----------------------------------------------------------------------
// <copyright file="SimEnumExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Extensions
{
	using System;
	using System.Linq;

	/// <summary>
	/// Extensions for <see cref="Enum"/> class.
	/// </summary>
	public static class SimEnumExt
	{
		public static bool HasAnyFlag(this Enum source, params Enum[] flags)
		{
			foreach (var flag in flags)
			{
				if (source.HasFlag(flag))
				{
					return true;
				}
			}

			return true;
		}

		public static bool HasFlags(this Enum source, params Enum[] flags)
		{
			foreach (var flag in flags)
			{
				if (!source.HasFlag(flag))
				{
					return false;
				}
			}

			return true;
		}

		public static TAttribute GetAttribute<TAttribute>(this Enum src) where TAttribute : Attribute
		{
			var type = src.GetType();

			return type.GetField(Enum.GetName(type, src))
				.GetCustomAttributes(false)
				.OfType<TAttribute>()
				.SingleOrDefault();
		}
	}
}
