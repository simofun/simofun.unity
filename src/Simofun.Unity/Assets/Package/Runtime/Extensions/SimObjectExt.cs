﻿//-----------------------------------------------------------------------
// <copyright file="SimObjectExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Extensions
{
	/// <summary>
	/// Extensions for <see cref="object"/> class.
	/// </summary>
	public static class SimObjectExt
	{
		/// <summary>
		/// <paramref name="obj"/>
		///		is a
		///		<typeparamref name="T"/>.
		/// </summary>
		/// <typeparam name="T">Type to detect
		///		<paramref name="obj"/>
		///		is a
		///		<typeparamref name="T"/>
		///		.
		///	</typeparam>
		/// <param name="obj">Object.</param>
		/// <returns>Returns if the object is
		///		<typeparamref name="T"/>
		///		.
		///	</returns>
		public static bool Is<T>(this object obj)
		{
			return obj is T;
		}

		/// <summary>
		/// <paramref name="obj"/>
		///		is not a
		///		<typeparamref name="T"/>.
		/// </summary>
		/// <typeparam name="T">Type to detect
		///		<paramref name="obj"/>
		///		is not a
		///		<typeparamref name="T"/>
		///		.
		///	</typeparam>
		/// <param name="obj">Object.</param>
		/// <returns>Returns if the object is
		///		<typeparamref name="T"/>
		///		.
		///	</returns>
		public static bool IsNot<T>(this object obj)
		{
			return !(obj is T);
		}
	}
}
