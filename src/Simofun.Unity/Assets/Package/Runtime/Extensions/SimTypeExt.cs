﻿//-----------------------------------------------------------------------
// <copyright file="SimTypeExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Extensions
{
	using System;
	using System.Collections.Generic;
	using System.Linq;

	/// <summary>
	/// Extensions for <see cref="Type"/> class.
	/// </summary>
	public static class SimTypeExt
	{
		public static bool IsTypeOrSubclassOf(this Type src, Type type) => src == type || src.IsSubclassOf(type);

		public static IEnumerable<string> GetFieldNames(this Type type) => type.GetFields().Select(f => f.Name);

		public static IEnumerable<Type> GetConcreteTypes(this Type type) =>
			type.Assembly.GetTypes().Where(x => !x.IsAbstract);

		public static IEnumerable<Type> GetConcreteTypesSubclassOf(this Type type) =>
			type.Assembly.GetTypes().Where(x => !x.IsAbstract && x.IsSubclassOf(type));
	}
}
