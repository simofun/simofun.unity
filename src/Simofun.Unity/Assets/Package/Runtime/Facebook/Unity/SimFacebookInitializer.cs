#if SIM_FACEBOOK
//-----------------------------------------------------------------------
// <copyright file="SimFacebookInitializer.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

using Facebook.Unity;

namespace Simofun.Facebook.Unity
{
	using Simofun.Unity.DesignPatterns.Singleton;
	using UnityEngine;

	public class SimFacebookInitializer : SimGlobalSingletonBase<SimFacebookInitializer>
	{
		#region Unity Methods
		/// <inheritdoc />
		protected override void Awake()
		{
			base.Awake();

			if (!FB.IsInitialized)
			{
				FB.Init(this.HandleOnFbInitCallback); // Initialize the Facebook SDK

				return;
			}
			
			FB.ActivateApp(); // Already initialized, signal an app activation App Event
		}

		/// <inheritdoc />
		protected virtual void OnApplicationPause(bool paused)
		{
			if (paused)
			{
				return;
			}

			if (FB.IsInitialized)
			{
				FB.ActivateApp();
			}
		}
		#endregion

		#region Methods
		#region Event Handlers
		void HandleOnFbInitCallback()
		{
			if (FB.IsInitialized)
			{
				FB.ActivateApp();
			}
			else
			{
				Debug.LogError("Failed to Initialize the Facebook SDK");
			}

#if UNITY_ANDROID || UNITY_IOS
			FB.Mobile.SetAutoLogAppEventsEnabled(true);
#endif

#if UNITY_IOS
			this.Invoke(nameof(this.SkAdNetworkRegisterAppForNetworkAttribution), 1);
#endif
		}
		#endregion

#if UNITY_IOS
		void SkAdNetworkRegisterAppForNetworkAttribution()
		{
			if (Application.platform == RuntimePlatform.IPhonePlayer)
			{
				// Make sure you call 'SkAdNetworkRegisterAppForNetworkAttribution()' AFTER initializing FB.
				global::Unity.Advertisement.IosSupport.SkAdNetworkBinding.SkAdNetworkRegisterAppForNetworkAttribution();
			}
		}
#endif
		#endregion
	}
}
#endif
