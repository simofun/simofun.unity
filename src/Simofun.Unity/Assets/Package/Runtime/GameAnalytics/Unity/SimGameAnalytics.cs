﻿#if SIM_GAME_ANALYTICS
//-----------------------------------------------------------------------
// <copyright file="SimGameAnalytics.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.GameAnalytics.Unity
{
	using GameAnalyticsSDK;
	using Simofun.Unity.Analytics;
	using Simofun.Unity.Analytics.Model;
	using GA = GameAnalyticsSDK.GameAnalytics;

	public class SimGameAnalytics : SimAnalyticsBase<ISimAnalytics, SimGameAnalytics>
	{
		#region Properties
		#region ISimAnalytics
		/// <inheritdoc />
		public override string UserId
		{
			get => base.UserId;
			
			set
			{
				base.UserId = value;
				GA.SetCustomId(value);
			}
		}
		#endregion
		#endregion

		#region Public Methods
		#region ISimGameAnalytics
		#region Initialization
		/// <inheritdoc />
		public override void Initialize()
		{
			GA.Initialize();

			this.IsInitialized = true;
		}
		#endregion

		#region Timer
		/// <inheritdoc />
		public override void PauseTimer(string key) => GA.PauseTimer(key);

		/// <inheritdoc />
		public override void ResumeTimer(string key) => GA.ResumeTimer(key);

		/// <inheritdoc />
		public override void StartTimer(string key) => GA.StartTimer(key);

		/// <inheritdoc />
		public override long StopTimer(string key) => GA.StopTimer(key);
		#endregion

		/// <inheritdoc />
		public override void NewAdEvent(
			SimAdAction adAction, SimAdType adType, string adProviderName, string adPlacement)
			=> GA.NewAdEvent(ConvertAdActionToGA(adAction), ConvertAdTypeToGA(adType), adProviderName, adPlacement);

		/// <inheritdoc />
		public override void NewAdEvent(
			SimAdAction adAction, SimAdType adType, string adProviderName, string adPlacement, long duration)
			=> GA.NewAdEvent(
					ConvertAdActionToGA(adAction), ConvertAdTypeToGA(adType), adProviderName, adPlacement, duration);

		/// <inheritdoc />
		public override void NewAdEvent(
			SimAdAction adAction, SimAdType adType, string adProviderName, string adPlacement, SimAdError noAdReason)
			=> GA.NewAdEvent(
					ConvertAdActionToGA(adAction),
					ConvertAdTypeToGA(adType),
					adProviderName,
					adPlacement,
					ConvertAdErrorToGA(noAdReason));

		/// <inheritdoc />
		public override void NewDesignEvent(string eventName) => GA.NewDesignEvent(eventName);

		/// <inheritdoc />
		public override void NewDesignEvent(string eventName, float eventValue) => GA.NewDesignEvent(eventName, eventValue);

		/// <inheritdoc />
		public override void NewProgressionEvent(SimAnalyticsProgressionStatus progressionStatus, string progression)
			=> GA.NewProgressionEvent(ConvertProgressionStatusToGA(progressionStatus), progression);

		/// <inheritdoc />
		public override void NewProgressionEvent(
			SimAnalyticsProgressionStatus progressionStatus, string progression, int score) =>
				GA.NewProgressionEvent(ConvertProgressionStatusToGA(progressionStatus), progression, score);
		#endregion
		#endregion

		#region Static Methods
		static GAAdAction ConvertAdActionToGA(SimAdAction adAction)
		{
			switch (adAction)
			{
				case SimAdAction.Undefined:
					{
						return GAAdAction.Undefined;
					}
				case SimAdAction.Loaded:
					{
						return GAAdAction.Loaded;
					}
				case SimAdAction.Show:
					{
						return GAAdAction.Show;
					}
				case SimAdAction.Error:
					{
						return GAAdAction.FailedShow;
					}
				case SimAdAction.Clicked:
					{
						return GAAdAction.Clicked;
					}
				case SimAdAction.Request:
					{
						return GAAdAction.Request;
					}
				case SimAdAction.RewardReceived:
					{
						return GAAdAction.RewardReceived;
					}
				default:
					{
						return GAAdAction.Undefined;
					}
			}
		}

		static GAAdError ConvertAdErrorToGA(SimAdError adNoReason)
		{
			switch (adNoReason)
			{
				case SimAdError.Undefined:
					{
						return GAAdError.Undefined;
					}
				case SimAdError.Unknown:
					{
						return GAAdError.Unknown;
					}
				case SimAdError.Offline:
					{
						return GAAdError.Offline;
					}
				case SimAdError.NoFill:
					{
						return GAAdError.NoFill;
					}
				case SimAdError.InternalError:
					{
						return GAAdError.InternalError;
					}
				case SimAdError.InvalidRequest:
					{
						return GAAdError.InvalidRequest;
					}
				case SimAdError.UnableToPrecache:
					{
						return GAAdError.UnableToPrecache;
					}
				default:
					{
						return GAAdError.Undefined;
					}
			}
		}

		static GAAdType ConvertAdTypeToGA(SimAdType adType)
		{
			switch (adType)
			{
				case SimAdType.Undefined:
					{
						return GAAdType.Undefined;
					}
				case SimAdType.Video:
					{
						return GAAdType.Video;
					}
				case SimAdType.RewardedVideo:
					{
						return GAAdType.RewardedVideo;
					}
				case SimAdType.Interstitial:
					{
						return GAAdType.Interstitial;
					}
				case SimAdType.Banner:
					{
						return GAAdType.Banner;
					}
				default:
					{
						return GAAdType.Undefined;
					}
			}
		}

		static GAProgressionStatus ConvertProgressionStatusToGA(SimAnalyticsProgressionStatus progressionStatus)
		{
			switch (progressionStatus)
			{
				case SimAnalyticsProgressionStatus.Undefined:
					{
						return GAProgressionStatus.Undefined;
					}
				case SimAnalyticsProgressionStatus.Start:
					{
						return GAProgressionStatus.Start;
					}
				case SimAnalyticsProgressionStatus.Success:
					{
						return GAProgressionStatus.Complete;
					}
				case SimAnalyticsProgressionStatus.Fail:
					{
						return GAProgressionStatus.Fail;
					}
				default:
					{
						return GAProgressionStatus.Undefined;
					}
			}
		}
		#endregion
	}
}
#endif
