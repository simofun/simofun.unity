//-----------------------------------------------------------------------
// <copyright file="ISimRootPath.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.IO
{
	public interface ISimRootPath
	{
		string RootPath { get; set; }
	}
}
