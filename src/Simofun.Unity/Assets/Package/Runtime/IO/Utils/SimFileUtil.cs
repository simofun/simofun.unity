//-----------------------------------------------------------------------
// <copyright file="SimFileUtil.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Utils
{
	using System.IO;

	/// <summary>
	/// Utility class for <see cref="File"/>.
	/// </summary>
	public static class SimFileUtil
	{
		#region Public Methods
		public static bool DeleteFileIfExist(string path)
		{
			if (!File.Exists(path))
			{
				return false;
			}
			
			File.Delete(path);

			return true;
		}

		public static bool DeleteFilesIfExist(params string[] paths)
		{
			var isAllDeleted = true;
			foreach (var path in paths)
			{
				if (!DeleteFileIfExist(path))
				{
					isAllDeleted = false;
				}
			}

			return isAllDeleted;
		}
		#endregion
	}
}
