﻿//-----------------------------------------------------------------------
// <copyright file="ISimConstructible.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun
{
	public interface ISimConstructible
	{
		#region Methods
		void Construct(params object[] deps);
		#endregion
	}
}
