﻿//-----------------------------------------------------------------------
// <copyright file="ISimDestroyable.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun
{
	public interface ISimDestroyable
	{
		#region Properties				
		/// <summary>
		/// Gets or sets a value indicating whether this instance is destroyable.
		/// </summary>
		/// <value>
		///   <c>true</c> if this instance is destroyable; otherwise, <c>false</c>.
		/// </value>
		bool IsDestroyable { get; set; }
		#endregion
	}
}
