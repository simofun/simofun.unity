﻿//-----------------------------------------------------------------------
// <copyright file="ISimInitializable.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun
{
	public interface ISimInitializable
	{
		#region Properties
		/// <summary>
		/// Gets or sets a value indicating whether this instance is automatic initialized.
		/// </summary>
		/// <value>
		///   <c>true</c> if this instance is automatic initialized; otherwise, <c>false</c>.
		/// </value>
		bool IsAutoInitialized { get; set; }

		/// <summary>
		/// Gets a value indicating whether this instance is initialized.
		/// </summary>
		/// <value>
		///   <c>true</c> if this instance is initialized; otherwise, <c>false</c>.
		/// </value>
		bool IsInitialized { get; }
		#endregion

		#region Methods
		/// <summary>
		/// Initializes this instance. Set <see cref="IsInitialized"/> as true after initialization.
		/// </summary>
		void Initialize();
		#endregion
	}
}
