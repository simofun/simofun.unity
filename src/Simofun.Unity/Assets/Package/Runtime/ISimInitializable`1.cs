﻿//-----------------------------------------------------------------------
// <copyright file="ISimInitializable`1.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun
{
    public interface ISimInitializable<in T>
    {
        #region Properties
        bool IsInitialized { get; }
        #endregion

        #region Methods
        void Initialize(T value);
        #endregion
    }
}
