﻿//-----------------------------------------------------------------------
// <copyright file="ISimNamable.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun
{
	public interface ISimNamable
	{
		#region Properties		
		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		string Name { get; set; }
		#endregion
	}
}
