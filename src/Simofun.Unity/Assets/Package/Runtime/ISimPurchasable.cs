﻿//-----------------------------------------------------------------------
// <copyright file="ISimPurchasable.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun
{
	public interface ISimPurchasable
	{
		bool IsPurchasable { get; set; }
	}
}
