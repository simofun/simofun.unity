﻿//-----------------------------------------------------------------------
// <copyright file="ISimUnlockable.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun
{
	public interface ISimUnlockable
	{
		bool IsUnlocked { get; set; }
	}
}
