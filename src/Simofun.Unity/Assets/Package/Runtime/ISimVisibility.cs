﻿//-----------------------------------------------------------------------
// <copyright file="ISimVisibility.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun
{
	public interface ISimVisibility : ISimVisible
	{
		#region Methods		
		void Hide();

		void Show();
		#endregion
	}
}
