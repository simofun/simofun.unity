﻿//-----------------------------------------------------------------------
// <copyright file="ISimVisible.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun
{
	public interface ISimVisible
	{
		#region Properties		
		/// <summary>
		/// Gets a value indicating whether the visibility state.
		/// </summary>
		bool IsVisible { get; set; }
		#endregion
	}
}
