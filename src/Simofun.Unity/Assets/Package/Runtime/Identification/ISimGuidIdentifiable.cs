﻿//-----------------------------------------------------------------------
// <copyright file="ISimGuidIdentifiable.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Identification
{
	using System;

	public interface ISimGuidIdentifiable : ISimIdentifiable<Guid>
	{
	}
}
