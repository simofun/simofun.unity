﻿//-----------------------------------------------------------------------
// <copyright file="ISimIdentifiable<T>.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Identification
{
	public interface ISimIdentifiable<T>
	{
		T Id { get; set; }
	}
}
