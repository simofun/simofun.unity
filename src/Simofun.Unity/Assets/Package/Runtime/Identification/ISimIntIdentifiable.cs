﻿//-----------------------------------------------------------------------
// <copyright file="ISimIntIdentifiable.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Identification
{
	public interface ISimIntIdentifiable : ISimIdentifiable<int>
	{
	}
}
