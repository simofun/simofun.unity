//-----------------------------------------------------------------------
// <copyright file="SimUnityConverterInitializer.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

[assembly: UnityEngine.Scripting.AlwaysLinkAssembly]

namespace Simofun.JsonNet.Unity.Converter
{
	using Cysharp.Threading.Tasks;
	using Newtonsoft.Json;
	using Simofun.JsonNet.Unity.Serialization;
	using UnityEngine;

	public static class SimUnityConverterInitializer
	{
		#region Methods
		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
#if UNITY_EDITOR
		[UnityEditor.InitializeOnLoadMethod]
#endif
		internal static void Init()
		{
			var settings = JsonConvert.DefaultSettings?.Invoke();
			if (settings != null)
			{
				settings.ContractResolver = new SimUnityTypeContractResolver();
			}

#if UNITY_EDITOR
			if (settings != null)
			{
				return;
			}

			var cts = new System.Threading.CancellationTokenSource();
			cts.CancelAfterSlim(System.TimeSpan.FromSeconds(3));

			UniTask.Create(async () =>
			{
				await UniTask.WaitUntil(() => JsonConvert.DefaultSettings != null, cancellationToken: cts.Token);
				var newSettings = JsonConvert.DefaultSettings?.Invoke();
				newSettings.ContractResolver = new SimUnityTypeContractResolver();

			});
#endif
		}
		#endregion
	}
}
