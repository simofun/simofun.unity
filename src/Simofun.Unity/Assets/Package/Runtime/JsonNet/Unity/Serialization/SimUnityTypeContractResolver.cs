//-----------------------------------------------------------------------
// <copyright file="SimUnityTypeContractResolver.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.JsonNet.Unity.Serialization
{
	using Newtonsoft.Json;
	using Newtonsoft.Json.Serialization;
	using System.Collections.Generic;
	using System.Linq;
	using System.Reflection;
	using UnityEngine;

	public class SimUnityTypeContractResolver : DefaultContractResolver
	{
		// [SIMOFUN PACKAGE UPDATE] constructor
		public SimUnityTypeContractResolver()
		{
			this.SerializeCompilerGeneratedMembers = true;
		}

		protected override List<MemberInfo> GetSerializableMembers(System.Type objectType)
		{
			List<MemberInfo> members = base.GetSerializableMembers(objectType);

			members.AddRange(GetMissingMembers(objectType, members));

			return members;
		}

		protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
		{
			JsonProperty jsonProperty = base.CreateProperty(member, memberSerialization);

			// [SIMOFUN PACKAGE UPDATE] !jsonProperty.Ignored && 
			if (!jsonProperty.Ignored && member.GetCustomAttribute<SerializeField>() != null)
			{
				jsonProperty.Ignored = false;
				jsonProperty.Writable = CanWriteMemberWithSerializeField(member);
				jsonProperty.Readable = CanReadMemberWithSerializeField(member);
				jsonProperty.HasMemberAttribute = true;
			}

			return jsonProperty;
		}

		protected override IList<JsonProperty> CreateProperties(System.Type type, MemberSerialization memberSerialization)
		{
			IList<JsonProperty> lists = base.CreateProperties(type, memberSerialization);

			return lists;
		}

		protected override JsonObjectContract CreateObjectContract(System.Type objectType)
		{
			JsonObjectContract jsonObjectContract = base.CreateObjectContract(objectType);

			if (typeof(ScriptableObject).IsAssignableFrom(objectType))
			{
				jsonObjectContract.DefaultCreator = () =>
				{
					return ScriptableObject.CreateInstance(objectType);
				};
			}

			return jsonObjectContract;
		}

		private static IEnumerable<MemberInfo> GetMissingMembers(System.Type type, List<MemberInfo> alreadyAdded)
		{
			return type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy)
				.Cast<MemberInfo>()
				.Concat(type.GetProperties(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy))
				.Where(o => o.GetCustomAttribute<SerializeField>() != null
					&& !alreadyAdded.Contains(o));
		}

		private static bool CanReadMemberWithSerializeField(MemberInfo member)
		{
			if (member is PropertyInfo property)
			{
				return property.CanRead;
			}
			else
			{
				return true;
			}
		}

		private static bool CanWriteMemberWithSerializeField(MemberInfo member)
		{
			if (member is PropertyInfo property)
			{
				return property.CanWrite;
			}
			else
			{
				return true;
			}
		}
	}
}
