﻿//-----------------------------------------------------------------------
// <copyright file="LogViewerInvoker.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.LogViewer.Unity
{
	using Sirenix.OdinInspector;
	using UnityEngine;

	public class SimLogViewerInvoker : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimLogViewerInvoker), "Settings")]
		bool invokeOnAwake;

		[SerializeField]
		bool invokeOnStart = true;

		[SerializeField, Title("", "References")]
		GameObject reporter;
		#endregion

		#region Unity Methods
		/// <inheritdoc/>
		protected virtual void Awake()
		{
			if (this.invokeOnAwake)
			{
				this.Execute();
			}
		}

		/// <inheritdoc/>
		protected virtual void Start()
		{
			if (this.invokeOnStart)
			{
				this.Execute();
			}
		}
		#endregion

		#region Public Methods
		/// <summary>
		/// Invokes 'Log Viewer' on 'Development Build Mode'.
		/// Otherwise, deletes initially disabled 'Log Viewer' object and self.
		/// </summary>
		public void Execute()
		{
#if DEVELOPMENT_BUILD
			this.reporter.gameObject.SetActive(true);
#else
			Destroy(this.reporter.gameObject);
#endif
			Destroy(this.gameObject);
		}
		#endregion
	}
}
