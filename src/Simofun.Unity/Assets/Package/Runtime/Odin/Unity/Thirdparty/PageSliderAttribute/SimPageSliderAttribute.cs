﻿//-----------------------------------------------------------------------
// <copyright file="SimPageSliderAttribute.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Odin.Unity.ThirdParty.PageSliderAttributeDrawer
{
    using System;

    public class SimPageSliderAttribute : Attribute
    {
    }
}
