﻿//-----------------------------------------------------------------------
// <copyright file="SimProbability.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Odin.Unity.ThirdParty.Probability
{
    using System.Collections.Generic;
    using System.Linq;

    public static class SimProbability
    {
        #region Public Static Methods
        public static (bool, T) Random<T>(T[] obj, float[] prob)
        {
            if (obj.Length == 1)
            {
                return ((obj[0] != null), obj[0]);
            }

            if (obj.Length == prob.Length + 1)
            {

                List<int> tempRanges = (from value in prob select (int)(value * 100)).ToList();
                tempRanges.Add(100);
                int randomValue = UnityEngine.Random.Range(0, 100);

                if (prob.Length == 0)
                {
                    return ((obj[0] != null), default);
                }

                for (int i = 0; i < tempRanges.Count; i++)
                {
                    if (randomValue <= tempRanges[i])
                    {

                        return ((obj[i] != null), obj[i]);
                    }

                }
            }

            return (false, default);
        }
        #endregion
    }
}
