﻿//-----------------------------------------------------------------------
// <copyright file="SimProbabilityAttribute.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Odin.Unity.ThirdParty.Probability
{
    using System;

    public sealed class SimProbabilityAttribute : Attribute
    {
        #region Public Fields
        public string ColorName;

        public string DataMember;
        #endregion

        #region Constructors
        public SimProbabilityAttribute(string dataMember, string colors = "Fall")
        {
            this.DataMember = dataMember;
            this.ColorName = colors;
        }
        #endregion
    }
}
