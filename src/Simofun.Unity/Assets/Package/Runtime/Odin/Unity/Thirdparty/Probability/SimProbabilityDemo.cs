﻿//-----------------------------------------------------------------------
// <copyright file="SimProbabilityDemo.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Odin.Unity.ThirdParty.Probability
{
    using Sirenix.OdinInspector;
    using UnityEngine;

    public class SimProbabilityDemo : MonoBehaviour
    {
        #region Unity Fields
        [SerializeField]
        [SimProbability("OriginObj")]
        float[] probability;

        [SerializeField]
        string[] OriginObj;
        #endregion

        #region Methods
        [Button]
        // Start is called before the first frame update
        void Demo()
        {
            bool b;
            string go;

            (b, go) = SimProbability.Random(OriginObj, probability);

            Debug.Log(go);
        }
        #endregion
    }
}
