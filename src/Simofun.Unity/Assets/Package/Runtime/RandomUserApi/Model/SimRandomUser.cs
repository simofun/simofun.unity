﻿//-----------------------------------------------------------------------
// <copyright file="SimRandomUser.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.RandomUserApi.Model
{
	using System;

	[Serializable]
	public class SimRandomUser
	{
		public SimRandomUserPicture picture;
	}
}
