﻿//-----------------------------------------------------------------------
// <copyright file="SimRandomUserApiResponse.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.RandomUserApi.Model
{
	using System;
	using System.Collections.Generic;

	[Serializable]
	public class SimRandomUserApiResponse
	{
		public List<SimRandomUser> results = new List<SimRandomUser>();
	}
}
