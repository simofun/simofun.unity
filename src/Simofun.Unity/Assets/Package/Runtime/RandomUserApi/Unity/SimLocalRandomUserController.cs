﻿//-----------------------------------------------------------------------
// <copyright file="SimLocalRandomUserController.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.RandomUserApi.Unity
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class SimLocalRandomUserController : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField]
		List<Sprite> avatars;
		#endregion

		#region Properties
		public bool IsGetRandomUserPictureCoroutineCancelled { get; set; }
		#endregion

		#region Public Methods
		/// <summary>
		/// Coroutine to get random user picture until cancelled.
		/// </summary>
		/// <param name="onGet">Action on get a random user picture.</param>
		/// <param name="waitSeconds">Wait for seconds between images. Set '-1' to pass waiting.</param>
		/// <returns></returns>
		public IEnumerator GetRandomUserPictureCoroutine(
			Action<Sprite> onGet,
			Action<string> onError = null,
			float waitSeconds = 0.25f)
		{
			this.IsGetRandomUserPictureCoroutineCancelled = false;

			var isFirstIteration = true;

			while (true)
			{
				if (!isFirstIteration && waitSeconds != -1)
				{
					isFirstIteration = false;

					yield return new WaitForSeconds(waitSeconds);
				}
				else if (isFirstIteration)
				{
					isFirstIteration = false;
				}

				if (this.IsGetRandomUserPictureCoroutineCancelled)
				{
					yield break;
				}

				var avatar = this.avatars[new System.Random().Next(this.avatars.Count)];
				onGet(avatar);

				if (this.IsGetRandomUserPictureCoroutineCancelled)
				{
					yield break;
				}
			}
		}
		#endregion
	}
}
