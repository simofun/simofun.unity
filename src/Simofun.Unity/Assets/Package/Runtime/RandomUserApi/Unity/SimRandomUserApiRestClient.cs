﻿//-----------------------------------------------------------------------
// <copyright file="SimRandomUserApiRestClient.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.RandomUserApi.Unity
{
	using Simofun.RandomUserApi.Model;
	using System;
	using System.Collections;
	using System.Linq;
	using UnityEngine;
	using UnityEngine.Networking;

	public class SimRandomUserApiRestClient
	{
		#region Public Fields
		public const string Url = "https://randomuser.me/api";
		#endregion

		#region Properties
		public bool IsGetRandomUserPictureCoroutineCancelled { get; set; }
		#endregion

		#region Public Methods
		/// <summary>
		/// Coroutine to get random user picture until cancelled.
		/// </summary>
		/// <param name="onGet">Action on get a random user picture.</param>
		/// <param name="waitSeconds">Wait for seconds between images. Set '-1' to pass waiting.</param>
		/// <returns></returns>
		public IEnumerator GetRandomUserPictureCoroutine(
			Action<Texture2D> onGet,
			Action<string> onError = null,
			float waitSeconds = 0.25f)
		{
			this.IsGetRandomUserPictureCoroutineCancelled = false;

			SimRandomUserApiResponse randomUserApi = null;
			var isFirstIteration = true;

			while (true)
			{
				if (!isFirstIteration && waitSeconds != -1)
				{
					isFirstIteration = false;

					yield return new WaitForSeconds(waitSeconds);
				}
				else if (isFirstIteration)
				{
					isFirstIteration = false;
				}

				if (this.IsGetRandomUserPictureCoroutineCancelled)
				{
					yield break;
				}

				using (var www = UnityWebRequest.Get(Url))
				{
					yield return www.SendWebRequest();

					if (www.result is UnityWebRequest.Result.ConnectionError
							or UnityWebRequest.Result.ProtocolError
							or UnityWebRequest.Result.DataProcessingError)
					{
						onError?.Invoke(www.error);

						continue;
					}

					randomUserApi = JsonUtility.FromJson<SimRandomUserApiResponse>(www.downloadHandler.text);
				}

				using (var www = UnityWebRequestTexture.GetTexture(randomUserApi.results.First().picture.large))
				{
					yield return www.SendWebRequest();

					if (www.result is UnityWebRequest.Result.ConnectionError
							or UnityWebRequest.Result.ProtocolError
							or UnityWebRequest.Result.DataProcessingError)
					{
						onError?.Invoke(www.error);

						continue;
					}

					if (this.IsGetRandomUserPictureCoroutineCancelled)
					{
						yield break;
					}

					onGet(DownloadHandlerTexture.GetContent(www));
				}

				if (this.IsGetRandomUserPictureCoroutineCancelled)
				{
					yield break;
				}
			}
		}
		#endregion
	}
}
