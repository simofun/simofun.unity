﻿//-----------------------------------------------------------------------
// <copyright file="SimEncryptUtil.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Security.Cryptography.Utils
{
	using System;
	using System.IO;
	using System.Security.Cryptography;
	using System.Text;

	public static class SimEncryptUtil
	{
		public static string EncryptString(string value)
		{
			byte[] IV = { 12, 21, 43, 17, 57, 35, 67, 27 };
			var encryptKey = "aXb2uy4z";
			var key = Encoding.UTF8.GetBytes(encryptKey);
			var byteInput = Encoding.UTF8.GetBytes(value);
			var provider = new DESCryptoServiceProvider();
			var memStream = new MemoryStream();
			ICryptoTransform transform = provider.CreateEncryptor(key, IV);
			var cryptoStream = new CryptoStream(memStream, transform, CryptoStreamMode.Write);
			cryptoStream.Write(byteInput, 0, byteInput.Length);
			cryptoStream.FlushFinalBlock();

			return Convert.ToBase64String(memStream.ToArray());
		}

		public static string DecryptString(string value)
		{
			byte[] IV = { 12, 21, 43, 17, 57, 35, 67, 27 };
			var encryptKey = "aXb2uy4z";
			var key = Encoding.UTF8.GetBytes(encryptKey);
			var byteInput = new byte[value.Length];
			byteInput = Convert.FromBase64String(value);
			var provider = new DESCryptoServiceProvider();
			var memStream = new MemoryStream();
			var transform = provider.CreateDecryptor(key, IV);
			var cryptoStream = new CryptoStream(memStream, transform, CryptoStreamMode.Write);
			cryptoStream.Write(byteInput, 0, byteInput.Length);
			cryptoStream.FlushFinalBlock();

			var encoding = Encoding.UTF8;

			return encoding.GetString(memStream.ToArray());
		}
	}
}
