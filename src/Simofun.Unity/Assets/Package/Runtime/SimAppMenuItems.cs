﻿//-----------------------------------------------------------------------
// <copyright file="SimAppMenuItems.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun
{
	public class SimAppMenuItems
	{
		#region Public Fields
		public const string Path = SimMenuItems.Path + " App";
		
		public const string Path_ = Path + SimMenuItems.Seperator;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="SimAppMenuItems"/> class.
		/// </summary>
		protected SimAppMenuItems()
		{
		}
		#endregion
	}
}
