﻿//-----------------------------------------------------------------------
// <copyright file="SimMenuItems.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun
{
	public class SimMenuItems
	{
		#region Public Fields
		public const string Path = nameof(Simofun);
		
		public const string Path_ = Path + Seperator;
		
		public const string Seperator = "/";
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="SimMenuItems"/> class.
		/// </summary>
		protected SimMenuItems()
		{
		}
		#endregion
	}
}
