﻿//-----------------------------------------------------------------------
// <copyright file="SimRandomPlayerNameGenerator.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun
{
	public class SimRandomPlayerNameGenerator : SimRandomUserNameGenerator
	{
		#region Constructors
		public SimRandomPlayerNameGenerator() : base("Player")
		{
		}
		#endregion
	}
}
