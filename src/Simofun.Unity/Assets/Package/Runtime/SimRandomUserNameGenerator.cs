﻿//-----------------------------------------------------------------------
// <copyright file="SimRandomUserNameGenerator.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun
{
	using System;

	public class SimRandomUserNameGenerator
	{
		#region Constructors
		public SimRandomUserNameGenerator() : this("User")
		{
		}

		public SimRandomUserNameGenerator(string namePrefix)
		{
			this.NamePrefix = namePrefix;
		}
		#endregion

		#region Properties
		public string NamePrefix { get; protected set; }
		#endregion

		#region Public Methods
		public string Execute() => this.Execute(0, 1000);

		public string Execute(int maxValue) => this.Execute(0, maxValue);

		public string Execute(int minValue, int maxValue) => this.NamePrefix + new Random().Next(minValue, maxValue);
		#endregion
	}
}
