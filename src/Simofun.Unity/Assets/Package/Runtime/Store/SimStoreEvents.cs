﻿//-----------------------------------------------------------------------
// <copyright file="SimStoreEvents.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Store.Events
{
	using Simofun.Unity.Events;
	
	public sealed class SimStoreEvents
	{
		public class OnOpenRateUs : SimBaseUnityEvent
		{
		}
	}
}
