﻿//-----------------------------------------------------------------------
// <copyright file="SimStringExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.TMPro.Unity.Extensions
{
	using UnityEngine;

	/// <summary>
	/// Extensions for TMPro.
	/// </summary>
	public static class SimStringExt
	{
		/// <summary>
		/// Use <b> and </b>.
		/// </summary>
		public static string ToBold(this string src) => $"<b>{src}</b>";

		/// <summary>
		/// <color=red> to set the color to red. Several other colors are pre-defined.
		/// <#FF8000> or any other hexadecimal code to define a color.
		/// </color> to end the color tag.
		/// </summary>
		public static string ToColor(this string src, Color value) =>
			$"<color=#{ColorUtility.ToHtmlStringRGB(value)}>{src}</color>";

		/// <summary>
		/// <color=red> to set the color to red. Several other colors are pre-defined.
		/// <#FF8000> or any other hexadecimal code to define a color.
		/// </color> to end the color tag.
		/// </summary>
		public static string ToColor(this string src, string value) => $"<color={value}>{src}</color>";

		/// <summary>
		///	Use <i> and </i>.
		/// </summary>
		public static string ToItalic(this string src) => $"<i>{src}</i>";

		/// <summary>
		/// Use \n to force a linefeed.
		/// </summary>
		public static string ToLineFeed(this string src) => src + "\n";

		/// <summary>
		/// Use <pos=4.25> advances about 4.25 spaces.
		/// </summary>
		public static string ToPosition(this string src, float value) => $"<pos={value}>{src}</pos>";

		/// <summary>
		/// Use <pos=4.25> advances about 4.25 spaces.
		/// </summary>
		public static string ToPosition(this string src, string value) => $"<pos={value}>{src}</pos>";

		/// <summary>
		/// Use <size=36> to set point size to 36.
		/// <size=+18> to increase the point size by 18 points.
		/// <size=-14> to decrease the point size by 14 points.
		/// </size> to end the size tag.
		/// </summary>
		public static string ToSize(this string src, float value) => $"<size={value}>{src}</size>";

		/// <summary>
		/// Use <size=36> to set point size to 36.
		/// <size=+18> to increase the point size by 18 points.
		/// <size=-14> to decrease the point size by 14 points.
		/// </size> to end the size tag.
		/// </summary>
		public static string ToSize(this string src, int value) => $"<size={value}>{src}</size>";

		/// <summary>
		/// Use <size=36> to set point size to 36.
		/// <size=+18> to increase the point size by 18 points.
		/// <size=-14> to decrease the point size by 14 points.
		/// </size> to end the size tag.
		/// </summary>
		public static string ToSize(this string src, string value) => $"<size={value}>{src}</size>";

		/// <summary>
		/// Use <size=36> to set point size to 36.
		/// <size=+18> to increase the point size by 18 points.
		/// <size=-14> to decrease the point size by 14 points.
		/// </size> to end the size tag.
		/// </summary>
		public static string ToSizeDecrement(this string src, float value) => $"<size=-{value}>{src}</size>";

		/// <summary>
		/// Use <size=36> to set point size to 36.
		/// <size=+18> to increase the point size by 18 points.
		/// <size=-14> to decrease the point size by 14 points.
		/// </size> to end the size tag.
		/// </summary>
		public static string ToSizeDecrement(this string src, int value) => $"<size=-{value}>{src}</size>";

		/// <summary>
		/// Use <size=36> to set point size to 36.
		/// <size=+18> to increase the point size by 18 points.
		/// <size=-14> to decrease the point size by 14 points.
		/// </size> to end the size tag.
		/// </summary>
		public static string ToSizeDecrement(this string src, string value) => $"<size=-{value}>{src}</size>";

		/// <summary>
		/// Use <size=36> to set point size to 36.
		/// <size=+18> to increase the point size by 18 points.
		/// <size=-14> to decrease the point size by 14 points.
		/// </size> to end the size tag.
		/// </summary>
		public static string ToSizeIncrement(this string src, float value) => $"<size=+{value}>{src}</size>";

		/// <summary>
		/// Use <size=36> to set point size to 36.
		/// <size=+18> to increase the point size by 18 points.
		/// <size=-14> to decrease the point size by 14 points.
		/// </size> to end the size tag.
		/// </summary>
		public static string ToSizeIncrement(this string src, int value) => $"<size=+{value}>{src}</size>";

		/// <summary>
		/// Use <size=36> to set point size to 36.
		/// <size=+18> to increase the point size by 18 points.
		/// <size=-14> to decrease the point size by 14 points.
		/// </size> to end the size tag.
		/// </summary>
		public static string ToSizeIncrement(this string src, string value) => $"<size=+{value}>{src}</size>";

		/// <summary>
		/// Use <size=36> to set point size to 36.
		/// <size=+18> to increase the point size by 18 points.
		/// <size=-14> to decrease the point size by 14 points.
		/// </size> to end the size tag.
		/// </summary>
		public static string ToSizePercent(this string src, float value) => $"<size={value}%>{src}</size>";

		/// <summary>
		/// Use <size=36> to set point size to 36.
		/// <size=+18> to increase the point size by 18 points.
		/// <size=-14> to decrease the point size by 14 points.
		/// </size> to end the size tag.
		/// </summary>
		public static string ToSizePercent(this string src, int value) => $"<size={value}%>{src}</size>";

		/// <summary>
		/// Use <size=36> to set point size to 36.
		/// <size=+18> to increase the point size by 18 points.
		/// <size=-14> to decrease the point size by 14 points.
		/// </size> to end the size tag.
		/// </summary>
		public static string ToSizePercent(this string src, string value) => $"<size={value}%>{src}</size>";

		/// <summary>
		///	Use <sub> and </sub>.
		/// </summary>
		public static string ToSubscript(this string src) => $"<sub>{src}</sub>";

		/// <summary>
		///	Use <sup> and </sup>.
		/// </summary>
		public static string ToSuperscript(this string src) => $"<sup>{src}</sup>";

		/// <summary>
		/// Use \t which will add one tab stop.
		/// </summary>
		public static string ToTab(this string src) => src + "\t";

		/// <summary>
		///	Use <u> and </u>.
		/// </summary>
		public static string ToUnderline(this string src) => $"<u>{src}</u>";
	}
}
