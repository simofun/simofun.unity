﻿//-----------------------------------------------------------------------
// <copyright file="SimMessageBus.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

using UniRx;

namespace Simofun.UniRx.Unity
{
	using Simofun.Events;
	
	public static class SimMessageBus
	{
		#region Public Methods
		/// <summary>
		/// Subscribe to a event
		/// </summary>
		/// <typeparam name="T">Event type to listen to</typeparam>
		/// <returns>An IObservable which can be subscribed to</returns>
		public static System.IObservable<T> OnEvent<T>() where T : SimBaseEvent => MessageBroker.Default.Receive<T>();

		/// <summary>
		/// Raises a custom-type event
		/// </summary>
		/// <typeparam name="T">System.Type of event</typeparam>
		/// <param name="evnt">Event instance</param>
		public static void Publish<T>(T evnt) where T : SimBaseEvent => MessageBroker.Default.Publish(evnt);
		#endregion
	}
}
