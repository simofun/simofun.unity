//-----------------------------------------------------------------------
// <copyright file="ObservableDisableTrigger.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Hasan Emre Tonguc</author>
//-----------------------------------------------------------------------

using UniRx;
using UniRx.Triggers;

namespace Simofun.UniRx.Unity.Triggers
{
	using System;
	using UnityEngine;

	[DisallowMultipleComponent]
    public class ObservableDisableTrigger : ObservableTriggerBase
    {
        #region Fields
        Subject<Unit> onDisable;
        #endregion

        #region Unity Methods
        /// <inheritdoc />
        protected virtual void OnDisable()
        {
            if (this.onDisable != null)
			{
                this.onDisable.OnNext(Unit.Default);
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// If <see cref="MonoBehaviour"/> or <see cref="GameObject"/> disabled this callback will be invoked.
        /// </summary>
        /// <returns></returns>
        public IObservable<Unit> OnDisableAsObservable() => this.onDisable ?? (this.onDisable = new Subject<Unit>());
        #endregion

        #region Protected Methods
        #region ObservableTriggerBase
        /// <inheritdoc />
        protected override void RaiseOnCompletedOnDestroy()
        {
            if (this.onDisable != null)
            {
                this.onDisable.OnCompleted();
            }
        }
        #endregion
        #endregion
    }
}
