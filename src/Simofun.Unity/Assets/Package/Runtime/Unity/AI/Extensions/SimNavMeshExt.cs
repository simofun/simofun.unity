﻿//-----------------------------------------------------------------------
// <copyright file="SimNavMeshExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.AI.Extensions
{
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;
	using UnityEngine.AI;

	/// <summary>
	/// Extension class for <see cref="NavMesh"/>
	/// </summary>
	public static class SimNavMeshExt
	{
		public static IEnumerable<System.Tuple<GameObject, NavMeshHit>> FindSamplePoints(
			this IEnumerable<GameObject> src) => src.FindSamplePoints(1, NavMesh.AllAreas);

		public static IEnumerable<System.Tuple<GameObject, NavMeshHit>> FindSamplePoints(
			this IEnumerable<GameObject> src, float maxDistance) =>
				src.FindSamplePoints(maxDistance, NavMesh.AllAreas);

		public static IEnumerable<System.Tuple<GameObject, NavMeshHit>> FindSamplePoints(
			this IEnumerable<GameObject> src, float maxDistance, int areaMask)
		{
			foreach (var item in src)
			{
				if (NavMesh.SamplePosition(item.transform.position, out var hit, maxDistance, areaMask))
				{
					yield return System.Tuple.Create(item, hit);
				}
			}
		}

		public static IEnumerable<System.Tuple<T, NavMeshHit>> FindSamplePoints<T>(this IEnumerable<T> src)
			where T : Component => src.FindSamplePoints<T>(1, NavMesh.AllAreas);

		public static IEnumerable<System.Tuple<T, NavMeshHit>> FindSamplePoints<T>(
			this IEnumerable<T> src, float maxDistance) where T : Component =>
				src.FindSamplePoints<T>(maxDistance, NavMesh.AllAreas);

		public static IEnumerable<System.Tuple<T, NavMeshHit>> FindSamplePoints<T>(
			this IEnumerable<T> src, float maxDistance, int areaMask) where T : Component
		{
			foreach (var item in src)
			{
				if (NavMesh.SamplePosition(item.transform.position, out var hit, maxDistance, areaMask))
				{
					yield return System.Tuple.Create(item, hit);
				}
			}
		}

		public static IEnumerable<System.Tuple<Transform, NavMeshHit>> FindSamplePoints(
			this IEnumerable<Transform> src) => src.FindSamplePoints(1, NavMesh.AllAreas);

		public static IEnumerable<System.Tuple<Transform, NavMeshHit>> FindSamplePoints(
			this IEnumerable<Transform> src, float maxDistance) => src.FindSamplePoints(maxDistance, NavMesh.AllAreas);

		public static IEnumerable<System.Tuple<Transform, NavMeshHit>> FindSamplePoints(
			this IEnumerable<Transform> src, float maxDistance, int areaMask)
		{
			foreach (var item in src)
			{
				if (NavMesh.SamplePosition(item.transform.position, out var hit, maxDistance, areaMask))
				{
					yield return System.Tuple.Create(item, hit);
				}
			}
		}

		public static IEnumerable<System.Tuple<Vector3, NavMeshHit>> FindSamplePositions(
			this IEnumerable<Vector3> src) => src.FindSamplePositions(1, NavMesh.AllAreas);

		public static IEnumerable<System.Tuple<Vector3, NavMeshHit>> FindSamplePositions(
			this IEnumerable<Vector3> src, float maxDistance) =>
				src.FindSamplePositions(maxDistance, NavMesh.AllAreas);

		public static IEnumerable<System.Tuple<Vector3, NavMeshHit>> FindSamplePositions(
			this IEnumerable<Vector3> src, float maxDistance, int areaMask)
		{
			foreach (var pos in src)
			{
				if (NavMesh.SamplePosition(pos, out var hit, maxDistance, areaMask))
				{
					yield return System.Tuple.Create(pos, hit);
				}
			}
		}

		public static IEnumerable<System.Tuple<GameObject, NavMeshHit>> FindSoClosestSamplePoints(
			this IEnumerable<GameObject> src, Vector3 position) =>
				src.FindSoClosestSamplePoints(position, 1, NavMesh.AllAreas);

		public static IEnumerable<System.Tuple<GameObject, NavMeshHit>> FindSoClosestSamplePoints(
			this IEnumerable<GameObject> src, Vector3 position, float maxDistance) =>
				src.FindSoClosestSamplePoints(position, maxDistance, NavMesh.AllAreas);

		public static IEnumerable<System.Tuple<GameObject, NavMeshHit>> FindSoClosestSamplePoints(
			this IEnumerable<GameObject> src, Vector3 position, float maxDistance, int areaMask)
		{
			foreach (var item in src.OrderBy(i => Vector3.Distance(position, i.transform.position)))
			{
				if (NavMesh.SamplePosition(item.transform.position, out var hit, maxDistance, areaMask))
				{
					yield return System.Tuple.Create(item, hit);
				}
			}
		}

		public static IEnumerable<System.Tuple<T, NavMeshHit>> FindSoClosestSamplePoints<T>(
			this IEnumerable<T> src, Vector3 position)
			where T : Component => src.FindSoClosestSamplePoints<T>(position, 1, NavMesh.AllAreas);

		public static IEnumerable<System.Tuple<T, NavMeshHit>> FindSoClosestSamplePoints<T>(
			this IEnumerable<T> src, Vector3 position, float maxDistance) where T : Component =>
				src.FindSoClosestSamplePoints<T>(position, maxDistance, NavMesh.AllAreas);

		public static IEnumerable<System.Tuple<T, NavMeshHit>> FindSoClosestSamplePoints<T>(
			this IEnumerable<T> src, Vector3 position, float maxDistance, int areaMask) where T : Component
		{
			foreach (var item in src.OrderBy(i => Vector3.Distance(position, i.transform.position)))
			{
				if (NavMesh.SamplePosition(item.transform.position, out var hit, maxDistance, areaMask))
				{
					yield return System.Tuple.Create(item, hit);
				}
			}
		}

		public static IEnumerable<System.Tuple<Transform, NavMeshHit>> FindSoClosestSamplePoints(
			this IEnumerable<Transform> src, Vector3 position) =>
				src.FindSoClosestSamplePoints(position, 1, NavMesh.AllAreas);

		public static IEnumerable<System.Tuple<Transform, NavMeshHit>> FindSoClosestSamplePoints(
			this IEnumerable<Transform> src, Vector3 position, float maxDistance) =>
				src.FindSoClosestSamplePoints(position, maxDistance, NavMesh.AllAreas);

		public static IEnumerable<System.Tuple<Transform, NavMeshHit>> FindSoClosestSamplePoints(
			this IEnumerable<Transform> src, Vector3 position, float maxDistance, int areaMask)
		{
			foreach (var item in src.OrderBy(i => Vector3.Distance(position, i.position)))
			{
				if (NavMesh.SamplePosition(item.transform.position, out var hit, maxDistance, areaMask))
				{
					yield return System.Tuple.Create(item, hit);
				}
			}
		}

		public static IEnumerable<System.Tuple<Vector3, NavMeshHit>> FindSoClosestSamplePoints(
			this IEnumerable<Vector3> src, Vector3 position) =>
				src.FindSoClosestSamplePoints(position, 1, NavMesh.AllAreas);

		public static IEnumerable<System.Tuple<Vector3, NavMeshHit>> FindSoClosestSamplePoints(
			this IEnumerable<Vector3> src, Vector3 position, float maxDistance) =>
				src.FindSoClosestSamplePoints(position, maxDistance, NavMesh.AllAreas);

		public static IEnumerable<System.Tuple<Vector3, NavMeshHit>> FindSoClosestSamplePoints(
			this IEnumerable<Vector3> src, Vector3 position, float maxDistance, int areaMask)
		{
			foreach (var pos in src.OrderBy(i => Vector3.Distance(position, i)))
			{
				if (NavMesh.SamplePosition(pos, out var hit, maxDistance, areaMask))
				{
					yield return System.Tuple.Create(pos, hit);
				}
			}
		}
	}
}
