﻿//-----------------------------------------------------------------------
// <copyright file="ISimAITargetController.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.AI
{
	using UnityEngine;

	public interface ISimAITargetController
	{
		#region Properties
		Transform Owner { get; }

		Transform Target { get; }

		Vector3 TargetPosition { get; }
		#endregion

		#region Methods
		#region Initialization
		void Initialize(Transform owner);
		#endregion

		Transform GoToLast();

		Transform GoToNext();

		void GoToPosition(Vector3 position);

		bool IsReachedToTargetPosition(float stoppingDistance);
		#endregion
	}
}
