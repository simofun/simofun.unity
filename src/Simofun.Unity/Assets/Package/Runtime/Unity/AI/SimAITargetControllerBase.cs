﻿//-----------------------------------------------------------------------
// <copyright file="SimAITargetControllerBase.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.AI
{
	using Simofun.Unity.Extensions;
	using UnityEngine;

	public abstract class SimAITargetControllerBase : MonoBehaviour, ISimAITargetController
	{
		#region Properties
		#region IAITargetController Properties
		public Transform Owner { get; protected set; }

		public Transform Target { get; protected set; }

		public Vector3 TargetPosition { get; protected set; }
		#endregion
		#endregion

		#region Public Methods
		#region IAITargetController Methods
		#region Initialization
		public virtual void Initialize(Transform owner) => this.Owner = owner;
		#endregion

		public abstract Transform GoToLast();

		public abstract Transform GoToNext();

		public virtual bool IsReachedToTargetPosition(float stoppingDistance) =>
			Vector3.Distance(this.Owner.position.ToVector2XZ(), this.TargetPosition.ToVector2XZ()) < stoppingDistance;

		public virtual void GoToPosition(Vector3 position) => this.TargetPosition = position;
		#endregion
		#endregion
	}
}
