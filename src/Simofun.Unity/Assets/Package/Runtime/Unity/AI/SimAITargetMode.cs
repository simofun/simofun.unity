﻿//-----------------------------------------------------------------------
// <copyright file="SimAITargetMode.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.AI
{
	public enum SimAITargetMode
	{
		MoveTargetWithoutNotice,

		RandomTarget,

		SequentialRandomTarget
	}
}
