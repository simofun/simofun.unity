﻿//-----------------------------------------------------------------------
// <copyright file="SimAITargetPoint.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.AI
{
	using UnityEngine;

	public class SimAITargetPoint : MonoBehaviour
	{
		public bool IsEmpty { get; set; } = true;
	}
}
