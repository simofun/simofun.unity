﻿//-----------------------------------------------------------------------
// <copyright file="SimMoveTargetWithoutNoticeAITargetControllerBase.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.AI
{
	using Simofun.Unity.Extensions;
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public abstract class SimMoveTargetWithoutNoticeAITargetControllerBase : SimAITargetControllerBase
	{
		#region Fields
		const float deflectionDuration = 2;

		const float goingCollectableItemDuration = 2;

		/// <summary>
		/// -10___-5 & 5___10
		/// </summary>
		readonly List<int> randomMinMaxList = new List<int> { -10, -9, -8, -7, -6, -5, 5, 6, 7, 8, 9, 10 };

		bool isReachedToTargetPosition;
		#endregion

		#region Public Methods
		#region IAITargetController Methods
		public override bool IsReachedToTargetPosition(float stoppingDistance)
		{
			if (this.Target == null)
			{
				return false;
			}

			this.isReachedToTargetPosition = Vector3.Distance(
				this.Owner.position.ToVector2XZ(),
				this.Target.position.ToVector2XZ()) < stoppingDistance;

			return this.isReachedToTargetPosition;
		}

		public override Transform GoToLast()
		{
			this.TargetPosition = this.Target.position;

			return this.Target;
		}

		public override Transform GoToNext()
		{
			this.StartCoroutine(this.GoToNextCoroutine());

			return null;
		}
		#endregion
		#endregion

		#region Protected Methods
		protected abstract Transform GetCurrentTarget();
		#endregion

		#region Methods
		IEnumerator GoToNextCoroutine()
		{
			this.isReachedToTargetPosition = true;

			yield return new WaitUntil(() => this.GetCurrentTarget() != null);

			var newTarget = this.GetCurrentTarget();

			this.Target = newTarget;
			this.TargetPosition = newTarget.position;

			this.StartCoroutine(this.MoveTargetWithoutNoticeCoroutine());
		}

		IEnumerator MoveTargetWithoutNoticeCoroutine()
		{
			this.isReachedToTargetPosition = false;

			while (!this.isReachedToTargetPosition)
			{
				yield return new WaitForSeconds(goingCollectableItemDuration);

				this.GoToPosition(
					this.Owner.position
						+ new Vector3(
							this.randomMinMaxList[Random.Range(0, this.randomMinMaxList.Count)],
							0,
							this.randomMinMaxList[Random.Range(0, this.randomMinMaxList.Count)]));

				yield return new WaitForSeconds(deflectionDuration);

				var newTarget = this.GetCurrentTarget();

				this.Target = newTarget;
				this.TargetPosition = newTarget.position;

				this.GoToPosition(this.TargetPosition);
			}
		}
		#endregion
	}
}
