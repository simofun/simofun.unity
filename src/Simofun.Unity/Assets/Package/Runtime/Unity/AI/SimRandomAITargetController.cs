﻿//-----------------------------------------------------------------------
// <copyright file="SimRandomAITargetController.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.AI
{
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;

	public class SimRandomAITargetController : SimAITargetControllerBase
	{
		#region Protected Properties
		protected List<SimAITargetPoint> RandomTargets { get; set; }
		#endregion

		#region Public Methods
		#region IAITargetController Methods
		#region Initialization
		public override void Initialize(Transform owner)
		{
			base.Initialize(owner);

			this.RandomTargets = FindObjectsByType<SimAITargetPoint>(
				FindObjectsInactive.Exclude, FindObjectsSortMode.None)
				.ToList();
		}
		#endregion

		public override Transform GoToLast()
		{
			this.TargetPosition = this.Target.position;

			return this.Target;
		}

		public override Transform GoToNext()
		{
			var newTarget = this.WhenSetNewRandomTarget();
			if (newTarget == null)
			{
				return null;
			}

			this.Target = newTarget;
			this.TargetPosition = newTarget.position;

			return newTarget;
		}
		#endregion
		#endregion

		#region Protected Methods
		protected virtual Transform WhenSetNewRandomTarget() =>
			this.RandomTargets.Count > 0
				? this.RandomTargets[Random.Range(0, this.RandomTargets.Count)].transform
				: null;
		#endregion
	}
}
