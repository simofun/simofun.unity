﻿//-----------------------------------------------------------------------
// <copyright file="SimSequentialRandomAITargetController.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.AI
{
	using System.Linq;
	using UnityEngine;

	public class SimSequentialRandomAITargetController : SimRandomAITargetController
	{
		#region Protected Methods
		protected override Transform WhenSetNewRandomTarget()
		{
			if (this.RandomTargets.Count <= 0)
			{
				this.RandomTargets = FindObjectsByType<SimAITargetPoint>(
					FindObjectsInactive.Exclude, FindObjectsSortMode.None)
					.ToList();

				return this.WhenSetNewRandomTarget();
			}

			var newTarget = this.RandomTargets[Random.Range(0, this.RandomTargets.Count)];
			this.RandomTargets.Remove(newTarget);

			return newTarget.transform;
		}
		#endregion
	}
}
