﻿//-----------------------------------------------------------------------
// <copyright file="SimBasicAnalyticsEventsListenerBase`1.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Analytics.Events.Listener
{
	public abstract class SimBasicAnalyticsEventsListenerBase<T>
		: SimBasicAnalyticsEventsListenerBase<T, T> where T : SimBasicAnalyticsEventsListenerBase<T>, new()
	{
		#region Protected Constructors
		protected SimBasicAnalyticsEventsListenerBase() : base()
		{
		}
		#endregion
	}
}
