//-----------------------------------------------------------------------
// <copyright file="SimBasicAnalyticsEventsListenerBase`2.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Simofun</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Analytics.Events.Listener
{
	using Cysharp.Threading.Tasks;
	using Simofun.Unity.DesignPatterns.Singleton;
	using Sirenix.OdinInspector;
	using System.Threading;
	using UnityEngine;

	public class SimBasicAnalyticsEventsListenerBase<TInstance, TConcrete>
		: SimGlobalSingletonBase<TInstance, TConcrete>
		where TConcrete : SimBasicAnalyticsEventsListenerBase<TInstance, TConcrete>, TInstance, new()
	{
		#region Fields
		CancellationToken? cancellationToken;
		#endregion

		#region Unity Fields
		[field: SerializeField, Title("SimBasicAnalyticsEventsListenerBase", "Settings")]
		public float InitialListenDelay { get; set; } = 5;
		#endregion

		#region Protected Properties
		protected virtual bool IsElapsedTimeValid => this.ElapsedTime.TotalSeconds > this.InitialListenDelay;

		protected virtual CancellationToken CancellationToken =>
			this.cancellationToken.HasValue
				? this.cancellationToken.Value
				: (this.cancellationToken = this.GetCancellationTokenOnDestroy()).Value;

		protected virtual ISimAnalytics Analytics { get; set; }

		protected virtual System.TimeSpan StartTime { get; set; }

		protected virtual System.TimeSpan ElapsedTime =>
			System.TimeSpan.FromSeconds(Time.realtimeSinceStartup) - this.StartTime;
		#endregion

		#region Construct
		/// <inheritdoc />
		public virtual void Construct(params object[] deps)
		{
			if (deps == null)
			{
				return;
			}

			var deps0 = deps[0];
			if (deps0 != null)
			{
				this.Analytics = (ISimAnalytics)deps0;
			}
		}
		#endregion

		#region Unity Methods				
		/// <inheritdoc />
		protected override void Awake()
		{
			base.Awake();

			this.Construct(
#if SIM_GAME_ANALYTICS
				GameAnalytics.Unity.SimGameAnalytics.Instance
#else
				null
#endif
				);

			this.StartTime = System.TimeSpan.FromSeconds(Time.realtimeSinceStartup);

			this.RegisterEventHandlers();
		}

		/// <inheritdoc />
		protected override void OnDestroy()
		{
			this.UnRegisterEventHandlers();

			base.OnDestroy();
		}
		#endregion

		#region Protected Methods
		#region Event Handlers
		#region Registration
		protected virtual void RegisterEventHandlers()
		{
		}

		protected virtual void UnRegisterEventHandlers()
		{
		}
		#endregion
		#endregion

		protected virtual string NicifyValue(string value) =>
			value.Replace(" ", string.Empty).Replace("[", string.Empty).Replace("]", string.Empty);
		#endregion
	}
}
