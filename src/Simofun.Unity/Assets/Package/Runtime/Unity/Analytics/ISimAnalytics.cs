﻿//-----------------------------------------------------------------------
// <copyright file="ISimAnalytics.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Analytics
{
	using Simofun.Unity.Analytics.Model;

	public interface ISimAnalytics : ISimInitializable, ISimNamable
	{
		#region Properties
		string UserId { get; set; }
		#endregion

		#region Methods
		/// <summary>
		/// Set <see cref="IsInitialized"/> as true after initialization.
		/// </summary>
		/// <param name="userId">The custom user id instead auto generated one.</param>
		void Initialize(string userId);

		void LevelFail(int levelIndex);

		void LevelFail(string progression);

		void LevelSuccess(int levelIndex);

		void LevelSuccess(string progression);

		void LevelStart(int levelIndex);

		void LevelStart(string progression);

		/// <summary>
		/// Track fill-rate of you ads.
		/// </summary>
		/// <param name="adAction">Action of ad (for example loaded, show).</param>
		/// <param name="adType">Type of ad (for video, interstitial).</param>
		/// <param name="adProviderName">Name of ad provider.</param>
		/// <param name="adPlacement">Placement of ad or identifier of the ad in the app</param>
		void NewAdEvent(SimAdAction adAction, SimAdType adType, string adProviderName, string adPlacement);

		/// <summary>
		/// Track fill-rate of you ads.
		/// </summary>
		/// <param name="adAction">Action of ad (for example loaded, show).</param>
		/// <param name="adType">Type of ad (for video, interstitial).</param>
		/// <param name="adProviderName">Name of ad provider.</param>
		/// <param name="adPlacement">Placement of ad or identifier of the ad in the app</param>
		/// <param name="duration">Duration of ad video</param>
		void NewAdEvent(
			SimAdAction adAction, SimAdType adType, string adProviderName, string adPlacement, long duration);

		/// <summary>
		/// Track fill-rate of you ads.
		/// </summary>
		/// <param name="adAction">Action of ad (for example loaded, show).</param>
		/// <param name="adType">Type of ad (for video, interstitial).</param>
		/// <param name="adProviderName">Name of ad provider.</param>
		/// <param name="adPlacement">Placement of ad or identifier of the ad in the app</param>
		/// <param name="noAdReason">Error reason for no ad available</param>
		void NewAdEvent(SimAdAction adAction, SimAdType adType, string adProviderName, string adPlacement, SimAdError noAdReason);

		/// <summary>
		/// Track any type of design event that you want to measure i.e. GUI elements or tutorial steps. Custom dimensions are not supported.
		/// </summary>
		/// <param name="eventName">String can consist of 1 to 5 segments. Segments are seperated by ':' and segments can have a max length of 16. (e.g. segment1:anotherSegment:gold).</param>
		void NewDesignEvent(string eventName);

		/// <summary>
		/// Track any type of design event that you want to measure i.e. GUI elements or tutorial steps. Custom dimensions are not supported.
		/// </summary>
		/// <param name="eventName">String can consist of 1 to 5 segments. Segments are seperated by ':' and segments can have a max length of 16. (e.g. segment1:anotherSegment:gold).</param>
		/// <param name="eventValue">Number value of event.</param>
		void NewDesignEvent(string eventName, float eventValue);

		/// <summary>
		/// Measure player progression in the game.
		/// It follows a 3 hierarchy structure (world, level and phase) to indicate a player's path or place.
		/// </summary>
		/// <param name="progressionStatus">Status of added progression.</param>
		/// <param name="progression">The progression.</param>
		void NewProgressionEvent(SimAnalyticsProgressionStatus progressionStatus, string progression);

		/// <summary>
		/// Measure player progression in the game. It follows a 3 hierarchy structure (world, level and phase) to indicate a player's path or place.
		/// </summary>
		/// <param name="progressionStatus">Status of added progression.</param>
		/// <param name="progression">The progression.</param>
		/// /// <param name="score">The player's score.</param>
		void NewProgressionEvent(SimAnalyticsProgressionStatus progressionStatus, string progression, int score);

		void PauseTimer(string key);

		void ResumeTimer(string key);

		void StartTimer(string key);

		long StopTimer(string key);
		#endregion
	}
}
