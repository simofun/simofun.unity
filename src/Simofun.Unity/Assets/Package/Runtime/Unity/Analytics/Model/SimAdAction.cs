﻿//-----------------------------------------------------------------------
// <copyright file="SimAdAction.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Analytics.Model
{
	public enum SimAdAction
	{
		Undefined,

		Loaded,

		Show,

		Error,

		Clicked,

		Request,

		RewardReceived
	}
}
