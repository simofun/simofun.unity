﻿//-----------------------------------------------------------------------
// <copyright file="SimAdError.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Analytics.Model
{
    public enum SimAdError
    {
        Undefined,

        Unknown,

        Offline,

        NoFill,

        InternalError,

        InvalidRequest,

        UnableToPrecache
    }
}
