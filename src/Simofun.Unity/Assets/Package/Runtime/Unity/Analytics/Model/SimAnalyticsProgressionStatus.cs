﻿//-----------------------------------------------------------------------
// <copyright file="SimAnalyticsProgressionStatus.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Analytics.Model
{
	public enum SimAnalyticsProgressionStatus
	{
		/// <summary>
		/// Undefined progression
		/// </summary>
		Undefined,

		/// <summary>
		/// User started progression
		/// </summary>
		Start,

		/// <summary>
		/// User successfully ended a progression
		/// </summary>
		Success,

		/// <summary>
		/// User failed a progression
		/// </summary>
		Fail
	}
}
