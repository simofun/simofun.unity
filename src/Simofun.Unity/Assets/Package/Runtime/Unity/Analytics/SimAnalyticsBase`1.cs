﻿//-----------------------------------------------------------------------
// <copyright file="SimAnalyticsBase`1.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Analytics
{
	public abstract class SimAnalyticsBase<T> : SimAnalyticsBase<T, T> where T : SimAnalyticsBase<T>, new()
	{
		#region Protected Constructors
		protected SimAnalyticsBase() : base()
		{
		}
		#endregion
	}
}
