﻿//-----------------------------------------------------------------------
// <copyright file="SimAnalyticsBase`2.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Analytics
{
	using Simofun.Unity.Analytics.Model;
	using Simofun.Unity.DesignPatterns.Singleton;

	public abstract class SimAnalyticsBase<TInstance, TConcrete>
		: SimGlobalSingletonBase<TInstance, TConcrete>, ISimAnalytics
		where TConcrete : SimAnalyticsBase<TInstance, TConcrete>, TInstance, new()
	{
		#region Fields
		const string levelPrefix = "Level-";
		#endregion

		#region Properties
		#region ISimAnalytics
		/// <inheritdoc />
		public virtual string UserId { get; set; }
		#endregion
		#endregion

		#region Public Methods
		#region ISimAnalytics
		#region Abstract
		#region Timer
		/// <inheritdoc />
		public abstract void PauseTimer(string key);

		/// <inheritdoc />
		public abstract void ResumeTimer(string key);

		/// <inheritdoc />
		public abstract void StartTimer(string key);

		/// <inheritdoc />
		public abstract long StopTimer(string key);
		#endregion

		/// <inheritdoc />
		public abstract void NewAdEvent(
			SimAdAction adAction, SimAdType adType, string adProviderName, string adPlacement);

		/// <inheritdoc />
		public abstract void NewAdEvent(
			SimAdAction adAction, SimAdType adType, string adProviderName, string adPlacement, long duration);

		/// <inheritdoc />
		public abstract void NewAdEvent(
			SimAdAction adAction, SimAdType adType, string adProviderName, string adPlacement, SimAdError noAdReason);

		/// <inheritdoc />
		public abstract void NewDesignEvent(string eventName);

		/// <inheritdoc />
		public abstract void NewDesignEvent(string eventName, float eventValue);

		/// <inheritdoc />
		public abstract void NewProgressionEvent(SimAnalyticsProgressionStatus progressionStatus, string progression);

		/// <inheritdoc />
		public abstract void NewProgressionEvent(
			SimAnalyticsProgressionStatus progressionStatus, string progression, int score);
		#endregion

		/// <inheritdoc />
		public virtual void Initialize(string userId)
		{
			if (!string.IsNullOrEmpty(userId))
			{
				this.UserId = userId;
			}

			this.Initialize();
		}

		/// <inheritdoc />
		public virtual void LevelFail(int levelIndex) => this.LevelFail(levelPrefix + levelIndex);

		/// <inheritdoc />
		public virtual void LevelFail(string progression) =>
			this.NewProgressionEvent(SimAnalyticsProgressionStatus.Fail, progression);

		/// <inheritdoc />
		public virtual void LevelSuccess(int levelIndex) => this.LevelSuccess(levelPrefix + levelIndex);

		/// <inheritdoc />
		public virtual void LevelSuccess(string progression) =>
			this.NewProgressionEvent(SimAnalyticsProgressionStatus.Success, progression);

		/// <inheritdoc />
		public virtual void LevelStart(int levelIndex) => this.LevelStart(levelPrefix + levelIndex);

		/// <inheritdoc />
		public virtual void LevelStart(string progression) =>
			this.NewProgressionEvent(SimAnalyticsProgressionStatus.Start, progression);
		#endregion
		#endregion
	}
}
