﻿//-----------------------------------------------------------------------
// <copyright file="ISimConfigLoader.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.AppConfig
{
    using Simofun.Unity.Data.Configuration.AppConfig.Model;

    public interface ISimConfigLoader
    {
        #region Properties
        SimAppConfigBase Config { get; set; }
        #endregion

        #region Methods
        void LoadDefaults();
        #endregion
    }
}
