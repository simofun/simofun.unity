﻿//-----------------------------------------------------------------------
// <copyright file="ISimConfigurable.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.AppConfig
{
	using Simofun.Unity.Data.Configuration.AppConfig.Model;

	public interface ISimConfigurable
	{
		public ISimConfig Config { get; set; }
	}
}
