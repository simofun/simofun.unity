﻿//-----------------------------------------------------------------------
// <copyright file="SimAppPrefs.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.AppConfig.Model
{
	public enum SimAppPrefs
	{
		AppConfig
	}
}
