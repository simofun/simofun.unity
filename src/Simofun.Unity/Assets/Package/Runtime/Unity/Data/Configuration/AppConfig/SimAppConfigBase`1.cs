﻿//-----------------------------------------------------------------------
// <copyright file="SimAppConfigBase`1.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.AppConfig.Model
{
	public abstract class SimAppConfigBase<T> : SimAppConfigBase
	{
		#region Static Properties
		/// <summary>
		/// Gets singleton instance.
		/// </summary>
		public static T Instance { get; set; }
		#endregion
	}
}
