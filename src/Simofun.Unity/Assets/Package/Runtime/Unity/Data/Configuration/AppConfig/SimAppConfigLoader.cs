﻿//-----------------------------------------------------------------------
// <copyright file="SimAppConfigLoader.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.AppConfig
{
	using Simofun.Unity.Data.Configuration.AppConfig.Model;
	using Simofun.Unity.Data.Extensions;
	using Simofun.Utils;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Reflection;
	using UnityEngine;

	public class SimAppConfigLoader : MonoBehaviour
	{
		#region Fields
		static readonly List<SimAppConfigBase> configs = new List<SimAppConfigBase>();
		#endregion

		#region Public Static Methods
		public static IEnumerable<SimAppConfigBase> GetConfigs()
		{
			var appConfigLoaders = FindAnyObjectByType<SimAppConfigLoader>()
				.GetComponents<ISimConfigLoader>().ToList();

			foreach (var configType in SimTypeUtil.GetChildrenTypes<SimAppConfigBase>()
				         .Where(t => !t.IsAbstract && !t.IsGenericType))
			{
				var cfg = SimAppPrefs.AppConfig.GetObject(configType);
				if (cfg != null)
				{
					foreach (var configLoader in appConfigLoaders)
					{
						configLoader.Config = cfg as SimAppConfigBase;
					}
				}
				else
				{
					cfg = Activator.CreateInstance(configType);
					foreach (var configLoader in appConfigLoaders)
					{
						configLoader.Config = cfg as SimAppConfigBase;
						configLoader.LoadDefaults();
					}
				}

				yield return cfg as SimAppConfigBase;
			}
		}
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Awake() => LoadConfigs();

		/// <inheritdoc />
		protected virtual void OnApplicationPause(bool pause)
		{
			if (!pause)
			{
				return;
			}

			this.SaveConfigs();
		}

#if UNITY_EDITOR
		/// <inheritdoc />
		protected virtual void OnApplicationQuit() => this.SaveConfigs();
#endif
		#endregion

		#region Methods
		void LoadConfigs()
		{
			foreach (var config in GetConfigs())
			{
				foreach (var prop in config
					.GetType()
					.GetProperties(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy))
				{
					prop.SetValue(config, config);
				}

				if (!configs.Contains(config))
				{
					configs.Add(config);
				}
			}
		}

		void SaveConfigs() => configs.ForEach(c => SimAppPrefs.AppConfig.SetObject(c));
		#endregion
	}
}
