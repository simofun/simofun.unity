﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tonguç</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset.Extensions
{
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using Simofun.Unity.Data.Configuration.CustomAsset.Utils;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;

	/// <summary>
	/// Extensions for custom asset config classes.
	/// </summary>
	public static class SimCustomAssetConfigExt
	{
		#region Public Methods
		public static IEnumerable<T> IgnoreDefaults<T>(this IEnumerable<T> src) where T : ISimCustomAssetConfig =>
				src.Where(config => !(config as Object).name.Equals(SimCustomAssetConfigUtils.DefaultConfigName));

		public static IEnumerable<T> IgnoreDefaultsByBase<T>(this IEnumerable<T> src) where T: SimCustomAssetConfigBase
			=> src.Where(config => !config.name.Equals(SimCustomAssetConfigUtils.DefaultConfigName));

		public static void Save<T>(this T src) where T : ISimCustomAssetConfig =>
			SimCustomAssetConfigSaveService.Instance.Save(src);
		#endregion
	}
}
