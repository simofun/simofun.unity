﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigLoadServiceConfigJsonWriter.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset.IO
{
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using Simofun.Unity.Data.Configuration.CustomAsset.Serialization;
	using System.Runtime.Serialization.Formatters.Binary;

	public class SimCustomAssetConfigLoadServiceConfigJsonWriter
	{
		#region Static Fields
		static readonly BinaryFormatter binaryFormatter = new();
		#endregion

		#region Constructors
		public SimCustomAssetConfigLoadServiceConfigJsonWriter(ISimCustomAssetConfigLoadServiceConfig config)
		{
			this.Config = config;

			this.JsonConverter = new SimCustomAssetConfigLoadServiceConfigJsonConverter(this.Config);
			this.Writer = new SimCustomAssetConfigLoadServiceConfigWriter(this.Config);
		}
		#endregion

		#region Properties
		protected ISimCustomAssetConfigLoadServiceConfig Config { get; }

		protected SimCustomAssetConfigLoadServiceConfigJsonConverter JsonConverter { get; }

		protected SimCustomAssetConfigLoadServiceConfigWriter Writer { get; }
		#endregion

		#region Public Methods
		public void Write(string path, object value) => this.Write(path, value, this.Config.IsBinary, binaryFormatter);

		public void Write(string path, object value, bool isBinary) =>
			this.Write(path, value, isBinary, binaryFormatter);

		public void Write(string path, object value, bool isBinary, BinaryFormatter binaryFormatter) =>
			this.Writer.Write(path, this.JsonConverter.SerializeObject(value), isBinary, binaryFormatter);

		public void Write(string path, object value, BinaryFormatter binaryFormatter) =>
			this.Write(path, value, this.Config.IsBinary, binaryFormatter);
		#endregion
	}
}
