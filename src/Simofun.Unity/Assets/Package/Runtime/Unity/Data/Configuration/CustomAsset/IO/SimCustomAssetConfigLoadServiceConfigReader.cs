﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigLoadServiceConfigReader.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset.IO
{
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using System.IO;
	using System.Runtime.Serialization.Formatters.Binary;

	public class SimCustomAssetConfigLoadServiceConfigReader
	{
		#region Static Fields
		static readonly BinaryFormatter binaryFormatter = new();
		#endregion

		#region Constructors
		public SimCustomAssetConfigLoadServiceConfigReader(ISimCustomAssetConfigLoadServiceConfig config)
		{
			this.Config = config;
		}
		#endregion

		#region Properties
		protected ISimCustomAssetConfigLoadServiceConfig Config { get; }
		#endregion

		#region Public Methods
		public string Read(string path) => this.Read(path, this.Config.IsBinary, binaryFormatter);

		public string Read(string path, bool isBinary) => this.Read(path, isBinary, binaryFormatter);

		public string Read(string path, bool isBinary, BinaryFormatter binaryFormatter)
		{
			if (!isBinary)
			{
				return File.ReadAllText(path);
			}

			var file = File.Open(path, FileMode.Open);
			var res = binaryFormatter.Deserialize(file) as string;
			file.Close();

			return res;
		}

		public string Read(string path, BinaryFormatter binaryFormatter) =>
			this.Read(path, this.Config.IsBinary, binaryFormatter);
		#endregion
	}
}
