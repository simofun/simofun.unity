﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigLoadServiceConfigWriter.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset.IO
{
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using System.IO;
	using System.Runtime.Serialization.Formatters.Binary;

	public class SimCustomAssetConfigLoadServiceConfigWriter
	{
		#region Static Fields
		static readonly BinaryFormatter binaryFormatter = new();
		#endregion

		#region Constructors
		public SimCustomAssetConfigLoadServiceConfigWriter(ISimCustomAssetConfigLoadServiceConfig config)
		{
			this.Config = config;
		}
		#endregion

		#region Properties
		protected ISimCustomAssetConfigLoadServiceConfig Config { get; }
		#endregion

		#region Public Methods
		public void Write(string path, string value) => this.Write(path, value, this.Config.IsBinary, binaryFormatter);

		public void Write(string path, string value, bool isBinary) =>
			this.Write(path, value, isBinary, binaryFormatter);

		public void Write(string path, string value, bool isBinary, BinaryFormatter binaryFormatter)
		{
			var fileInfo = new FileInfo(path);
			if (!fileInfo.Directory.Exists)
			{
				fileInfo.Directory.Create();
			}

			if (isBinary)
			{
				var file = fileInfo.Create();
				binaryFormatter.Serialize(file, value);
				file.Close();

				return;
			}

			File.WriteAllText(path, value);

#if UNITY_EDITOR
			if (this.Config.DataPath != Data.Model.SimAppDataPath.PersistentDataPath)
			{
				UnityEditor.AssetDatabase.Refresh();
			}
#endif
		}

		public void Write(string path, string value, BinaryFormatter binaryFormatter) =>
			this.Write(path, value, this.Config.IsBinary, binaryFormatter);
		#endregion
	}
}
