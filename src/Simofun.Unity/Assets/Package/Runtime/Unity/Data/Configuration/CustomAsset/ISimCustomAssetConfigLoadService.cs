﻿//-----------------------------------------------------------------------
// <copyright file="ISimCustomAssetConfigLoadService.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset
{
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using Simofun.Unity.LifeCycle;
	using System.Collections.Generic;

	public interface ISimCustomAssetConfigLoadService : ISimInitializable, ISimNamable, ISimResettable
	{
		#region Properties
		ISimCustomAssetConfigLoadServiceConfig Config { get; }

		HashSet<SimCustomAssetConfigBase> Configs { get; }
		#endregion

		#region Methods
		void Load();

		void Refresh();
		#endregion
	}
}
