﻿//-----------------------------------------------------------------------
// <copyright file="ISimCustomAssetConfigLocator.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset
{
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using System.Collections.Generic;

	public interface ISimCustomAssetConfigLocator : ISimInitializable, ISimNamable
	{
		#region Methods
		T Get<T>() where T : SimCustomAssetConfigBase;

		IEnumerable<T> GetAll<T>() where T : SimCustomAssetConfigBase;
		#endregion
	}
}
