﻿//-----------------------------------------------------------------------
// <copyright file="ISimCustomAssetConfigSaveService.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset
{
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;

	public interface ISimCustomAssetConfigSaveService : ISimInitializable, ISimNamable
	{
		#region Methods
		void DeleteAllSave();

		void HardReset();

		void Save(ISimCustomAssetConfig config);

		void SaveAll();
		#endregion
	}
}
