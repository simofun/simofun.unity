﻿//-----------------------------------------------------------------------
// <copyright file="ISimCustomAssetConfigurable.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset
{
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;

	public interface ISimCustomAssetConfigurable
	{
		public ISimCustomAssetConfig Config { get; set; }
	}
}
