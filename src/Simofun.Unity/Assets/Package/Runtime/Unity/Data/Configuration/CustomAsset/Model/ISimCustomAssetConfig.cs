﻿//-----------------------------------------------------------------------
// <copyright file="ISimCustomAssetConfig.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset.Model
{
	using Simofun.Unity.Data.Configuration.AppConfig.Model;
	using Simofun.Unity.LifeCycle;

	public interface ISimCustomAssetConfig : ISimConfig, ISimInitializable, ISimResettable
	{
		#region Events
		event System.Action<SimCustomAssetConfigBase> OnEditorValidation;
		#endregion

		#region Properties
		bool IsDefault { get; }
		
		string DefaultName { get; }
		#endregion
	}
}
