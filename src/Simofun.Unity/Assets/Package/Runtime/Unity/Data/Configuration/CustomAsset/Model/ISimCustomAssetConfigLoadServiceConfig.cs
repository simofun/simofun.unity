﻿//-----------------------------------------------------------------------
// <copyright file="ISimCustomAssetConfigLoadServiceConfig.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset.Model
{
	using Simofun.IO;
	using Simofun.Unity.Data.Configuration.Model;
	using Simofun.Unity.Data.Model;
	using UnityEngine;

	public interface ISimCustomAssetConfigLoadServiceConfig : ISimRootPath
	{
		#region Properties
		public bool IsBinary { get; }

		public SimAppDataPath DataPath { get; }

		public SimJsonSerializerType JsonSerializerType { get; }

		public string Directory { get; }

		public string FileExtension { get; }
		#endregion

		#region Public Methods
		string GetFilePath(Object obj, params string[] addons);
		#endregion
	}
}
