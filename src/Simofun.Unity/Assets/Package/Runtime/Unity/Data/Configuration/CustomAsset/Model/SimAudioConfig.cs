﻿//-----------------------------------------------------------------------
// <copyright file="SimAudioConfig.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

using UniRx;

namespace Simofun.Unity.Data.Configuration.CustomAsset.Model
{
	using Newtonsoft.Json;
	using Simofun.Unity.Data.Configuration.CustomAsset.Extensions;
	using Sirenix.OdinInspector;
	using UnityEngine;

	[CreateAssetMenu(
		fileName = nameof(SimAudioConfig),
		menuName = SimMenuItems.Path_ + nameof(Unity) + "/" + nameof(Data) + "/" + nameof(Configuration) + "/"
					 + nameof(CustomAsset) + "/" + nameof(Model) + "/" + nameof(SimAudioConfig))]
	public class SimAudioConfig : SimCustomAssetConfigBase
	{
		#region Properties
		[field: JsonProperty, SerializeField, Title(nameof(SimAudioConfig), "Settings")]
		[JsonIgnore]
		public virtual BoolReactiveProperty IsEnabled { get; set; } = new(true);
		#endregion

		#region Protected Methods
		#region Event Handlers
		#region Initialization
		protected override void RegisterEventHandlers()
		{
			this.IsEnabled.Subscribe((v) => this.Save()).AddTo(this.Disposables);
		}
		#endregion
		#endregion
		#endregion
	}
}
