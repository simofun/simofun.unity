﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigBase.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tonguç</author>
//-----------------------------------------------------------------------

using UniRx;

namespace Simofun.Unity.Data.Configuration.CustomAsset.Model
{
	using Newtonsoft.Json;
	using Simofun.Unity.Data.Configuration.CustomAsset.Utils;
	using Sirenix.OdinInspector;
	using UnityEngine;

	public abstract class SimCustomAssetConfigBase : ScriptableObject, ISimCustomAssetConfig
	{
		#region Fields
		readonly CompositeDisposable disposables = new();
		#endregion

		#region Events
		public event System.Action<SimCustomAssetConfigBase> OnEditorValidation;
		#endregion

		#region Properties
		[JsonIgnore]
		/// <inheritdoc />
		public virtual bool IsAutoInitialized { get; set; } = true;

		[JsonIgnore]
		/// <inheritdoc />
		public virtual bool IsDefault => this.name == this.DefaultName;

		[JsonIgnore]
		/// <inheritdoc />
		public virtual bool IsInitialized { get; protected set; }

		[JsonIgnore]
		/// <inheritdoc />
		public virtual string DefaultName => SimCustomAssetConfigUtils.DefaultConfigName;
		#endregion

		#region Protected Properties
		[JsonIgnore]
		protected bool IsRegisteredEventHandlers { get; set; }

		[JsonIgnore]
		protected CompositeDisposable Disposables => this.disposables;
		#endregion

		#region Unity Methods
		#region Editor
		/// <inheritdoc />
		protected virtual void OnValidate()
		{
			if (!this.IsDefault)
			{
				this.OnEditorValidation?.Invoke(this);
			}
		}
		#endregion

		/// <inheritdoc />
		protected virtual void OnDisable()
		{
			if (this.IsDefault)
			{
				return;
			}

			if (!this.IsRegisteredEventHandlers)
			{
				return;
			}

			this.UnRegisterEventHandlers();
			this.IsRegisteredEventHandlers = false;
		}

		/// <inheritdoc />
		public virtual void Reset()
		{
#if UNITY_EDITOR
			if (!this.IsDefault)
			{
				this.LoadDefaults();
			}
#endif
		}
		#endregion

		#region Public Methods
		#region Initialization
		/// <inheritdoc />
		public virtual void Initialize()
		{
			if (this.IsDefault)
			{
				return;
			}

			if (this.IsRegisteredEventHandlers)
			{
				this.UnRegisterEventHandlers();
			}

			this.RegisterEventHandlers();
			this.IsRegisteredEventHandlers = true;
			this.IsInitialized = true;
		}
		#endregion

		[PropertyOrder(-1), ButtonGroup("Editor", VisibleIf = "@this.name != \"Default\"")]
		public virtual void LoadDefaults()
		{
#if UNITY_EDITOR
			Editor.Data.Configuration.CustomAsset.Utils.SimCustomAssetConfigEditorUtils.LoadDefaultsIfDifferent(this);
#endif
		}
		#endregion

		#region Protected Methods
		#region Event Handlers
		#region Initialization
		protected virtual void RegisterEventHandlers()
		{
		}
		
		protected virtual void UnRegisterEventHandlers() => this.Disposables.Clear();
		#endregion
		#endregion
		#endregion

		#region Methods
#if UNITY_EDITOR
		#region Editor
		[PropertyOrder(-1), ButtonGroup("Editor")]
		void UpdateDefaults()
		{
			Editor.Data.Configuration.CustomAsset.Utils.SimCustomAssetConfigEditorUtils.UpdateDefaults(this);
		}
		#endregion
#endif
		#endregion
	}
}
