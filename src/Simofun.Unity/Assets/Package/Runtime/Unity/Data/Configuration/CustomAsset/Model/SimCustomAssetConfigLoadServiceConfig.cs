﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetLoadConfigServiceConfig.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tonguç</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset.Model
{
	using UnityEngine;

	[CreateAssetMenu(
		fileName = nameof(SimCustomAssetConfigLoadServiceConfig),
		menuName = SimMenuItems.Path_ + nameof(Unity) + "/" + nameof(Data) + "/" + nameof(Configuration) + "/"
					+ nameof(CustomAsset) + "/" + nameof(Model) + "/"
					+ nameof(SimCustomAssetConfigLoadServiceConfig))]
	public class SimCustomAssetConfigLoadServiceConfig : SimCustomAssetConfigLoadServiceConfigBase
	{
	}
}
