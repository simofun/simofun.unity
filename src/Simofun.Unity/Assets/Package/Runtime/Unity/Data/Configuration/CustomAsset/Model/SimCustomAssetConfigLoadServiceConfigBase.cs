﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigServiceConfigBase.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset.Model
{
	using Simofun.Unity.Data.Configuration.Model;
	using Simofun.Unity.Data.Model;
	using Sirenix.OdinInspector;
	using System.IO;
	using UnityEngine;

	public abstract class SimCustomAssetConfigLoadServiceConfigBase: ScriptableObject,
		ISimCustomAssetConfigLoadServiceConfig
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimCustomAssetConfigLoadServiceConfig), "Settings")]
		SimAppDataPath dataPath = SimAppDataPath.PersistentDataPath;

		[SerializeField]
		SimJsonSerializerType jsonSerializerType = SimJsonSerializerType.JsonNet;

		[SerializeField]
		bool isBinary = true;

		[SerializeField]
		string[] directoryNames = new string[] { "Data", "Configuration" };

		[SerializeField]
		string fileExtension = ".cache";
		#endregion

		#region Fields
		string directory;

		string rootPath;
		#endregion

		#region Properties
		/// <inheritdoc />
		public virtual bool IsBinary => this.isBinary;

		/// <inheritdoc />
		public virtual SimAppDataPath DataPath => this.dataPath;

		/// <inheritdoc />
		public virtual SimJsonSerializerType JsonSerializerType => this.jsonSerializerType;

		/// <inheritdoc />
		public virtual string Directory =>
			!string.IsNullOrEmpty(this.directory)
				? this.directory
				: (this.directory = Path.Combine(this.directoryNames));

		/// <inheritdoc />
		public virtual string FileExtension => this.fileExtension;

		/// <inheritdoc />
		public virtual string RootPath
		{
			get
			{
				if (!string.IsNullOrEmpty(this.rootPath))
				{
					return this.rootPath;
				}

				switch (this.DataPath)
				{
					case SimAppDataPath.Default:
						{
							return this.rootPath = Path.Combine(Application.dataPath, this.Directory);
						}
					case SimAppDataPath.PersistentDataPath:
						{
							return this.rootPath = Path.Combine(Application.persistentDataPath, this.Directory);
						}
					case SimAppDataPath.Resources:
					default:
						{
							return this.rootPath = null;
						}
				}
			}

			set { this.rootPath = value; }
		}
		#endregion

		#region Public Methods
		public virtual string GetFilePath(Object obj, params string[] addons) =>
			Path.Combine(
				this.RootPath,
				$"{obj.name}_{obj.GetInstanceID()}_{string.Join("_", addons)}{this.FileExtension}");
		#endregion
	}
}
