﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigLoadServiceConfigJsonConverter.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset.Serialization
{
	using Newtonsoft.Json;
	using Simofun.JsonNet.Unity.Serialization;
	using Simofun.Unity.Data.Configuration.CustomAsset.IO;
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using Simofun.Unity.Data.Configuration.Model;
	using UnityEngine;

	public class SimCustomAssetConfigLoadServiceConfigJsonConverter
	{
		#region Constructors
		public SimCustomAssetConfigLoadServiceConfigJsonConverter(ISimCustomAssetConfigLoadServiceConfig config)
		{
			this.Config = config;

			this.ConfigReader = new SimCustomAssetConfigLoadServiceConfigReader(config);
		}
		#endregion

		#region Properties
		protected ISimCustomAssetConfigLoadServiceConfig Config { get; }

		protected SimCustomAssetConfigLoadServiceConfigReader ConfigReader { get; }
		#endregion

		#region Public Methods
		public void PopulateObject(string json, object objectToOverwrite) =>
			this.PopulateObject(json, objectToOverwrite, this.Config.JsonSerializerType);

		public void PopulateObject(string json, object objectToOverwrite, SimJsonSerializerType jsonSerializerType)
		{
			switch (jsonSerializerType)
			{
				case SimJsonSerializerType.Unity:
					{
						JsonUtility.FromJsonOverwrite(json, objectToOverwrite);

						return;
					}
				case SimJsonSerializerType.JsonNet:
					{
						var settings = JsonConvert.DefaultSettings?.Invoke();
						var objectCreationHandling = settings.ObjectCreationHandling;
						settings.ObjectCreationHandling = ObjectCreationHandling.Replace;

						try
						{
							JsonConvert.PopulateObject(json, objectToOverwrite, settings);
						}
						catch (System.Exception ex)
						{
							Debug.LogException(ex, objectToOverwrite as Object);
						}
						finally
						{
							settings.ObjectCreationHandling = objectCreationHandling;
						}

						return;
					}
				default:
					{
						return;
					}
			}
		}

		public void PopulateObjectAtPath(string path, object objectToOverwrite) =>
			this.PopulateObjectAtPath(path, objectToOverwrite, this.Config.JsonSerializerType);

		public void PopulateObjectAtPath(
			string path, object objectToOverwrite, SimJsonSerializerType jsonSerializerType) =>
				this.PopulateObject(this.ConfigReader.Read(path), objectToOverwrite, jsonSerializerType);

		public string SerializeObject(object value) => this.SerializeObject(value, this.Config.JsonSerializerType);

		public string SerializeObject(object value, SimJsonSerializerType jsonSerializerType)
		{
			switch (jsonSerializerType)
			{
				case SimJsonSerializerType.Unity:
					{
						return JsonUtility.ToJson(value);
					}
				case SimJsonSerializerType.JsonNet:
					{
#if UNITY_EDITOR
						var settings = JsonConvert.DefaultSettings?.Invoke();
						if (settings != null
								&& settings.ContractResolver != null
								&& settings.ContractResolver is not SimUnityTypeContractResolver)
						{
							settings.ContractResolver = new SimUnityTypeContractResolver();
						}
#endif
						return JsonConvert.SerializeObject(value);
					}
				default:
					{
						return null;
					}
			}
		}
		#endregion
	}
}
