﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigLoadServiceBase`1.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset
{
	public abstract class SimCustomAssetConfigLoadServiceBase<T>
		: SimCustomAssetConfigLoadServiceBase<T, T> where T : SimCustomAssetConfigLoadServiceBase<T>, new()
	{
		#region Protected Constructors
		protected SimCustomAssetConfigLoadServiceBase() : base()
		{
		}
		#endregion
	}
}
