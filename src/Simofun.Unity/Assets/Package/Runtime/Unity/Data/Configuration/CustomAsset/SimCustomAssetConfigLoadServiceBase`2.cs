﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigLoadServiceBase`2.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset
{
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using Simofun.Unity.Data.Configuration.CustomAsset.Serialization;
	using Simofun.Unity.Data.Configuration.CustomAsset.Utils;
	using Simofun.Unity.DesignPatterns.Singleton;
	using Sirenix.OdinInspector;
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using UnityEngine;
	using UnityEngine.SceneManagement;

	public abstract class SimCustomAssetConfigLoadServiceBase<TInstance, TConcrete>
		: SimGlobalSingletonBase<TInstance, TConcrete>, ISimCustomAssetConfigLoadService
		where TConcrete : SimCustomAssetConfigLoadServiceBase<TInstance, TConcrete>, TInstance, new()
	{
		#region Unity Fields
		[SerializeField,
			Title(nameof(SimCustomAssetConfigLoadService), "Settings"),
			ValueDropdown(nameof(SelectConfigs))]
		SimCustomAssetConfigLoadServiceConfigBase config;
		#endregion

		#region Fields
		int initializedSceneBuildIndex = -1;

		SimCustomAssetConfigLoadServiceConfigJsonConverter configJsonConverter;
		#endregion

		#region Properties
		/// <inheritdoc />
		public virtual HashSet<SimCustomAssetConfigBase> Configs { get; protected set; }
			= new HashSet<SimCustomAssetConfigBase>();

		/// <inheritdoc />
		public virtual ISimCustomAssetConfigLoadServiceConfig Config
		{
			get => this.config != null ? this.config : (this.config = SimCustomAssetConfigUtils.LoadServiceConfig());
			set => this.config = value as SimCustomAssetConfigLoadServiceConfigBase;
		}
		#endregion

		#region Protected Properties
		protected SimCustomAssetConfigLoadServiceConfigJsonConverter ConfigJsonConverter
		{
			get => this.configJsonConverter != null
						? this.configJsonConverter
						: (this.configJsonConverter
								= new SimCustomAssetConfigLoadServiceConfigJsonConverter(this.Config));
			set => this.configJsonConverter = value;
		}
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		public virtual void Reset() => this.Foreach((ISimCustomAssetConfig config, string path) => config.Reset());

		/// <inheritdoc />
		protected override void OnDestroy()
		{
			this.UnRegisterEventHandlers();
			
			base.OnDestroy();
		}
		#endregion

		#region Public Methods
		#region ISimInitializable Methods
		/// <inheritdoc />
		public override void Initialize()
		{
			if (!Application.isPlaying)
			{
				base.Initialize();

				return;
			}

			this.Load();

			this.initializedSceneBuildIndex = SceneManager.GetActiveScene().buildIndex;
			this.RegisterEventHandlers();

			base.Initialize();
		}
		#endregion

		/// <inheritdoc />
		public virtual void Load() => this.LoadInternal(this.GetConfigs());

		/// <inheritdoc />
		public virtual void Refresh() => this.LoadInternal(this.GetLoadedConfigs());
		#endregion

		#region Protected Methods
		#region Event Handlers
		#region Initialization
		protected virtual void RegisterEventHandlers() => SceneManager.sceneLoaded += this.HandleOnSceneLoaded;

		protected virtual void UnRegisterEventHandlers() => SceneManager.sceneLoaded -= this.HandleOnSceneLoaded;
		#endregion

		protected virtual void HandleOnSceneLoaded(Scene scene, LoadSceneMode mode)
		{
			if (scene.buildIndex != this.initializedSceneBuildIndex)
			{
				this.Refresh();
			}
		}
		#endregion

		protected virtual void LoadInternal(IEnumerable<ISimCustomAssetConfig> configs) =>
			this.ForeachInternal(
				configs,
				(ISimCustomAssetConfig config, string path) =>
				{
					if (!File.Exists(path))
					{
						config.Initialize();
						
						return;
					}

					this.ConfigJsonConverter.PopulateObjectAtPath(path, config);
					config.Initialize();
				});
		#endregion

		#region Internal Methods
		internal IEnumerable<ISimCustomAssetConfig> GetConfigs() =>
			SimCustomAssetConfigUtils.GetConfigs().Where(cfg => this.Configs.Add(cfg));

		internal IEnumerable<ISimCustomAssetConfig> GetLoadedConfigs() =>
			SimCustomAssetConfigUtils.GetLoadedConfigs().Where(cfg => this.Configs.Add(cfg));

		internal IEnumerable<ISimCustomAssetConfig> GetResourcesConfigs() =>
			SimCustomAssetConfigUtils.GetResourcesConfigs().Where(cfg => this.Configs.Add(cfg));

		internal void Foreach(System.Action<ISimCustomAssetConfig, string> onConfig) =>
			this.ForeachInternal(this.GetConfigs(), onConfig);

		internal void ForeachInternal(
			IEnumerable<ISimCustomAssetConfig> configs, System.Action<ISimCustomAssetConfig, string> onConfig)
		{
			foreach (var cfg in configs)
			{
				if (cfg != null)
				{
					onConfig?.Invoke(cfg, this.Config.GetFilePath(cfg as Object));
				}
			}
		}
		#endregion

		#region Methods
		System.Collections.IEnumerable SelectConfigs() =>
			Resources.LoadAll<SimCustomAssetConfigLoadServiceConfigBase>("Data/Configuration/");
		#endregion
	}
}
