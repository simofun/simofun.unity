﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigLocator.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset
{
	public class SimCustomAssetConfigLocator
		: SimCustomAssetConfigLocatorBase<ISimCustomAssetConfigLocator, SimCustomAssetConfigLocator>
	{
	}
}
