﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigLocatorBase`1.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset
{
	public abstract class SimCustomAssetConfigLocatorBase<T>
		: SimCustomAssetConfigLocatorBase<T, T> where T : SimCustomAssetConfigLocatorBase<T>, new()
	{
		#region Protected Constructors
		protected SimCustomAssetConfigLocatorBase() : base()
		{
		}
		#endregion
	}
}
