﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigLocatorBase`2.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset
{
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using Simofun.Unity.DesignPatterns.Singleton;
	using System.Collections.Generic;
	using System.Linq;

	public abstract class SimCustomAssetConfigLocatorBase<TInstance, TConcrete>
		: SimGlobalSingletonBase<TInstance, TConcrete>, ISimCustomAssetConfigLocator
		where TConcrete : SimCustomAssetConfigLocatorBase<TInstance, TConcrete>, TInstance, new()
	{
		#region Fields
		ISimCustomAssetConfigLoadService customAssetConfigLoadService;
		#endregion

		#region Protected Properties
		protected virtual ISimCustomAssetConfigLoadService CustomAssetConfigLoadService
		{
			get => this.customAssetConfigLoadService != null
						? this.customAssetConfigLoadService
						: (this.customAssetConfigLoadService = SimCustomAssetConfigLoadService.Instance);
			set => this.customAssetConfigLoadService = value;
		}
		#endregion

		#region Public Methods
		/// <inheritdoc />
		public virtual T Get<T>() where T : SimCustomAssetConfigBase
		{
			var type = typeof(T);

			return (T)(this.CustomAssetConfigLoadService.Configs.FirstOrDefault(c => type.IsInstanceOfType(c)));
		}

		/// <inheritdoc />
		public virtual IEnumerable<T> GetAll<T>() where T : SimCustomAssetConfigBase
		{
			var type = typeof(T);

			return this.CustomAssetConfigLoadService.Configs
				.Where(config => type.IsInstanceOfType(config))
				.Select(config => (T)config);
		}
		#endregion
	}
}
