﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigSaveServiceBase`1.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset
{
	public abstract class SimCustomAssetConfigSaveServiceBase<T>
		: SimCustomAssetConfigSaveServiceBase<T, T> where T : SimCustomAssetConfigSaveServiceBase<T>, new()
	{
		#region Protected Constructors
		protected SimCustomAssetConfigSaveServiceBase() : base()
		{
		}
		#endregion
	}
}
