﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigSaveServiceBase`2.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset
{
	using Simofun.Unity.Data.Configuration.CustomAsset.IO;
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using Simofun.Unity.DesignPatterns.Singleton;
	using System.IO;
	using UnityEngine;

	public abstract class SimCustomAssetConfigSaveServiceBase<TInstance, TConcrete>
		: SimGlobalSingletonBase<TInstance, TConcrete>, ISimCustomAssetConfigSaveService
		where TConcrete : SimCustomAssetConfigSaveServiceBase<TInstance, TConcrete>, TInstance, new()
	{
		#region Fields
		SimCustomAssetConfigLoadService loadService;

		SimCustomAssetConfigLoadServiceConfigJsonWriter configJsonWriter;
		#endregion

		#region Protected Properties
		protected SimCustomAssetConfigLoadService LoadService
		{
			get => this.loadService != null
						? this.loadService
						: (this.loadService = this.GetComponent<SimCustomAssetConfigLoadService>());
			set => this.loadService = value;
		}

		protected SimCustomAssetConfigLoadServiceConfigJsonWriter ConfigJsonWriter
		{
			get => this.configJsonWriter != null
						? this.configJsonWriter
						: (this.configJsonWriter = new SimCustomAssetConfigLoadServiceConfigJsonWriter(this.LoadService.Config));
			set => this.configJsonWriter = value;
		}
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected override void OnDestroy()
		{
#if UNITY_EDITOR
			this.SaveAll();
#endif
		}
		#endregion

		#region Public Methods
		/// <inheritdoc />
		public virtual void DeleteAllSave()
		{
			foreach (var config in this.LoadService.GetConfigs())
			{
				File.Delete(this.LoadService.Config.GetFilePath(config as Object));
			}
		}

		/// <inheritdoc />
		public virtual void HardReset()
		{
			this.DeleteAllSave();
			this.LoadService.Reset();
		}

		/// <inheritdoc />
		public virtual void Save(ISimCustomAssetConfig config) =>
			this.ConfigJsonWriter.Write(this.LoadService.Config.GetFilePath(config as Object), config);

		/// <inheritdoc />
		public virtual void SaveAll()
		{
			var config = this.LoadService.Config;
			
			this.LoadService.Foreach(
				(ISimCustomAssetConfig cfg, string path) =>
					this.ConfigJsonWriter.Write(config.GetFilePath(cfg as Object), cfg));
		}
		#endregion
	}
}
