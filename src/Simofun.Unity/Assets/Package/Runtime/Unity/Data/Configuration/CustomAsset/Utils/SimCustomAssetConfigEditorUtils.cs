//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigEditorUtils.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Editor.Data.Configuration.CustomAsset.Utils
{
#if UNITY_EDITOR
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using Simofun.Unity.Data.Configuration.CustomAsset.Serialization;
	using Simofun.Unity.Data.Configuration.CustomAsset.Utils;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;

	public static class SimCustomAssetConfigEditorUtils
	{
		#region Properties
		public static SimCustomAssetConfigLoadServiceConfigBase Config
		{
			get
			{
				var configGuids = UnityEditor.AssetDatabase.FindAssets(
					$"t:{nameof(SimCustomAssetConfigLoadServiceConfig)}");
				if (configGuids == null || configGuids.Length == 0)
				{
					return null;
				}

				return UnityEditor.AssetDatabase.LoadAssetAtPath<SimCustomAssetConfigLoadServiceConfig>(
					UnityEditor.AssetDatabase.GUIDToAssetPath(configGuids.FirstOrDefault()));
			}
		}
		#endregion

		#region Public Methods
		public static bool IsDefaultsExist(SimCustomAssetConfigBase asset)
		{
			var isExist = GetDefault(asset) != null;
			if (!isExist)
			{
				CreateDefaults(asset);
			}

			return isExist;
		}

		public static Object GetDefault(Object source) =>
			UnityEditor.AssetDatabase.LoadAllAssetsAtPath(UnityEditor.AssetDatabase.GetAssetPath(source))
				.FirstOrDefault(asset => asset.name.Equals(SimCustomAssetConfigUtils.DefaultConfigName));

		public static T LoadFromPath<T>(string path) where T : ScriptableObject =>
			UnityEditor.AssetDatabase.LoadAssetAtPath<T>(path);

		public static T[] LoadAll<T>() where T : ScriptableObject =>
			LoadFromGuids<T>(UnityEditor.AssetDatabase.FindAssets(string.Format("t:{0}", nameof(ScriptableObject))));

		public static T[] LoadFromGuids<T>(string[] guids) where T : ScriptableObject
		{
			var objectList = new List<T>();
			foreach (var guid in guids)
			{
				var obj = LoadFromPath<T>(UnityEditor.AssetDatabase.GUIDToAssetPath(guid));
				if (obj == null)
				{
					continue;
				}

				objectList.Add(obj);
			}

			return objectList.ToArray();
		}

		public static T[] LoadSelected<T>() where T : ScriptableObject =>
			LoadFromGuids<T>(UnityEditor.Selection.assetGUIDs);

		public static void CheckDefaults(SimCustomAssetConfigBase asset) => IsDefaultsExist(asset);

		public static void Copy(Object source, Object destination)
		{
			if (destination == null || source == null)
			{
				return;
			}

			var assetName = destination.name;
			UnityEditor.EditorUtility.CopySerialized(source, destination);
			destination.name = assetName;
		}

		public static void CopyIfDifferent(Object source, Object destination)
		{
			if (destination == null || source == null)
			{
				return;
			}

			var config = SimCustomAssetConfigUtils.LoadServiceConfig();
			if (config == null)
			{
				// 'EditorUtility.CopySerializedIfDifferent' is not working.
				// 'EditorUtility.CopySerializedIfDifferent' is always same as 'EditorUtility.CopySerialized'
				var assetName = destination.name;
				destination.name = source.name; // Set 'source' file name to 'destination' to pass if different case
				UnityEditor.EditorUtility.CopySerializedIfDifferent(source, destination);
				destination.name = assetName;

				return;
			}

			var jsonConverter = new SimCustomAssetConfigLoadServiceConfigJsonConverter(config);
			var sourceJson = jsonConverter.SerializeObject(source)
				.Replace(
					$"\"name\":\"{SimCustomAssetConfigUtils.DefaultConfigName}\"",
					$"\"name\":\"{destination.name}\"");
			var destJson = jsonConverter.SerializeObject(destination);
			if (sourceJson != destJson)
			{
				Copy(source, destination);
			}
		}

		public static void CreateDefaults(SimCustomAssetConfigBase asset)
		{
			if (UnityEditor.EditorUtility.IsDirty(asset))
			{
				return;
			}

			var defaultInstance = ScriptableObject.CreateInstance(asset.GetType());
			UnityEditor.EditorUtility.CopySerialized(asset, defaultInstance);
			defaultInstance.name = SimCustomAssetConfigUtils.DefaultConfigName;

			UnityEditor.AssetDatabase.AddObjectToAsset(defaultInstance, asset);
			UnityEditor.AssetDatabase.SaveAssets();

			UnityEditor.EditorUtility.SetDirty(asset);
			UnityEditor.EditorUtility.SetDirty(defaultInstance);
		}

		public static void LoadDefaults(SimCustomAssetConfigBase asset) => Copy(GetDefault(asset), asset);

		public static void LoadDefaultsIfDifferent(SimCustomAssetConfigBase asset) =>
			CopyIfDifferent(GetDefault(asset), asset);

		public static void UpdateDefaults(SimCustomAssetConfigBase asset) => Copy(asset, GetDefault(asset));
		#endregion
	}
#endif
}
