//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigUtils.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset.Utils
{
	using Simofun.Unity.Data.Configuration.CustomAsset.Extensions;
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;

	public static class SimCustomAssetConfigUtils
	{
		#region Public Fields
		public const string DefaultConfigName = "Default";
		#endregion

		#region Public Methods
		public static SimCustomAssetConfigLoadServiceConfigBase LoadServiceConfig() =>
			Resources.Load<SimCustomAssetConfigLoadServiceConfigBase>(
				"Data/Configuration/SimCustomAssetConfigServiceConfig");

		public static IEnumerable<SimCustomAssetConfigBase> GetConfigs() =>
			GetResourcesConfigs().Concat(GetLoadedConfigs()).Distinct();

		public static IEnumerable<SimCustomAssetConfigBase> GetLoadedConfigs() =>
			Resources.FindObjectsOfTypeAll<SimCustomAssetConfigBase>().IgnoreDefaultsByBase();

		public static IEnumerable<SimCustomAssetConfigBase> GetResourcesConfigs() =>
			Resources.LoadAll<SimCustomAssetConfigBase>("Data/Configuration").IgnoreDefaultsByBase();
		#endregion
	}
}
