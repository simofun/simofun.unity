//-----------------------------------------------------------------------
// <copyright file="SimSerializableSprite.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.Model
{
	using Newtonsoft.Json;
	using UnityEngine;

	[System.Serializable]
	public class SimSerializableSprite
	{
		#region Unity Fields
		[JsonProperty, SerializeField]
		Rect rect;

		[JsonProperty, SerializeField]
		float pixelsPerUnit;

		[JsonProperty, SerializeField]
		Vector2 pivot;

		[JsonProperty, SerializeField]
		byte[] bytes;
		#endregion

		#region Fields
		[JsonIgnore]
		Sprite sprite;
		#endregion

		#region Properties
		[JsonIgnore]
		public Sprite Sprite
		{
			get
			{
				if (this.sprite != null && this.bytes != null && this.bytes.Length > 0)
				{
					return this.sprite;
				}

				if (this.bytes == null || this.bytes.Length <= 0)
				{
					return null;
				}

				var texture = new Texture2D(1, 1);
				texture.LoadImage(this.bytes);

				return this.sprite = Sprite.Create(texture, this.rect, this.pivot, this.pixelsPerUnit);
			}
			set
			{
				this.sprite = value;
				if (value == null)
				{
					return;
				}

				this.rect = this.sprite.rect;
				this.pixelsPerUnit = this.sprite.pixelsPerUnit;
				this.pivot = this.sprite.pivot;
				this.bytes = this.sprite.texture.EncodeToPNG();
			}
		}
		#endregion
	}
}
