﻿//-----------------------------------------------------------------------
// <copyright file="SimUnityPrefs.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.Prefs
{
	using System;
	using System.Globalization;
	using UnityEngine;

	public class SimUnityPrefs : PlayerPrefs
	{
		/// <summary>
		/// Helper method to retrieve a bool from PlayerPrefs (stored as an int)
		/// </summary>
		public static bool GetBool(string key, bool defaultValue = false)
		{
			// Use HasKey to check if the bool has been stored (as int defaults to 0 which is ambiguous with a stored False)
			if (!PlayerPrefs.HasKey(key))
			{
				return defaultValue;
			}

			var value = PlayerPrefs.GetInt(key);

			// As in C, assume zero is false and any non-zero value (including its intended 1) is true
			return value != 0;

			// No existing player pref value, so return defaultValue instead
		}

		public static bool GetBool<T>(T key, bool defaultValue = false) => GetBool(key.ToString(), defaultValue);

		public static bool HasKey<T>(T key) => PlayerPrefs.HasKey(key.ToString());

		/// <summary>
		/// Helper method to retrieve a DateTime from PlayerPrefs (stored as a string) and return a DateTime complete with
		/// timezone (works with UTC and local DateTimes)
		/// </summary>
		public static DateTime GetDateTime(string key, DateTime defaultValue = new DateTime())
		{
			// Fetch the string value from PlayerPrefs
			string stringValue = PlayerPrefs.GetString(key);

			return !string.IsNullOrEmpty(stringValue)
				? DateTime.Parse(stringValue, CultureInfo.InvariantCulture, DateTimeStyles.RoundtripKind)
				: defaultValue;
		}

		public static DateTime GetDateTime<T>(T key, DateTime defaultValue = new DateTime()) =>
			GetDateTime(key.ToString(), defaultValue);

		public static float GetFloat<T>(T key) => PlayerPrefs.GetFloat(key.ToString());

		public static float GetFloat<T>(T key, float defaultValue) =>
			PlayerPrefs.GetFloat(key.ToString(), defaultValue);

		public static int GetInt<T>(T key) => PlayerPrefs.GetInt(key.ToString());

		public static int GetInt<T>(T key, int defaultValue) => PlayerPrefs.GetInt(key.ToString(), defaultValue);

		/// <summary>
		/// Non-generic helper method to retrieve an enum value from PlayerPrefs (stored as a string). Default value must be
		/// passed, passing null will mean you need to do a null check where you call this method. Generally try to use the
		/// generic version of this method instead: GetEnum<T>
		/// </summary>
		public static object GetEnum(string key, Type enumType, object defaultValue)
		{
			var value = PlayerPrefs.GetString(key); // Fetch the string value from PlayerPrefs

			return !string.IsNullOrEmpty(value) ? Enum.Parse(enumType, value) : defaultValue;
		}

		public static object GetEnum<T>(T key, Type enumType, object defaultValue) =>
			GetEnum(key.ToString(), enumType, defaultValue);

		public static string GetString<T>(T key) => PlayerPrefs.GetString(key.ToString());

		public static string GetString<T>(T key, string defaultValue) =>
			PlayerPrefs.GetString(key.ToString(), defaultValue);

		/// <summary>
		/// Generic helper method to retrieve an enum value from PlayerPrefs and parse it from its stored string into the 
		/// specified generic type. This method should generally be preferred over the non-generic equivalent
		/// </summary>
		public static T GetEnum<T>(string key, T defaultValue = default(T)) where T : struct
		{
			var stringValue = PlayerPrefs.GetString(key); // Fetch the string value from PlayerPrefs

			if (!string.IsNullOrEmpty(stringValue))
			{
				// Existing value, so parse it using the supplied generic type and cast before returning it
				return (T)Enum.Parse(typeof(T), stringValue);
			}

			// No player pref for this, just return default. If no default is supplied this will be the enum's default
			return defaultValue;
		}

		public static TValue GetEnum<T, TValue>(T key, TValue defaultValue = default(TValue)) where TValue : struct =>
			GetEnum(key.ToString(), defaultValue);

		/// <summary>
		/// Helper method to retrieve a TimeSpan from PlayerPrefs (stored as a string)
		/// </summary>
		public static TimeSpan GetTimeSpan(string key, TimeSpan defaultValue = new TimeSpan())
		{
			var stringValue = PlayerPrefs.GetString(key); // Fetch the string value from PlayerPrefs

			return !string.IsNullOrEmpty(stringValue) ? TimeSpan.Parse(stringValue) : defaultValue;
		}

		public static TimeSpan GetTimeSpan<T>(T key, TimeSpan defaultValue = new TimeSpan()) =>
			GetTimeSpan(key.ToString(), defaultValue);

		public static void DeleteKey<T>(T key) => PlayerPrefs.DeleteKey(key.ToString());

		/// <summary>
		/// Helper method to store a bool in PlayerPrefs (stored as an int)
		/// </summary>
		public static void SetBool(string key, bool value) =>
			PlayerPrefs.SetInt(key, value ? 1 : 0); // Store the bool as an int (1 for true, 0 for false)

		public static void SetBool<T>(T key, bool value) => SetBool(key.ToString(), value);

		/// <summary>
		/// Helper method to store a DateTime (complete with its timezone) in PlayerPrefs as a string
		/// </summary>
		public static void SetDateTime(string key, DateTime value) =>
			PlayerPrefs.SetString(key, value.ToString("o", CultureInfo.InvariantCulture)); // Convert to an ISO 8601 compliant string ("o"), so that it's fully qualified, then store in PlayerPrefs

		public static void SetDateTime<T>(T key, DateTime value) => SetDateTime(key.ToString(), value);

		/// <summary>
		/// Helper method to store an enum value in PlayerPrefs (stored using the string name of the enum)
		/// </summary>
		public static void SetEnum(string key, Enum value) =>
			PlayerPrefs.SetString(key, value.ToString()); // Convert the enum value to its string name (as opposed to integer index) and store it in a string PlayerPref

		public static void SetEnum<T>(T key, Enum value) => SetEnum(key.ToString(), value);

		public static void SetFloat<T>(T key, float value) => PlayerPrefs.SetFloat(key.ToString(), value);

		public static void SetInt<T>(T key, int value) => PlayerPrefs.SetInt(key.ToString(), value);

		public static void SetString<T>(T key, string value) => PlayerPrefs.SetString(key.ToString(), value);

		/// <summary>
		/// Helper method to store a TimeSpan in PlayerPrefs as a string
		/// </summary>
		public static void SetTimeSpan(string key, TimeSpan value) =>
			PlayerPrefs.SetString(key, value.ToString()); // Use the TimeSpan's ToString() method to encode it as a string which is then stored in PlayerPrefs

		public static void SetTimeSpan<T>(T key, TimeSpan value) => SetTimeSpan(key.ToString(), value);
	}
}
