﻿//-----------------------------------------------------------------------
// <copyright file="SimEnumExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Extensions
{
	using Newtonsoft.Json;
	using Simofun.Unity.Data.Configuration.Prefs;
	using System;

	/// <summary>
	/// Extensions for <see cref="Enum"/> class.
	/// </summary>
	public static class SimEnumExt
	{
		public static bool GetBool(this Enum src, bool defaultValue = false) => SimUnityPrefs.GetBool(src, defaultValue);

		public static bool HasKey(this Enum src) => SimUnityPrefs.HasKey(src);

		public static DateTime GetDateTime(this Enum src, DateTime defaultValue = new DateTime()) =>
			SimUnityPrefs.GetDateTime(src, defaultValue);

		public static float GetFloat(this Enum src) => SimUnityPrefs.GetFloat(src);

		public static float GetFloat(this Enum src, float defaultValue) => SimUnityPrefs.GetFloat(src, defaultValue);

		public static int GetInt(this Enum src) => SimUnityPrefs.GetInt(src);

		public static int GetInt(this Enum src, int defaultValue) => SimUnityPrefs.GetInt(src, defaultValue);

		public static object GetEnum(this Enum src, Type enumType, object defaultValue) =>
			SimUnityPrefs.GetEnum(src, enumType, defaultValue);

		public static T GetEnum<T>(this Enum src, T defaultValue) where T : struct =>
			SimUnityPrefs.GetEnum(src, defaultValue);

		public static object GetObject(this Enum src, Type type) =>
			JsonConvert.DeserializeObject(SimUnityPrefs.GetString(src.ToString()), type);

		public static T GetObject<T>(this Enum src) => (T)src.GetObject(typeof(T));

		public static TimeSpan GetTimeSpan(this Enum src, TimeSpan defaultValue = new TimeSpan()) =>
			SimUnityPrefs.GetTimeSpan(src, defaultValue);

		public static void DeleteKey(this Enum src) => SimUnityPrefs.DeleteKey(src);

		public static void SetBool(this Enum src, bool value) => SimUnityPrefs.SetBool(src, value);

		public static void SetDateTime(this Enum src, DateTime value) => SimUnityPrefs.SetDateTime(src, value);

		public static void SetEnum(this Enum src, Enum value) => SimUnityPrefs.SetEnum(src, value);

		public static void SetFloat(this Enum src, float value) => SimUnityPrefs.SetFloat(src, value);

		public static void SetInt(this Enum src, int value) => SimUnityPrefs.SetInt(src, value);

		public static void SetObject(this Enum src, object value) =>
			SimUnityPrefs.SetString(src.ToString(), JsonConvert.SerializeObject(value));

		public static void SetString(this Enum src, string value) => SimUnityPrefs.SetString(src, value);

		public static void SetTimeSpan(this Enum src, TimeSpan value) => SimUnityPrefs.SetTimeSpan(src, value);
	}
}
