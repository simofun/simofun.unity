﻿//-----------------------------------------------------------------------
// <copyright file="SimAppDataPath.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Model
{
	public enum SimAppDataPath
	{
		/// <summary>
		/// 'Application.dataPath'
		/// </summary>
		Default,

		/// <summary>
		/// 'Application.persistentDataPath'
		/// </summary>
		PersistentDataPath,

		/// <summary>
		/// 'Resources'
		/// </summary>
		Resources
	}
}
