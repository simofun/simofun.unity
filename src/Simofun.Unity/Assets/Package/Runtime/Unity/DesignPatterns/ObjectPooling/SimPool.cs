//-----------------------------------------------------------------------
// <copyright file="Pool.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Hasan Emre Tonguç</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.DesignPatterns.ObjectPooling
{
	using System.Collections.Generic;
	using UnityEngine;

	public class SimPool
	{
		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="SimPool"/> class.
		/// </summary>
		/// <param name="prefabModel">Prefab model</param>
		/// <param name="size">Size</param>
		public SimPool(GameObject prefabModel, int size)
		{
			this.PrefabModel = prefabModel;
			this.Size = size;
			this.Parent = new GameObject(prefabModel.name + " Pool");

			this.FillPool();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SimPool"/> class.
		/// </summary>
		/// <param name="prefab">Prefab</param>
		/// <param name="size">Size</param>
		/// <param name="parent">Parent</param>
		public SimPool(GameObject prefab, int size, GameObject parent)
		{
			this.Parent = parent;
			this.PrefabModel = prefab;
			this.Size = size;

			this.FillPool();
		}
		#endregion

		#region Properties
		public Queue<GameObject> ObjectPool { get; protected set; } = new Queue<GameObject>();
		#endregion

		#region Protected Properties
		protected GameObject PrefabModel { get; }

		protected GameObject Parent { get; }

		protected int Size { get; }
		#endregion

		#region Public Methods
		public GameObject GetFromPool()
		{
			if (this.ObjectPool.Count <= 0)
			{
				this.GrowPool();
			}

			var obj = this.ObjectPool.Dequeue();
			obj.SetActive(true);

			return obj;
		}

		public void AddToPool(GameObject obj)
		{
			if (obj == null)
			{
				return;
			}

			//obj.transform.SetParent(this.parent.transform);
			obj.SetActive(false);
			this.ObjectPool.Enqueue(obj);
		}
		#endregion

		#region Methods
		void FillPool()
		{
			for (var i = 0; i < this.Size; i++)
			{
				this.GrowPool();
			}
		}

		void GrowPool()
		{
			var obj = Object.Instantiate(this.PrefabModel);
			this.AddToPool(obj);

			if (this.Parent != null)
			{
				obj.transform.SetParent(this.Parent.transform);
			}
		}
		#endregion
	}
}
