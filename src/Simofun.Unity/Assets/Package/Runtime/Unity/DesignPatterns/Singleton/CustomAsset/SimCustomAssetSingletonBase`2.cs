﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetSingletonBase`2.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.DesignPatterns.Singleton.CustomAsset
{
	using Sirenix.OdinInspector;
	using UnityEngine;

	public abstract class SimCustomAssetSingletonBase<TInstance, TConcrete> : ScriptableObject, ISimInitializable
		where TConcrete : SimCustomAssetSingletonBase<TInstance, TConcrete>, TInstance, new()
	{
		#region Unity Fields
		[SerializeField, Title("SimCustomAssetSingletonBase", "Settings")]
		bool isAutoInitialized;
		#endregion

		#region Protected Constructors
		protected SimCustomAssetSingletonBase()
		{
		}
		#endregion

		#region Properties
		#region ISimInitializable Properties
		/// <inheritdoc />
		public virtual bool IsAutoInitialized { get => this.isAutoInitialized; set => this.isAutoInitialized = value; }

		/// <inheritdoc />
		public virtual bool IsInitialized { get; protected set; }
		#endregion
		#endregion

		#region Public Methods
		#region ISimInitializable Methods
		/// <inheritdoc />
		public virtual void Initialize() => this.IsInitialized = true;
		#endregion
		#endregion
	}
}
