﻿//-----------------------------------------------------------------------
// <copyright file="SimLoadedCustomAssetSingletonBase`1.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.DesignPatterns.Singleton.CustomAsset
{
	public abstract class SimLoadedCustomAssetSingletonBase<T> : SimLoadedCustomAssetSingletonBase<T, T>
		where T : SimLoadedCustomAssetSingletonBase<T>, new()
	{
		#region Protected Constructors
		protected SimLoadedCustomAssetSingletonBase() : base()
		{
		}
		#endregion
	}
}
