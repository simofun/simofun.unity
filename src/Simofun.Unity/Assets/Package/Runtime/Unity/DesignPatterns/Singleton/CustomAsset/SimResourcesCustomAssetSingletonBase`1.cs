﻿//-----------------------------------------------------------------------
// <copyright file="SimResourcesCustomAssetSingletonBase`1.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.DesignPatterns.Singleton.CustomAsset
{
	public abstract class SimResourcesCustomAssetSingletonBase<T> : SimResourcesCustomAssetSingletonBase<T, T>
		where T : SimResourcesCustomAssetSingletonBase<T>, new()
	{
		#region Protected Constructors
		protected SimResourcesCustomAssetSingletonBase() : base()
		{
		}
		#endregion
	}
}
