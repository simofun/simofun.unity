﻿//-----------------------------------------------------------------------
// <copyright file="SimResourcesCustomAssetSingletonBase`2.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.DesignPatterns.Singleton.CustomAsset
{
	using System;
	using System.Linq;
	using UnityEngine;

	public abstract class SimResourcesCustomAssetSingletonBase<TInstance, TConcrete>
		: SimCustomAssetSingletonBase<TInstance, TConcrete>
		where TConcrete : SimResourcesCustomAssetSingletonBase<TInstance, TConcrete>, TInstance, new()
	{
		#region Protected Constructors
		protected SimResourcesCustomAssetSingletonBase() : base()
		{
		}
		#endregion

		#region Static Properties
		/// <summary>
		/// Gets singleton instance.
		/// </summary>
		public static TInstance Instance => LazyInstance.Value;
		#endregion

		#region Static Protected Properties
		protected static string InstanceLookupPath { get; set; } = string.Empty;

		/// <summary>
		/// Gets value for lazy initialization.
		/// </summary>
		protected static Lazy<TInstance> LazyInstance { get; set; } = new Lazy<TInstance>(() => CreateNewInstance());
		#endregion

		#region Protected Static Methods
		protected static TConcrete CreateNewInstance()
		{
			var instance = Resources.LoadAll<TConcrete>(InstanceLookupPath).FirstOrDefault();
			if (instance != null)
			{
				return instance;
			}

			instance = (TConcrete)CreateInstance(typeof(TConcrete));
			instance.Initialize();

			return instance;
		}
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Awake()
		{
			if (this.IsAutoInitialized)
			{
				var instance = Instance;
			}
		}
		#endregion
	}
}
