﻿//-----------------------------------------------------------------------
// <copyright file="SimGlobalSingletonBase`1.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.DesignPatterns.Singleton
{
	public abstract class SimGlobalSingletonBase<T> : SimGlobalSingletonBase<T, T>
		where T : SimGlobalSingletonBase<T>, new()
	{
		#region Protected Constructors
		protected SimGlobalSingletonBase() : base()
		{
		}
		#endregion
	}
}
