﻿//-----------------------------------------------------------------------
// <copyright file="SimGlobalSingletonBase`2.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.DesignPatterns.Singleton
{
	using System;

	public abstract class SimGlobalSingletonBase<TInstance, TConcrete> : SimSingletonBase<TInstance, TConcrete>
		where TConcrete : SimGlobalSingletonBase<TInstance, TConcrete>, TInstance, new()
	{
		#region Protected Constructors
		protected SimGlobalSingletonBase() : base()
		{
		}
		#endregion

		#region Static Properties
		/// <summary>
		/// Gets singleton instance.
		/// </summary>
		public static TInstance Instance => LazyInstance.Value;
		#endregion

		#region Static Protected Properties
		/// <summary>
		/// Gets value for lazy initialization.
		/// </summary>
		protected static Lazy<TInstance> LazyInstance { get; set; } = new Lazy<TInstance>(() => CreateNewInstance());
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Awake()
		{
			if (this.IsAutoInitialized)
			{
				var instance = Instance;
			}

			// Destroy instance duplications in new scenes.
			if (LazyInstance.IsValueCreated && this != (LazyInstance.Value as TConcrete))
			{
				Destroy(this.gameObject);

				return;
			}

			DontDestroyOnLoad(this);
		}
		#endregion
	}
}
