﻿//-----------------------------------------------------------------------
// <copyright file="SimSceneSingletonBase`1.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.DesignPatterns.Singleton
{
	public abstract class SimSceneSingletonBase<T> : SimSceneSingletonBase<T, T>
		where T : SimSceneSingletonBase<T>, new()
	{
		#region Protected Constructors
		protected SimSceneSingletonBase() : base()
		{
		}
		#endregion
	}
}
