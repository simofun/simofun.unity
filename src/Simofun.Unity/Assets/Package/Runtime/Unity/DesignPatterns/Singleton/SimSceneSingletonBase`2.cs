﻿//-----------------------------------------------------------------------
// <copyright file="SimSceneSingletonBase`2.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.DesignPatterns.Singleton
{
	public abstract class SimSceneSingletonBase<TInstance, TConcrete> : SimSingletonBase<TInstance, TConcrete>
		where TConcrete : SimSceneSingletonBase<TInstance, TConcrete>, TInstance, new()
	{
		#region Fields
		static readonly object lockObject = new object();

		static TConcrete instance;
		#endregion

		#region Protected Constructors
		protected SimSceneSingletonBase() : base()
		{
		}
		#endregion

		#region Static Properties
		/// <summary>
		/// Gets singleton instance.
		/// </summary>
		public static TInstance Instance
		{
			get
			{
				lock (lockObject)
				{
					if (instance != null)
					{
						return instance;
					}

					return instance = CreateNewInstance();
				}
			}
		}
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Awake()
		{
			if (this.IsAutoInitialized)
			{
				var instance = Instance;
			}
		}
		#endregion
	}
}
