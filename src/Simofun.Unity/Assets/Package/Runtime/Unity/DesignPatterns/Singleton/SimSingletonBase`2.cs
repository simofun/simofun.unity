﻿//-----------------------------------------------------------------------
// <copyright file="SimSingletonBase`2.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.DesignPatterns.Singleton
{
	using Sirenix.OdinInspector;
	using UnityEngine;

	public abstract class SimSingletonBase<TInstance, TConcrete> : MonoBehaviour, ISimInitializable, ISimNamable
		where TConcrete : SimSingletonBase<TInstance, TConcrete>, TInstance, new()
	{
		#region Unity Fields
		[SerializeField, Title("SimSingletonBase<TInstance, TConcrete>", "Settings")]
		bool isAutoInitialized;
		#endregion

		#region Protected Constructors
		protected SimSingletonBase()
		{
		}
		#endregion

		#region Protected Static Properties
		/// <summary>
		/// When Unity quits, it destroys objects in a random order.
		/// In principle, a Singleton is only destroyed when application quits.
		/// If any script calls Instance after it have been destroyed, 
		///   it will create a buggy ghost object that will stay on the Editor scene
		///   even after stopping playing the Application. Really bad!
		/// So, this was made to be sure we're not creating that buggy ghost object.
		/// </summary>
		protected static bool IsShuttingDown { get; set; }
		#endregion

		#region Properties
		#region ISimInitializable Properties
		/// <inheritdoc />
		public virtual bool IsAutoInitialized { get => this.isAutoInitialized; set => this.isAutoInitialized = value; }

		/// <inheritdoc />
		public virtual bool IsInitialized { get; protected set; }
		#endregion

		#region ISimNamable Properties
		/// <inheritdoc />
		public virtual string Name { get; set; }
		#endregion
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void OnApplicationQuit() => IsShuttingDown = true;

		/// <inheritdoc />
		protected virtual void OnDestroy() => IsShuttingDown = true;
		#endregion

		#region Public Methods
		#region ISimInitializable Methods
		/// <inheritdoc />
		public virtual void Initialize() => this.IsInitialized = true;
		#endregion
		#endregion

		#region Protected Static Methods
		protected static TConcrete CreateNewInstance()
		{
#if UNITY_EDITOR
			if (UnityEditor.EditorApplication.isCompiling)
			{
				return null;
			}
#endif
			var instance = FindAnyObjectByType<TConcrete>(FindObjectsInactive.Include);
			if (instance != null)
			{
				if (!instance.IsInitialized)
				{
					instance.Initialize();
				}

				return instance;
			}

			// Not working if a comp is added to multiple scenes
			//if (IsShuttingDown)
			//{
			//	Debug.LogWarning($"[{typeof(TConcrete)}] Instance is already destroyed. Returning null.");
			//
			//	return null;
			//}

#if UNITY_EDITOR
			if (FindObjectsByType<TConcrete>(FindObjectsInactive.Include, FindObjectsSortMode.None).Length > 1)
			{
				Debug.LogError($"[{typeof(TConcrete)}] Something went really wrong "
					+ " - there should never be more than 1 singleton!"
					+ " Reopening the scene might fix it.");

				return instance;
			}
#endif
			var go = new GameObject();
			instance = go.AddComponent<TConcrete>();
			go.name = string.IsNullOrEmpty(instance.Name) ? typeof(TConcrete).Name : instance.Name;
			instance.Initialize();

			return instance;
		}
		#endregion
	}
}
