﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetState.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.DesignPatterns.StateMachine.FSM.Model
{
	using UnityEngine;
	
	public abstract class SimCustomAssetState : ScriptableObject, ISimState
	{
		#region Protected Properties
		public virtual ISimStateMachine StateMachine { get; set; }
		#endregion

		#region Public Methods
		#region Abstract
		public abstract void Enter();

		public abstract void Exit();

		public abstract void Update();
		#endregion
		#endregion
	}
}
