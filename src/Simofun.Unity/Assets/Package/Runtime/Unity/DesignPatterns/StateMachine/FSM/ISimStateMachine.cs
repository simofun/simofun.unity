﻿//-----------------------------------------------------------------------
// <copyright file="ISimStateMachine.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.DesignPatterns.StateMachine.FSM
{
	using Simofun.Unity.DesignPatterns.StateMachine.FSM.Model;
	using System.Collections.Generic;

	public interface ISimStateMachine : IList<ISimState>
	{
		#region Properties
		ISimState CurrentState { get; }
		#endregion

		#region Public Methods
		bool Add<TState>() where TState : ISimState, new();
		
		bool Change<TState>() where TState : ISimState;

		bool Change(ISimState state);

		bool Change(System.Type stateType);
		
		void Update();
		#endregion
	}
}
