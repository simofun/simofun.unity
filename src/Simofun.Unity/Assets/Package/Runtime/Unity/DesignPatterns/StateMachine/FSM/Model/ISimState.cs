﻿//-----------------------------------------------------------------------
// <copyright file="ISimState.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.DesignPatterns.StateMachine.FSM.Model
{
	public interface ISimState
	{
		#region Properties
		ISimStateMachine StateMachine { get; set; }
		#endregion

		#region Methods
		void Enter();

		void Exit();

		void Update();
		#endregion
	}
}
