//-----------------------------------------------------------------------
// <copyright file="SimState.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.DesignPatterns.StateMachine.FSM.Model
{
	[System.Serializable]
	public abstract class SimState : ISimState
	{
		#region Constructors
		protected SimState()
		{
		}

		protected SimState(ISimStateMachine stateMachine)
		{
			this.StateMachine = stateMachine;
		}
		#endregion

		#region Protected Properties
		public virtual ISimStateMachine StateMachine { get; set; }
		#endregion

		#region Public Methods
		#region Abstract
		public abstract void Enter();

		public abstract void Exit();

		public abstract void Update();
		#endregion
		#endregion
	}
}
