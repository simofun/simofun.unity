//-----------------------------------------------------------------------
// <copyright file="SimStateMachine.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.DesignPatterns.StateMachine.FSM
{
	using Simofun.Unity.DesignPatterns.StateMachine.FSM.Model;
	using System.Collections;
	using System.Collections.Generic;
	using System.Linq;

	public class SimStateMachine : ISimStateMachine
	{
		#region Fields
		readonly List<ISimState> items = new();
		#endregion
		
		#region Properties
		public virtual bool IsReadOnly => false;

		public virtual int Count => this.Items.Count;

		public virtual ISimState CurrentState { get; protected set; }
		#endregion

		#region Protected Properties
		protected virtual List<ISimState> Items => this.items;
		#endregion

		#region Indexers
		public virtual ISimState this[int index] { get => this.Items[index]; set => this.Items[index] = value; }
		#endregion

		#region Public Methods
		#region ISimStateMachine
		public virtual bool Add<TState>() where TState : ISimState, new()
		{
			var stateType = typeof(TState);
			if (this.Items.Any(i => i.GetType() == stateType))
			{
				return false;
			}
			
			this.Items.Add(new TState { StateMachine = this });

			return true;
		}

		public virtual bool Change<TState>() where TState : ISimState => this.Change(typeof(TState));

		public virtual bool Change(ISimState state) => this.Change(state.GetType());

		public virtual bool Change(System.Type stateType)
		{
			var item = this.Items.FirstOrDefault(i => i.GetType() == stateType);
			if (item == null)
			{
				return false;
			}

			this.CurrentState?.Exit();
			this.CurrentState = item;
			this.CurrentState.Enter();

			return true;
		}

		public virtual void Update() => this.CurrentState?.Update();
		#endregion
		
		public virtual void Add(ISimState item)
		{
			if (this.Items.Contains(item))
			{
				return;
			}

			if (this != item.StateMachine)
			{
				item.StateMachine = this;
			}

			this.Items.Add(item);
		}

		public virtual void Clear() => this.Items.Clear();

		public virtual bool Contains(ISimState item) => this.Items.Contains(item);

		public virtual void CopyTo(ISimState[] array, int arrayIndex) => this.Items.CopyTo(array, arrayIndex);

		public virtual IEnumerator<ISimState> GetEnumerator() => this.Items.GetEnumerator();

		public virtual int IndexOf(ISimState item) => this.Items.IndexOf(item);

		public virtual void Insert(int index, ISimState item) => this.Items.Insert(index, item);

		public virtual bool Remove(ISimState item) => this.Items.Remove(item);

		public virtual void RemoveAt(int index) => this.Items.RemoveAt(index);
		#endregion

		#region Methods
		IEnumerator IEnumerable.GetEnumerator() => this.Items.GetEnumerator();
		#endregion
	}
}
