//-----------------------------------------------------------------------
// <copyright file="DesignVariableAttribute.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Hasan Emre Tongu�</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.DesignVariables.Attributes
{
	using System;

	[AttributeUsage(AttributeTargets.Field)]
	public class DesignVariableAttribute : Attribute
	{
		#region Properties
		public string DisplayName { get; set; }
		#endregion

		#region Constructors
		public DesignVariableAttribute(string displayName = null)
		{
			this.DisplayName = displayName;
		}
		#endregion
	}
}
