﻿//-----------------------------------------------------------------------
// <copyright file="SimMeshGeneratorMeshData.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Drawing.Model
{
	using UnityEngine;

	public class SimMeshGeneratorMeshData
	{
		public float RightMostPoint { get; set; }

		public Mesh Mesh { get; set; }
	}
}
