﻿//-----------------------------------------------------------------------
// <copyright file="SimMeshGenerator.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Drawing
{
	using Simofun.Unity.Drawing.Model;
	using System.Collections.Generic;
	using UnityEngine;

	public class SimMeshGenerator : MonoBehaviour
	{
		#region Events
		public event System.Action<SimMeshGeneratorMeshData> OnMeshGenerated;
		#endregion

		#region Public Methods
		public void Generate(List<Vector2> points)
		{
			var triangles = new List<int>();
			var uvs = new List<Vector2>();
			var vertices = new List<Vector3>();

			var initialIndex = 0;
			var initialSlope = this.GetSlope(points[0], points[1]);
			for (var i = 1; i < points.Count; i++)
			{
				var slope = this.GetSlope(points[initialIndex], points[i]);
				if (Mathf.Abs(slope - initialSlope) < 0.1f)
				{
					continue;
				}

				var firstVerticeNormal = this.GetNormal(points[initialIndex], points[i - 1]);
				var secondVerticeNormal = firstVerticeNormal + (points[i - 1] - points[initialIndex]);

				#region Calculation Vertices
				#region Front Side Vertices
				var initialPoint = new Vector3(points[initialIndex].x, points[initialIndex].y, Random.Range(.95f, 1f));
				var firstVertice = new Vector3(firstVerticeNormal.x, firstVerticeNormal.y, Random.Range(.95f, 1f));
				var secondVertice = new Vector3(secondVerticeNormal.x, secondVerticeNormal.y, Random.Range(.95f, 1f));
				var finalPoint = new Vector3(points[i - 1].x, points[i - 1].y, Random.Range(.95f, 1f));
				#endregion

				#region Back Side Vertices
				var initialPointBack = new Vector3(points[initialIndex].x, points[initialIndex].y, 0.75f);
				var firstVerticeBack = new Vector3(firstVerticeNormal.x, firstVerticeNormal.y, 0.75f);
				var secondVerticeBack = new Vector3(secondVerticeNormal.x, secondVerticeNormal.y, 0.75f);
				var finalPointBack = new Vector3(points[i - 1].x, points[i - 1].y, 0.75f);
				#endregion
				#endregion

				#region Adding Vertices
				vertices.Add(initialPoint);
				vertices.Add(firstVertice);
				vertices.Add(secondVertice);
				vertices.Add(finalPoint);
				vertices.Add(initialPointBack);
				vertices.Add(firstVerticeBack);
				vertices.Add(secondVerticeBack);
				vertices.Add(finalPointBack);
				#endregion

				initialIndex = i - 1;
				initialSlope = slope;
			}

			// Adding last point to necessary lists
			var firstVerticeNormalLast = this.GetNormal(points[initialIndex], points[points.Count - 1]);
			var secondVerticeNormalLast = firstVerticeNormalLast + (points[points.Count - 1] - points[initialIndex]);

			#region Calculation Last Vertices
			#region Front Side Vertices
			var initialPointLast = new Vector3(points[initialIndex].x, points[initialIndex].y, Random.Range(.95f, 1f));
			var firstVerticeLast = new Vector3(firstVerticeNormalLast.x, firstVerticeNormalLast.y, Random.Range(.95f, 1f));
			var secondVerticeLast = new Vector3(secondVerticeNormalLast.x, secondVerticeNormalLast.y, Random.Range(.95f, 1f));
			var finalPointLast = new Vector3(points[points.Count - 1].x, points[points.Count - 1].y, Random.Range(.95f, 1f));
			#endregion

			#region Back Side Vertices
			var initialPointBackLast = new Vector3(points[initialIndex].x, points[initialIndex].y, 0.75f);
			var firstVerticeBackLast = new Vector3(firstVerticeNormalLast.x, firstVerticeNormalLast.y, 0.75f);
			var secondVerticeBackLast = new Vector3(secondVerticeNormalLast.x, secondVerticeNormalLast.y, 0.75f);
			var finalPointBackLast = new Vector3(points[points.Count - 1].x, points[points.Count - 1].y, 0.75f);
			#endregion
			#endregion

			#region Adding Last Vertices
			vertices.Add(initialPointLast);
			vertices.Add(firstVerticeLast);
			vertices.Add(secondVerticeLast);
			vertices.Add(finalPointLast);
			vertices.Add(initialPointBackLast);
			vertices.Add(firstVerticeBackLast);
			vertices.Add(secondVerticeBackLast);
			vertices.Add(finalPointBackLast);
			#endregion
			///////////////////////////////
			
			for (var i = 0; i < vertices.Count; i += 8)
			{
				#region Adding Side Triangles
				#region Adding Front Side Triangles
				triangles.Add(i + 4);
				triangles.Add(i + 5);
				triangles.Add(i + 6);
				triangles.Add(i + 4);
				triangles.Add(i + 6);
				triangles.Add(i + 7);
				#endregion

				#region Adding Back Side Triangles
				triangles.Add(i + 2);
				triangles.Add(i + 1);
				triangles.Add(i + 0);
				triangles.Add(i + 3);
				triangles.Add(i + 2);
				triangles.Add(i + 0);
				#endregion

				#region Adding Right Side Triangles
				triangles.Add(i + 6);
				triangles.Add(i + 2);
				triangles.Add(i + 3);
				triangles.Add(i + 7);
				triangles.Add(i + 6);
				triangles.Add(i + 3);
				#endregion

				#region Adding Left Side Triangles
				triangles.Add(i + 1);
				triangles.Add(i + 5);
				triangles.Add(i + 4);
				triangles.Add(i + 0);
				triangles.Add(i + 1);
				triangles.Add(i + 4);
				#endregion

				#region Adding Top Side Triangles
				triangles.Add(i + 5);
				triangles.Add(i + 1);
				triangles.Add(i + 2);
				triangles.Add(i + 6);
				triangles.Add(i + 5);
				triangles.Add(i + 2);
				#endregion

				#region Adding Bottom Side Triangles
				triangles.Add(i + 3);
				triangles.Add(i);
				triangles.Add(i + 4);
				triangles.Add(i + 3);
				triangles.Add(i + 4);
				triangles.Add(i + 7);
				#endregion
				#endregion

				#region Filing Voids Between Rectangles
				/*if (i + 8 < vertices.Count)
				{
					triangles.Add(i + 2);
					triangles.Add(i + 5);
					triangles.Add(i + 4);

					triangles.Add(i + 6);
					triangles.Add(i + 9);
					triangles.Add(i + 8);
				}*/
				#endregion
			}

			var mesh = new Mesh
			{
				vertices = vertices.ToArray(),
				uv = uvs.ToArray(),
				triangles = triangles.ToArray()
			};

			var _rightMostPoint = points[0].x;
			for (var i = 0; i < points.Count; i++)
			{
				_rightMostPoint = Mathf.Max(_rightMostPoint, points[i].x);
			}

			this.OnMeshGenerated?.Invoke(
				new SimMeshGeneratorMeshData
				{
					Mesh = mesh,
					RightMostPoint = _rightMostPoint
				});
		}
		#endregion

		#region Methods
		float GetSlope(Vector2 p1, Vector2 p2) => p2.y - p1.y / p2.x - p1.x;

		Vector2 GetNormal(Vector2 p1, Vector2 p2)
		{
			var newVec = (p2 - p1).normalized / 10f;

			return p1 + new Vector2(-newVec.y, newVec.x);
		}
		#endregion
	}
}
