﻿//-----------------------------------------------------------------------
// <copyright file="SimBounceEffect.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Effects
{
	using DG.Tweening;
	using Sirenix.OdinInspector;
	using UnityEngine;

	public class SimBounceEffect : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimBounceEffect), "Settings")]
		float bounceTime = 0.5f;

		[SerializeField]
		float bounceHeight = 1;
		#endregion

		#region Properties
		public virtual float BounceTime { get => this.bounceTime; set => this.bounceTime = value; }

		public virtual float BounceHeight { get => this.bounceHeight; set => this.bounceHeight = value; }
		#endregion

		#region Unity Methods
		protected virtual void OnEnable() => this.StartBounce();

		protected virtual void OnDisable() => this.StopBounce();
		#endregion

		#region Public Methods
		public virtual void StartBounce() => this.InvokeRepeating(nameof(this.WhenBounce), 0, this.BounceTime);

		public virtual void StopBounce() => this.CancelInvoke(nameof(this.WhenBounce));
		#endregion

		#region Protected Methods
		protected virtual void WhenBounce() =>
			this.transform
				.DOMoveY(this.transform.position.y + this.BounceHeight, this.BounceTime / 2)
				.SetEase(Ease.OutQuad)
				.OnComplete(() =>
				{
					this.transform
						.DOMoveY(this.transform.position.y - this.BounceHeight, this.BounceTime / 2)
						.SetEase(Ease.InQuad);
				});
		#endregion
	}
}
