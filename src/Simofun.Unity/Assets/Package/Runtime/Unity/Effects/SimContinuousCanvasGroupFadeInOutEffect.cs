﻿//-----------------------------------------------------------------------
// <copyright file="SimContinuousCanvasGroupFadeInOutEffect.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Effects
{
	using DG.Tweening;
	using Sirenix.OdinInspector;
	using UnityEngine;

	public class SimContinuousCanvasGroupFadeInOutEffect : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimContinuousCanvasGroupFadeInOutEffect), "Settings")]
		float fadeInDuration = 0.5f;

		[SerializeField]
		float fadeOutDuration = 0.5f;

		[SerializeField, Title("", "References")]
		CanvasGroup canvasGroup;
		#endregion

		#region Fields
		Sequence fadeSequence;
		#endregion

		#region Protected Properties
		protected CanvasGroup CanvasGroup
		{
			get => this.canvasGroup != null ? this.canvasGroup : (this.canvasGroup = this.GetComponent<CanvasGroup>());
			set => this.canvasGroup = value;
		}
		#endregion

		#region Unity Methods
		protected virtual void OnEnable() => this.StartEffect();

		protected virtual void OnDisable()
		{
			this.fadeSequence?.Kill(true);
			this.fadeSequence = null;
		}
		#endregion

		#region Public Methods
		public void StartEffect()
		{
			this.fadeSequence?.Kill(true);

			this.fadeSequence = DOTween.Sequence();
			this.fadeSequence.Append(this.CanvasGroup.DOFade(0, this.fadeInDuration));
			this.fadeSequence.Append(this.CanvasGroup.DOFade(1, this.fadeOutDuration));
			this.fadeSequence.onComplete += () => this.RestartEffect();
			this.fadeSequence.SetUpdate(true);
			this.fadeSequence.Play();
		}

		public void RestartEffect()
		{
			this.fadeSequence?.Kill(true);
			this.fadeSequence?.Restart();
		}

		public void StopEffect() => this.fadeSequence?.Pause();
		#endregion
	}
}
