﻿//-----------------------------------------------------------------------
// <copyright file="SimEffectsPlayer.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Effects
{
	using UnityEngine;

	public class SimEffectsPlayer : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField]
		GameObject effects;
		#endregion

		#region Public Methods
		public void Play() => this.effects.SetActive(true);

		public void PlayAt(Vector3 position)
		{
			this.effects.transform.position = position;
			this.effects.SetActive(true);
		}
		#endregion
	}
}
