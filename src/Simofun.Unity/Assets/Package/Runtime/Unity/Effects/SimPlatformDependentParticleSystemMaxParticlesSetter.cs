//-----------------------------------------------------------------------
// <copyright file="SimPlatformDependentParticleSystemMaxParticlesSetter.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity
{
	using Sirenix.OdinInspector;
	using System.Collections.Generic;
	using UnityEngine;

	public class SimPlatformDependentParticleSystemMaxParticlesSetter : MonoBehaviour
	{
		#region Unity Properties
		[field: SerializeField, Title(nameof(SimPlatformDependentParticleSystemMaxParticlesSetter), "Settings")]
		public bool InvokeOnAwake { get; set; }

		[field: SerializeField]
		public bool InvokeOnStart { get; set; } = true;

		[field: SerializeField]
		public List<PlatformDependentMaxParticlesKeyValuePair> Values { get; set; }

		[SerializeField, Title("", "References")]
		ParticleSystem effect;
		#endregion

		#region Protected Properties
		protected ParticleSystem Effect
		{
			get => this.effect != null ? this.effect : (this.effect = this.GetComponent<ParticleSystem>());
			set => this.effect = value;
		}
		#endregion

		#region Unity Methods
		/// <inheritdoc/>
		protected virtual void Awake()
		{
			if (this.InvokeOnAwake)
			{
				this.Execute();
			}
		}

		/// <inheritdoc/>
		protected virtual void Start()
		{
			if (this.InvokeOnStart)
			{
				this.Execute();
			}
		}
		#endregion

		#region Public Methods
		public void Execute()
		{
			var platform = Application.platform;

			foreach (var value in this.Values)
			{
				if (platform != value.Platform)
				{
					continue;
				}

				var main = this.Effect.main;
				main.maxParticles = value.Value;
			}
		}
		#endregion

		#region Nested Types
		[System.Serializable]
		public class PlatformDependentMaxParticlesKeyValuePair
		{
			#region Unity Properties
			[field: SerializeField,
				DrawWithUnity,
				Tooltip("Using default value over 'ParticleSystem' if not set current platform.")]
			public RuntimePlatform Platform { get; set; }

			[field: SerializeField, Tooltip("'Max Particles' value")]
			public int Value { get; set; }
			#endregion
		}
		#endregion
	}
}
