﻿//-----------------------------------------------------------------------
// <copyright file="SimScenarioEvent.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Events.Scenario
{
	public abstract class SimScenarioEvent : SimBaseUnityEvent
	{
		public int NextScene { get; set; }
	}
}
