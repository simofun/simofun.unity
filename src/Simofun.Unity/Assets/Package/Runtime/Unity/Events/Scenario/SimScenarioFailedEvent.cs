﻿//-----------------------------------------------------------------------
// <copyright file="SimScenarioFailedEvent.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Events.Scenario
{
	public class SimScenarioFailedEvent : SimScenarioEvent
	{
	}
}
