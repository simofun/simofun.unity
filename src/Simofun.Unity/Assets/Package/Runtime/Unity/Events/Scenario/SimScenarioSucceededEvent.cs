﻿//-----------------------------------------------------------------------
// <copyright file="SimScenarioSucceededEvent.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Events.Scenario
{
	using UnityEngine;

	public class SimScenarioSucceededEvent : SimScenarioEvent
	{
		public Vector3? Position { get; set; }
	}
}
