﻿//-----------------------------------------------------------------------
// <copyright file="SimBaseUnityEvent.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Events
{
	using Simofun.Events;
	
	public class SimBaseUnityEvent : SimBaseEvent
	{
	}
}
