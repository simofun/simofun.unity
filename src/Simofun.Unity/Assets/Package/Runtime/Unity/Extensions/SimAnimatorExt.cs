//-----------------------------------------------------------------------
// <copyright file="SimAnimatorExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tonguc</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Extensions
{
	using UnityEngine;

	public static class SimAnimatorExt
	{
		public static void ResetTriggers(this Animator src)
		{
			foreach (AnimatorControllerParameter param in src.parameters)
			{
				if (param.type == AnimatorControllerParameterType.Trigger)
				{
					src.ResetTrigger(param.name);
				}
			}
		}
	}
}
