//-----------------------------------------------------------------------
// <copyright file="SimBoxColliderExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Extensions
{
	using UnityEngine;

	public static class SimBoxColliderExt
	{
		#region Public Methods
		public static Vector3 GetRandomPoint(this BoxCollider src)
		{
			var extents = src.size / 2f;

			return src.transform
				.TransformPoint(
					new Vector3(
						Random.Range(-extents.x, extents.x),
						Random.Range(-extents.y, extents.y),
						Random.Range(-extents.z, extents.z))
					+ src.center);
		}
		#endregion
	}
}
