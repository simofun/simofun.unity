//-----------------------------------------------------------------------
// <copyright file="SimColliderExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tonguc</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Extensions
{
	using UnityEngine;

	public static class SimColliderExt
	{
		public static bool IsColliding(this Collider src, Collider other) => src.bounds.Intersects(other.bounds);

		public static bool IsColliding(this Collider src, Collider[] others)
		{
			foreach (Collider other in others)
			{
				if (IsColliding(src, other))
				{
					return true;
				}
			}

			return false;
		}

		public static bool IsColliding(this Collider[] src, Collider other)
		{
			foreach (Collider collider in src)
			{
				if (IsColliding(collider, other))
				{
					return true;
				}
			}

			return false;
		}

		public static bool IsColliding(this Collider[] src, Collider[] others)
		{
			foreach (Collider collider in src)
			{
				if (IsColliding(collider, others))
				{
					return true;
				}
			}

			return false;
		}

		public static void DisableCollisions(this Collider src) => src.enabled = false;
	}
}
