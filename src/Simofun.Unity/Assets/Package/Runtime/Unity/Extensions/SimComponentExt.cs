﻿//-----------------------------------------------------------------------
// <copyright file="SimComponentExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Extensions
{
	using System.Collections.Generic;
	using UnityEngine;

	public static class SimComponentExt
	{
		/// <summary>
		/// Attaches a component to the given component's game object.
		/// </summary>
		/// <typeparam name="T">The type parameter.</typeparam>
		/// <param name="src">The component.</param>
		/// <returns>Newly attached component.</returns>
		public static T AddComponent<T>(this Component src) where T : Component => src.gameObject.AddComponent<T>();

		/// <summary>
		/// Gets a component attached to the given component's game object.
		/// If one isn't found, a new one is attached and returned.
		/// </summary>
		/// <typeparam name="T">The type parameter.</typeparam>
		/// <param name="src">The component.</param>
		/// <returns>Previously or newly attached component.</returns>
		public static T GetOrAddComponent<T>(this Component src) where T : Component =>
			src.TryGetComponent<T>(out var comp) ? comp : src.AddComponent<T>();

		public static Component GetSoClosest(this Component src, IEnumerable<Component> list)
		{
			var pos = src.transform.position;
			var currentDist = float.MaxValue;
			Component currentComp = null;

			foreach (var item in list)
			{
				var dist = Vector3.Distance(pos, item.transform.position);
				if (dist < currentDist)
				{
					currentDist = dist;
					currentComp = item;
				}
			}

			return currentComp;
		}

		/// <summary>
		/// Determines whether this instance has component.
		/// </summary>
		/// <typeparam name="T">The type parameter.</typeparam>
		/// <param name="src">The component.</param>
		/// <returns>
		///   <c>true</c> if the specified component is attached; otherwise, <c>false</c>.
		/// </returns>
		public static bool HasComponent<T>(this Component src) where T : Component =>
			src.TryGetComponent<T>(out var _);

		/// <summary>
		/// Determines whether the specified type has component.
		/// </summary>
		/// <param name="src">The source.</param>
		/// <param name="type">The type.</param>
		/// <param name="component">The component.</param>
		/// <returns>
		///   <c>true</c> if the specified type has component; otherwise, <c>false</c>.
		/// </returns>
		public static bool HasComponent(this Component src, System.Type type, out Component component)
			=> src.TryGetComponent(type, out component);

		/// <summary>
		/// Gets the closest component in the given list.
		/// </summary>
		/// <param name="src">The component array</param>
		/// <param name="toPoint">Compare point</param>
		/// <typeparam name="T">The component</typeparam>
		/// <returns>Closest component</returns>
		public static T GetClosest<T>(this T[] src, Vector3 toPoint) where T : Component
		{
			if (src == null || src.Length == 0)
			{
				return null;
			}

			T closest = null;
			var closestDistance = Mathf.Infinity;
			for (var i = 0; i < src.Length; i++)
			{
				if (src[i] == null)
				{
					continue;
				}

				var sqrMagnitude = (src[i].transform.position - toPoint).sqrMagnitude;
				if (sqrMagnitude < closestDistance)
				{
					closestDistance = sqrMagnitude;
					closest = src[i];
				}
			}

			return closest;
		}
	}
}
