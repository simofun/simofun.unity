//-----------------------------------------------------------------------
// <copyright file="SimGUISkinExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Hasan Emre Tonguc</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Extensions
{
	using UnityEngine;

	/// <summary>
	/// Extension class for <see cref="GUISkin"/>
	/// </summary>
	public static class SimGUISkinExt
	{
		public static void DrawText(
			this GUISkin src, string text, Vector3 position, Color? color = null, int fontSize = 0, float yOffset = 0)
		{
#if UNITY_EDITOR
			var prevSkin = GUI.skin;
			GUIStyle style = null;
			if (src == null)
			{
				style = new GUIStyle();
			}
			else
			{
				GUI.skin = src;
				style = new GUIStyle(src.GetStyle("Label"));
			}

			var textContent = new GUIContent(text);
			if (color != null)
			{
				style.normal.textColor = (Color)color;
			}

			if (fontSize > 0)
			{
				style.fontSize = fontSize;
			}

			var textSize = style.CalcSize(textContent);
			var screenPoint = Camera.current.WorldToScreenPoint(position);
			if (screenPoint.z > 0)
			{
				var worldPosition = Camera.current.ScreenToWorldPoint(
					new Vector3(
						screenPoint.x - textSize.x * 0.5f,
						screenPoint.y + textSize.y * 0.5f + yOffset,
						screenPoint.z));
				UnityEditor.Handles.Label(worldPosition, textContent, style);
			}

			GUI.skin = prevSkin;
#endif
		}
	}
}
