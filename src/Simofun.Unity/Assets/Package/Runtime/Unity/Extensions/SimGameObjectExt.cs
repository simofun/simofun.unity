﻿//-----------------------------------------------------------------------
// <copyright file="SimGameObjectExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Extensions
{
	using UnityEngine;

	public static class SimGameObjectExt
	{
		public static bool ChangeVisibilityByCondition(
			this GameObject src, ISimVisibilityCondition condition, bool value) =>
				src.ChangeVisibilityByCondition(condition, value, null);

		public static bool ChangeVisibilityByCondition(
			this GameObject src, ISimVisibilityCondition condition, bool value, GameObject reference)
		{
			if (src == null)
			{
				return false;
			}

			switch (condition)
			{
				case ISimVisibilityCondition.Self:
					{
						src.SetActive(value);

						return true;
					}
				case ISimVisibilityCondition.Parent:
					{
						var parent = src.transform.parent;
						if (parent != null)
						{
							parent.gameObject.SetActive(value);

							return true;
						}

						return false;
					}
				case ISimVisibilityCondition.Reference:
					{
						reference.SetActive(value);

						return true;
					}
				case ISimVisibilityCondition.None:
				default:
					{
						return false;
					}
			}
		}

		/// <summary>
		/// Checks whether a game object has a component of type T attached.
		/// </summary>
		/// <typeparam name="T">The type parameter.</typeparam>
		/// <param name="src">The game object.</param>
		/// <returns>
		///   <c>true</c> if the specified game object has component; otherwise, <c>false</c>.
		/// </returns>
		public static bool HasComponent<T>(this GameObject src) where T : Component =>
			src.TryGetComponent<T>(out var _);

		/// <summary>
		/// Determines whether the specified type has component.
		/// </summary>
		/// <param name="src">The source.</param>
		/// <param name="type">The type.</param>
		/// <param name="component">The component.</param>
		/// <returns>
		///   <c>true</c> if the specified type has component; otherwise, <c>false</c>.
		/// </returns>
		public static bool HasComponent(this GameObject src, System.Type type, out Component component)
			=> src.TryGetComponent(type, out component);

		/// <summary>
		/// Gets a component attached to the given game object.
		/// If one isn't found, a new one is attached and returned.
		/// </summary>
		/// <typeparam name="T">The type parameter.</typeparam>
		/// <param name="src">The game object.</param>
		/// <returns>Previously or newly attached component.</returns>
		public static T GetOrAddComponent<T>(this GameObject src) where T : Component =>
			src.TryGetComponent<T>(out var comp) ? comp : src.AddComponent<T>();

		/// <summary>
		/// Gets the component attached to the given game object at any cost. First tries himself than children than its parents at last adds the component in the object.
		/// </summary>
		/// <param name="src">The game object.</param>
		/// <typeparam name="T">The type parameter.</typeparam>
		/// <returns>
		/// 	<c>true</c> Gets the component at any cost :) 
		/// </returns>
		public static T GetComponentAtAnyCost<T>(this GameObject src) where T : Component
		{
			var component = src.GetComponentInChildren<T>();
			component ??= src.GetComponentInParent<T>();
			component ??= src.AddComponent<T>();

			return component;
		}
	}
}
