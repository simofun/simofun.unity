//-----------------------------------------------------------------------
// <copyright file="SimLayerMaskExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tonguc</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Extensions
{
	using UnityEngine;

	public static class SimLayerMaskExt
	{
		/// <summary>
		/// Checks the layer mask contains the layer.
		/// </summary>
		/// <param name="layer">Layer</param>
		/// <returns>If contains returns true</returns>
		public static bool Contains(this LayerMask src, int layer) => (src.value & (1 << layer)) != 0;
	}
}
