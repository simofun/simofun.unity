﻿//-----------------------------------------------------------------------
// <copyright file="SimMeshExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Extensions
{
	using UnityEngine;

	public static class SimMeshExt
	{
		public static Vector3 GetBottomVertex(this Mesh src)
		{
			var vertex = src.GetTopVertex();
			vertex.y *= -1;

			return vertex;
		}

		public static Vector3 GetLeftVertex(this Mesh src)
		{
			var vertex = src.GetRightVertex();
			vertex.x *= -1;

			return vertex;
		}

		public static Vector3 GetRightVertex(this Mesh src)
		{
			var verts = src.vertices;
			var vertex = new Vector3(float.NegativeInfinity, 0, 0);
			for (int i = 0; i < verts.Length; i++)
			{
				//Vector3 vert = transform.TransformPoint(verts[i]);
				var vert = verts[i];
				if (vert.x > vertex.x)
				{
					vertex = vert;
				}
			}

			return vertex;
		}

		public static Vector3 GetTopVertex(this Mesh src)
		{
			var verts = src.vertices;
			var vertex = new Vector3(0, float.NegativeInfinity, 0);
			for (int i = 0; i < verts.Length; i++)
			{
				//Vector3 vert = transform.TransformPoint(verts[i]);
				var vert = verts[i];
				if (vert.y > vertex.y)
				{
					vertex = vert;
				}
			}

			return vertex;
		}
	}
}
