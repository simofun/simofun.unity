﻿//-----------------------------------------------------------------------
// <copyright file="SimObjectExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Extensions
{
	using Simofun.Unity.Utils;
	using UnityEngine;

	public static class SimObjectExt
	{
		/// <summary>
		/// Destroys the specified object. Calls <see cref="Object.Destroy"/>
		/// if <see cref="Application.isPlaying"/> is true, otherwise calls <see cref="Object.DestroyImmediate"/>.
		/// </summary>
		/// <param name="src">The object to destroy.</param>
		public static void Destroy(this Object src) => SimObjectUtil.Destroy(src);

		/// <summary>
		/// Destroys the specified object.
		/// </summary>
		/// <param name="src">The object to destroy.</param>
		/// <param name="delay">The optional amount of time to delay before destroying the object.</param>
		public static void Destroy(this Object src, float delay) => Object.Destroy(src, delay);

		/// <summary>
		/// Destroys the specified object.
		/// </summary>
		/// <param name="src">The object to destroy.</param>
		public static void DestroyImmediate(this Object src) => Object.DestroyImmediate(src);

		/// <summary>
		/// Destroys the specified object.
		/// </summary>
		/// <param name="src">The object to destroy.</param>
		/// <param name="allowDestroyingAssets">Set to true to allow assets to be destroyed.</param>
		public static void DestroyImmediate(this Object src, bool allowDestroyingAssets) =>
			Object.DestroyImmediate(src, allowDestroyingAssets);
	}
}
