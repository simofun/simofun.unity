//-----------------------------------------------------------------------
// <copyright file="SimRigidbodyExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tonguc</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Extensions
{
	using UnityEngine;

	public static class SimRigidbodyExt
	{
		/// <summary>
		/// Throws the rigidbody to the target.
		/// </summary>
		/// <param name="target">Target point</param>
		/// <param name="angle">Given angle</param>
		public static void ThrowAt(this Rigidbody src, Vector3 target, float angle)
		{
			var direction = target - src.position;
			var height = direction.y;
			direction.y = 0;
			var distance = direction.magnitude;
			var a = angle * Mathf.Deg2Rad;
			direction.y = distance * Mathf.Tan(a);
			distance += height / Mathf.Tan(a);

			var velocity = Mathf.Sqrt(distance * Physics.gravity.magnitude / Mathf.Sin(2 * a));
			var force = velocity * direction.normalized;

			src.AddForce(force, ForceMode.VelocityChange);
		}

		/// <summary>
		/// Resets the rigidbody.
		/// </summary>
		public static void ResetVelocity(this Rigidbody src)
		{
			src.velocity = Vector3.zero;
			src.angularVelocity = Vector3.zero;
		}

		public static void Lock(this Rigidbody src) => src.constraints = RigidbodyConstraints.FreezeAll;
	}
}
