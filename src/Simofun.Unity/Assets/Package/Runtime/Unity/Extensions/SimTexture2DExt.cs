﻿//-----------------------------------------------------------------------
// <copyright file="SimTexture2DExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Extensions
{
	using UnityEngine;

	public static class SimTexture2DExt
	{
		public static Sprite ConvertToSprite(this Texture2D src) =>
			Sprite.Create(src, new Rect(0.0f, 0.0f, src.width, src.height), new Vector2(0.5f, 0.5f), 100.0f);
	}
}
