﻿//-----------------------------------------------------------------------
// <copyright file="SimTransformExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Extensions
{
	using System.Collections.Generic;
	using UnityEngine;

	public static class SimTransformExt
	{
		#region Children
		public static IEnumerable<Transform> GetChildren(this Transform src, bool isAll = true)
		{
			foreach (Transform child in src)
			{
				yield return child;

				if (!isAll)
				{
					continue;
				}

				foreach (Transform child2 in child.GetChildren())
				{
					yield return child2;
				}
			}
		}

		public static IEnumerable<Transform> GetParents(this Transform src)
		{
			var currentParent = src.parent;

			while (currentParent != null)
			{
				yield return currentParent;

				currentParent = currentParent.parent;
			}
		}
		#endregion

		#region Reset Transform
		public static Transform ResetLocalPosition(this Transform src)
		{
			src.localPosition = Vector3.zero;

			return src;
		}

		public static Transform ResetLocalRotation(this Transform src)
		{
			src.localRotation = Quaternion.identity;

			return src;
		}

		public static Transform ResetLocalScale(this Transform src)
		{
			src.localScale = Vector3.one;

			return src;
		}

		public static Transform ResetPosition(this Transform src)
		{
			src.position = Vector3.zero;

			return src;
		}

		public static Transform ResetRotation(this Transform src)
		{
			src.rotation = Quaternion.identity;

			return src;
		}

		public static Transform ResetTransform(this Transform src)
		{
			src.ResetPosition();
			src.ResetLocalRotation();
			src.ResetLocalScale();

			return src;
		}
		#endregion

		#region XYZ
		public static Transform DecreasePositionX(this Transform src, float x)
		{
			var pos = src.position;
			pos.x -= x;
			src.position = pos;

			return src;
		}

		public static Transform DecreasePositionXY(this Transform src, float x, float y)
		{
			var pos = src.position;
			pos.x -= x;
			pos.y -= y;
			src.position = pos;

			return src;
		}

		public static Transform DecreasePositionXY(this Transform src, Vector2 xy)
		{
			var pos = src.position;
			pos.x -= xy.x;
			pos.y -= xy.y;
			src.position = pos;

			return src;
		}

		public static Transform DecreasePositionXZ(this Transform src, float x, float z)
		{
			var pos = src.position;
			pos.x -= x;
			pos.z -= z;
			src.position = pos;

			return src;
		}

		public static Transform DecreasePositionXZ(this Transform src, Vector2 xz)
		{
			var pos = src.position;
			pos.x -= xz.x;
			pos.z -= xz.y;
			src.position = pos;

			return src;
		}

		public static Transform DecreasePositionY(this Transform src, float y)
		{
			var pos = src.position;
			pos.y -= y;
			src.position = pos;

			return src;
		}

		public static Transform DecreasePositionZ(this Transform src, float z)
		{
			var pos = src.position;
			pos.z -= z;
			src.position = pos;

			return src;
		}

		public static Transform DecreasePositionYZ(this Transform src, float y, float z)
		{
			var pos = src.position;
			pos.y -= y;
			pos.z -= z;
			src.position = pos;

			return src;
		}

		public static Transform DecreasePositionYZ(this Transform src, Vector2 yz)
		{
			var pos = src.position;
			pos.y -= yz.x;
			pos.z -= yz.y;
			src.position = pos;

			return src;
		}

		public static Transform IncreasePositionX(this Transform src, float x)
		{
			var pos = src.position;
			pos.x += x;
			src.position = pos;

			return src;
		}

		public static Transform IncreasePositionXY(this Transform src, float x, float y)
		{
			var pos = src.position;
			pos.x += x;
			pos.y += y;
			src.position = pos;

			return src;
		}

		public static Transform IncreasePositionXY(this Transform src, Vector2 xy)
		{
			var pos = src.position;
			pos.x += xy.x;
			pos.y += xy.y;
			src.position = pos;

			return src;
		}

		public static Transform IncreasePositionXZ(this Transform src, float x, float z)
		{
			var pos = src.position;
			pos.x += x;
			pos.z += z;
			src.position = pos;

			return src;
		}

		public static Transform IncreasePositionXZ(this Transform src, Vector2 xz)
		{
			var pos = src.position;
			pos.x += xz.x;
			pos.z += xz.y;
			src.position = pos;

			return src;
		}

		public static Transform IncreasePositionY(this Transform src, float y)
		{
			var pos = src.position;
			pos.y += y;
			src.position = pos;

			return src;
		}

		public static Transform IncreasePositionZ(this Transform src, float z)
		{
			var pos = src.position;
			pos.z += z;
			src.position = pos;

			return src;
		}

		public static Transform IncreasePositionYZ(this Transform src, float y, float z)
		{
			var pos = src.position;
			pos.y += y;
			pos.z += z;
			src.position = pos;

			return src;
		}

		public static Transform IncreasePositionYZ(this Transform src, Vector2 yz)
		{
			var pos = src.position;
			pos.y += yz.x;
			pos.z += yz.y;
			src.position = pos;

			return src;
		}

		public static Transform SetPositionX(this Transform src, float x)
		{
			var pos = src.position;
			pos.x = x;
			src.position = pos;

			return src;
		}

		public static Transform SetPositionXY(this Transform src, float x, float y)
		{
			var pos = src.position;
			pos.x = x;
			pos.y = y;
			src.position = pos;

			return src;
		}

		public static Transform SetPositionXY(this Transform src, Vector2 xy)
		{
			var pos = src.position;
			pos.x = xy.x;
			pos.y = xy.y;
			src.position = pos;

			return src;
		}

		public static Transform SetPositionXZ(this Transform src, float x, float z)
		{
			var pos = src.position;
			pos.x = x;
			pos.z = z;
			src.position = pos;

			return src;
		}

		public static Transform SetPositionXZ(this Transform src, Vector2 xz)
		{
			var pos = src.position;
			pos.x = xz.x;
			pos.z = xz.y;
			src.position = pos;

			return src;
		}

		public static Transform SetPositionY(this Transform src, float y)
		{
			var pos = src.position;
			pos.y = y;
			src.position = pos;

			return src;
		}

		public static Transform SetPositionZ(this Transform src, float z)
		{
			var pos = src.position;
			pos.z = z;
			src.position = pos;

			return src;
		}

		public static Transform SetPositionYZ(this Transform src, float y, float z)
		{
			var pos = src.position;
			pos.y = y;
			pos.z = z;
			src.position = pos;

			return src;
		}

		public static Transform SetPositionYZ(this Transform src, Vector2 yz)
		{
			var pos = src.position;
			pos.y = yz.x;
			pos.z = yz.y;
			src.position = pos;

			return src;
		}
		#endregion

		public static bool ChangeVisibilityByCondition(
			this Transform src, ISimVisibilityCondition condition, bool value) =>
				src.gameObject.ChangeVisibilityByCondition(condition, value);

		public static bool ChangeVisibilityByCondition(
			this Transform src, ISimVisibilityCondition condition, bool value, GameObject reference) =>
				src.gameObject.ChangeVisibilityByCondition(condition, value, reference);

		/// <summary>
		/// Gets the first parent has a different scale than the <see cref="Vector3.one"/>.
		/// </summary>
		/// <returns>Parent transform component</returns>
		public static Transform GetFirstParentHasDifferentLocalScale(this Transform src) =>
			src.GetFirstParentHasDifferentLocalScale(Vector3.one);

		/// <summary>
		/// Gets the first parent has a different scale than <paramref name="localScale"/>.
		/// </summary>
		/// <returns>Parent transform component</returns>
		public static Transform GetFirstParentHasDifferentLocalScale(this Transform src, Vector3 localScale)
		{
			var currentParent = src;

			while (currentParent != null)
			{
				if (currentParent.localScale != localScale)
				{
					return currentParent;
				}

				currentParent = currentParent.parent;
			}

			return currentParent;
		}

		public static Vector3 GetCenterPosition(this IEnumerable<Transform> src)
		{
			var center = Vector3.zero;
			var count = 0;
			foreach (var transform in src)
			{
				center += transform.position;
				count++;
			}

			return center / count;
		}

		/// <summary>
		/// Inverts the local scale of the transform.
		/// </summary>
		/// <returns>Inverted vector3</returns>
		public static Vector3 Invert(this Transform src) =>
			new(1 / src.localScale.x, 1 / src.localScale.y, 1 / src.localScale.z);

		/// <summary>
		/// Looks at the target only using y axis.
		/// </summary>
		/// <param name="target">Target to look at</param>
		public static void LookAtY(this Transform src, Vector3 target)
		{
			var lookPos = target - src.position;
			lookPos.y = src.position.y;
			if (lookPos == Vector3.zero)
			{
				return;
			}

			src.rotation = Quaternion.LookRotation(lookPos);
		}

		/// <summary>
		/// Looks at the target only using y axis with linear interpolation.
		/// </summary>
		/// <param name="target">Target to look at</param>
		/// <param name="damp">Time</param>
		public static void LookAtY(this Transform src, Vector3 target, float damp)
		{
			var lookPos = target - src.position;
			lookPos.y = src.position.y;
			if (lookPos == Vector3.zero)
			{
				return;
			}

			src.rotation = Quaternion.Slerp(src.rotation, Quaternion.LookRotation(lookPos), damp);
		}
	}
}
