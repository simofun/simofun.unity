﻿//-----------------------------------------------------------------------
// <copyright file="SimVector3Ext.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Extensions
{
	using System;
	using UnityEngine;

	public static class SimVector3Ext
	{
		#region XYZ
		#region Decrease New Reference
		public static Vector3 DecreaseNewX(this Vector3 src, float x) => new(src.x - x, src.y, src.z);

		public static Vector3 DecreaseNewXY(this Vector3 src, float x, float y) =>
			new(src.x - x, src.y - y, src.z);

		public static Vector3 DecreaseNewXY(this Vector3 src, Vector2 xy) =>
			new(src.x - xy.x, src.y - xy.y, src.z);

		public static Vector3 DecreaseNewXZ(this Vector3 src, float x, float z) =>
			new(src.x - x, src.y, src.z - z);

		public static Vector3 DecreaseNewXZ(this Vector3 src, Vector2 xz) =>
			new(src.x - xz.x, src.y, src.z - xz.y);

		public static Vector3 DecreaseNewY(this Vector3 src, float y) => new(src.x, src.y - y, src.z);

		public static Vector3 DecreaseNewZ(this Vector3 src, float z) => new(src.x, src.y, src.z - z);

		public static Vector3 DecreaseNewYZ(this Vector3 src, float y, float z) =>
			new(src.x, src.y - y, src.z - z);

		public static Vector3 DecreaseNewYZ(this Vector3 src, Vector2 yz) =>
			new(src.x, src.y - yz.x, src.z - yz.y);
		#endregion

		#region Decrease Same Reference
		public static Vector3 DecreaseX(this Vector3 src, float x)
		{
			src.x -= x;

			return src;
		}

		public static Vector3 DecreaseXY(this Vector3 src, float x, float y)
		{
			src.x -= x;
			src.y -= y;

			return src;
		}

		public static Vector3 DecreaseXY(this Vector3 src, Vector2 xy)
		{
			src.x -= xy.x;
			src.y -= xy.y;

			return src;
		}

		public static Vector3 DecreaseXZ(this Vector3 src, float x, float z)
		{
			src.x -= x;
			src.z -= z;

			return src;
		}

		public static Vector3 DecreaseXZ(this Vector3 src, Vector2 xz)
		{
			src.x -= xz.x;
			src.z -= xz.y;

			return src;
		}

		public static Vector3 DecreaseY(this Vector3 src, float y)
		{
			src.y -= y;

			return src;
		}

		public static Vector3 DecreaseZ(this Vector3 src, float z)
		{
			src.z -= z;

			return src;
		}

		public static Vector3 DecreaseYZ(this Vector3 src, float y, float z)
		{
			src.y -= y;
			src.z -= z;

			return src;
		}

		public static Vector3 DecreaseYZ(this Vector3 src, Vector2 yz)
		{
			src.y -= yz.x;
			src.z -= yz.y;

			return src;
		}
		#endregion

		#region Increase New References
		public static Vector3 IncreaseNewX(this Vector3 src, float x) => new(src.x + x, src.y, src.z);

		public static Vector3 IncreaseNewXY(this Vector3 src, float x, float y) =>
			new(src.x + x, src.y + y, src.z);

		public static Vector3 IncreaseNewXY(this Vector3 src, Vector2 xy) =>
			new(src.x + xy.x, src.y + xy.y, src.z);

		public static Vector3 IncreaseNewXZ(this Vector3 src, float x, float z) =>
			new(src.x + x, src.y, src.z + z);

		public static Vector3 IncreaseNewXZ(this Vector3 src, Vector2 xz) =>
			new(src.x + xz.x, src.y, src.z + xz.y);

		public static Vector3 IncreaseNewY(this Vector3 src, float y) => new(src.x, src.y + y, src.z);

		public static Vector3 IncreaseNewZ(this Vector3 src, float z) => new(src.x, src.y, src.z + z);

		public static Vector3 IncreaseNewYZ(this Vector3 src, float y, float z) =>
			new(src.x, src.y + y, src.z + z);

		public static Vector3 IncreaseNewYZ(this Vector3 src, Vector2 yz) =>
			new(src.x, src.y + yz.x, src.z + yz.y);
		#endregion

		#region Increase Same Reference
		public static Vector3 IncreaseX(this Vector3 src, float x)
		{
			src.x += x;

			return src;
		}

		public static Vector3 IncreaseXY(this Vector3 src, float x, float y)
		{
			src.x += x;
			src.y += y;

			return src;
		}

		public static Vector3 IncreaseXY(this Vector3 src, Vector2 xy)
		{
			src.x += xy.x;
			src.y += xy.y;

			return src;
		}

		public static Vector3 IncreaseXZ(this Vector3 src, float x, float z)
		{
			src.x += x;
			src.z += z;

			return src;
		}

		public static Vector3 IncreaseXZ(this Vector3 src, Vector2 xz)
		{
			src.x += xz.x;
			src.z += xz.y;

			return src;
		}

		public static Vector3 IncreaseY(this Vector3 src, float y)
		{
			src.y += y;

			return src;
		}

		public static Vector3 IncreaseZ(this Vector3 src, float z)
		{
			src.z += z;

			return src;
		}

		public static Vector3 IncreaseYZ(this Vector3 src, float y, float z)
		{
			src.y += y;
			src.z += z;

			return src;
		}

		public static Vector3 IncreaseYZ(this Vector3 src, Vector2 yz)
		{
			src.y += yz.x;
			src.z += yz.y;

			return src;
		}
		#endregion

		#region Reset New Reference
		public static Vector3 ResetNewX(this Vector3 src) => new(0, src.y, src.z);

		public static Vector3 ResetNewXY(this Vector3 src) => new(0, 0, src.z);

		public static Vector3 ResetNewXZ(this Vector3 src) => new(0, src.y, 0);

		public static Vector3 ResetNewY(this Vector3 src) => new(src.x, 0, src.z);

		public static Vector3 ResetNewZ(this Vector3 src) => new(src.x, src.y, 0);

		public static Vector3 ResetNewYZ(this Vector3 src) => new(src.x, 0, 0);
		#endregion

		#region Reset Same Reference
		public static Vector3 ResetX(this Vector3 src)
		{
			src.x = 0;

			return src;
		}

		public static Vector3 ResetXY(this Vector3 src)
		{
			src.x = 0;
			src.y = 0;

			return src;
		}

		public static Vector3 ResetXZ(this Vector3 src)
		{
			src.x = 0;
			src.z = 0;

			return src;
		}

		public static Vector3 ResetY(this Vector3 src)
		{
			src.y = 0;

			return src;
		}

		public static Vector3 ResetZ(this Vector3 src)
		{
			src.z = 0;

			return src;
		}

		public static Vector3 ResetYZ(this Vector3 src)
		{
			src.y = 0;
			src.z = 0;

			return src;
		}
		#endregion

		#region Set New Reference
		public static Vector3 SetNewX(this Vector3 src, float x) => new(x, src.y, src.z);

		public static Vector3 SetNewXY(this Vector3 src, float x, float y) => new(x, y, src.z);

		public static Vector3 SetNewXY(this Vector3 src, Vector2 xy) => new(xy.x, xy.y, src.z);

		public static Vector3 SetNewXZ(this Vector3 src, float x, float z) => new(x, src.y, z);

		public static Vector3 SetNewXZ(this Vector3 src, Vector2 xz) => new(xz.x, src.y, xz.y);

		public static Vector3 SetNewY(this Vector3 src, float y) => new(src.x, y, src.z);

		public static Vector3 SetNewZ(this Vector3 src, float z) => new(src.x, src.y, z);

		public static Vector3 SetNewYZ(this Vector3 src, float y, float z) => new(src.x, y, z);

		public static Vector3 SetNewYZ(this Vector3 src, Vector2 yz) => new(src.x, yz.x, yz.y);
		#endregion

		#region Set Same Reference
		public static Vector3 SetX(this Vector3 src, float x)
		{
			src.x = x;

			return src;
		}

		public static Vector3 SetXY(this Vector3 src, float x, float y)
		{
			src.x = x;
			src.y = y;

			return src;
		}

		public static Vector3 SetXY(this Vector3 src, Vector2 xy)
		{
			src.x = xy.x;
			src.y = xy.y;

			return src;
		}

		public static Vector3 SetXZ(this Vector3 src, float x, float z)
		{
			src.x = x;
			src.z = z;

			return src;
		}

		public static Vector3 SetXZ(this Vector3 src, Vector2 xz)
		{
			src.x = xz.x;
			src.z = xz.y;

			return src;
		}

		public static Vector3 SetY(this Vector3 src, float y)
		{
			src.y = y;

			return src;
		}

		public static Vector3 SetZ(this Vector3 src, float z)
		{
			src.z = z;

			return src;
		}

		public static Vector3 SetYZ(this Vector3 src, float y, float z)
		{
			src.y = y;
			src.z = z;

			return src;
		}

		public static Vector3 SetYZ(this Vector3 src, Vector2 yz)
		{
			src.y = yz.x;
			src.z = yz.y;

			return src;
		}
		#endregion
		#endregion

		#region Vector2
		public static Vector2 ToVector2XY(this Vector3 src) => new Vector2(src.x, src.y);

		public static Vector2 ToVector2XZ(this Vector3 src) => new Vector2(src.x, src.z);

		public static Vector2 ToVector2YZ(this Vector3 src) => new Vector2(src.y, src.z);

		[Obsolete(nameof(XY) + " is deprecated, please use " + nameof(ToVector2XY) + " instead.", true)]
		public static Vector2 XY(this Vector3 src) => src.ToVector2XY();

		[Obsolete(nameof(XZ) + " is deprecated, please use " + nameof(ToVector2XZ) + " instead.", true)]
		public static Vector2 XZ(this Vector3 src) => src.ToVector2XZ();

		[Obsolete(nameof(YZ) + " is deprecated, please use " + nameof(ToVector2YZ) + " instead.", true)]
		public static Vector2 YZ(this Vector3 src) => src.ToVector2YZ();
		#endregion

		#region Vector4
		public static Vector2 ToVector4(this Vector3 src) => new Vector4(src.x, src.y, src.z);

		public static Vector2 ToVector4(this Vector3 src, float w) => new Vector4(src.x, src.y, src.z, w);

		public static Vector2 ToVector4X(this Vector3 src, float x) => new Vector4(x, src.y, src.z);

		public static Vector2 ToVector4X(this Vector3 src, float x, float w) => new Vector4(x, src.y, src.z, w);

		public static Vector2 ToVector4XY(this Vector3 src, float x, float y) => new Vector4(x, y, src.z);

		public static Vector2 ToVector4XY(this Vector3 src, float x, float y, float w) => new Vector4(x, y, src.z, w);

		public static Vector2 ToVector4XY(this Vector3 src, Vector2 xy) => new Vector4(xy.x, xy.y, src.z);

		public static Vector2 ToVector4XY(this Vector3 src, Vector2 xy, float w) => new Vector4(xy.x, xy.y, src.z, w);

		public static Vector2 ToVector4XZ(this Vector3 src, float x, float z) => new Vector4(x, src.y, z);

		public static Vector2 ToVector4XZ(this Vector3 src, float x, float z, float w) => new Vector4(x, src.y, z, w);

		public static Vector2 ToVector4XZ(this Vector3 src, Vector2 xz) => new Vector4(xz.x, src.y, xz.y);

		public static Vector2 ToVector4XZ(this Vector3 src, Vector2 xz, float w) => new Vector4(xz.x, src.y, xz.y, w);

		public static Vector2 ToVector4Y(this Vector3 src, float y) => new Vector4(src.x, y, src.z);

		public static Vector2 ToVector4Y(this Vector3 src, float y, float w) => new Vector4(src.x, y, src.z, w);

		public static Vector2 ToVector4YZ(this Vector3 src, float y, float z) => new Vector4(src.x, y, z);

		public static Vector2 ToVector4YZ(this Vector3 src, float y, float z, float w) => new Vector4(src.x, y, z, w);

		public static Vector2 ToVector4YZ(this Vector3 src, Vector2 yz) => new Vector4(src.x, yz.x, yz.y);

		public static Vector2 ToVector4YZ(this Vector3 src, Vector2 yz, float w) => new Vector4(src.x, yz.x, yz.y, w);

		public static Vector2 ToVector4Z(this Vector3 src, float z) => new Vector4(src.x, src.y, z);

		public static Vector2 ToVector4Z(this Vector3 src, float z, float w) => new Vector4(src.x, src.y, z, w);
		#endregion
	}
}
