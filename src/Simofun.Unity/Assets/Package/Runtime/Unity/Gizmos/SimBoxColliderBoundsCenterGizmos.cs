﻿//-----------------------------------------------------------------------
// <copyright file="SimBoxColliderBoundsCenterGizmos.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Gizmos
{
	using Sirenix.OdinInspector;
	using UnityEngine;

	public class SimBoxColliderBoundsCenterGizmos : SimGizmosDrawerBase
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimGizmosDrawerBase), "Settings")]
		Color color = Color.red;

		[SerializeField, Title("", "References")]
		BoxCollider boxCollider;
		#endregion

		#region Properties
		public Color Color { get => this.color; set => this.color = value; }

		public BoxCollider BoxCollider { get => this.boxCollider; set => this.boxCollider = value; }
		#endregion

		#region Public Methods
		/// <inheritdoc />
		public override void OnGizmos()
		{
			if (this.BoxCollider == null)
			{
				return;
			}

			Gizmos.color = this.Color;

			var pos = this.transform.TransformPoint(this.BoxCollider.center);

			Gizmos.DrawLine(new Vector3(pos.x, pos.y - 1000, pos.z), new Vector3(pos.x, pos.y + 1000, pos.z));
		}
		#endregion
	}
}
