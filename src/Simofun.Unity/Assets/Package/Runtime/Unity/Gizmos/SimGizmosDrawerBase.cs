﻿//-----------------------------------------------------------------------
// <copyright file="SimGizmosDrawerBase.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Gizmos
{
	using Sirenix.OdinInspector;
	using UnityEngine;

	public abstract class SimGizmosDrawerBase : MonoBehaviour
	{
		#region Fields
		[SerializeField, Title(nameof(SimGizmosDrawerBase), "Settings")]
		bool isDrawGizmos = true;

		[SerializeField]
		bool isDrawGizmosWhenSelected = true;
		#endregion

		#region Properties
		public bool IsDrawGizmos { get => this.isDrawGizmos; set => this.isDrawGizmos = value; }

		public bool IsDrawGizmosWhenSelected
		{
			get => this.isDrawGizmosWhenSelected;
			set => this.isDrawGizmosWhenSelected = value;
		}
		#endregion

		#region Unity Methods
		#region Editor		
		/// <inheritdoc />
		protected virtual void OnDrawGizmos()
		{
			if (this.IsDrawGizmos && !this.IsDrawGizmosWhenSelected)
			{
				this.OnGizmos();
			}
		}

		/// <inheritdoc />
		protected virtual void OnDrawGizmosSelected()
		{
			if (this.IsDrawGizmos && this.IsDrawGizmosWhenSelected)
			{
				this.OnGizmos();
			}
		}
		#endregion
		#endregion

		#region Public Methods
		public abstract void OnGizmos();
		#endregion
	}
}
