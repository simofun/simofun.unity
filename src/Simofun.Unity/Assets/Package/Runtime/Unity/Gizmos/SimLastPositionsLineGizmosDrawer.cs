﻿//-----------------------------------------------------------------------
// <copyright file="SimLastPositionsLineGizmosDrawer.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Gizmos
{
	using Sirenix.OdinInspector;
	using System.Collections.Generic;
	using UnityEngine;

	public class SimLastPositionsLineGizmosDrawer : SimGizmosDrawerBase
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimLastPositionsLineGizmosDrawer), "Settings")]
		Color color = Color.blue;
		#endregion

		#region Properties
		public Color Color { get => this.color; set => this.color = value; }

		public List<Vector3> LastPositions { get; set; } = new List<Vector3>();
		#endregion

		#region Public Methods
		/// <inheritdoc />
		public override void OnGizmos()
		{
			if (this.LastPositions.Count <= 1)
			{
				return;
			}

			for (var i = 1; i < this.LastPositions.Count; i++)
			{
				Gizmos.color = this.Color;
				Gizmos.DrawLine(this.LastPositions[i - 1], this.LastPositions[i]);
			}
		}
		#endregion
	}
}
