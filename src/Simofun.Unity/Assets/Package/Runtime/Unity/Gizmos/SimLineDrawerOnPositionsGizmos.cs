﻿//-----------------------------------------------------------------------
// <copyright file="SimLineDrawerOnPositionsGizmos.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Gizmos
{
	using Sirenix.OdinInspector;
	using UnityEngine;

	public class SimLineDrawerOnPositionsGizmos : SimGizmosDrawerBase
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimLineDrawerOnPositionsGizmos), "Settings")]
		Color color = Color.red;

		[SerializeField]
		Vector3[] positions;
		#endregion

		#region Properties
		public Color Color { get => this.color; set => this.color = value; }

		public Vector3[] Positions { get => this.positions; set => this.positions = value; }
		#endregion

		#region Public Methods
		/// <inheritdoc />
		public override void OnGizmos()
		{
			if (this.Positions == null)
			{
				return;
			}

			Gizmos.color = this.Color;

			foreach (var pos in this.Positions)
			{
				Gizmos.DrawLine(pos, new Vector3(pos.x, pos.y + 1000, pos.z));
			}
		}
		#endregion
	}
}
