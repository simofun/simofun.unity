﻿//-----------------------------------------------------------------------
// <copyright file="SimSphereGizmosDrawer.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Gizmos
{
	using Sirenix.OdinInspector;
	using UnityEngine;

	public class SimSphereGizmosDrawer : SimGizmosDrawerBase
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimSphereGizmosDrawer), "Settings")]
		bool isWireframe = true;

		[SerializeField]
		Color color = Color.green;

		[SerializeField]
		Vector3 center;

		[SerializeField]
		float radius;
		#endregion

		#region Properties
		public bool IsWireframe { get => this.isWireframe; set => this.isWireframe = value; }

		public Color Color { get => this.color; set => this.color = value; }

		public float Radius { get => this.radius; set => this.radius = value; }

		public Vector3 Center { get => this.center; set => this.center = value; }
		#endregion

		#region Public Methods
		/// <inheritdoc />
		public override void OnGizmos()
		{
			if (this.Radius <= 0)
			{
				return;
			}

			Gizmos.color = this.Color;
			this.Center = this.transform.position;
			this.DrawSphere(this.Center, this.Radius);
		}
		#endregion

		#region Protected Methods
		protected virtual void DrawSphere(Vector3 center, float radius)
		{
			if (this.IsWireframe)
			{
				Gizmos.DrawWireSphere(center, radius);

				return;
			}

			Gizmos.DrawSphere(center, radius);
		}
		#endregion
	}
}
