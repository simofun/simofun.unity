﻿//-----------------------------------------------------------------------
// <copyright file="SimAssets.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.HardCodeds
{
	/// <summary>
	/// Hard-Codeds for 'Resources' folder
	/// </summary>
	public class SimAssets
	{
		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="SimAssets"/> class.
		/// </summary>
		protected SimAssets()
		{
		}
		#endregion

		#region Nested Types
		/// <summary>
		/// Hard-Codeds for 'Resources/Animations' folder
		/// </summary>
		public class Animations
		{
			#region Constructors
			/// <summary>
			/// Initializes a new instance of the <see cref="Animations"/> class.
			/// </summary>
			protected Animations()
			{
			}
			#endregion

			#region Static Properties
			public static string Path { get; } = nameof(Animations);

			public static string Path_ { get; } = Path + "/";

			public static string Get(string resource) => Path_ + resource;
			#endregion
		}

		/// <summary>
		/// Hard-Codeds for 'Resources/Animators' folder
		/// </summary>
		public class Animators
		{
			#region Constructors
			/// <summary>
			/// Initializes a new instance of the <see cref="Animators"/> class.
			/// </summary>
			protected Animators()
			{
			}
			#endregion

			#region Static Properties
			public static string Path { get; } = nameof(Animators);

			public static string Path_ { get; } = Path + "/";

			public static string Get(string resource) => Path_ + resource;
			#endregion
		}

		/// <summary>
		/// Hard-Codeds for 'Resources/Data' folder
		/// </summary>
		public class Data
		{
			#region Constructors
			/// <summary>
			/// Initializes a new instance of the <see cref="Data"/> class.
			/// </summary>
			protected Data()
			{
			}
			#endregion

			#region Static Properties
			public static string Path { get; } = nameof(Data);

			public static string Path_ { get; } = Path + "/";

			public static string Get(string resource) => Path_ + resource;
			#endregion
		}

		/// <summary>
		/// Hard-Codeds for 'Resources/Fonts' folder
		/// </summary>
		public class Fonts
		{
			#region Constructors
			/// <summary>
			/// Initializes a new instance of the <see cref="Fonts"/> class.
			/// </summary>
			protected Fonts()
			{
			}
			#endregion

			#region Static Properties
			public static string Path { get; } = nameof(Fonts);

			public static string Path_ { get; } = Path + "/";

			public static string Get(string resource) => Path_ + resource;
			#endregion
		}

		/// <summary>
		/// Hard-Codeds for 'Resources/Materials' folder
		/// </summary>
		public class Materials
		{
			#region Constructors
			/// <summary>
			/// Initializes a new instance of the <see cref="Materials"/> class.
			/// </summary>
			protected Materials()
			{
			}
			#endregion

			#region Static Properties
			public static string Path { get; } = nameof(Materials);

			public static string Path_ { get; } = Path + "/";

			public static string Get(string resource) => Path_ + resource;
			#endregion
		}

		/// <summary>
		/// Hard-Codeds for 'Resources/Models' folder
		/// </summary>
		public class Models
		{
			#region Constructors
			/// <summary>
			/// Initializes a new instance of the <see cref="Fonts"/> class.
			/// </summary>
			protected Models()
			{
			}
			#endregion

			#region Static Properties
			public static string Path { get; } = nameof(Models);

			public static string Path_ { get; } = Path + "/";

			public static string Get(string resource) => Path_ + resource;
			#endregion
		}

		/// <summary>
		/// Hard-Codeds for 'Resources/PhysicMaterials' folder
		/// </summary>
		public class PhysicMaterials
		{
			#region Constructors
			/// <summary>
			/// Initializes a new instance of the <see cref="PhysicMaterials"/> class.
			/// </summary>
			protected PhysicMaterials()
			{
			}
			#endregion

			#region Static Properties
			public static string Path { get; } = nameof(PhysicMaterials);

			public static string Path_ { get; } = Path + "/";

			public static string Get(string resource) => Path_ + resource;
			#endregion
		}

		/// <summary>
		/// Hard-Codeds for 'Resources/Prefabs' folder
		/// </summary>
		public class Prefabs
		{
			#region Constructors
			/// <summary>
			/// Initializes a new instance of the <see cref="Prefabs"/> class.
			/// </summary>
			protected Prefabs()
			{
			}
			#endregion

			#region Static Properties
			public static string Path { get; } = nameof(Prefabs);

			public static string Path_ { get; } = Path + "/";

			public static string Get(string resource) => Path_ + resource;
			#endregion
		}

		/// <summary>
		/// Hard-Codeds for 'Resources/Shaders' folder
		/// </summary>
		public class Shaders
		{
			#region Constructors
			/// <summary>
			/// Initializes a new instance of the <see cref="Shaders"/> class.
			/// </summary>
			protected Shaders()
			{
			}
			#endregion

			#region Static Properties
			public static string Path { get; } = nameof(Shaders);

			public static string Path_ { get; } = Path + "/";

			public static string Get(string resource) => Path_ + resource;
			#endregion
		}

		/// <summary>
		/// Hard-Codeds for 'Resources/Textures' folder
		/// </summary>
		public class Textures
		{
			#region Constructors
			/// <summary>
			/// Initializes a new instance of the <see cref="Textures"/> class.
			/// </summary>
			protected Textures()
			{
			}
			#endregion

			#region Static Properties
			public static string Path { get; } = nameof(Textures);

			public static string Path_ { get; } = Path + "/";

			public static string Get(string resource) => Path_ + resource;
			#endregion
		}
		#endregion
	}
}
