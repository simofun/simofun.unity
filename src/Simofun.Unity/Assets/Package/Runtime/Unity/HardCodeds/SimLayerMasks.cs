﻿//-----------------------------------------------------------------------
// <copyright file="SimLayerMasks.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.HardCodeds
{
	/// <summary>
	/// Hard-Codeds for 'LayerMask's
	/// </summary>
	public class SimLayerMasks
	{
		#region Public Fields
		public const int Everything = -1;

		public const int Nothing = 0;

		public const int Default = 1 << SimLayers.Default;

		public const int TransparentFX = 1 << SimLayers.TransparentFX;

		public const int IgnoreRaycast = 1 << SimLayers.IgnoreRaycast;

		public const int Water = 1 << SimLayers.Water;

		public const int UI = 1 << SimLayers.UI;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="SimLayerMasks"/> class.
		/// </summary>
		protected SimLayerMasks()
		{
		}
		#endregion
	}
}
