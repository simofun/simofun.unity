﻿//-----------------------------------------------------------------------
// <copyright file="SimLayers.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.HardCodeds
{
	/// <summary>
	/// Hard-Codeds for 'Layers'
	/// </summary>
	public class SimLayers
	{
		#region Public Fields
		public const int Default = 0;

		public const int TransparentFX = 1;

		public const int IgnoreRaycast = 2;

		public const int Water = 4;

		public const int UI = 5;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="SimLayers"/> class.
		/// </summary>
		protected SimLayers()
		{
		}
		#endregion
	}
}
