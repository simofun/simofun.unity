﻿//-----------------------------------------------------------------------
// <copyright file="SimScenes.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.HardCodeds
{
	/// <summary>
	/// Hard-Codeds for 'Scenes'
	/// </summary>
	public class SimScenes
	{
		#region Public Fields
		public const int Boot = 0;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="SimScenes"/> class.
		/// </summary>
		protected SimScenes()
		{
		}
		#endregion
	}
}
