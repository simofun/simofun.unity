﻿//-----------------------------------------------------------------------
// <copyright file="SimTags.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.HardCodeds
{
	/// <summary>
	/// Hard-Codeds for 'Tags'
	/// </summary>
	public class SimTags
	{
		#region Public Fields
		public const string EditorOnly = nameof(EditorOnly);

		public const string Finish = nameof(Finish);

		public const string GameController = nameof(GameController);

		public const string MainCamera = nameof(MainCamera);

		public const string Player = nameof(Player);

		public const string Respawn = nameof(Respawn);

		public const string Untagged = nameof(Untagged);
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="SimTags"/> class.
		/// </summary>
		protected SimTags()
		{
		}
		#endregion
	}
}
