//-----------------------------------------------------------------------
// <copyright file="ISimActivate.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity
{
    public interface ISimActivate
    {
        #region Properties
        bool IsActive { get; }
        #endregion

        #region Methods
        void Activate();

        void Deactivate();
        #endregion
    }
}
