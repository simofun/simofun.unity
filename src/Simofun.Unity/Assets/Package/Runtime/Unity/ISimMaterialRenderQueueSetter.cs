﻿//-----------------------------------------------------------------------
// <copyright file="ISimMaterialRenderQueueSetter.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity
{
	public interface ISimMaterialRenderQueueSetter
	{
		#region Properties
		int Value { get; set; }
		#endregion
	}
}
