﻿//-----------------------------------------------------------------------
// <copyright file="ISimVisibilityCondition.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity
{
	public enum ISimVisibilityCondition
	{
		None,

		Self,

		Parent,

		Reference
	}
}
