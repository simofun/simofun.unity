﻿//-----------------------------------------------------------------------
// <copyright file="SimSwipeData.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Input
{
	using UnityEngine;

	public struct SimSwipeData
	{
		public SimSwipeDirection Direction { get; set; }

		public Vector2 EndPosition { get; set; }

		public Vector2 StartPosition { get; set; }
	}
}
