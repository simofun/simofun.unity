﻿//-----------------------------------------------------------------------
// <copyright file="SimSwipeDetector.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Input
{
	using System;
	using UnityEngine;

	public class SimSwipeDetector : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField]
		bool detectSwipeOnlyAfterRelease = false;

		[SerializeField]
		float minDistanceForSwipe = 20f;
		#endregion

		#region Fields
		Vector2 fingerDownPosition;

		Vector2 fingerUpPosition;
		#endregion

		#region Events
		public event Action<SimSwipeData> OnSwipe = delegate { };
		#endregion

		#region Unity Methods
		protected virtual void Update()
		{
			foreach (var touch in Input.touches)
			{
				if (touch.phase == TouchPhase.Began)
				{
					this.fingerUpPosition = touch.position;
					this.fingerDownPosition = touch.position;
				}

				if (!this.detectSwipeOnlyAfterRelease && touch.phase == TouchPhase.Moved)
				{
					this.fingerDownPosition = touch.position;
					this.DetectSwipe();
				}

				if (touch.phase == TouchPhase.Ended)
				{
					this.fingerDownPosition = touch.position;
					this.DetectSwipe();
				}
			}
		}
		#endregion

		#region Protected Methods
		#region Event Invokers
		protected void InvokeOnSwipe(SimSwipeDirection direction) =>
			this.OnSwipe(
				new SimSwipeData
				{
					Direction = direction,
					EndPosition = fingerUpPosition,
					StartPosition = fingerDownPosition
				});
		#endregion
		#endregion

		#region Methods
		bool IsSwipeAvailable() =>
			this.VerticalMovementDistance() > this.minDistanceForSwipe
				|| this.HorizontalMovementDistance() > this.minDistanceForSwipe;

		bool IsVerticalSwipe() => this.VerticalMovementDistance() > this.HorizontalMovementDistance();

		float HorizontalMovementDistance() => Mathf.Abs(this.fingerDownPosition.x - this.fingerUpPosition.x);

		float VerticalMovementDistance() => Mathf.Abs(this.fingerDownPosition.y - this.fingerUpPosition.y);

		void DetectSwipe()
		{
			if (!this.IsSwipeAvailable())
			{
				return;
			}

			if (this.IsVerticalSwipe())
			{
				var dir = this.fingerDownPosition.y - this.fingerUpPosition.y > 0
					? SimSwipeDirection.Up
					: SimSwipeDirection.Down;
				this.InvokeOnSwipe(dir);
			}
			else
			{
				var dir = this.fingerDownPosition.x - this.fingerUpPosition.x > 0
					? SimSwipeDirection.Right
					: SimSwipeDirection.Left;
				this.InvokeOnSwipe(dir);
			}

			this.fingerUpPosition = this.fingerDownPosition;
		}
		#endregion
	}
}
