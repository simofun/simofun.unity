﻿//-----------------------------------------------------------------------
// <copyright file="SimSwipeDirection.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Input
{
	public enum SimSwipeDirection
	{
		None,
		
		Up,

		Down,

		Left,

		Right
	}
}
