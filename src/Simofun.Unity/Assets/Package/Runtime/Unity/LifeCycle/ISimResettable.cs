﻿//-----------------------------------------------------------------------
// <copyright file="ISimResettable.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.LifeCycle
{
	public interface ISimResettable
	{
		#region Methods		
		void Reset();
		#endregion
	}
}
