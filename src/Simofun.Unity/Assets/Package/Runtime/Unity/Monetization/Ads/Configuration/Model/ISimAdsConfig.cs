﻿//-----------------------------------------------------------------------
// <copyright file="ISimAdsConfig.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Monetization.Ads.Configuration.Model
{
	public interface ISimAdsConfig
	{
		#region Properties
		bool IsEnabled { get; set; }

		string Key { get; }
		#endregion
	}
}
