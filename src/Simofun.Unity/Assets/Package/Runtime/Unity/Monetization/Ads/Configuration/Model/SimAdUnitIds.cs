﻿//-----------------------------------------------------------------------
// <copyright file="SimAdUnitIds.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Monetization.Ads.Configuration.Model
{
	using Sirenix.OdinInspector;
	using UnityEngine;

	[CreateAssetMenu(
		fileName = nameof(SimAdUnitIds),
		menuName = SimMenuItems.Path_ + nameof(Unity) + "/" + nameof(Monetization) + "/" + nameof(Ads) + "/"
					 + nameof(Configuration) + "/" + nameof(Model) + "/" + nameof(SimAdUnitIds))]
	public class SimAdUnitIds : ScriptableObject
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimAdUnitIds), "Settings")]
		string bannerAdUnitId = "ca-app-pub-3940256099942544/6300978111";

		[SerializeField]
		string interstitialAdUnitId = "ca-app-pub-3940256099942544/1033173712";

		[SerializeField]
		string rewardedAdUnitId = "ca-app-pub-3940256099942544/5224354917";
		#endregion

		#region Properties
		public string BannerAdUnitId => this.bannerAdUnitId;

		public string InterstitialAdUnitId => this.interstitialAdUnitId;

		public string RewardedAdUnitId => this.rewardedAdUnitId;
		#endregion
	}
}
