﻿//-----------------------------------------------------------------------
// <copyright file="SimAdsConfig.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Monetization.Ads.Configuration.Model
{
	using Sirenix.OdinInspector;
	using UnityEngine;

	[CreateAssetMenu(
		fileName = nameof(SimAdsConfig),
		menuName = SimMenuItems.Path_ + nameof(Unity) + "/" + nameof(Monetization) + "/" + nameof(Ads) + "/"
					 + nameof(Configuration) + "/" + nameof(Model) + "/" + nameof(SimAdsConfig))]
	public class SimAdsConfig : ScriptableObject, ISimAdsConfig
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimAdsConfig), "Settings")]
		bool isEnabled = true;

		[SerializeField]
		string key;
		#endregion

		#region Properties
		public bool IsEnabled { get => this.isEnabled; set => this.isEnabled = value; }

		public string Key => this.key;
		#endregion
	}
}
