﻿//-----------------------------------------------------------------------
// <copyright file="SimAdsEvents.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Monetization.Ads.Events
{
	using Simofun.Unity.Events;

	/// <summary>
	/// Publishing for an ad event.
	/// </summary>
	public sealed class SimAdsEvents
	{
		/// <summary>
		/// Publishing when ads initialized.
		/// </summary>
		public class OnInitialized : SimBaseUnityEvent
		{
		}
	}
}
