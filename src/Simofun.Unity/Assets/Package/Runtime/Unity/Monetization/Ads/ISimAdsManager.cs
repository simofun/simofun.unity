﻿//-----------------------------------------------------------------------
// <copyright file="ISimAdsManager.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Monetization.Ads
{
	public interface ISimAdsManager : ISimInitializable
	{
		#region Properties
		bool IsEnabled { get; }

		ISimAdsProvider Offline { get; }

		ISimAdsProvider Online { get; }
		#endregion
	}
}
