//-----------------------------------------------------------------------
// <copyright file="ISimAdsProvider.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Monetization.Ads
{
	using Cysharp.Threading.Tasks;
	using Simofun.Unity.Analytics;
	using Simofun.Unity.Monetization.Ads.Configuration.Model;
	using Simofun.Unity.Monetization.Ads.Model;

	public interface ISimAdsProvider : ISimInitializable
	{
		#region Properties
		bool IsSupportedPlatform { get; }

		bool IsTestAdsEnabled { get; }

		ISimAdsConfig Config { get; set; }

		ISimAnalytics Analytics { get; set; }

		SimAdUnitIds AdUnitIds { get; set; }

		string Directory { get; set; }
		
		string ProviderName { get; set; }
		#endregion

		#region Methods
		void HideBannerAd();

		bool IsReady();

		bool IsBannerAdReady();

		bool IsInterstitialAdReady();

		bool IsRewardedAdReady();

		UniTask<SimBannerAdShowResult> ShowBannerAdAsync();

		UniTask<SimBannerAdShowResult> ShowBannerAdAsync(SimBannerAdPosition position);

		UniTask<SimInterstitialAdShowResult> ShowInterstitialAdAsync();

		UniTask<SimRewardedAdShowResult> ShowRewardedAdAsync();
		#endregion
	}
}
