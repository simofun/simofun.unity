﻿//-----------------------------------------------------------------------
// <copyright file="SimAdPlatform.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Monetization.Ads.Model
{
	public enum SimAdPlatform
	{
		Unsupported,

		Android,

		iOS
	}
}
