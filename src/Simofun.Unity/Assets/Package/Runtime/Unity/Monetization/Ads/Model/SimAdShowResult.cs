﻿//-----------------------------------------------------------------------
// <copyright file="SimAdShowResult.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Monetization.Ads.Model
{
	public class SimAdShowResult
	{
		#region Properties
		public virtual SimAdLoadException Exception { get; set; }

		public virtual bool IsClicked { get; set; }

		public virtual bool IsClosed { get; set; }

		public virtual bool IsFailed { get; set; }

		public virtual bool IsLoaded { get; set; }

		public virtual bool IsOpened { get; set; }
		#endregion
	}
}
