﻿//-----------------------------------------------------------------------
// <copyright file="SimBannerAdPosition.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Monetization.Ads.Model
{
	public enum SimBannerAdPosition
	{
		Top,
		
		Bottom,

		TopLeft,

		TopRight,

		BottomLeft,

		BottomRight,

		Center
	}
}
