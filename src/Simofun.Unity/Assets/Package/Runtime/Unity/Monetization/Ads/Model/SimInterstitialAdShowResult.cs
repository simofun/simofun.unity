﻿//-----------------------------------------------------------------------
// <copyright file="SimInterstitialAdShowResult.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Monetization.Ads.Model
{
	public class SimInterstitialAdShowResult : SimAdShowResult
	{
	}
}
