﻿//-----------------------------------------------------------------------
// <copyright file="SimRewardedAdShowResult.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Monetization.Ads.Model
{
	public class SimRewardedAdShowResult : SimAdShowResult
	{
		#region Properties
		public virtual bool IsReceivedReward { get; set; }

		public virtual double Amount { get; set; }

		public virtual string Type { get; set; }
		#endregion
	}
}
