﻿//-----------------------------------------------------------------------
// <copyright file="SimAdsManager.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Monetization.Ads
{
	public class SimAdsManager : SimAdsManagerBase<ISimAdsManager, SimAdsManager>
	{
	}
}
