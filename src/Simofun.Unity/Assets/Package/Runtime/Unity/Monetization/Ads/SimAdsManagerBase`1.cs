﻿//-----------------------------------------------------------------------
// <copyright file="SimAdsManagerBase`1.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Monetization.Ads
{
	public abstract class SimAdsManagerBase<T> : SimAdsManagerBase<T, T> where T : SimAdsManagerBase<T>, new()
	{
		#region Protected Constructors
		protected SimAdsManagerBase() : base()
		{
		}
		#endregion
	}
}
