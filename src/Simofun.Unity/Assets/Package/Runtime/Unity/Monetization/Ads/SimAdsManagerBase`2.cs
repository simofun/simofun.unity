﻿//-----------------------------------------------------------------------
// <copyright file="SimAdsManagerBase`2.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Monetization.Ads
{
	using Simofun.Unity.DesignPatterns.Singleton;

	public abstract class SimAdsManagerBase<TInstance, TConcrete>
		: SimGlobalSingletonBase<TInstance, TConcrete>, ISimAdsManager
		where TConcrete : SimAdsManagerBase<TInstance, TConcrete>, TInstance, new()
	{
		#region Fields
		ISimAdsProvider offline;

		ISimAdsProvider online;
		#endregion

		#region Properties
		// TODO: Open comment when offline(local) ads manager is implemented
		public virtual bool IsEnabled => this.Online.Config.IsEnabled /*|| this.Offline.Config.IsEnabled*/;

		// TODO: Add offline(local) ads manager
		public virtual ISimAdsProvider Offline => this.offline ??= SimFakeAdsProvider.Instance;

		public virtual ISimAdsProvider Online =>
			this.online ??= (this.GetComponent<ISimAdsProvider>() ?? SimFakeAdsProvider.Instance);
		#endregion
	}
}
