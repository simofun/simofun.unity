﻿//-----------------------------------------------------------------------
// <copyright file="SimAdsProviderBase`1.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Monetization.Ads
{
	public abstract class SimAdsProviderBase<T> : SimAdsProviderBase<T, T> where T : SimAdsProviderBase<T>, new()
	{
		#region Protected Constructors
		protected SimAdsProviderBase() : base()
		{
		}
		#endregion
	}
}
