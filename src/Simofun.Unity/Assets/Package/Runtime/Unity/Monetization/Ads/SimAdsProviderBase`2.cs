﻿//-----------------------------------------------------------------------
// <copyright file="SimAdsProviderBase`2.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Monetization.Ads
{
	using Cysharp.Threading.Tasks;
	using Simofun.UniRx.Unity;
	using Simofun.Unity.Analytics;
	using Simofun.Unity.DesignPatterns.Singleton;
	using Simofun.Unity.Monetization.Ads.Configuration.Model;
	using Simofun.Unity.Monetization.Ads.Events;
	using Simofun.Unity.Monetization.Ads.Model;
	using Sirenix.OdinInspector;
	using UnityEngine;

	public abstract class SimAdsProviderBase<TInstance, TConcrete>
		: SimGlobalSingletonBase<TInstance, TConcrete>, ISimAdsProvider
		where TConcrete : SimAdsProviderBase<TInstance, TConcrete>, TInstance, new()
	{
		#region Unity Fields
		[SerializeField, Title("SimAdsProviderBase<TInstance, TConcrete>", "Settings")]
		SimAdUnitIds adUnitIds;

		[SerializeField]
		SimAdsConfig config;
		#endregion

		#region Properties
		#region ISimInitializable
		/// <inheritdoc />
		public override bool IsInitialized
		{
			get => base.IsInitialized;
			protected set
			{
				if (base.IsInitialized == value)
				{
					return;
				}

				base.IsInitialized = value;

				SimMessageBus.Publish(new SimAdsEvents.OnInitialized());
			}
		}
		#endregion

		#region ISimAdsProvider
		/// <inheritdoc />
		public virtual bool IsSupportedPlatform
		{
			get =>
#if UNITY_EDITOR || UNITY_ANDROID || UNITY_IOS
				true;
#else
				false;
#endif
		}

		/// <inheritdoc />
		public virtual bool IsTestAdsEnabled
		{
			get =>
#if UNITY_EDITOR || DEVELOPMENT_BUILD
				true;
#else
				false;
#endif
		}

		/// <inheritdoc />
		public virtual ISimAdsConfig Config
		{
			get
			{
				if (this.config != null)
				{
					return this.config;
				}

				if (!this.IsSupportedPlatform)
				{
					return null;
				}

				this.config = Resources.Load<SimAdsConfig>(this.Directory + this.ConfigName);
				if (this.config == null)
				{
					Debug.LogError($"Could not find any '{nameof(SimAdsConfig)}' file under '{this.Directory}' path");

					return null;
				}

				return this.config;
			}

			set
			{
				this.config = (SimAdsConfig)value;
			}
		}

		/// <inheritdoc />
		public virtual SimAdUnitIds AdUnitIds
		{
			get
			{
				if (this.adUnitIds != null)
				{
					return this.adUnitIds;
				}

				if (!this.IsSupportedPlatform)
				{
					return null;
				}

				var adUnitIdsPlatform = string.Empty;
#if UNITY_EDITOR || UNITY_ANDROID
				adUnitIdsPlatform = "Android";
#elif UNITY_IOS
				adUnitIdsPlatform = "IOS";
#endif
				if (!string.IsNullOrEmpty(adUnitIdsPlatform))
				{
					var adUnitIdsPathTestSuffix = this.IsTestAdsEnabled ? "Test" : string.Empty;
					this.adUnitIds = Resources.Load<SimAdUnitIds>(
						this.Directory + $"Sim{adUnitIdsPlatform}{adUnitIdsPathTestSuffix}AdUnitIds");
				}

				if (this.adUnitIds == null)
				{
					Debug.LogError($"Could not find any 'SimAdUnitIds' file under '{this.Directory}' path");

					return null;
				}

				return this.adUnitIds;
			}

			set
			{
				this.adUnitIds = value;
			}
		}

		/// <inheritdoc />
		public virtual string Directory { get; set; } = "Monetization/Ads/";

		/// <inheritdoc />
		public virtual ISimAnalytics Analytics { get; set; }

		/// <inheritdoc />
		public virtual string ProviderName { get; set; }
		#endregion
		#endregion

		#region Protected Properties
		protected virtual ISimAdsProvider FakeAdsProvider { get; set; }

		protected virtual string ConfigName { get; set; } = nameof(SimAdsConfig);
		
		protected virtual string CurrentAdUnitId { get; set; }
		#endregion

		#region Construct
		/// <inheritdoc />
		public virtual void Construct(params object[] deps)
		{
			if (deps == null)
			{
				return;
			}

			var dep0 = deps[0];
			if (dep0 != null)
			{
				this.Analytics = (ISimAnalytics)deps[0];
			}
		}
		#endregion

		#region Unity Methods		
		/// <summary>
		/// When application goes to background (during the display of a rewarded video ad)
		/// then the timer needs to stop. Therefore we need to call code in Unity method OnApplicationPause.
		/// </summary>
		protected virtual void OnApplicationPause(bool pauseStatus)
		{
			if (pauseStatus)
			{
				if (this.CurrentAdUnitId != null)
				{
					this.Analytics?.PauseTimer(this.CurrentAdUnitId);
				}

				return;
			}
			
			if (this.CurrentAdUnitId != null)
			{
				this.Analytics?.ResumeTimer(this.CurrentAdUnitId);
			}
		}
		#endregion

		#region Public Methods
		#region ISimInitializable
		public override void Initialize()
		{
			if (this.IsSupportedPlatform)
			{
				this.Construct(
#if SIM_GAME_ANALYTICS
					GameAnalytics.Unity.SimGameAnalytics.Instance
#else
					null
#endif
					);

				return;
			}

			this.FakeAdsProvider = SimFakeAdsProvider.Instance;
			this.IsInitialized = true;

			Debug.LogWarning($"{Application.platform} is unsupported platform for 'Sim Ads'. Using fake one.");
		}
		#endregion

		#region ISimAdsProvider
		#region Abstract
		/// <inheritdoc />
		public abstract void HideBannerAd();

		/// <inheritdoc />
		public abstract bool IsReady();

		/// <inheritdoc />
		public abstract bool IsBannerAdReady();

		/// <inheritdoc />
		public abstract bool IsInterstitialAdReady();

		/// <inheritdoc />
		public abstract bool IsRewardedAdReady();

		/// <inheritdoc />
		public abstract UniTask<SimBannerAdShowResult> ShowBannerAdAsync();

		/// <inheritdoc />
		public abstract UniTask<SimBannerAdShowResult> ShowBannerAdAsync(SimBannerAdPosition position);

		/// <inheritdoc />
		public abstract UniTask<SimInterstitialAdShowResult> ShowInterstitialAdAsync();

		/// <inheritdoc />
		public abstract UniTask<SimRewardedAdShowResult> ShowRewardedAdAsync();
		#endregion
		#endregion
		#endregion
	}
}
