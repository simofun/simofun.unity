﻿//-----------------------------------------------------------------------
// <copyright file="SimFakeAdsProvider.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Monetization.Ads
{
	using Cysharp.Threading.Tasks;
	using Simofun.Unity.Monetization.Ads.Model;

	public class SimFakeAdsProvider : SimAdsProviderBase<ISimAdsProvider, SimFakeAdsProvider>
	{
		#region Public Methods
		#region Initialization
		/// <inheritdoc />
		public override void Initialize() => this.IsInitialized = true;
		#endregion

		#region Banner
		/// <inheritdoc />
		public override void HideBannerAd()
		{
		}

		/// <inheritdoc />
		public override bool IsBannerAdReady() => true;

		/// <inheritdoc />
		public async override UniTask<SimBannerAdShowResult> ShowBannerAdAsync() =>
			await UniTask.FromResult(
				new SimBannerAdShowResult
				{
					Exception = null,
					IsClosed = true,
					IsFailed = false,
					IsLoaded = true
				});

		/// <inheritdoc />
		public async override UniTask<SimBannerAdShowResult> ShowBannerAdAsync(SimBannerAdPosition position) =>
			await this.ShowBannerAdAsync();
		#endregion

		#region Interstitial
		/// <inheritdoc />
		public override bool IsInterstitialAdReady() => true;

		/// <inheritdoc />
		public async override UniTask<SimInterstitialAdShowResult> ShowInterstitialAdAsync() =>
			await UniTask.FromResult(
				new SimInterstitialAdShowResult
				{
					Exception = null,
					IsClosed = true,
					IsFailed = false,
					IsLoaded = true,
					IsOpened = false
				});
		#endregion

		#region Rewarded
		/// <inheritdoc />
		public override bool IsRewardedAdReady() => true;

		/// <inheritdoc />
		public async override UniTask<SimRewardedAdShowResult> ShowRewardedAdAsync() =>
			await UniTask.FromResult(
				new SimRewardedAdShowResult
				{
					Exception = null,
					IsClosed = true,
					IsFailed = false,
					IsLoaded = true,
					IsOpened = false,
					IsReceivedReward = true
				});
		#endregion

		/// <inheritdoc />
		public override bool IsReady() => true;
		#endregion
	}
}
