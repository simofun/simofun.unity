﻿//-----------------------------------------------------------------------
// <copyright file="SimAdLoadException.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Monetization.Ads
{
	using System;

	[Serializable]
	public class SimAdLoadException : Exception
	{
		#region Constructors
		public SimAdLoadException() : base()
		{
		}

		public SimAdLoadException(string message) : base(message)
		{
		}

		public SimAdLoadException(string message, Exception innerException) : base(message, innerException)
		{
		}
		#endregion
	}
}
