﻿//-----------------------------------------------------------------------
// <copyright file="ISimPopup.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Popup
{
	public interface ISimPopup : ISimVisibility
	{
		#region Properties
		ISimPopupSource Source { get; set; }

		string Key { get; set; }
		#endregion
	}
}
