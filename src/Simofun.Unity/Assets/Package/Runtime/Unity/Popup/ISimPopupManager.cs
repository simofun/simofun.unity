﻿//-----------------------------------------------------------------------
// <copyright file="ISimPopupManager.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Popup
{
	using Simofun.IO;
	using UnityEngine;

	public interface ISimPopupManager : ISimRootPath
	{
		#region Properties
		Transform Parent { get; set; }
		#endregion

		#region Public Methods
		ISimPopup GetOrAdd(ISimPopup popup);

		ISimPopup GetOrAdd(ISimPopup popup, Transform parent);

		ISimPopup GetOrAdd<T>(Object source) where T : ISimPopup;

		ISimPopup GetOrAdd<T>(Object source, Transform parent) where T : ISimPopup;

		ISimPopup GetOrAdd<T>() where T : ISimPopup;

		ISimPopup GetOrAdd<T>(Transform parent) where T : ISimPopup;

		void Remove(ISimPopup popup);

		void Remove(string key);

		void Remove<T>();

		void Remove<T>(Object source) where T : ISimPopup;

		void RemoveAll();

		void RevertParent();
		#endregion
	}
}
