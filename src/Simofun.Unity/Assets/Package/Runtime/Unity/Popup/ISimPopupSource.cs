﻿//-----------------------------------------------------------------------
// <copyright file="ISimPopupSource.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Popup
{
	using UnityEngine;

	public interface ISimPopupSource
	{
		#region Properties
		Object Value { get; set; }

		Transform ValueTransform { get; }
		#endregion

		#region Public Methods
		bool TryGetValue<T>(out T value) where T : Object;

		T GetValueAs<T>() where T : Object;
		#endregion
	}
}
