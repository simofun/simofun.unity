﻿//-----------------------------------------------------------------------
// <copyright file="SimPopupBase.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Popup
{
	using UnityEngine;

	public abstract class SimPopupBase : MonoBehaviour, ISimPopup
	{
		#region Fields
		ISimPopupManager manager;
		#endregion

		#region Properties
		/// <inheritdoc />
		public virtual bool IsVisible { get; set; }

		public virtual ISimPopupSource Source { get; set; } = new SimPopupSource();

		public virtual string Key { get; set; }
		#endregion

		#region Protected Properties
		protected ISimPopupManager Manager => this.manager ?? (this.manager = SimPopupManager.Instance);
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void OnDestroy() => this.Hide();
		#endregion

		#region Public Methods
		/// <inheritdoc />
		public virtual void Hide()
		{
			this.Manager.Remove(this);

			this.IsVisible = false;
		}

		/// <inheritdoc />
		public virtual void Show()
		{
			this.Manager.GetOrAdd(this);

			this.IsVisible = true;
		}
		#endregion
	}
}
