﻿//-----------------------------------------------------------------------
// <copyright file="SimPopupManager.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Popup
{
	using Simofun.Unity.DesignPatterns.Singleton;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;

	public class SimPopupManager : SimSceneSingletonBase<ISimPopupManager, SimPopupManager>, ISimPopupManager
	{
		#region Fields
		readonly Dictionary<string, ISimPopup> popups = new Dictionary<string, ISimPopup>();

		readonly List<Transform> parents = new List<Transform>();

		List<ISimPopup> popupPrefabModels;

		Transform parent;
		#endregion

		#region Properties
		public virtual string RootPath { get; set; } = "Prefabs/Presentation/View/Popup";

		public virtual Transform Parent
		{
			get
			{
				if (this.parent != null)
				{
					return this.parent;
				}

				this.parent = this.transform.parent;
				this.parents.Add(this.parent);

				return this.parent;
			}

			set
			{
				if (value != null)
				{
					var currentParent = this.Parent;
					if (!this.parents.Contains(currentParent))
					{
						this.parents.Add(currentParent);
					}
				}

				this.parent = value;
				this.transform.SetParent(value);
			}
		}
		#endregion

		#region Protected Properties
		protected virtual Dictionary<string, ISimPopup> Popups => this.popups;

		protected virtual List<ISimPopup> PopupPrefabModels
		{
			get
			{
				if (this.popupPrefabModels != null)
				{
					return this.popupPrefabModels;
				}

				this.popupPrefabModels = new List<ISimPopup>();
				this.popupPrefabModels = Resources
					.LoadAll<GameObject>(this.RootPath)
					.Select(m => m.GetComponent<ISimPopup>())
					.Where(m => m != null)
					.ToList();

				return this.popupPrefabModels;
			}
		}
		#endregion

		#region Public Methods
		public virtual ISimPopup GetOrAdd(ISimPopup popup) => this.GetOrAdd(popup.Key, popup.GetType());

		public virtual ISimPopup GetOrAdd(ISimPopup popup, Transform parent)
			 => this.GetOrAdd(popup.Key, popup.GetType(), parent: parent);

		public virtual ISimPopup GetOrAdd<T>(Object source) where T : ISimPopup
			 => this.GetOrAdd(type: typeof(T), source: source);

		public virtual ISimPopup GetOrAdd<T>(Object source, Transform parent) where T : ISimPopup
			 => this.GetOrAdd(type: typeof(T), source: source, parent: parent);

		public virtual ISimPopup GetOrAdd<T>() where T : ISimPopup => this.GetOrAdd(type: typeof(T));

		public virtual ISimPopup GetOrAdd<T>(Transform parent) where T : ISimPopup
			 => this.GetOrAdd(type: typeof(T), parent: parent);

		public virtual void Remove(ISimPopup popup)
		{
			if (!this.Popups.ContainsKey(popup.Key))
			{
				return;
			}

			this.RemoveInternal(popup);
		}

		public virtual void Remove(string key)
		{
			if (!this.Popups.ContainsKey(key))
			{
				return;
			}

			this.RemoveInternal(this.Popups[key]);
		}

		public virtual void Remove<T>()
		{
			var id = typeof(T);
			this.Popups
				.Where(p => id.IsInstanceOfType(p.Value))
				.ToList()
				.ForEach(p => this.RemoveInternal(p.Value));
		}

		public virtual void Remove<T>(Object source) where T : ISimPopup
		{
			var id = typeof(T);
			this.Popups
				.Where(p => p.Value.Source.Value == source && id.IsInstanceOfType(p.Value))
				.ToList()
				.ForEach(p => this.RemoveInternal(p.Value));
		}

		public virtual void RemoveAll()
		{
			foreach (var popup in this.Popups)
			{
				Destroy((popup.Value as Component).gameObject);
			}

			this.Popups.Clear();
		}

		public virtual void RevertParent()
		{
			if (this.parents.Count <= 0)
			{
				this.Parent = null;

				return;
			}

			var previousParent = this.parents.Last();
			this.Parent = previousParent;
			this.parents.Remove(previousParent);
		}
		#endregion

		#region Protected Methods
		protected virtual bool CheckPopup(ISimPopup popup, Object source = null, Transform parent = null)
		{
			if (source == null && parent == null)
			{
				return true;
			}

			if (source != null
					&& parent != null
					&& popup.Source.Value == source
					&& (popup as Component).transform.parent == parent)
			{
				return true;
			}

			if (source != null && popup.Source.Value == source)
			{
				return true;
			}

			if (parent != null && (popup as Component).transform.parent == parent)
			{
				return true;
			}

			return false;
		}

		protected virtual ISimPopup Create(System.Type id)
		{
			var popupPrefabModel = this.PopupPrefabModels.FirstOrDefault(m => id.IsInstanceOfType(m));
			if (popupPrefabModel == null)
			{
				throw new System.Exception($"Couldn't find pop-up with '{id}' id");
			}

			if (!Instantiate((popupPrefabModel as Component).gameObject, this.transform)
					.TryGetComponent<ISimPopup>(out var popup))
			{
				throw new System.Exception($"Couldn't find {nameof(ISimPopup)} comp in pop-up with '{id}' id");
			}

			return popup;
		}

		protected virtual ISimPopup Create<T>() where T : ISimPopup => this.Create(typeof(T));

		protected virtual ISimPopup GetOrAdd(
			string key = null,
			System.Type type = null,
			Object source = null,
			Transform parent = null)
		{
			// Search by key
			if (!string.IsNullOrEmpty(key)
					&& this.Popups.TryGetValue(key, out var foundPopup)
					&& this.CheckPopup(foundPopup, source, parent))
			{
				return foundPopup;
			}

			// Search by instances of type
			if (this.Popups.Any(p => type.IsInstanceOfType(p.Value)))
			{
				foundPopup = this.Popups.First(p => type.IsInstanceOfType(p.Value)).Value;
				if (this.CheckPopup(foundPopup, source, parent))
				{
					return foundPopup;
				}
			}

			// Create new pop-up
			foundPopup = this.Create(type);
			foundPopup.Key = this.CreateNewKey();
			if (source != null)
			{
				foundPopup.Source.Value = source;
			}

			if (parent != null)
			{
				(foundPopup as Component).transform.SetParent(parent);
			}

			this.Popups.Add(foundPopup.Key, foundPopup);

			return foundPopup;
		}

		protected virtual string CreateNewKey()
		{
			string newKey;
			do
			{
				newKey = System.Guid.NewGuid().ToString();
			} while (this.Popups.ContainsKey(newKey));

			return newKey;
		}

		protected virtual void RemoveInternal(ISimPopup popup)
		{
			Destroy((popup as Component).gameObject);
			this.Popups.Remove(popup.Key);
		}
		#endregion
	}
}
