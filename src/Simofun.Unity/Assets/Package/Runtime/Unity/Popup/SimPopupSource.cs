﻿//-----------------------------------------------------------------------
// <copyright file="ISimPopupSource.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Popup
{
	using UnityEngine;

	public class SimPopupSource : ISimPopupSource
	{
		#region Constructors		
		/// <summary>
		/// Initializes a new instance of the <see cref="SimPopupSource"/> class.
		/// </summary>
		public SimPopupSource()
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SimPopupSource"/> class.
		/// </summary>
		/// <param name="value">The value.</param>
		public SimPopupSource(Object value)
		{
			this.Value = value;
		}
		#endregion

		#region Properties		
		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>
		/// The value.
		/// </value>
		public Object Value { get; set; }

		/// <summary>
		/// Gets the value transform.
		/// </summary>
		/// <value>
		/// The value transform.
		/// </value>
		public virtual Transform ValueTransform
		{
			get
			{
				if (this.TryGetValue(out GameObject valueGo))
				{
					return valueGo.transform;
				}

				if (this.TryGetValue(out Component valueComp))
				{
					return valueComp.transform;
				}

				return null;
			}
		}
		#endregion

		#region Public Methods		
		/// <summary>
		/// Tries the get value.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="value">The value.</param>
		/// <returns>Returns true if gets the value. Otherwise, false.</returns>
		public virtual bool TryGetValue<T>(out T value) where T : Object
		{
			value = this.GetValueAs<T>();

			return value != null;
		}

		/// <summary>
		/// Gets the value as specified type.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns>Returns value as specified type</returns>
		public virtual T GetValueAs<T>() where T : Object => this.Value as T;
		#endregion
	}
}
