﻿//-----------------------------------------------------------------------
// <copyright file="SimPopupSourceFollower.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Popup
{
	using Sirenix.OdinInspector;
	using UnityEngine;

	[RequireComponent(typeof(ISimPopup))]
	public class SimPopupSourceFollower : MonoBehaviour, ISimPopupSourceFollower
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimPopupSourceFollower), "Settings")]
		Vector3 sourceOffset = Vector3.up * 3 + Vector3.right;

		[SerializeField]
		Vector3 screenOffset = Vector3.zero;

		[SerializeField]
		float scaleFactor = 0.5f;

		[SerializeField, Title("", "References"), Tooltip("Using default 'Camera.main'")]
		Camera cam;
		#endregion

		#region Fields
		float heightToScreenHeightRatio;

		ISimPopup popup;

		RectTransform rectTransform;
		#endregion

		#region Protected Properties
		protected virtual Camera Camera
		{
			get => this.cam != null ? this.cam : (this.cam = Camera.main);
			set => this.cam = value;
		}

		protected virtual float ScaleFactor { get => this.scaleFactor; set => this.scaleFactor = value; }

		protected virtual Vector3 ScreenOffset { get => this.screenOffset; set => this.screenOffset = value; }

		protected virtual Vector3 SourceOffset { get => this.sourceOffset; set => this.sourceOffset = value; }
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Awake()
		{
			this.popup = this.GetComponent<ISimPopup>();
			this.rectTransform = this.GetComponent<RectTransform>();

			this.heightToScreenHeightRatio = this.rectTransform.rect.height * this.rectTransform.localScale.y
				/ Screen.height;
		}

		/// <inheritdoc />
		protected virtual void LateUpdate()
		{
			if (this.popup == null || this.popup.Source.Value == null)
			{
				return;
			}

			var sourceTransform = this.popup.Source.ValueTransform;
			if (sourceTransform == null)
			{
				return;
			}

			var localScale = this.ScaleFactor
				* (Screen.height - this.heightToScreenHeightRatio / this.rectTransform.rect.height)
				* Vector3.one;

			var pos = this.Camera.WorldToScreenPoint(sourceTransform.position + this.SourceOffset) + this.ScreenOffset;

			this.rectTransform.localScale = localScale;
			this.rectTransform.position = pos;
		}
		#endregion
	}
}
