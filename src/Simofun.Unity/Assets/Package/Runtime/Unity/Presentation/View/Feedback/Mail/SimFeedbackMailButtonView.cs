//-----------------------------------------------------------------------
// <copyright file="SimFeedbackMailButtonView.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Feedback.Mail
{
	using Simofun.DOTween.Unity.Extensions;
	using Sirenix.OdinInspector;
	using UnityEngine;
	using UnityEngine.UI;

	public class SimFeedbackMailButtonView : MonoBehaviour
	{
		#region Unity Properties
		[field: SerializeField, Title(nameof(SimFeedbackMailButtonView), "References")]
		public virtual Button Button { get; protected set; }
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Start() => this.RegisterEventHandlers();
		#endregion

		#region Protected Methods
		#region Event Handlers
		#region Registration
		protected virtual void RegisterEventHandlers()
		{
			this.Button?.onClick.AddListener(() =>
				this.Button.transform.DoPunchScale(() =>
					Application.OpenURL(
						$"mailto:simofungames@gmail.com?subject=[User Feedback] '{Application.productName}' v{Application.version} {Application.platform}")));
		}
		#endregion
		#endregion
		#endregion
	}
}
