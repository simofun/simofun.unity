﻿//-----------------------------------------------------------------------
// <copyright file="ISimBaseView.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View
{
	public interface ISimBaseView
	{
		#region Methods
		void HideInstantly();

		void Show();

		void ShowInstantly();
		#endregion
	}
}
