﻿//-----------------------------------------------------------------------
// <copyright file="SimGameSettingsMenuButtonView.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Menu.Game.Settings
{
	public class SimGameSettingsMenuButtonView : SimButtonViewBase
	{
		#region Protected Methods
		#region Event Handlers
		protected override void HandleOnClick() => SimGameSettingsMenuView.Instance.Show();
		#endregion
		#endregion
	}
}
