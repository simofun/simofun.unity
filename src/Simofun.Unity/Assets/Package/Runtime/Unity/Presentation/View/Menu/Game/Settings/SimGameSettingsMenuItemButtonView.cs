//-----------------------------------------------------------------------
// <copyright file="SimGameSettingsMenuItemButtonView.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Menu.Game.Settings
{
	using Sirenix.OdinInspector;
	using UnityEngine;
	using UnityEngine.UI;

	public class SimGameSettingsMenuItemButtonView : MonoBehaviour
	{
		#region Unity Properties
		[field: SerializeField, Title(nameof(SimGameSettingsMenuItemButtonView), "References")]
		public virtual Button Button { get; protected set; }

		[field: SerializeField]
		protected virtual Sprite IsOnSprite { get; set; }

		[field: SerializeField]
		protected virtual Sprite IsOffSprite { get; set; }
		#endregion

		#region Properties
		public virtual bool IsOn
		{
			get => this.Button.image.sprite == this.IsOnSprite;
			set => this.Button.image.sprite = value ? this.IsOnSprite : this.IsOffSprite;
		}
		#endregion
	}
}
