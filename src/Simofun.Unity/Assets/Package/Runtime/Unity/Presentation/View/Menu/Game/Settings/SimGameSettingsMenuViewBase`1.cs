﻿//-----------------------------------------------------------------------
// <copyright file="SimGameSettingsMenuViewBase`1.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Menu.Game.Settings
{
	public abstract class SimGameSettingsMenuViewBase<T> : SimGameSettingsMenuViewBase<T, T>
		where T : SimGameSettingsMenuViewBase<T>, new()
	{
		#region Protected Constructors
		protected SimGameSettingsMenuViewBase() : base()
		{
		}
		#endregion
	}
}
