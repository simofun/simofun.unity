﻿//-----------------------------------------------------------------------
// <copyright file="SimGameSettingsMenuViewBase`2.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Menu.Game.Settings
{
	using Simofun.DOTween.Unity.Extensions;
	using Simofun.Unity.Data.Configuration.CustomAsset;
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using Sirenix.OdinInspector;
	using UnityEngine;
	using UnityEngine.UI;

	public abstract class SimGameSettingsMenuViewBase<TInstance, TConcrete> : SimMenuPanelViewBase<TInstance, TConcrete>
		where TInstance : ISimMenuView
		where TConcrete : SimGameSettingsMenuViewBase<TInstance, TConcrete>, TInstance, new()
	{
		#region Unity Properties
		[field: SerializeField, Title(nameof(SimGameSettingsMenuView), "References")]
		protected virtual Button CloseButton { get; set; }

		[field: SerializeField]
		protected virtual SimGameSettingsMenuItemButtonView AudioView { get; set; }

		[field: SerializeField]
		protected virtual SimGameSettingsMenuItemButtonView VibrationView { get; set; }
		#endregion

		#region Fields
		SimAudioConfig audioConfig;

		SimVibrationConfig vibrationConfig;
		#endregion

		#region Protected Properties
		protected virtual SimAudioConfig AudioConfig =>
			this.audioConfig != null
				? this.audioConfig
				: (this.audioConfig = SimCustomAssetConfigLocator.Instance.Get<SimAudioConfig>());

		protected virtual SimVibrationConfig VibrationConfig =>
			this.vibrationConfig != null
				? this.vibrationConfig
				: (this.vibrationConfig = SimCustomAssetConfigLocator.Instance.Get<SimVibrationConfig>());
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Start()
		{
			if (this.AudioView != null)
			{
				this.AudioView.IsOn = this.AudioConfig.IsEnabled.Value;
			}

			if (this.VibrationView != null)
			{
				this.VibrationView.IsOn = this.VibrationConfig.IsEnabled.Value;
			}

			this.RegisterEventHandlers();
		}
		#endregion

		#region Protected Methods
		#region Event Handlers
		#region Registration
		protected virtual void RegisterEventHandlers()
		{
			this.AudioView?.Button.onClick.AddListener(() =>
				this.AudioView.transform.DoPunchScale(() =>
				{
					this.AudioConfig.IsEnabled.Value = !this.AudioConfig.IsEnabled.Value;
					this.AudioView.IsOn = this.AudioConfig.IsEnabled.Value;
				}));
			this.CloseButton?.onClick.AddListener(() =>
				this.CloseButton.transform.DoPunchScale(() => this.Hide()));
			this.VibrationView?.Button.onClick.AddListener(() =>
				this.VibrationView.transform.DoPunchScale(() =>
				{
					this.VibrationConfig.IsEnabled.Value = !this.VibrationConfig.IsEnabled.Value;
					this.VibrationView.IsOn = this.VibrationConfig.IsEnabled.Value;
				}));
		}
		#endregion
		#endregion
		#endregion
	}
}
