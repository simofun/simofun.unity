//-----------------------------------------------------------------------
// <copyright file="SimGameplayMenuView.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Menu.Game
{
	public class SimGameplayMenuView : SimGameplayMenuViewBase<ISimGameplayMenuView, SimGameplayMenuView>
	{
	}
}
