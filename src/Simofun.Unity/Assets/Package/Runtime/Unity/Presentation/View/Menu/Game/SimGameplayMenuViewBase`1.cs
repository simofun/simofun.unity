﻿//-----------------------------------------------------------------------
// <copyright file="SimGameplayMenuViewBase`1.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Menu.Game
{
	public abstract class SimGameplayMenuViewBase<T> : SimGameplayMenuViewBase<T, T>
		where T : SimGameplayMenuViewBase<T>, new()
	{
		#region Protected Constructors
		protected SimGameplayMenuViewBase() : base()
		{
		}
		#endregion
	}
}
