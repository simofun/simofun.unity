﻿//-----------------------------------------------------------------------
// <copyright file="SimGameplayMenuViewBase`2.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Menu.Game
{
	public abstract class SimGameplayMenuViewBase<TInstance, TConcrete>
		: SimMenuView<TInstance, TConcrete>, ISimGameplayMenuView
		where TInstance : ISimGameplayMenuView
		where TConcrete : SimGameplayMenuViewBase<TInstance, TConcrete>, TInstance, new()
	{
		#region Fields
		bool isInitial = true;
		#endregion

		#region Properties
		/// <inheritdoc />
		public override bool IsInitial { get => this.isInitial; set => this.isInitial = value; }
		#endregion
	}
}
