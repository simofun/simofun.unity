﻿//-----------------------------------------------------------------------
// <copyright file="ISimMenuManager.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Menu
{
	public interface ISimMenuManager
	{
		#region Properties
		ISimMenuView RootMenu { get; }
		#endregion

		#region Public Methods
		void Hide();

		void Hide(bool isShowInitialMenu);

		void HideAll();

		void Show(ISimMenuView menu);

		void Show(ISimMenuView menu, bool isHideAllMenu);
		#endregion
	}
}
