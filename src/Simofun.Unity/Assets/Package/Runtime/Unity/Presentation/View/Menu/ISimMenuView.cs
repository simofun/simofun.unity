//-----------------------------------------------------------------------
// <copyright file="ISimMenuView.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Menu
{
	public interface ISimMenuView : ISimInitializable, ISimNamable, ISimVisibility
	{
		#region Properties
		/// <summary>
		/// Gets a value indicating whether the menu is visible at start-up.
		/// </summary>
		bool IsInitial { get; set; }
		#endregion

		#region Methods
		void Hide(bool isShowInitialMenu);

		void HideAll();

		void Show(bool isHideAllMenu);
		#endregion
	}
}
