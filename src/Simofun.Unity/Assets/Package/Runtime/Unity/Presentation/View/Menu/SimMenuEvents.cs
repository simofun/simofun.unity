//-----------------------------------------------------------------------
// <copyright file="SimMenuEvents.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Menu.Events
{
	using Simofun.Unity.Events;

	public class SimMenuEvents
	{
		public class OnShow : SimBaseUnityEvent
		{
			#region Public Fields
			public ISimMenuView Menu { get; set; }
			#endregion
		}

		public class OnHide : SimBaseUnityEvent
		{
			#region Public Fields
			public ISimMenuView Menu { get; set; }
			#endregion
		}
	}
}
