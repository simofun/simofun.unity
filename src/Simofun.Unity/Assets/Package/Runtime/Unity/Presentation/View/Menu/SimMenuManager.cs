//-----------------------------------------------------------------------
// <copyright file="SimMenuManager.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Menu
{
	public class SimMenuManager : SimMenuManagerBase<ISimMenuManager, SimMenuManager>
	{
	}
}
