﻿//-----------------------------------------------------------------------
// <copyright file="SimMenuManagerBase`1.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Menu
{
	public abstract class SimMenuManagerBase<T> : SimMenuManagerBase<T, T> where T : SimMenuManagerBase<T>, new()
	{
		#region Protected Constructors
		protected SimMenuManagerBase() : base()
		{
		}
		#endregion
	}
}
