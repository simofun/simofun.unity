﻿//-----------------------------------------------------------------------
// <copyright file="SimMenuManagerBase`2.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Menu
{
	using Simofun.UniRx.Unity;
	using Simofun.Unity.DesignPatterns.Singleton;
	using Simofun.Unity.Extensions;
	using Simofun.Unity.Presentation.View.Menu.Events;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;

	public abstract class SimMenuManagerBase<TInstance, TConcrete>
		: SimSceneSingletonBase<TInstance, TConcrete>, ISimMenuManager
		where TInstance : ISimMenuManager
		where TConcrete : SimMenuManagerBase<TInstance, TConcrete>, TInstance, new()
	{
		#region Fields
		readonly Stack<ISimMenuView> menuStack = new();

		ISimMenuView initialMenu;
		#endregion

		#region Properties
		public ISimMenuView RootMenu => this.menuStack.Count > 0 ? this.menuStack.Peek() : null;
		#endregion

		#region Unity Methods
		#region Editor
		/// <inheritdoc />
		protected virtual void OnDrawGizmos()
		{
			if (this.menuStack == null)
			{
				return;
			}

			if (this.RootMenu == null)
			{
				return;
			}

			for (var i = 0; i < this.menuStack.Count; i++)
			{
				var menu = this.menuStack.ElementAt(i);
				GUI.skin.DrawText(
					$"Index: {i} Name: {(menu as Component).name}",
					this.transform.position,
					Color.white,
					25,
					25 * i
				);
			}
		}
		#endregion

		/// <inheritdoc />
		protected override void Awake()
		{
			base.Awake();

			var isShowInitial = false;
			foreach (var menuPrefabModel in Resources.LoadAll<GameObject>("Prefabs/Menu/"))
			{
				var menuGo = Instantiate(menuPrefabModel, this.transform, false);
				var menu = menuGo.GetComponent<ISimMenuView>();
				if (isShowInitial || !menu.IsInitial)
				{
					menuGo.SetActive(false);

					continue;
				}

				menu.Show();
				isShowInitial = true;
				this.initialMenu = menu;
			}
		}
		#endregion

		#region Public Methods
		public virtual void Hide()
		{
			if (this.menuStack.Count == 0)
			{
				return;
			}

			var menu = this.menuStack.Pop();
			(menu as Component).gameObject.SetActive(false);

			if (this.menuStack.Count <= 0)
			{
				this.Show(this.initialMenu);

				return;
			}

			ISimMenuView topMenu = this.menuStack.Peek();
			this.ActivateMenu((topMenu as Component));

			SimMessageBus.Publish(new SimMenuEvents.OnHide { Menu = topMenu });
		}

		public virtual void Hide(bool isShowInitialMenu)
		{
			if (this.menuStack.Count == 0)
			{
				return;
			}

			var menu = this.menuStack.Pop();
			(menu as Component).gameObject.SetActive(false);

			if (this.menuStack.Count <= 0 || isShowInitialMenu)
			{
				this.Show(this.initialMenu);

				return;
			}

			ISimMenuView topMenu = this.menuStack.Peek();
			this.ActivateMenu((topMenu as Component));

			SimMessageBus.Publish(new SimMenuEvents.OnHide { Menu = topMenu });
		}

		public virtual void HideAll() =>
			this.menuStack.ToList().ForEach(m => (m as Component).gameObject.SetActive(false));

		public virtual void Show(ISimMenuView menu)
		{
			if (menu == null)
			{
				return;
			}

			var menuComp = (menu as Component);
			if (this.RootMenu != null && this.RootMenu.Equals(menu))
			{
				this.ActivateMenu(menuComp);

				return;
			}

			if (this.menuStack.Count > 0)
			{
				this.menuStack.ToList().ForEach(m => (m as Component).gameObject.SetActive(false));
			}

			this.ActivateMenu(menuComp);
			this.menuStack.Push(menu);

			SimMessageBus.Publish(new SimMenuEvents.OnShow { Menu = menu });
		}

		public virtual void Show(ISimMenuView menu, bool isHideAllMenu)
		{
			if (menu == null)
			{
				return;
			}

			var menuComp = (menu as Component);
			if (this.RootMenu != null && this.RootMenu.Equals(menu))
			{
				this.ActivateMenu(menuComp);

				return;
			}

			if (this.menuStack.Count > 0 && isHideAllMenu)
			{
				this.menuStack.ToList().ForEach(m => (m as Component).gameObject.SetActive(false));
			}

			this.ActivateMenu(menuComp);
			this.menuStack.Push(menu);

			SimMessageBus.Publish(new SimMenuEvents.OnShow { Menu = menu });
		}
		#endregion

		#region Methods
		void ActivateMenu(Component menuComp)
		{
			var go = menuComp.gameObject;
			if (!go.activeSelf)
			{
				go.SetActive(true);
			}
		}
		#endregion
	}
}
