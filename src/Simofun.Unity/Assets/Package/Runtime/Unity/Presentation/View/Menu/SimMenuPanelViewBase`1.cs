﻿//-----------------------------------------------------------------------
// <copyright file="SimMenuPanelViewBase`1.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Menu
{
	public abstract class SimMenuPanelViewBase<T> : SimMenuPanelViewBase<T, T> where T : SimMenuPanelViewBase<T>, new()
	{
		#region Protected Constructors
		protected SimMenuPanelViewBase() : base()
		{
		}
		#endregion
	}
}
