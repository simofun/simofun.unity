﻿//-----------------------------------------------------------------------
// <copyright file="SimMenuPanelViewBase`2.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Menu
{
	using DG.Tweening;
	using UnityEngine;

	public abstract class SimMenuPanelViewBase<TInstance, TConcrete> : SimMenuView<TInstance, TConcrete>
		where TInstance : ISimMenuView
		where TConcrete : SimMenuPanelViewBase<TInstance, TConcrete>, TInstance, new()
	{
		#region Fields
		RectTransform rectTransform;
		#endregion

		#region Protected Properties
		protected virtual RectTransform RectTransform
		{
			get => this.rectTransform != null
						? this.rectTransform
						: (this.rectTransform = this.GetComponent<RectTransform>());
			set => this.rectTransform = value;
		}
		#endregion

		#region Public Methods
		public override void Hide() =>
			this.RectTransform.DOScaleY(0, 0.2f).SetEase(Ease.InBack).From(1).OnComplete(() => base.Hide());

		public override void Show()
		{
			base.Show();

			this.RectTransform.DOScaleY(1, 0.5f).SetEase(Ease.OutBack).From(0);
		}

		public override void Show(bool beforeAllMenuPasife)
		{
			base.Show(beforeAllMenuPasife);

			this.RectTransform.DOScaleY(1, 0.5f).SetEase(Ease.OutBack).From(0);
		}
		#endregion
	}
}
