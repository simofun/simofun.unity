//-----------------------------------------------------------------------
// <copyright file="SimMenuView`2.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Menu
{
	using Simofun.Unity.DesignPatterns.Singleton;

	public abstract class SimMenuView<TInstance, TConcrete> : SimSceneSingletonBase<TInstance, TConcrete>, ISimMenuView
		where TInstance : ISimMenuView
		where TConcrete : SimMenuView<TInstance, TConcrete>, TInstance, new()
	{
		#region Fields
		ISimMenuManager menuManager;
		#endregion

		#region Properties
		/// <inheritdoc />
		public virtual bool IsInitial { get; set; }

		/// <inheritdoc />
		public virtual bool IsVisible { get; set; }
		#endregion

		#region Protected Properties
		protected virtual ISimMenuManager MenuManager
		{
			get => this.menuManager ??= SimMenuManager.Instance;
			set => this.menuManager = value;
		}
		#endregion

		#region Public  Methods
		/// <inheritdoc />
		public virtual void Hide()
		{
			this.MenuManager.Hide();

			this.IsVisible = false;
		}

		public virtual void Hide(bool isShowInitialMenu)
		{
			this.MenuManager.Hide(isShowInitialMenu);

			this.IsVisible = false;
		}

		public virtual void HideAll()
		{
			this.MenuManager.HideAll();
			this.IsVisible = false;
		}

		/// <inheritdoc />
		public virtual void Show()
		{
			this.MenuManager.Show(Instance);

			this.IsVisible = true;
		}

		public virtual void Show(bool isHideAllMenu)
		{
			this.MenuManager.Show(Instance, isHideAllMenu);

			this.IsVisible = true;
		}
		#endregion
	}
}
