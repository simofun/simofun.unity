//-----------------------------------------------------------------------
// <copyright file="SimOpenMenuParent.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

using UniRx;

namespace Simofun.Unity.Presentation.View.Menu
{
	using Simofun.UniRx.Unity;
	using Simofun.Unity.Presentation.View.Menu.Events;
	using UnityEngine;

	public class SimOpenMenuParent : MonoBehaviour
	{
		#region Fields
		ISimMenuManager menuManager;
		#endregion

		#region Protected Properties
		protected virtual ISimMenuManager MenuManager
		{
			get => this.menuManager != null ? this.menuManager : (this.menuManager = Menu.SimMenuManager.Instance);
			set => this.menuManager = value;
		}
		#endregion

		#region Unity Methods        
		/// <inheritdoc />
		protected virtual void Awake() => this.RegisterEventHandlers();
		#endregion

		#region Methods
		#region Event Handlers
		#region Registration
		protected virtual void RegisterEventHandlers()
		{
			this.UpdateParent();

			SimMessageBus.OnEvent<SimMenuEvents.OnShow>().Subscribe(this.HandleOnShow).AddTo(this);
			SimMessageBus.OnEvent<SimMenuEvents.OnHide>().Subscribe(this.HandleOnHide).AddTo(this);
		}
		#endregion

		protected virtual void HandleOnHide(SimMenuEvents.OnHide obj) => this.UpdateParent();

		protected virtual void HandleOnShow(SimMenuEvents.OnShow obj) => this.UpdateParent();
		#endregion

		protected virtual void UpdateParent()
		{
			var openMenu = (this.MenuManager.RootMenu as Component).transform;
			if (openMenu == null)
			{
				return;
			}

			var parent = this.transform.parent;
			if (parent == openMenu)
			{
				return;
			}

			this.transform.SetParent(openMenu);
		}
		#endregion
	}
}
