﻿//-----------------------------------------------------------------------
// <copyright file="SimPlaceholderMenuView`1.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Menu
{
	public abstract class SimPlaceholderMenuView<T> : SimPlaceholderMenuView<T, T>
		where T : SimPlaceholderMenuView<T>, new()
	{
		#region Protected Constructors
		protected SimPlaceholderMenuView() : base()
		{
		}
		#endregion
	}
}
