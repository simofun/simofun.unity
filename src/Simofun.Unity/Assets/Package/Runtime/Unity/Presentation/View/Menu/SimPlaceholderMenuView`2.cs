﻿//-----------------------------------------------------------------------
// <copyright file="SimPlaceholderMenuView`2.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Menu
{
	public abstract class SimPlaceholderMenuView<TInstance, TConcrete>
		: SimMenuView<TInstance, TConcrete>, ISimPlaceholderMenuView
		where TInstance : ISimPlaceholderMenuView
		where TConcrete : SimPlaceholderMenuView<TInstance, TConcrete>, TInstance, new()
	{
	}
}
