//-----------------------------------------------------------------------
// <copyright file="SimBaseView.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View
{
	using DG.Tweening;
	using Simofun.Unity.Extensions;
	using Sirenix.OdinInspector;
	using UnityEngine;

	[RequireComponent(typeof(CanvasGroup))]
	public class SimBaseView : MonoBehaviour, ISimBaseView
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimBaseView), "References")]
		CanvasGroup canvasGroup;
		#endregion

		#region Protected Properties
		protected virtual bool CanChangeVisibilitySelf { get; set; } = true;

		protected virtual CanvasGroup CanvasGroup
		{
			get => (this.canvasGroup != null)
						? this.canvasGroup
						: (this.canvasGroup = this.GetOrAddComponent<CanvasGroup>());
		}
		#endregion

		#region Public Methods
		public virtual void Hide()
		{
			this.CanvasGroup.interactable = false;
			this.CanvasGroup.DOKill(true);
			this.CanvasGroup.DOFade(0, 0.25f).SetUpdate(true).onComplete += () => {
				this.CanvasGroup.blocksRaycasts = false;
				this.ChangeVisibilitySelf(false);
			};
		}

		public virtual void HideInstantly()
		{
			this.CanvasGroup.interactable = false;
			this.CanvasGroup.blocksRaycasts = false;
			this.ChangeVisibilitySelf(false);
		}

		public virtual void Show()
		{
			this.ChangeVisibilitySelf(true);
			this.CanvasGroup.interactable = true;
			this.CanvasGroup.DOKill(true);
			this.CanvasGroup.DOFade(1, 0.25f).SetUpdate(true).onComplete += () => {
				this.CanvasGroup.blocksRaycasts = true;
			};
		}

		public virtual void ShowInstantly()
		{
			this.ChangeVisibilitySelf(true);
			this.CanvasGroup.interactable = true;
			this.CanvasGroup.blocksRaycasts = true;
		}
		#endregion

		#region Protected Methods
		protected virtual void ChangeVisibilitySelf(bool value)
		{
			if (!this.CanChangeVisibilitySelf)
			{
				return;
			}

			this.gameObject.SetActive(value);
		}
		#endregion
	}
}
