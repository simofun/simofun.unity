﻿//-----------------------------------------------------------------------
// <copyright file="SimBootView.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View
{
	using Simofun.Unity.Extensions;
	using Sirenix.OdinInspector;
	using System.Linq;
	using UnityEngine;

	[RequireComponent(typeof(Canvas))]
	public class SimBootView : SimBaseView
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimBootView), "References")]
		Canvas canvas;
		#endregion

		#region Protected Properties
		protected virtual Canvas Canvas =>
			(this.canvas != null) ? this.canvas : (this.canvas = this.GetOrAddComponent<Canvas>());
		#endregion

		#region Unity Methods
		/// <inheritdoc/>
		protected virtual void Awake()
		{
			this.Canvas.sortingOrder = FindObjectsByType<Canvas>(FindObjectsSortMode.None)
				.Max(c => c.sortingOrder) + 1;

			this.CanvasGroup.alpha = 1f;
			this.CanvasGroup.blocksRaycasts = true;
			this.CanvasGroup.interactable = true;
		}

		/// <inheritdoc/>
		protected virtual void OnDestroy()
		{
			this.CanvasGroup.alpha = 0f;
			this.CanvasGroup.blocksRaycasts = false;
			this.CanvasGroup.interactable = false;
		}
		#endregion
	}
}
