﻿//-----------------------------------------------------------------------
// <copyright file="SimButtonViewBase.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View
{
	using DG.Tweening;
	using Simofun.DOTween.Unity.Extensions;
	using UnityEngine;
	using UnityEngine.UI;

	public abstract class SimButtonViewBase : MonoBehaviour
	{
		#region Fields
		Tween tween;
		#endregion

		#region Protected Properties
		protected Button Button { get; set; }
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Awake()
		{
			this.Button = this.GetComponent<Button>();
			
			this.RegisterEventHandlers();
		}

		/// <inheritdoc />
		protected virtual void OnDestroy() => this.UnRegisterEventHandlers();
		#endregion

		#region Protected Methods
		#region Event Handlers
		#region Registration
		protected virtual void RegisterEventHandlers() =>
			this.Button.onClick.AddListener(this.HandleOnClickWithAnimation);

		protected virtual void UnRegisterEventHandlers()
		{
			this.Button.onClick.RemoveListener(this.HandleOnClickWithAnimation);

			this.tween?.Kill();
		}
		#endregion

		protected abstract void HandleOnClick();

		protected virtual void HandleOnClickWithAnimation()
		{
			this.tween?.Kill();

			this.tween = this.Button.transform.DoPunchScale(() => this.HandleOnClick());
		}
		#endregion
		#endregion
	}
}
