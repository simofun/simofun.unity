﻿//-----------------------------------------------------------------------
// <copyright file="SimFlexibleGridLayoutGroupFitType.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View
{
	public enum SimFlexibleGridLayoutGroupFitType
	{
		Uniform,

		Width,

		Height,

		FixedRows,

		FixedColumns
	}
}
