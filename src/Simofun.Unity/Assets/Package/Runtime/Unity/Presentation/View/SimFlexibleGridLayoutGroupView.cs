﻿//-----------------------------------------------------------------------
// <copyright file="SimFlexibleGridLayoutGroupView.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View
{
	using Sirenix.OdinInspector;
	using UnityEngine;
	using UnityEngine.UI;

	public class SimFlexibleGridLayoutGroupView : LayoutGroup
	{
		#region Unity Properties
		[field: SerializeField, Title(nameof(SimFlexibleGridLayoutGroupView), "Settings")]
		public virtual SimFlexibleGridLayoutGroupFitType FitType { get; protected set; }
			= SimFlexibleGridLayoutGroupFitType.Uniform;

		[field: SerializeField]
		public virtual int Rows { get; protected set; }

		[field: SerializeField]
		public virtual int Columns { get; protected set; }

		[field: SerializeField]
		public virtual Vector2 CellSize { get; protected set; }

		[field: SerializeField]
		public virtual Vector2 Spacing { get; protected set; }

		[field: SerializeField]
		public virtual bool IsFitX { get; protected set; }

		[field: SerializeField]
		public virtual bool IsFitY { get; protected set; }
		#endregion

		#region Public Methods
		public override void CalculateLayoutInputHorizontal()
		{
			base.CalculateLayoutInputHorizontal();

			if (this.FitType == SimFlexibleGridLayoutGroupFitType.Width
					|| this.FitType == SimFlexibleGridLayoutGroupFitType.Height
					|| this.FitType == SimFlexibleGridLayoutGroupFitType.Uniform)
			{
				this.Rows = this.Columns = Mathf.CeilToInt(Mathf.Sqrt(this.transform.childCount));

				switch (this.FitType)
				{
					case SimFlexibleGridLayoutGroupFitType.Width:
						{
							this.IsFitX = true;
							this.IsFitY = false;

							break;
						}
					case SimFlexibleGridLayoutGroupFitType.Height:
						{
							this.IsFitX = false;
							this.IsFitY = true;

							break;
						}
					case SimFlexibleGridLayoutGroupFitType.Uniform:
						{
							this.IsFitX = this.IsFitY = true;

							break;
						}
				}
			}

			if (this.FitType == SimFlexibleGridLayoutGroupFitType.Width
					|| this.FitType == SimFlexibleGridLayoutGroupFitType.FixedColumns)
			{
				this.Rows = Mathf.CeilToInt(this.transform.childCount / (float)this.Columns);
			}

			if (this.FitType == SimFlexibleGridLayoutGroupFitType.Height
					|| this.FitType == SimFlexibleGridLayoutGroupFitType.FixedRows)
			{
				this.Columns = Mathf.CeilToInt(this.transform.childCount / (float)this.Rows);
			}

			var parentWidth = this.rectTransform.rect.width;
			var parentHeight = this.rectTransform.rect.height;

			var cellWidth = parentWidth
				/ (float)this.Columns
				- ((this.Spacing.x / (float)this.Columns) * (this.Columns - 1))
				- (this.padding.left / (float)this.Columns)
				- (this.padding.right / (float)this.Columns);
			var cellHeight = parentHeight
				/ (float)this.Rows
				- ((this.Spacing.y / (float)this.Rows) * (this.Rows - 1))
				- (this.padding.top / (float)this.Rows)
				- (this.padding.bottom / (float)this.Rows);

			var cellSize = this.CellSize;
			cellSize.x = this.IsFitX ? cellWidth : this.CellSize.x;
			cellSize.y = this.IsFitY ? cellHeight : this.CellSize.y;
			this.CellSize = cellSize;

			for (var i = 0; i < this.rectChildren.Count; i++)
			{
				var rowCount = i / this.Columns;
				var columnCount = i % this.Columns;

				var item = this.rectChildren[i];

				var xPos = (this.CellSize.x * columnCount) + (this.Spacing.x * columnCount) + this.padding.left;
				var yPos = (this.CellSize.y * rowCount) + (this.Spacing.y * rowCount) + this.padding.top;

				this.SetChildAlongAxis(item, 0, xPos, this.CellSize.x);
				this.SetChildAlongAxis(item, 1, yPos, this.CellSize.y);
			}
		}

		public override void CalculateLayoutInputVertical()
		{
		}

		public override void SetLayoutHorizontal()
		{
		}

		public override void SetLayoutVertical()
		{
		}
		#endregion
	}
}
