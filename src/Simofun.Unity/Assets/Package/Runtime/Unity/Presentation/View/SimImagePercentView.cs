//-----------------------------------------------------------------------
// <copyright file="SimImagePercentView.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

using TMPro;

namespace Simofun.Unity.Presentation.View
{
	using Sirenix.OdinInspector;
	using UnityEngine;
	using UnityEngine.UI;

	public class SimImagePercentView : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimContinuousRotater), "Settings")]
		string format;

		[SerializeField, Title("", "References")]
		TextMeshProUGUI text;

		[SerializeField]
		Image image;
		#endregion

		#region Fields
		float previousFillAmount = -1;
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Update()
		{
			var fillAmount = this.image.fillAmount;
			if (this.previousFillAmount == fillAmount)
			{
				return;
			}

			this.text.text = string.Format(this.format, ((int)(fillAmount * 100)));
			this.previousFillAmount = fillAmount;
		}
		#endregion
	}
}
