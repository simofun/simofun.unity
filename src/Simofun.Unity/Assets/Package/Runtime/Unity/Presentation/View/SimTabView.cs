﻿//-----------------------------------------------------------------------
// <copyright file="SimTabView.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

using UniRx;

namespace Simofun.CandyLab.Unity.Presentation.View
{
	using Sirenix.OdinInspector;
	using UnityEngine;
	using UnityEngine.UI;

	public class SimTabView : MonoBehaviour
	{
		#region Unity Properties
		[field: SerializeField, Title(nameof(SimTabView), "References")]
		public virtual Transform Content { get; set; }

		[field: SerializeField]
		public virtual Toggle Toggle { get; set; }
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Awake() =>
			this.Toggle.OnValueChangedAsObservable()
				.Subscribe((isOn) => this.Content.gameObject.SetActive(isOn))
				.AddTo(this);
		#endregion
	}
}
