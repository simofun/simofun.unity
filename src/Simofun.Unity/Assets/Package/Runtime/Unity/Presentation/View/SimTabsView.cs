﻿//-----------------------------------------------------------------------
// <copyright file="TabsView.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.CandyLab.Unity.Presentation.View
{
	using Sirenix.OdinInspector;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.UI;

	public class SimTabsView : MonoBehaviour
	{
		#region Unity Properties
		[field: SerializeField, Title(nameof(SimTabsView), "References")]
		public List<SimTabView> Tabs { get; set; }
		#endregion

		#region Public Methods
		public virtual void SetActiveTab(int index)
		{
			for (var i = 0; i < this.Tabs.Count; i++)
			{
				this.Tabs[i].gameObject.SetActive(i == index);
			}
		}
		#endregion
	}
}
