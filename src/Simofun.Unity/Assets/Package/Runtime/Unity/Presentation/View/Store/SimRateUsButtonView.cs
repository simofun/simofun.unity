//-----------------------------------------------------------------------
// <copyright file="SimRateUsButtonView.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Store
{
	using Simofun.Store.Events;
	using Simofun.UniRx.Unity;
	using Sirenix.OdinInspector;
	using UnityEngine;
	using UnityEngine.UI;

	public class SimRateUsButtonView : MonoBehaviour
	{
		#region Unity Properties
		[field: SerializeField, Title(nameof(SimRateUsButtonView), "References")]
		public virtual Button Button { get; protected set; }
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Start() => this.RegisterEventHandlers();
		#endregion

		#region Protected Methods
		#region Event Handlers
		#region Registration
		protected virtual void RegisterEventHandlers()
		{
			this.Button?.onClick.AddListener(() => SimMessageBus.Publish(new SimStoreEvents.OnOpenRateUs()));
		}
		#endregion
		#endregion
		#endregion
	}
}
