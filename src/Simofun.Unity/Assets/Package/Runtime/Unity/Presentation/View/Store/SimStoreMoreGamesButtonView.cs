//-----------------------------------------------------------------------
// <copyright file="SimStoreMoreGamesButtonView.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Presentation.View.Store
{
	using Simofun.DOTween.Unity.Extensions;
	using Sirenix.OdinInspector;
	using UnityEngine;
	using UnityEngine.UI;

	public class SimStoreMoreGamesButtonView : MonoBehaviour
	{
		#region Unity Properties
		[field: SerializeField, Title(nameof(SimStoreMoreGamesButtonView), "References")]
		public virtual Button Button { get; protected set; }
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Start() => this.RegisterEventHandlers();
		#endregion

		#region Protected Methods
		#region Event Handlers
		#region Registration
		protected virtual void RegisterEventHandlers()
		{
			this.Button?.onClick.AddListener(() =>
				this.Button.transform.DoPunchScale(() =>
					Application.OpenURL(
#if UNITY_ANDROID
						"https://play.google.com/store/apps/dev?id=8282371912455247840"
#elif UNITY_IOS
						"https://apps.apple.com/us/developer/si-mofun-oyun-teknoloji-leri-anoni-m-%C5%9Fi-rketi/id1642942075"
#else
						"https://www.simofun.com/our-games"
#endif
						)));
		}
		#endregion
		#endregion
		#endregion
	}
}
