//-----------------------------------------------------------------------
// <copyright file="SimStoreRateUsPanelView.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

using UniRx;

namespace Simofun.Unity.Presentation.View.Store
{
	using Cysharp.Threading.Tasks;
	using Simofun.Store.Events;
	using Simofun.UniRx.Unity;
	using Simofun.Unity.Presentation.View;
	using Sirenix.OdinInspector;
	using UnityEngine;
	using UnityEngine.UI;

	public class SimStoreRateUsPanelView : SimBaseView
	{
		#region Unity Properties
		[field: SerializeField, Title(nameof(SimStoreRateUsPanelView), "Settings")]
		protected virtual string AppleAppId { get; set; }

		[field: SerializeField, Title("", "References")]
		protected virtual Button CancelButton { get; set; }

		[field: SerializeField]
		protected virtual Button OkButton { get; set; }
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Awake()
		{
			this.RegisterEventHandlers();

			this.HideInstantly();
		}

		/// <inheritdoc />
		protected virtual void OnDestroy() => this.UnRegisterEventHandlers();
		#endregion

		#region Protected Methods
		#region Event Handlers
		#region Registration
		protected virtual void RegisterEventHandlers()
		{
			SimMessageBus.OnEvent<SimStoreEvents.OnOpenRateUs>()
				.Subscribe(ev => this.HandleOnOpenRateUs(ev))
				.AddTo(this);
			this.CancelButton?.onClick.AddListener(this.HandleOnCancelButtonClick);
			this.OkButton?.onClick.AddListener(this.HandleOnOkButtonClick);
		}

		protected virtual void UnRegisterEventHandlers()
		{
			this.CancelButton?.onClick.RemoveListener(this.HandleOnCancelButtonClick);
			this.OkButton?.onClick.RemoveListener(this.HandleOnOkButtonClick);
		}
		#endregion

		protected virtual void HandleOnCancelButtonClick() => this.Hide();

		protected virtual void HandleOnOkButtonClick()
		{
			this.Hide();
			
#if !UNITY_EDITOR && UNITY_ANDROID
			Application.OpenURL("market://details?id=" + Application.identifier);
#elif !UNITY_EDITOR && UNITY_IOS
			Application.OpenURL("itms-apps://itunes.apple.com/app/id" + this.AppleAppId);
#endif
		}

		protected virtual void HandleOnOpenRateUs(SimStoreEvents.OnOpenRateUs ev) => this.Show();
		#endregion
		#endregion
	}
}
