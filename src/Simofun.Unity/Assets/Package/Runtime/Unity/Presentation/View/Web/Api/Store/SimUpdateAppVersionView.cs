//-----------------------------------------------------------------------
// <copyright file="SimUpdateAppVersionView.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

using UniRx;

namespace Simofun.Unity.Presentation.View.Web.Api.Store
{
	using Cysharp.Threading.Tasks;
	using Simofun.UniRx.Unity;
	using Simofun.Unity.Presentation.View;
	using Simofun.Unity.Web.API.Store.Events;
	using Sirenix.OdinInspector;
	using System.Threading;
	using UnityEngine;
	using UnityEngine.UI;

	public class SimUpdateAppVersionView : SimBaseView
	{
		#region Fields
		CancellationToken? cancellationToken;

		string applicationIdentifier;
		#endregion

		#region Unity Properties
		[field: SerializeField, Title(nameof(SimUpdateAppVersionView), "Settings")]
		protected virtual string AppleAppId { get; set; }

		[field: SerializeField, Title("", "References")]
		protected virtual Button CancelButton { get; set; }

		[field: SerializeField]
		protected virtual Button OkButton { get; set; }
		#endregion

		#region Protected Properties
		protected virtual CancellationToken CancellationToken =>
			this.cancellationToken.HasValue
				? this.cancellationToken.Value
				: (this.cancellationToken = this.GetCancellationTokenOnDestroy()).Value;

		public string ApplicationIdentifier
		{
			get => this.applicationIdentifier ??= Application.identifier;
			set
			{
				if (value == null)
				{
					return;
				}

				this.applicationIdentifier = value;
			}
		}
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Awake()
		{
			this.RegisterEventHandlers();

			this.HideInstantly();

			UniTask.Create(async () =>
			{
				await UniTask.Delay(System.TimeSpan.FromSeconds(5), cancellationToken: this.CancellationToken);

				this.Destroy();
			});
		}

		/// <inheritdoc />
		protected virtual void OnDestroy() => this.UnRegisterEventHandlers();
		#endregion

		#region Public Methods
		public virtual void Destroy() => Destroy(this.gameObject);
		#endregion

		#region Protected Methods
		#region Event Handlers
		#region Registration
		protected virtual void RegisterEventHandlers()
		{
			SimMessageBus.OnEvent<SimStoreAppVersionEvents.OnLatest>()
				.Subscribe(ev => this.HandleOnVersionLatestEvent(ev))
				.AddTo(this);
			SimMessageBus.OnEvent<SimStoreAppVersionEvents.OnUpdate>()
				.Subscribe(ev => this.HandleOnVersionUpdateEvent(ev))
				.AddTo(this);
			this.CancelButton?.onClick.AddListener(this.HandleOnCancelButtonClick);
			this.OkButton?.onClick.AddListener(this.HandleOnOkButtonClick);
		}

		protected virtual void UnRegisterEventHandlers()
		{
			this.CancelButton?.onClick.RemoveListener(this.HandleOnCancelButtonClick);
			this.OkButton?.onClick.RemoveListener(this.HandleOnOkButtonClick);
		}
		#endregion

		protected virtual void HandleOnCancelButtonClick() => this.Destroy();

		protected virtual void HandleOnOkButtonClick()
		{
#if !UNITY_EDITOR && UNITY_ANDROID
			Application.OpenURL("market://details?id=" + this.ApplicationIdentifier);
#elif !UNITY_EDITOR && UNITY_IOS
			Application.OpenURL("itms-apps://itunes.apple.com/app/id" + this.AppleAppId);
#endif
			this.Destroy();
		}

		protected virtual void HandleOnVersionLatestEvent(SimStoreAppVersionEvents.OnLatest ev) => this.Destroy();

		protected virtual void HandleOnVersionUpdateEvent(SimStoreAppVersionEvents.OnUpdate ev)
		{
			this.ApplicationIdentifier = ev.ApplicationIdentifier;

			this.Show();
		}
		#endregion
		#endregion
	}
}
