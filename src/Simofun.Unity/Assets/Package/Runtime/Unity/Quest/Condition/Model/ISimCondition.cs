//-----------------------------------------------------------------------
// <copyright file="ISimCondition.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Quest.Condition.Model
{
    public interface ISimCondition : ISimConstructible
    {
        #region Events
        event System.Action<ISimCondition> OnMet;
        #endregion

        #region Methods
        bool Check();
        #endregion
    }
}
