//-----------------------------------------------------------------------
// <copyright file="SimCondition.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Quest.Condition.Model
{
	using Simofun.Unity.Quest.Model;
	using System.Linq;
	using UnityEngine;

	public abstract class SimCondition : ScriptableObject, ISimCondition
	{
		#region Constructors
		public virtual void Construct(params object[] deps)
		{
			if (deps.FirstOrDefault(dep => (dep is ISimQuest)) is not ISimQuest questInDeps)
			{
				Debug.LogError($"{this.GetType()} not constructed. Can't find Quest!");

				return;
			}

			this.Quest = questInDeps;
		}
		#endregion

		#region Events
		public event System.Action<ISimCondition> OnMet;
		#endregion

		#region Protected Properties
		protected ISimQuest Quest { get; set; }
		#endregion

		#region Protected Methods
		protected virtual void InvokeOnMet() => this.OnMet?.Invoke(this);
		#endregion

		#region Public Methods
		public abstract bool Check();
		#endregion
	}
}
