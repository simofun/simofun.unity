//-----------------------------------------------------------------------
// <copyright file="SimQuestEvents.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Quest.Events
{
	using Simofun.Unity.Events;
	using Simofun.Unity.Quest.Model;

	public class SimQuestEvents
	{
		public class OnCompleted : SimBaseUnityEvent
		{
			public ISimQuest Quest { get; set; }
		}

		public class OnStarted : SimBaseUnityEvent
		{
			public ISimQuest Quest { get; set; }
		}

		public class OnUpdated : SimBaseUnityEvent
		{
			public ISimQuest Quest { get; set; }
		}
	}
}
