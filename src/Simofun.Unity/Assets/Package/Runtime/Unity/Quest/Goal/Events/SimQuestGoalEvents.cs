﻿//-----------------------------------------------------------------------
// <copyright file="SimQuestGoalEvents.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Quest.Events
{
	using Simofun.Unity.Events;
	using Simofun.Unity.Quest.Goal;

	public class SimQuestGoalEvents
	{
		public class OnCompleted : SimBaseUnityEvent
		{
			public ISimGoal Goal { get; set; }
		}

		public class OnSkip : SimBaseUnityEvent
		{
			public ISimGoal Goal { get; set; }
		}

		public class OnStarted : SimBaseUnityEvent
		{
			public ISimGoal Goal { get; set; }
		}

		public class OnUpdate : SimBaseUnityEvent
		{
			public ISimGoal Goal { get; set; }
		}
	}
}
