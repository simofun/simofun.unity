//-----------------------------------------------------------------------
// <copyright file="ISimGoal.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

using UniRx;

namespace Simofun.Unity.Quest.Goal
{
	public interface ISimGoal : ISimConstructible
	{
		#region Properties
		BoolReactiveProperty IsCompleted { get; set; }

		IntReactiveProperty CurrentAmount { get; set; }

		IntReactiveProperty RequiredAmount { get; set; }

		StringReactiveProperty Description { get; set; }

		StringReactiveProperty Name { get; set; }
		#endregion

		#region Events
		event System.Action<ISimGoal> OnCompleted;

		event System.Action<ISimGoal> OnUpdate;
		#endregion

		#region Methods
		float EvaluatePercent();

		void Complete();

		void Evaluate();

		void Skip();
		#endregion
	}
}
