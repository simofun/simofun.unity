//-----------------------------------------------------------------------
// <copyright file="SimGoal.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

using UniRx;

namespace Simofun.Unity.Quest.Goal
{
	using Newtonsoft.Json;
	using Simofun.UniRx.Unity;
	using Simofun.Unity.Data.Configuration.CustomAsset.Extensions;
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using Simofun.Unity.Quest.Events;
	using Sirenix.OdinInspector;
	using UnityEngine;

	[System.Serializable]
	public abstract class SimGoal : SimCustomAssetConfigBase, ISimGoal
	{
		#region Unity Properties
		[field: JsonProperty, SerializeField, Title(nameof(SimGoal), "Settings")]
		[JsonIgnore]
		public StringReactiveProperty Name { get; set; } = new();

		[field: JsonProperty, SerializeField]
		[JsonIgnore]
		public StringReactiveProperty Description { get; set; } = new();

		[field: JsonProperty, SerializeField]
		[JsonIgnore]
		public IntReactiveProperty RequiredAmount { get; set; } = new();

		[field: JsonProperty, SerializeField]
		[JsonIgnore]
		public BoolReactiveProperty IsCompleted { get; set; } = new();

		[field: JsonProperty, SerializeField]
		[JsonIgnore]
		public IntReactiveProperty CurrentAmount { get; set; } = new();
		#endregion

		#region Events
		public event System.Action<ISimGoal> OnCompleted;

		public event System.Action<ISimGoal> OnUpdate;
		#endregion

		#region Construct
		/// <inheritdoc />
		public virtual void Construct(params object[] deps)
		{
			this.IsCompleted.Value = false;

			SimMessageBus.Publish(new SimQuestGoalEvents.OnStarted { Goal = this });
		}
		#endregion

		#region Public Methods
		public virtual float EvaluatePercent() => (float)this.CurrentAmount.Value / (float)this.RequiredAmount.Value;

		public virtual void Complete()
		{
			if (this.IsCompleted.Value)
			{
				return;
			}

			this.IsCompleted.Value = true;
			this.OnCompleted?.Invoke(this);

			SimMessageBus.Publish(new SimQuestGoalEvents.OnCompleted { Goal = this });
		}

		public virtual void Evaluate()
		{
			this.OnUpdate?.Invoke(this);

			SimMessageBus.Publish(new SimQuestGoalEvents.OnUpdate() { Goal = this });

			if (this.CurrentAmount.Value >= this.RequiredAmount.Value)
			{
				this.Complete();
			}
		}

		public virtual void Skip()
		{
			this.Complete();

			SimMessageBus.Publish(new SimQuestGoalEvents.OnSkip { Goal = this });
		}
		#endregion

		#region Protected Methods
		#region Event Handlers
		#region Initialization
		protected override void RegisterEventHandlers()
		{
			this.CurrentAmount.Subscribe((v) => this.Save()).AddTo(this.Disposables);
			this.Description.Subscribe((v) => this.Save()).AddTo(this.Disposables);
			this.IsCompleted.Subscribe((v) => this.Save()).AddTo(this.Disposables);
			this.Name.Subscribe((v) => this.Save()).AddTo(this.Disposables);
			this.RequiredAmount.Subscribe((v) => this.Save()).AddTo(this.Disposables);
		}
		#endregion
		#endregion
		#endregion
	}
}
