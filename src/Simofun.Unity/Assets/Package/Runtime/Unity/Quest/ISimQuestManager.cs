//-----------------------------------------------------------------------
// <copyright file="ISimQuestManager.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Quest
{
    using Simofun.Unity.Quest.Model;
    using System.Collections.Generic;

    public interface ISimQuestManager
    {
        #region Properties
        List<ISimQuest> ActiveQuests { get; }
        #endregion

        #region Methods
        void StartQuest(ISimQuest quest, float startDelay = 2);
        #endregion
    }
}
