//-----------------------------------------------------------------------
// <copyright file="ISimQuest.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

using UniRx;

namespace Simofun.Unity.Quest.Model
{
	using Simofun.IO;
	using Simofun.Unity.Quest.Condition.Model;
	using Simofun.Unity.Quest.Goal;
	using System.Collections.Generic;

	public interface ISimQuest : ISimRootPath, ISimCondition
	{
		#region Properties
		List<SimCondition> Conditions { get; }

		List<SimGoal> Goals { get; }

		BoolReactiveProperty IsActive { get; }

		BoolReactiveProperty IsCompleted { get; }

		BoolReactiveProperty IsGivenReward { get; }

		IntReactiveProperty Id { get; set; }

		ReactiveProperty<SimQuestInfo> Info { get; }

		ReactiveProperty<SimQuestReward> Reward { get; set; }
		#endregion

		#region Events
		event System.Action<ISimQuest> OnComplete;

		event System.Action<ISimQuest> OnStart;

		event System.Action<ISimQuest> OnUpdate;
		#endregion

		#region Methods
		float EvaluatePercent();

		void Complete();

		void Evaluate();
		#endregion
	}
}
