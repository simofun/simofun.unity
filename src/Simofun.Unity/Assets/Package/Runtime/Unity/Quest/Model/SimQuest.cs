//-----------------------------------------------------------------------
// <copyright file="SimQuest.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

using UniRx;

namespace Simofun.Unity.Quest.Model
{
	using Bayhaksam.Unity.Logging.DevelopmentBuild;
	using Newtonsoft.Json;
	using Simofun.UniRx.Unity;
	using Simofun.Unity.Data.Configuration.CustomAsset.Extensions;
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using Simofun.Unity.Quest.Condition.Model;
	using Simofun.Unity.Quest.Events;
	using Simofun.Unity.Quest.Goal;
	using Sirenix.OdinInspector;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;

	[CreateAssetMenu(
		fileName = "Quest",
		menuName = SimMenuItems.Path_ + nameof(Unity) + "/" + nameof(Quest) + "/Create Quest")]
	public class SimQuest : SimCustomAssetConfigBase, ISimQuest
	{
		#region Unity Properties
		[field: JsonProperty, SerializeField, Title(nameof(SimQuest), "Settings"),
			TitleGroup(
				"$name", "", alignment: TitleAlignments.Centered,
				horizontalLine: true,
				boldTitle: true,
				indent: false)]
		[JsonIgnore]
		public ReactiveProperty<SimQuestInfo> Info { get; protected set; } = new();

		[field: JsonProperty, SerializeField, Space(10)]
		[JsonIgnore]
		public IntReactiveProperty Id { get; set; } = new();

		[field: JsonProperty, SerializeField, Space(10)]
		[JsonIgnore]
		public BoolReactiveProperty IsGivenReward { get; protected set; } = new();

		[field: JsonProperty, SerializeField, Space(5), ShowIf("@this.IsGivenReward.Value")]
		[JsonIgnore]
		public ReactiveProperty<SimQuestReward> Reward { get; set; } = new(new SimQuestReward());

		[field: JsonProperty, SerializeField, Space(10)]
		[JsonIgnore]
		public BoolReactiveProperty IsCompleted { get; protected set; } = new();

		[field: JsonProperty, SerializeField, Space(10)]
		[JsonIgnore]
		public BoolReactiveProperty IsActive { get; protected set; } = new();

		[field: JsonIgnore, SerializeField, Space(10)]
		[JsonIgnore]
		public List<SimCondition> Conditions { get; protected set; }

		[field: JsonIgnore, SerializeField, Space(10)]
		[JsonIgnore]
		public List<SimGoal> Goals { get; protected set; }
		#endregion

		#region Properties
		[JsonIgnore]
		public string RootPath { get; set; } = "Data/Quest/";
		#endregion

		#region Protected Properties
		[JsonIgnore]
		protected virtual int CurrentConditionIndex { get; set; }

		[JsonIgnore]
		protected virtual ISimCondition CurrentCondition { get; set; }
		#endregion

		#region Events
		public event System.Action<ISimCondition> OnMet;

		public event System.Action<ISimQuest> OnComplete;

		public event System.Action<ISimQuest> OnStart;

		public event System.Action<ISimQuest> OnUpdate;
		#endregion

		#region Construct
		/// <inheritdoc />
		public virtual void Construct(params object[] deps)
		{
			if (this.Conditions == null || this.Conditions.Count <= 0)
			{
				debug.LogError($"{this.name} has no condition to construct!");

				return;
			}
			
			// Unity sets initial value as '1' instead '0'. Set before condition evaluation.
			this.CurrentConditionIndex = 0;

			this.NextCondition();
		}
		#endregion

		#region Public Methods
		/// <inheritdoc />
		public virtual bool Check() => this.CurrentCondition == null;

		/// <inheritdoc />
		public virtual float EvaluatePercent()
		{
			var goalSumPercent = this.Goals.Sum(goal =>
			{
				var value = goal.EvaluatePercent();

				return value;
			});

			return goalSumPercent / this.Goals.Count;
		}

		/// <inheritdoc />
		public virtual void Complete()
		{
			this.IsCompleted.Value = true;
			this.IsActive.Value = false;
			this.OnComplete?.Invoke(this);

			SimMessageBus.Publish(new SimQuestEvents.OnCompleted { Quest = this });
		}

		/// <inheritdoc />
		public virtual void Evaluate()
		{
			if (this.IsCompleted.Value)
			{
				return;
			}

			var isAllGoalsCompleted = true;

			foreach (var goal in this.Goals)
			{
				if (goal.IsCompleted.Value)
				{
					continue;
				}

				goal.Evaluate();

				if (!goal.IsCompleted.Value)
				{
					isAllGoalsCompleted = false;
				}
			}

			this.OnUpdate?.Invoke(this);
			SimMessageBus.Publish(new SimQuestEvents.OnUpdated { Quest = this });

			if (isAllGoalsCompleted)
			{
				this.Complete();
			}
		}
		#endregion

		#region Protected Methods
		#region Event Handlers
		#region Initialization
		protected override void RegisterEventHandlers()
		{
			this.Id.Subscribe((v) => this.Save()).AddTo(this.Disposables);
			this.Info.Subscribe((v) => this.Save()).AddTo(this.Disposables);
			this.IsActive.Subscribe((v) => this.Save()).AddTo(this.Disposables);
			this.IsCompleted.Subscribe((v) => this.Save()).AddTo(this.Disposables);
			this.IsGivenReward.Subscribe((v) => this.Save()).AddTo(this.Disposables);
			this.Reward.Subscribe((v) => this.Save()).AddTo(this.Disposables);
		}
		#endregion

		protected virtual void HandleOnAllConditionsMet()
		{
			this.CurrentCondition = null;

			// All conditions is met, then start the quest.
			this.OnMet?.Invoke(this);
			this.StartQuest();
		}

		protected virtual void HandleOnCurrentConditionMet(ISimCondition condition)
		{
			this.CurrentCondition.OnMet -= this.HandleOnCurrentConditionMet;

			if (++this.CurrentConditionIndex >= this.Conditions.Count)
			{
				this.HandleOnAllConditionsMet();

				return;
			}

			this.NextCondition();
		}

		protected virtual void HandleOnGoalCompleted(ISimGoal goal)
		{
			goal.OnCompleted -= this.HandleOnGoalCompleted;
			goal.OnUpdate -= this.HandleOnGoalUpdate;
			this.Evaluate();

			if (this.IsCompleted.Value)
			{
				return;
			}

			var nextGoal = this.Goals.FirstOrDefault(g => !g.IsCompleted.Value);
			if (nextGoal == null)
			{
				return;
			}

			nextGoal.Construct();
			nextGoal.OnCompleted += this.HandleOnGoalCompleted;
			nextGoal.OnUpdate += this.HandleOnGoalUpdate;
		}

		protected virtual void HandleOnGoalUpdate(ISimGoal goal) => this.OnUpdate?.Invoke(this);
		#endregion

		protected virtual void NextCondition()
		{
			var hasAnyNotCompletedCondition = false;
			for (; this.CurrentConditionIndex < this.Conditions.Count; this.CurrentConditionIndex++)
			{
				this.CurrentCondition = this.Conditions[this.CurrentConditionIndex];

				this.CurrentCondition.Construct(this);

				if (this.CurrentCondition.Check())
				{
					continue;
				}

				hasAnyNotCompletedCondition = true;
				this.CurrentCondition.OnMet += this.HandleOnCurrentConditionMet;
				debug.Log($"'{this.name}' quest couldn't be started. '{this.CurrentCondition}' condition isn't met");

				break;
			}

			if (!hasAnyNotCompletedCondition)
			{
				this.HandleOnAllConditionsMet();
			}
		}

		protected virtual void StartQuest()
		{
			this.IsCompleted.Value = false;
			this.IsActive.Value = true;

			// Start first quest
			if (this.Goals == null || this.Goals.Count == 0)
			{
				this.Complete();

				return;
			}

			var firstGoal = this.Goals.FirstOrDefault(goal => !goal.IsCompleted.Value);
			if (firstGoal == null)
			{
				this.Complete();

				return;
			}

			firstGoal.Construct();
			firstGoal.OnCompleted += this.HandleOnGoalCompleted;
			firstGoal.OnUpdate += this.HandleOnGoalUpdate;

			this.OnStart?.Invoke(this);
			SimMessageBus.Publish(new SimQuestEvents.OnStarted { Quest = this });

			debug.Log($"{this.name} quest starting..");
		}
		#endregion
	}
}
