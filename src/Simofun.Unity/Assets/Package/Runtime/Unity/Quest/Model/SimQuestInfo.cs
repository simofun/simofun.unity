//-----------------------------------------------------------------------
// <copyright file="SimQuestInfo.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Quest.Model
{
	using Newtonsoft.Json;
	using Sirenix.OdinInspector;
	using UnityEngine;

	[System.Serializable]
	public struct SimQuestInfo
	{
		#region Public Fields
		[PreviewField(75, ObjectFieldAlignment.Center), HideLabel, Space(10), System.NonSerialized, ShowInInspector]
		[JsonIgnore, SerializeField]
		Sprite icon;

		[TextArea, HideLabel, Space(10)]
		[JsonProperty, SerializeField, Title(nameof(SimQuestInfo), "Settings")]
		string description;

		[JsonProperty, SerializeField, Space(10)]
		bool isHide;
		#endregion

		#region Properties
		[JsonIgnore]
		public bool IsHide => this.isHide;

		[JsonIgnore]
		public Sprite Icon => this.icon;

		[JsonIgnore]
		public string Description => this.description;
		#endregion
	}
}
