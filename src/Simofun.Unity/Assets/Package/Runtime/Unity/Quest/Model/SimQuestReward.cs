//-----------------------------------------------------------------------
// <copyright file="SimQuestReward.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

using UniRx;

namespace Simofun.Unity.Quest.Model
{
	using Sirenix.OdinInspector;
	using UnityEngine;

	[System.Serializable]
	public class SimQuestReward
	{
		#region Unity Properties
		[field: SerializeField, Title(nameof(SimQuestReward), "Settings")]
		public BoolReactiveProperty IsCollected { get; set; } = new();

		[field: SerializeField]
		public IntReactiveProperty Currency { get; set; } = new();
		#endregion
	}
}
