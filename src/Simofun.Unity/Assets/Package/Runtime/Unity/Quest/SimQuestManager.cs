//-----------------------------------------------------------------------
// <copyright file="SimQuestManager.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

using UniRx;

namespace Simofun.Unity.Quest
{
	using Bayhaksam.Unity.Logging.DevelopmentBuild;
	using Cysharp.Threading.Tasks;
	using Simofun.CysharpUniTask.Unity.Utils;
	using Simofun.Unity.Data.Configuration.CustomAsset;
	using Simofun.Unity.DesignPatterns.Singleton;
	using Simofun.Unity.Extensions;
	using Simofun.Unity.Quest.Events;
	using Simofun.Unity.Quest.Model;
	using System.Collections.Generic;
	using System.Linq;
	using UniRx.Unity;
	using UnityEngine;

	public class SimQuestManager : SimSceneSingletonBase<SimQuestManager>, ISimQuestManager
	{
		#region Fields
		readonly List<ISimQuest> activeQuests = new();

		readonly List<SimQuest> quests = new();
		#endregion

		#region Properties
		/// <inheritdoc />
		public virtual List<ISimQuest> ActiveQuests => this.activeQuests;
		#endregion

		#region Protected Properties
		protected virtual List<SimQuest> Quests
		{
			get
			{
				if (this.quests.Count > 0)
				{
					return this.quests;
				}

				this.quests.AddRange(SimCustomAssetConfigLocator.Instance.GetAll<SimQuest>().ToList());

				return this.quests;
			}
		}
		#endregion

		#region Constructor
		public virtual void Construct() => this.Quests.ForEach(q => q.Construct());
		#endregion

		#region Unity Methods
		#region Editor
		/// <inheritdoc />
		protected virtual void OnDrawGizmos()
		{
			if (this.ActiveQuests == null || this.ActiveQuests.Count <= 0)
			{
				return;
			}

			GUI.skin.DrawText(
				$"Active Quest Count: {this.ActiveQuests.Count}",
				this.transform.position,
				Color.black,
				25);

			for (var i = 0; i < this.ActiveQuests.Count; i++)
			{
				foreach (var quest in this.ActiveQuests)
				{
					GUI.skin.DrawText(
						$"{(quest as Object)?.name}",
						this.transform.position,
						Color.black,
						25,
						25);

					GUI.skin.DrawText(
						$"{quest.Goals.FirstOrDefault(goal => !goal.IsCompleted.Value)?.name}",
						this.transform.position,
						Color.black,
						25,
						50);
				}
			}
		}
		#endregion

		/// <inheritdoc />
		protected override void Awake()
		{
			base.Awake();

			this.RegisterEventHandlers();
		}

		/// <inheritdoc />
		protected virtual void Start() => this.Construct();

		/// <inheritdoc />
		protected override void OnDestroy()
		{
			base.OnDestroy();
			this.UnRegisterEventHandlers();
		}
		#endregion

		#region Public Methods
		/// <inheritdoc />
		public virtual void StartQuest(ISimQuest quest, float startDelay = 2)
		{
			if (quest is null)
			{
				debug.LogError("Quest is null");

				return;
			}

			SimUniTaskUtil.ExecuteOrWaitUntil(
				() => startDelay <= 0,
				() =>
				{
					this.AddToActiveQuest(quest);
					quest.Construct();
				});
		}
		#endregion

		#region Methods
		#region Event Handlers
		#region Registration
		protected virtual void RegisterEventHandlers()
		{
			SimMessageBus.OnEvent<SimQuestEvents.OnStarted>().Subscribe(this.HandleOnStarted).AddTo(this);
			SimMessageBus.OnEvent<SimQuestEvents.OnCompleted>().Subscribe(this.HandleOnCompleted).AddTo(this);
		}

		protected virtual void UnRegisterEventHandlers()
		{
		}
		#endregion

		protected virtual void HandleOnCompleted(SimQuestEvents.OnCompleted ev) =>
			this.RemoveFromActiveQuests(ev.Quest);

		protected virtual void HandleOnStarted(SimQuestEvents.OnStarted ev) => this.AddToActiveQuest(ev.Quest);
		#endregion

		protected virtual void AddToActiveQuest(ISimQuest quest)
		{
			if (!this.activeQuests.Contains(quest))
			{
				this.activeQuests.Add(quest);
			}
		}

		protected virtual void RemoveFromActiveQuests(ISimQuest quest)
		{
			if (this.activeQuests.Contains(quest))
			{
				this.activeQuests.Remove(quest);
			}
		}
		#endregion
	}
}
