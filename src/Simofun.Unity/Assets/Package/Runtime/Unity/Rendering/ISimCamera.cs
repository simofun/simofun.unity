//-----------------------------------------------------------------------
// <copyright file="ISimCamera.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Rendering
{
    public interface ISimCamera : ISimActivate
    {
        #region Properties
        int Priority { get; }
        #endregion
    }
}
