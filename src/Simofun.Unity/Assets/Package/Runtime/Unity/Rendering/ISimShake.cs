﻿//-----------------------------------------------------------------------
// <copyright file="ISimShake.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Rendering
{
    public interface ISimShake
    {
        #region Methods
        void Shake(float amplitude, float frequency, float duration);

        void StartShake(float amplitude, float frequency);

        void StopShake();
        #endregion
    }
}
