//-----------------------------------------------------------------------
// <copyright file="SimCameraBase.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Rendering
{
	using UnityEngine;

	public abstract class SimCameraBase : MonoBehaviour, ISimCamera
	{
		#region Properties
		public virtual bool IsActive { get; protected set; } = false;

		public virtual int Priority { get; protected set; } = 1;
		#endregion

		#region Protected Properties
		protected int HighPriority { get; } = 1;

		protected int LowPriority { get; } = 0;
		#endregion

		#region Public Methods
		public virtual void Activate() => this.IsActive = true;

		public virtual void Deactivate() => this.IsActive = false;
		#endregion
	}
}
