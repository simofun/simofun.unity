//-----------------------------------------------------------------------
// <copyright file="SimCameraManager.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Rendering
{
	using Simofun.Unity.DesignPatterns.Singleton;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;

	public class SimCameraManager : SimSceneSingletonBase<SimCameraManager>
	{
		#region Fields
		readonly List<SimCameraBase> cameras = new List<SimCameraBase>();

		readonly Dictionary<object, int> objectToCameraCache = new Dictionary<object, int>();

		int selectedCameraIndex = -1;

		int previousCameraIndex = -1;
		#endregion

		#region Properties
		public ISimShake Shake => (ISimShake)this.SelectedActiveCamera;

		public SimCameraBase ActiveCamera
		{
			get
			{
				if (this.Cameras == null
						|| this.Cameras.Count <= 0
						|| this.selectedCameraIndex < 0
						|| this.selectedCameraIndex >= this.Cameras.Count)
				{
					return null;
				}

				return this.Cameras[this.selectedCameraIndex];
			}
		}

		public SimCameraBase SelectedActiveCamera =>
			this.selectedCameraIndex >= 0
				? this.Cameras[this.selectedCameraIndex]
				: null;

		public List<SimCameraBase> Cameras
		{
			get
			{
				if (this.cameras.Count > 0)
				{
					return this.cameras;
				}

				this.cameras.AddRange(FindObjectsByType<SimCameraBase>(FindObjectsSortMode.None));

				return this.cameras;
			}
		}
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected override void Awake()
		{
			base.Awake();
			
			if (this.Cameras.Count <= 0)
			{
				return;
			}

			var highestPriorityCamera = this.Cameras.OrderByDescending(cameras => cameras.Priority).FirstOrDefault();
			this.selectedCameraIndex = this.Cameras.IndexOf(highestPriorityCamera);
			this.ForceActivate(this.selectedCameraIndex);
		}
		#endregion

		#region Public Methods
		public void ReturnToPreviousCamera()
		{
			if (this.previousCameraIndex < 0)
			{
				return;
			}

			this.SetCamera(this.previousCameraIndex);
		}

		public void SetCamera<T>() where T : SimCameraBase => this.SetCamera(typeof(T));

		public void SetCamera(int index) => this.ActivateCamera(index);

		public void SetCamera(GameObject cameraObject)
		{
			if (cameraObject == null)
			{
				return;
			}

			var camera = cameraObject.GetComponentInChildren<SimCameraBase>();
			if (camera == null)
			{
				return;
			}

			this.SetCamera(this.TryToGetValue(cameraObject, camera));
		}

		public void SetCamera(SimCameraBase camera)
		{
			if (camera == null)
			{
				return;
			}

			this.SetCamera(this.TryToGetValue(camera, camera));
		}

		public void SetCamera(System.Type cameraType)
		{
			if (cameraType.IsSubclassOf(typeof(SimCameraBase)))
			{
				throw new System.ArgumentException($"{cameraType} not inherits from SimCameraBase");
			}

			if (!this.objectToCameraCache.TryGetValue(cameraType, out var index))
			{
				var firstTypeOf = this.Cameras.FirstOrDefault(camera => camera.GetType() == cameraType);
				if (firstTypeOf == null)
				{
					return;
				}

				index = this.TryToGetValue(cameraType, firstTypeOf);
			}

			this.SetCamera(index);
		}
		#endregion

		#region Methods
		int TryToGetValue(object obj, SimCameraBase camera)
		{
			if (!this.objectToCameraCache.TryGetValue(obj, out var index))
			{
				this.Cameras.Add(camera);
				index = this.Cameras.IndexOf(camera);
				this.objectToCameraCache.Add(camera, index);
			}

			return index;
		}

		void ActivateCamera(int index)
		{
			if (index > this.Cameras.Count || index < 0)
			{
				return;
			}

			index = Mathf.Clamp(index, 0, this.Cameras.Count);
			var previousCamera = this.Cameras[this.selectedCameraIndex];
			previousCamera.Deactivate();

			this.previousCameraIndex = this.selectedCameraIndex;
			this.selectedCameraIndex = index;
			var selectedCamera = this.Cameras[this.selectedCameraIndex];
			selectedCamera.Activate();
		}

		void ForceActivate(int index)
		{
			this.Foreach(camera => camera.Deactivate());
			this.Cameras[index].Activate();
		}

		void Foreach(System.Action<SimCameraBase> action)
		{
			foreach (var camera in this.Cameras)
			{
				if (camera == null)
				{
					return;
				}

				action?.Invoke(camera);
			}
		}
		#endregion
	}
}
