//-----------------------------------------------------------------------
// <copyright file="SimCinemachineCamera.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Rendering
{
	using Cinemachine;

	public class SimCinemachineCamera : SimCameraBase
	{
		#region Fields
		CinemachineVirtualCameraBase cameraBase;
		#endregion

		#region Properties
		public override int Priority => this.CameraBase.Priority;
		#endregion

		#region Protected Properties
		protected virtual CinemachineVirtualCameraBase CameraBase =>
			this.cameraBase != null
				? this.cameraBase
				: (this.cameraBase = this.GetComponent<CinemachineVirtualCameraBase>());

		#endregion

		#region Public Methods
		public override void Activate()
		{
			base.Activate();

			this.cameraBase.Priority = this.HighPriority;
		}

		public override void Deactivate()
		{
			base.Deactivate();

			this.cameraBase.Priority = this.LowPriority;
		}
		#endregion
	}
}
