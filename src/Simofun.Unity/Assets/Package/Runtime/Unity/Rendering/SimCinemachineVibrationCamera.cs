//-----------------------------------------------------------------------
// <copyright file="SimCinemachineVibrationCamera.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Rendering
{
    using Cinemachine;
    using System.Collections;
    using UnityEngine;

    public class SimCinemachineVibrationCamera : SimCinemachineCamera, ISimShake
    {
        #region Fields
        CinemachineBasicMultiChannelPerlin camNoise;

        Coroutine lastShakeCoroutine;
        #endregion

        #region Protected Properties
        protected virtual CinemachineVirtualCamera VirtualCamera => (CinemachineVirtualCamera)this.CameraBase;

        protected virtual CinemachineBasicMultiChannelPerlin CamNoise
        {
            get => this.camNoise != null
                ? this.camNoise
                : (this.camNoise = this.VirtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>());
        }
        #endregion

        #region Methods
        IEnumerator ShakeCoroutine(float amplitude, float frequency, float duration)
        {
            this.StartShake(amplitude, frequency);
            yield return new WaitForSeconds(duration);
            this.StopShake();
        }

        void SetGains(float amplitude, float frequency)
        {
            this.camNoise.m_AmplitudeGain = amplitude;
            this.camNoise.m_FrequencyGain = frequency;
        }
        #endregion

        #region Public Methods
        public void Shake(float amplitude, float frequency, float duration)
        {
            if (this.lastShakeCoroutine != null) StopCoroutine(this.lastShakeCoroutine);
            this.lastShakeCoroutine = this.StartCoroutine(this.ShakeCoroutine(amplitude, frequency, duration));
        }

        public void StartShake(float amplitude, float frequency) => this.SetGains(amplitude, frequency);

        public void StopShake() => this.SetGains(0, 0);
        #endregion
    }
}
