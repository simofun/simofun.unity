//-----------------------------------------------------------------------
// <copyright file="SimDebugCameraSwitcher.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Rendering
{
	using Bayhaksam.Unity.Logging.DevelopmentBuild;
	using Sirenix.OdinInspector;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;

	public class SimDebugCameraSwitcher : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField,
			Title(nameof(SimDebugCameraSwitcher), "References"),
			TableList,
			OnValueChanged(nameof(HandleOnCameraMapValueChanged))]
		List<CameraMap> cameraMaps;
		#endregion

		#region Unity Methods
		#region Editor
		/// <inheritdoc />
		protected virtual void OnValidate() => this.HandleOnCameraMapValueChanged();
		#endregion

		/// <inheritdoc />
		protected virtual void Update()
		{
			foreach (var cameraMap in this.cameraMaps)
			{
				this.CheckMap(cameraMap);
			}
		}
		#endregion

		#region Methods
		void CheckMap(CameraMap map)
		{
			if (Input.GetKeyDown(map.KeyCode))
			{
				SimCameraManager.Instance.SetCamera(map.Camera);
			}
		}

		void HandleOnCameraMapValueChanged()
		{
			if (this.cameraMaps == null)
			{
				return;
			}

			// Remove last dup
			var groupByKeyCode = this.cameraMaps.GroupBy(cameraMap => cameraMap.KeyCode);
			var anyGrouped = groupByKeyCode.Where(group => group.Count() > 1);
			if (!anyGrouped.Any())
			{
				return;
			}

			foreach (var grouped in anyGrouped)
			{
				var take = grouped.TakeLast(grouped.Count() - 1);
				foreach (var item in take)
				{
					debug.Log(item.KeyCode + " already exist!");

					this.cameraMaps.Remove(item);
				}
			}
		}
		#endregion

		#region Nested Types
		[System.Serializable]
		class CameraMap
		{
			#region Unity Fields
			[SerializeField, HideLabel]
			KeyCode keyCode;

			[SerializeField, HideLabel]
			SimCameraBase camera;
			#endregion

			#region Properties
			public KeyCode KeyCode => this.keyCode;

			public SimCameraBase Camera => this.camera;
			#endregion
		}
		#endregion
	}
}
