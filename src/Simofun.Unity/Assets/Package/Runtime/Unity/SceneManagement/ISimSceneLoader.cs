﻿//-----------------------------------------------------------------------
// <copyright file="ISimSceneLoader.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.SceneManagement
{
	using UnityEngine.SceneManagement;

	public interface ISimSceneLoader
	{
		#region Properties
		LoadSceneMode Mode { get; set; }
		#endregion

		#region Methods
		void Load();
		#endregion
	}
}
