﻿//-----------------------------------------------------------------------
// <copyright file="ISimSceneLoaderByIndex.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.SceneManagement
{
	using UnityEngine.SceneManagement;

	public interface ISimSceneLoaderByIndex : ISimSceneLoader
	{
		#region Properties
		int SceneBuildIndex { get; set; }
		#endregion

		#region Methods
		void Load(int sceneBuildIndex);

		void Load(int sceneBuildIndex, LoadSceneMode mode);
		#endregion
	}
}
