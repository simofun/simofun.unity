﻿//-----------------------------------------------------------------------
// <copyright file="ISimSceneLoaderByName.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.SceneManagement
{
	using UnityEngine.SceneManagement;

	public interface ISimSceneLoaderByName : ISimSceneLoader
	{
		#region Properties
		string SceneName { get; set; }
		#endregion

		#region Methods
		void Load(string sceneName);

		void Load(string sceneName, LoadSceneMode mode);
		#endregion
	}
}
