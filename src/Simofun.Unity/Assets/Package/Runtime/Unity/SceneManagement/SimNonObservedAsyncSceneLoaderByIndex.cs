﻿//-----------------------------------------------------------------------
// <copyright file="SimNonObservedAsyncSceneLoaderByIndex.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.SceneManagement
{
	using UnityEngine.SceneManagement;

	public class SimNonObservedAsyncSceneLoaderByIndex : SimSceneLoaderByIndex
	{
		#region ISimSceneLoaderByIndex Methods
		public override void Load() => SceneManager.LoadSceneAsync(this.SceneBuildIndex, this.Mode);

		public override void Load(int sceneBuildIndex) => SceneManager.LoadSceneAsync(sceneBuildIndex);

		public override void Load(int sceneBuildIndex, LoadSceneMode mode) =>
			SceneManager.LoadSceneAsync(sceneBuildIndex, mode);
		#endregion
	}
}
