﻿//-----------------------------------------------------------------------
// <copyright file="SimNonObservedAsyncSceneLoaderByName.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.SceneManagement
{
	using UnityEngine.SceneManagement;

	public class SimNonObservedAsyncSceneLoaderByName : SimSceneLoaderByName
	{
		#region ISimSceneLoaderByName Methods
		public override void Load() => SceneManager.LoadSceneAsync(this.SceneName, this.Mode);

		public override void Load(string sceneName) => SceneManager.LoadSceneAsync(sceneName);

		public override void Load(string sceneName, LoadSceneMode mode) =>
			SceneManager.LoadSceneAsync(sceneName, mode);
		#endregion
	}
}
