﻿//-----------------------------------------------------------------------
// <copyright file="SimSceneInvokerAfterSeconds.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.SceneManagement
{
	using System.Collections;
	using UnityEngine;

	public class SimSceneInvokerAfterSeconds : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField]
		bool initializeOnAwake;

		[SerializeField]
		bool initializeOnStart = true;

		[SerializeField]
		SimSceneLoaderBase sceneLoader;

		[SerializeField]
		float seconds = 2;
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Awake()
		{
			if (this.initializeOnAwake)
			{
				this.StartCoroutine(this.Execute());
			}
		}

		/// <inheritdoc />
		protected virtual void Start()
		{
			if (this.initializeOnStart)
			{
				this.StartCoroutine(this.Execute());
			}
		}
		#endregion

		#region Methods
		IEnumerator Execute()
		{
			if (this.seconds > 0)
			{
				yield return new WaitForSeconds(this.seconds);
			}

			this.sceneLoader.Load();
		}
		#endregion
	}
}
