﻿//-----------------------------------------------------------------------
// <copyright file="SimSceneLoaderBase.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.SceneManagement
{
	using UnityEngine;
	using UnityEngine.SceneManagement;

	public abstract class SimSceneLoaderBase : MonoBehaviour, ISimSceneLoader
	{
		#region Unity Fields
		[SerializeField]
		LoadSceneMode mode;
		#endregion

		#region ISimSceneLoader Properties
		public LoadSceneMode Mode { get => this.mode; set => this.mode = value; }
		#endregion

		#region ISimSceneLoader Methods
		public abstract void Load();
		#endregion
	}
}
