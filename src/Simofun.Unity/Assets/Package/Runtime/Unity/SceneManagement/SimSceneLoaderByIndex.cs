﻿//-----------------------------------------------------------------------
// <copyright file="SimSceneLoaderByIndex.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.SceneManagement
{
	using UnityEngine;
	using UnityEngine.SceneManagement;

	public class SimSceneLoaderByIndex : SimSceneLoaderBase, ISimSceneLoaderByIndex
	{
		#region Unity Fields
		[SerializeField]
		int sceneBuildIndex;
		#endregion

		#region ISimSceneLoaderByIndex Properties
		public int SceneBuildIndex { get => this.sceneBuildIndex; set => this.sceneBuildIndex = value; }
		#endregion

		#region ISimSceneLoaderByIndex Methods
		public override void Load() => SceneManager.LoadScene(this.SceneBuildIndex, this.Mode);

		public virtual void Load(int sceneBuildIndex) => SceneManager.LoadScene(sceneBuildIndex);

		public virtual void Load(int sceneBuildIndex, LoadSceneMode mode) =>
			SceneManager.LoadScene(sceneBuildIndex, mode);
		#endregion
	}
}
