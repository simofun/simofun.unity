﻿//-----------------------------------------------------------------------
// <copyright file="SimSceneLoaderByName.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.SceneManagement
{
	using UnityEngine;
	using UnityEngine.SceneManagement;

	public class SimSceneLoaderByName : SimSceneLoaderBase, ISimSceneLoaderByName
	{
		#region Unity Fields
		[SerializeField]
		string sceneName;
		#endregion

		#region ISimSceneLoaderByIndex Properties
		public string SceneName { get => this.sceneName; set => this.sceneName = value; }
		#endregion

		#region ISimSceneLoaderByIndex Methods
		public override void Load() => SceneManager.LoadScene(this.SceneName, this.Mode);

		public virtual void Load(string sceneName) => SceneManager.LoadScene(sceneName);

		public virtual void Load(string sceneName, LoadSceneMode mode) => SceneManager.LoadScene(sceneName, mode);
		#endregion
	}
}
