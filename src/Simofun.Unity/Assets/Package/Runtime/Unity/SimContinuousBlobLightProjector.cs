﻿//-----------------------------------------------------------------------
// <copyright file="SimContinuousBlobLightProjector.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity
{
	using UnityEngine;

	public class SimContinuousBlobLightProjector : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField]
		float speed = 10;
		#endregion

		#region Properties
		public virtual float Speed { get => this.speed; set => this.speed = value; }
		#endregion

		#region Unity Methods
		protected virtual void Update() =>
			this.transform.Rotate(this.transform.forward, this.Speed * Time.deltaTime, Space.World);
		#endregion
	}
}
