﻿//-----------------------------------------------------------------------
// <copyright file="SimContinuousRotater.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity
{
	using Sirenix.OdinInspector;
	using UnityEngine;

	public class SimContinuousRotater : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimContinuousRotater), "Settings")]
		float angle = 5;

		[SerializeField]
		Vector3 axis = Vector3.forward;
		#endregion

		#region Properties
		public float Angle { get => this.angle; set => this.angle = value; }

		public Vector3 Axis { get => this.axis; set => this.axis = value; }
		#endregion

		#region Unity Methods
		protected virtual void Update() => this.transform.Rotate(this.Axis, this.Angle);
		#endregion
	}
}
