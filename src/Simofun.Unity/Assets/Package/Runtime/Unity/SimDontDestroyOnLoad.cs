//-----------------------------------------------------------------------
// <copyright file="SimDontDestroyOnLoad.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity
{
	using Sirenix.OdinInspector;
	using UnityEngine;

	public class SimDontDestroyOnLoad : MonoBehaviour
	{
		#region Unity Properties
		[field: SerializeField, Title(nameof(SimDontDestroyOnLoad), "Settings")]
		protected bool InvokeOnAwake { get; set; } = true;

		[field: SerializeField]
		protected bool InvokeOnStart { get; set; }
		#endregion

		#region Unity Methods
		/// <inheritdoc/>
		protected virtual void Awake()
		{
			if (this.InvokeOnAwake)
			{
				this.Execute();
			}
		}

		/// <inheritdoc/>
		protected virtual void Start()
		{
			if (this.InvokeOnStart)
			{
				this.Execute();
			}
		}
		#endregion

		#region Public Methods
		public void Execute() => DontDestroyOnLoad(this.gameObject);
		#endregion
	}
}
