﻿//-----------------------------------------------------------------------
// <copyright file="SimMaterialRenderQueueSetter.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Hasan Emre Tonguc</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity
{
	using Sirenix.OdinInspector;
	using System.Linq;
	using UnityEngine;

	public class SimMaterialRenderQueueSetter : MonoBehaviour, ISimMaterialRenderQueueSetter
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimMaterialRenderQueueSetter), "Settings")]
		int value = 3002;

		[SerializeField]
		bool isIncludeSelf = true;

		[SerializeField]
		bool isIncludeChildren = true;

		[SerializeField]
		bool isIncludeChildrenInactiveRenderers;
		#endregion

		#region Properties
		/// <inheritdoc />
		public virtual int Value
		{
			get => this.value;
			set
			{
				this.value = value;
				this.UpdateValue(value);
			}
		}
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Start() => this.UpdateValue(this.Value);
		#endregion

		#region Protected Methods
		protected virtual void UpdateValue(int value)
		{
			if (this.isIncludeSelf && this.TryGetComponent<Renderer>(out var renderer))
			{
				renderer.material.renderQueue = value;
			}

			if (this.isIncludeChildren)
			{
				this.GetComponentsInChildren<Renderer>(this.isIncludeChildrenInactiveRenderers)
					.ToList()
					.ForEach(r => r.material.renderQueue = value);
			}
		}
		#endregion
	}
}
