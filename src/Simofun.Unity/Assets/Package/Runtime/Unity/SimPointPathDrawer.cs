//-----------------------------------------------------------------------
// <copyright file="SimPointPathDrawer.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tonguc</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity
{
	using Sirenix.OdinInspector;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;
	using UnityEngine.AI;

	public class SimPointPathDrawer : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimPointPathDrawer), "Settings")]
		Vector3 fromOffset;

		[SerializeField]
		Vector3 toOffset;

		[SerializeField, Title(nameof(SimPointPathDrawer), "References")]
		LineRenderer targetLine;

		[SerializeField]
		LineRenderer pathLine;
		#endregion

		#region Fields
		readonly int updatePathInterval = 15; // If you want this to be more performant, change this to a higher number

		int targetIndex = int.MaxValue;

		Vector3 toSampled;
		#endregion

		#region Events
		public event Action OnReachTarget;
		#endregion

		#region Static Properties
		public static string DefaultResourcePath { get; set; } = "Prefabs/" + nameof(SimPointPathDrawer);
		#endregion

		#region Protected Properties
		protected virtual Transform From { get; set; }

		protected virtual Vector3 FromPosition => this.From.position + this.fromOffset;

		protected virtual Vector3 To { get; set; }

		protected virtual Vector3 ToPosition => this.To + this.toOffset;
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Update()
		{
			if (this.From == null)
			{
				return;
			}

			var targetPosition = this.toSampled;
			if (this.pathLine.positionCount > 0 && this.targetIndex >= 0)
			{
				targetPosition = this.pathLine.GetPosition(this.targetIndex);
			}

			var distanceToTarget = Vector3.Distance(this.FromPosition, targetPosition);

			this.targetLine.SetPosition(0, this.FromPosition);
			this.targetLine.SetPosition(1, targetPosition);

			if (this.pathLine.positionCount > 1)
			{
				var distanceToLastTarget = Vector3.Distance(
					this.FromPosition, this.pathLine.GetPosition(this.targetIndex - 1));
				if (distanceToTarget > distanceToLastTarget)
				{
					distanceToTarget = 0; // Force update
				}

				if (distanceToTarget > 0)
				{
					var directionToTarget = (targetPosition - this.FromPosition).normalized;
					var directionToNextTarget = (this.pathLine.GetPosition(this.targetIndex - 1) - this.FromPosition)
						.normalized;
					var angle = Vector3.Angle(directionToTarget, directionToNextTarget);
					var dot = Vector3.Dot(directionToTarget, directionToNextTarget);
					if (angle > 50 && dot < 0.5f)
					{
						distanceToTarget = 0; // Force update
					}
				}

				if (distanceToTarget > 0 && Time.frameCount % this.updatePathInterval == 0)
				{
					this.UpdatePath(); // Maybe there is a better way

					return;
				}
			}

			if (distanceToTarget < 1f)
			{
				this.targetIndex--;
				if (this.pathLine.positionCount >= 1)
				{
					this.pathLine.positionCount--;
				}
			}

			if (Time.frameCount % this.updatePathInterval == 0 & distanceToTarget > 5)
			{
				this.UpdatePath();
			}

			var distanceToLast = Vector3.Distance(this.FromPosition, this.ToPosition);
			if (distanceToLast < 2)
			{
				this.OnReachTarget?.Invoke();
				Destroy(this.gameObject);
			}
		}
		#endregion

		#region Public Static Methods
		public static SimPointPathDrawer Create(Transform from, Vector3 to) => Create(from, to, DefaultResourcePath);

		public static SimPointPathDrawer Create(Transform from, Vector3 to, string resourcePath)
		{
			var comp = Instantiate(Resources.Load<GameObject>(resourcePath))
				.GetComponent<SimPointPathDrawer>();
			comp.Initialize(from, to);

			return comp;
		}
		#endregion

		#region Public Methods
		public virtual void Initialize(Transform from, Vector3 to)
		{
			this.From = from;
			this.To = to;
			this.CreatePath();
		}
		#endregion

		#region Protected Methods
		protected virtual NavMeshPath CalcPath()
		{
			var path = new NavMeshPath();

			NavMesh.SamplePosition(this.FromPosition, out var fromSamplePositionHit, 5, NavMesh.AllAreas);
			NavMesh.SamplePosition(this.ToPosition, out var toSamplePositionHit, 5, NavMesh.AllAreas);

			NavMesh.CalculatePath(
				fromSamplePositionHit.position,
				toSamplePositionHit.position,
				NavMesh.AllAreas,
				path);

			this.toSampled = toSamplePositionHit.position;

			return path;
		}

		protected virtual void ApplyPath(NavMeshPath path)
		{
			var pathPoints = new List<Vector3>();

			for (var i = 0; i < path.corners.Length; i++)
			{
				pathPoints.Add(path.corners[i]);
			}

			pathPoints.Reverse();
			if (pathPoints.Count > 1)
			{
				pathPoints.Remove(pathPoints.Last());
			}

			this.pathLine.positionCount = pathPoints.Count;
			this.pathLine.SetPositions(pathPoints.ToArray());

			this.targetIndex = pathPoints.Count - 1;
		}

		protected virtual void CreatePath()
		{
			this.ApplyPath(this.CalcPath());
			this.CreateToTargetLine();
		}

		protected virtual void CreateToTargetLine()
		{
			this.targetLine.positionCount = 2;
			this.targetLine.SetPosition(0, this.FromPosition);
			this.targetLine.SetPosition(1, this.pathLine.GetPosition(this.pathLine.positionCount - 1));
		}

		protected virtual void UpdatePath()
		{
			var path = this.CalcPath();
			if (path.corners.Length == this.pathLine.positionCount)
			{
				return;
			}

			this.ApplyPath(path);
		}
		#endregion
	}
}
