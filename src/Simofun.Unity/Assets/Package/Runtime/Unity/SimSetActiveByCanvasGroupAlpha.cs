﻿//-----------------------------------------------------------------------
// <copyright file="SimSetActiveByCanvasGroupAlpha.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

using UniRx;

namespace Simofun.Unity
{
	using Simofun.Unity.Extensions;
	using Sirenix.OdinInspector;
	using UnityEngine;

	[RequireComponent(typeof(CanvasGroup))]
	public class SimSetActiveByCanvasGroupAlpha : MonoBehaviour
	{
		#region Fields
		[SerializeField, Title(nameof(SimSetActiveByCanvasGroupAlpha), "References")]
		CanvasGroup canvasGroup;
		#endregion

		#region Protected Properties
		protected virtual CanvasGroup CanvasGroup =>
			(this.canvasGroup != null)
				? this.canvasGroup
			: (this.canvasGroup = this.GetOrAddComponent<CanvasGroup>());
		#endregion

		#region Unity Methods		
		/// <inheritdoc />
		protected virtual void Start() => this.RegisterEventHandlers();
		#endregion

		#region Protected Methods
		#region Event Handlers
		#region Registration
		protected virtual void RegisterEventHandlers()
		{
			this.CanvasGroup.ObserveEveryValueChanged(c => c.alpha)
				.Subscribe(a => this.gameObject.SetActive(a > 0))
				.AddTo(this);
		}
		#endregion
		#endregion
		#endregion
	}
}
