﻿//-----------------------------------------------------------------------
// <copyright file="SimSwipeDetector.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity
{
	using System;
	using UnityEngine;
	using UnityEngine.EventSystems;
	using input = UnityEngine.Input;

	public class SimSwipeDetector : MonoBehaviour
	{
		#region Private Variables
		private bool isTouchUI;
		#endregion

		#region Fields
		/// <summary>
		/// To calculate swipe time to control throw force in Z direction
		/// </summary>
		float touchTimeStart;

		/// <summary>
		/// Touch start position, touch end position, swipe direction
		/// </summary>
		Vector2 startPos;
		#endregion

		#region Events
		public event Action<Vector2> OnSwipeStarted;

		public event Action<Vector2, float> OnSwipe;

		public event Action<Vector2, float> OnSwipeUpdate;
		#endregion

		#region Unity Methods
		protected virtual void Update()
		{
#if UNITY_EDITOR
			this.MouseDetection();
#else
			this.TouchDetection();
#endif
		}
		#endregion

		#region Methods
		bool IsHitGameObjectOnPosition(Vector2 position) =>
			Physics.Raycast(Camera.main.ScreenPointToRay(position), out RaycastHit hit)
				&& this.transform == hit.transform;

		void MouseDetection()
		{
			if (input.GetMouseButtonDown(0))
			{
				this.isTouchUI = EventSystem.current.IsPointerOverGameObject();
				if (!this.isTouchUI)
				{
					this.touchTimeStart = Time.time;
					this.startPos = new Vector2(input.mousePosition.x, input.mousePosition.y);

					this.OnSwipeStarted?.Invoke(this.startPos);
				}
			}

			if (input.GetMouseButton(0) && !this.isTouchUI)
			{
				var touchTimeEnd = Time.time;
				var timeInterval = touchTimeEnd - this.touchTimeStart;
				var endPos = new Vector2(input.mousePosition.x, input.mousePosition.y);
				var direction = this.startPos - endPos;

				this.OnSwipeUpdate?.Invoke(direction, timeInterval);
			}

			if (input.GetMouseButtonUp(0) && !this.isTouchUI)
			{
				var touchTimeEnd = Time.time;
				var timeInterval = touchTimeEnd - this.touchTimeStart;
				var endPos = new Vector2(input.mousePosition.x, input.mousePosition.y);
				var direction = this.startPos - endPos;

				this.OnSwipe?.Invoke(direction, timeInterval);
			}
		}

		void TouchDetection()
		{
			if (input.touchCount < 1)
			{
				return;
			}

			// If you touch the screen
			var touch = input.GetTouch(0);
			if (touch.phase == TouchPhase.Began)
			{
				this.isTouchUI = EventSystem.current.IsPointerOverGameObject(0);
				if (!this.isTouchUI)
				{
					// Getting touch position and marking time when you touch the screen
					this.touchTimeStart = Time.time;
					this.startPos = touch.position;

					this.OnSwipeStarted?.Invoke(this.startPos);
				}
			}

			// If you release your finger
			if (touch.phase == TouchPhase.Ended && !this.isTouchUI)
			{
				var touchTimeEnd = Time.time; // Marking time when you release it

				var timeInterval = touchTimeEnd - this.touchTimeStart; // Calculate swipe time interval

				var endPos = touch.position; // Getting release finger position

				var direction = this.startPos - endPos; // Calculating swipe direction in 2D space

				this.OnSwipe?.Invoke(direction, timeInterval);
			}
		}
		#endregion
	}
}
