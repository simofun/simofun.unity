﻿//-----------------------------------------------------------------------
// <copyright file="SimTargetPositionFollower.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity
{
	using Sirenix.OdinInspector;
	using UnityEngine;

	public class SimTargetPositionFollower : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimTargetPositionFollower), "Settings")]
		Vector3 offset;

		[SerializeField, Title("", "References")]
		Transform target;
		#endregion

		#region Fields
		Vector3 velocity = Vector3.zero;
		#endregion

		#region Properties
		public Transform Target { get => this.target; set => this.target = value; }

		public Vector3 Offset { get => this.offset; set => this.offset = value; }
		#endregion

		#region Unity Methods		
		/// <inheritdoc />
		protected virtual void LateUpdate()
		{
			if (this.Target != null)
			{
				this.transform.position = Vector3.SmoothDamp(
					this.transform.position, this.Target.position + this.Offset, ref velocity, 0.3f);
			}
		}
		#endregion

		#region Public Methods
		public void ResetCamera()
		{
			this.Offset = new Vector3(0, 9.1f, -7.5f);
			transform.localEulerAngles = new Vector3(60, 0, 0);
		}

		public void SetCamera()
		{
			this.Offset += new Vector3(1, 0, -5);
			transform.localEulerAngles = new Vector3(40, -10, 0);
		}
		#endregion
	}
}
