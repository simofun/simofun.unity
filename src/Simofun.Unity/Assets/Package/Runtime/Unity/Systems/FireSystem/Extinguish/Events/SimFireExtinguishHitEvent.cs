﻿//-----------------------------------------------------------------------
// <copyright file="SimFireExtinguishHitEvent.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Extinguish.Events
{
	using Simofun.Unity.Events;
	using UnityEngine;

	public class SimFireExtinguishHitEvent : SimBaseUnityEvent
	{
		public float Strength { get; set; }

		public Vector3 Position { get; set; }
	}
}
