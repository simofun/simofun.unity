﻿//-----------------------------------------------------------------------
// <copyright file="SimRaycastFireExtinguish.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Extinguish
{
	using Simofun.UniRx.Unity;
	using Simofun.Unity.Systems.FireSystem.Extinguish.Events;
	using UnityEngine;

	public class SimRaycastFireExtinguish : MonoBehaviour
	{
		#region Unity Methods		
		/// <inheritdoc />
		protected virtual void FixedUpdate()
		{
			if (Physics.Raycast(new Ray(this.transform.position, this.transform.forward), out var hit, 100))
			{
				SimMessageBus.Publish(
					new SimFireExtinguishHitEvent
					{
						Position = hit.point,
						Strength = 1f
					});
			}
		}
		#endregion
	}
}
