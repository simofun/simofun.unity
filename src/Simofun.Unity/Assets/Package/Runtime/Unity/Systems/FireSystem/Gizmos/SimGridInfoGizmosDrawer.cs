﻿//-----------------------------------------------------------------------
// <copyright file="SimGridInfoGizmosDrawer.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Gizmos
{
	using Simofun.Unity.Gizmos;
	using Simofun.Unity.Systems.FireSystem.Model;
	using Sirenix.OdinInspector;
	using System.Collections.Generic;
	using UnityEngine;

	public class SimGridInfoGizmosDrawer : SimGizmosDrawerBase
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimGridInfoGizmosDrawer), "Settings")]
		bool isDrawBorders;

		[SerializeField]
		bool isDrawFlammables;

		[SerializeField]
		bool isDrawSpreadMap;

		[SerializeField]
		bool isDrawAllSpreadMap;
		#endregion

		#region Fields
		/// <summary>
		/// Only use for debug purposes
		/// </summary>
		const float debugMaxHitpoint = -30f;

		SimGridInfo grid;
		#endregion

		#region Protected Properties
		protected SimGridInfo Grid { get => this.grid ?? (this.grid = this.GetComponent<SimFireCreator>().Grid); }
		#endregion

		#region Public Methods
		public override void OnGizmos()
		{
			if (!Application.isPlaying)
			{
				return;
			}

			var grid = this.Grid;

			if (this.isDrawBorders)
			{
				var borders = new List<Vector3>();
				for (var i = 0; i < grid.Flammable.Length; i++)
				{
					var p = grid.Positions[i];

					grid.GetDimensionIndexes(i, out var ii, out var jj, out var kk);

					if (grid.CheckEdgeXAxis(i, ii) || grid.CheckEdgeYAxis(i, jj) || grid.CheckEdgeZAxis(i, kk, jj))
					{
						borders.Add(p + this.transform.position);
					}
					else
					{
						this.DrawNode(p + this.transform.position, Color.white, false);
					}
				}

				foreach (var v in borders)
				{
					this.DrawNode(v, Color.red);
				}

				return;
			}

			if (this.isDrawFlammables)
			{
				var flammables = new List<Vector3>();
				for (var i = 0; i < grid.Flammable.Length; i++)
				{
					var p = grid.Positions[i];
					if (grid.Flammable[i])
					{
						flammables.Add(p + this.transform.position);
					}
					else
					{
						this.DrawNode(p + this.transform.position, Color.white, false);
					}
				}

				foreach (var p in flammables)
				{
					this.DrawNode(p, Color.red);
				}

				return;
			}

			if (this.isDrawSpreadMap)
			{
				var drawedNodes = new HashSet<int>();
				foreach (var flammableIndex in grid.FlammableIndexes)
				{
					var p = grid.Positions[flammableIndex];

					if (grid.HitPoint[flammableIndex] < 0)
					{
						this.DrawNode(
							p + this.transform.position,
							Color.Lerp(Color.yellow, Color.red, grid.HitPoint[flammableIndex] / debugMaxHitpoint));
						drawedNodes.Add(flammableIndex);

						int minX = -1, maxX = 1,
							minY = -1, maxY = 1,
							minZ = -1, maxZ = 1;

						int dx, dy, dz;
						grid.GetDimensionIndexes(flammableIndex, out dx, out dy, out dz);

						minX = (dx == 0) ? 0 : -1;
						maxX = (dx == grid.XLength - 1) ? 0 : 1;

						minY = (dy == 0) ? 0 : -1;
						maxY = (dy == grid.XLength - 1) ? 0 : 1;

						minZ = (dz == 0) ? 0 : -1;
						maxZ = (dz == grid.XLength - 1) ? 0 : 1;

						for (var i = minX; i <= maxX; i++)
						{
							for (var j = minY; j <= maxY; j++)
							{
								for (var k = minZ; k <= maxZ; k++)
								{

									var newIndex = grid.GetNeighbourIndex(flammableIndex, i, j, k);
									if (newIndex < 0 || grid.Flammable.Length <= newIndex)
									{
										continue;
									}

									if (newIndex > 0 && !drawedNodes.Contains(newIndex))
									{
										this.DrawNode(
											grid.Positions[newIndex] + this.transform.position,
											Color.yellow);
										drawedNodes.Add(newIndex);
									}
								}
							}
						}
					}
					else if (this.isDrawAllSpreadMap)
					{
						if (drawedNodes.Contains(flammableIndex))
						{
							continue;
						}

						p = grid.Positions[flammableIndex];
						this.DrawNode(
							p + this.transform.position,
							Color.Lerp(Color.green, Color.yellow, grid.HitPoint[flammableIndex] / debugMaxHitpoint),
							false);
						drawedNodes.Add(flammableIndex);
					}
				}
			}
		}
		#endregion

		#region Methods
		void DrawNode(Vector3 position, Color color, bool drawFilled = true)
		{
			Gizmos.color = color;
			if (drawFilled)
			{
				Gizmos.DrawCube(position, this.Grid.CellSize);
			}

			Gizmos.DrawWireCube(position, this.Grid.CellSize);
		}
		#endregion
	}
}
