﻿//-----------------------------------------------------------------------
// <copyright file="ISimFireStartPositionSetter.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Initialization
{
	using UnityEngine;

	public interface ISimFireStartPositionSetter
	{
		#region Properties
		Vector3 FireStartPosition { get; set; }
		#endregion

		#region Methods
		void Execute();
		#endregion
	}
}
