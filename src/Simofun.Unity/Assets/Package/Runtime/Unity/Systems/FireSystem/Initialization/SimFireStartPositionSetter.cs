﻿//-----------------------------------------------------------------------
// <copyright file="SimFireStartPositionSetter.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Initialization
{
	using Sirenix.OdinInspector;
	using UnityEngine;

	public abstract class SimFireStartPositionSetter : MonoBehaviour, ISimFireStartPositionSetter
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimFireStartPositionSetter), "Settings")]
		Vector3 fireStartPosition;
		#endregion

		#region Properties
		/// <inheritdoc />
		public virtual Vector3 FireStartPosition
		{
			get => this.fireStartPosition;
			set => this.fireStartPosition = value;
		}
		#endregion

		#region Public Methods
		/// <inheritdoc />
		public abstract void Execute();
		#endregion
	}
}
