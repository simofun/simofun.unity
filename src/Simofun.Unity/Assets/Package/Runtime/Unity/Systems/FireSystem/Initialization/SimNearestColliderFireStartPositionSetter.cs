﻿//-----------------------------------------------------------------------
// <copyright file="SimNearestColliderFireStartPositionSetter.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Initialization
{
	using System.Linq;
	using UnityEngine;

	public class SimNearestColliderFireStartPositionSetter : SimFireStartPositionSetter
	{
		#region Public Methods
		/// <inheritdoc />
		public override void Execute()
		{
			var colliders = new Collider[0];
			var radius = 0f;
			while (!colliders.Any())
			{
				colliders = Physics.OverlapSphere(this.transform.position, radius);
				if (colliders.Any())
				{
					this.FireStartPosition = colliders.First().ClosestPointOnBounds(this.transform.position);
				}

				radius += 0.1f;
			}
		}
		#endregion
	}
}
