﻿//-----------------------------------------------------------------------
// <copyright file="SimRandomMeshFilterFireStartPositionSetter.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Initialization
{
	using UnityEngine;

	public class SimRandomMeshFilterFireStartPositionSetter : SimFireStartPositionSetter
	{
		#region Public Methods
		/// <inheritdoc />
		public override void Execute()
		{
			var mf = this.GetComponent<MeshFilter>() ?? this.GetComponentInChildren<MeshFilter>();
			if (mf == null)
			{
				Debug.Log($"Didn't found any {nameof(MeshFilter)} to set fire start position.");

				return;
			}

			var vertices = mf.mesh.vertices;

			this.FireStartPosition = this.transform.TransformPoint(vertices[Random.Range(0, vertices.Length)]);
		}
		#endregion
	}
}
