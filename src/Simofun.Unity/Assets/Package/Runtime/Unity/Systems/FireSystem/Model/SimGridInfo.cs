﻿//-----------------------------------------------------------------------
// <copyright file="SimGridInfo.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Model
{
	using Sirenix.OdinInspector;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;
	using Random = UnityEngine.Random;

	[Serializable]
	public class SimGridInfo
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimGridInfo), "Settings")]
		bool isFireStarted;

		[SerializeField]
		Vector3 referencePoint;

		[SerializeField]
		float resolution;

		[SerializeField]
		int xLength;

		[SerializeField]
		int yLength;

		[SerializeField]
		int zLength;

		[SerializeField]
		Vector3 mins;

		[SerializeField]
		Vector3 maxs;

		[SerializeField]
		Vector3 maxsMinsDif;

		[SerializeField]
		bool[] flammable;

		[SerializeField]
		int[] flammableIndexes;

		[SerializeField]
		HashSet<int> flammableIndexesHashSet;

		[SerializeField]
		float[] hitPoint;

		[SerializeField]
		float[] lifeTime;

		[Tooltip("0-100 range")]
		[SerializeField]
		float[] smoke;

		[SerializeField]
		bool[] fireSpreaded;

		[SerializeField]
		int[] borderIndexes;

		[SerializeField]
		bool[] borderEmit;

		[SerializeField]
		Vector3[] positions;

		[SerializeField]
		float[] meshBurnAmount;

		[SerializeField]
		Vector3 cellSize;

		[SerializeField]
		Vector3 pivotPoint;

		[SerializeField]
		readonly Dictionary<int, List<int>> meshToNodeMapping = new Dictionary<int, List<int>>();
		#endregion

		#region Public Fields
		public const float SmokeEmitTreshold = 1f;

		public const int HitPointTreshold = 50;
		#endregion

		#region Properties
		public bool IsFireStarted { get => this.isFireStarted; set => this.isFireStarted = value; }

		public bool[] Flammable { get => this.flammable; set => this.flammable = value; }

		public bool[] BorderEmit { get => this.borderEmit; set => this.borderEmit = value; }

		public bool[] FireSpreaded { get => this.fireSpreaded; set => this.fireSpreaded = value; }

		public float Resolution { get => this.resolution; set => this.resolution = value; }

		public float[] HitPoint { get => this.hitPoint; set => this.hitPoint = value; }

		public float[] LifeTime { get => this.lifeTime; set => this.lifeTime = value; }

		public float[] MeshBurnAmount { get => this.meshBurnAmount; set => this.meshBurnAmount = value; }

		public float[] Smoke { get => this.smoke; set => this.smoke = value; }

		public Dictionary<int, List<int>> MeshToNodeMapping => this.meshToNodeMapping;

		public int[] FlammableIndexes { get => this.flammableIndexes; set => this.flammableIndexes = value; }

		public HashSet<int> FlammableIndexesHashSet
		{
			get => this.flammableIndexesHashSet;
			set => this.flammableIndexesHashSet = value;
		}

		public int XLength { get => this.xLength; set => this.xLength = value; }

		public int YLength { get => this.yLength; set => this.yLength = value; }

		public int ZLength { get => this.zLength; set => this.zLength = value; }

		public int[] BorderIndexes { get => this.borderIndexes; set => this.borderIndexes = value; }

		public Vector3 CellSize { get => this.cellSize; set => this.cellSize = value; }

		public Vector3 Maxs { get => this.maxs; set => this.maxs = value; }

		public Vector3 MaxsMinsDif { get => this.maxsMinsDif; set => this.maxsMinsDif = value; }

		public Vector3 Mins { get => this.mins; set => this.mins = value; }

		public Vector3 PivotPoint { get => this.pivotPoint; set => this.pivotPoint = value; }

		public Vector3[] Positions { get => this.positions; set => this.positions = value; }

		public Vector3 ReferencePoint { get => this.referencePoint; set => this.referencePoint = value; }
		#endregion

		#region Public Methods
		public bool CheckEdgeXAxis(int index, int axisIndex) =>
			(axisIndex % this.XLength == 0
						&& (index <= this.XLength * this.YLength
								|| index >= this.XLength * this.YLength * this.ZLength - this.XLength * this.YLength))
				|| (axisIndex % this.XLength == this.XLength - 1 && (index >= this.Flammable.Length - this.XLength * this.YLength
						|| (index >= this.Flammable.Length - this.XLength * this.YLength * this.ZLength
								&& index <= this.Flammable.Length - this.XLength * this.YLength * this.ZLength
										+ this.XLength * this.YLength)));

		public bool CheckEdgeYAxis(int index, int axisIndex) =>
			(axisIndex % this.YLength == 0
						&& (index < this.XLength * this.YLength || index >= this.XLength * this.YLength * this.ZLength
								- this.XLength * this.YLength))
				|| (axisIndex % this.YLength == this.YLength - 1
						&& (index >= this.Flammable.Length - this.XLength * this.YLength
								|| (index >= this.Flammable.Length - this.XLength * this.YLength * this.ZLength
										&& index <= this.Flammable.Length - this.XLength * this.YLength * this.ZLength
											+ this.XLength * this.YLength)));

		public bool CheckEdgeZAxis(int i, int axisIndex, int yAxisIndex) =>
			(axisIndex % this.ZLength != 0
					&& i % this.XLength == 0
					&& (yAxisIndex == 0 || yAxisIndex == this.YLength - 1))
				|| (axisIndex % this.ZLength != 0
						&& i % this.XLength == this.XLength - 1
						&& (yAxisIndex == 0 || yAxisIndex == this.YLength - 1));

		public bool CheckLocalPositionIsOnFire(Vector3 pos, int radius = 0) // Local pos
		{
			var index = this.GetGridIndexOfLocalPosition(pos);
			this.GetDimensionIndexes(index, out var dx, out var dy, out var dz);

			var minX = (dx == 0) ? 0 : -1;
			var maxX = (dx == this.XLength - 1) ? 0 : 1;

			var minY = (dy == 0) ? 0 : -1;
			var maxY = (dy == this.YLength - 1) ? 0 : 1;

			var minZ = (dz == 0) ? 0 : -1;
			var maxZ = (dz == this.ZLength - 1) ? 0 : 1;

			for (var i = minX; i <= maxX; i++)
			{
				for (var j = minY; j <= maxY; j++)
				{
					for (var k = minZ; k <= maxZ; k++)
					{
						var newIndex = this.GetNeighbourIndex(index, i, j, k);
						if (newIndex > this.HitPoint.Length)
						{
							return false;
						}

						if (newIndex >= 0 && newIndex <= this.HitPoint.Length && this.HitPoint[newIndex] < 0)
						{
							return true;
						}
					}
				}
			}

			return false;
		}

		public bool IsInsideTheGrid(Vector3 pos) // World Pos
		{
			var minSum = this.ReferencePoint + this.Mins;
			var maxSum = this.ReferencePoint + this.Maxs;

			return this.IsBetween(pos.x, minSum.x, maxSum.x)
				&& this.IsBetween(pos.y, minSum.y, maxSum.y)
				&& this.IsBetween(pos.z, minSum.z, maxSum.z);
		}

		public int GetGridIndexOfLocalPosition(Vector3 position) =>
			this.IndexOf(
				this.GetIndex(position.x, this.Mins.x, this.CellSize.x, this.XLength - 1),
				this.GetIndex(position.y, this.Mins.y, this.CellSize.y, this.YLength - 1),
				this.GetIndex(position.z, this.Mins.z, this.CellSize.z, this.ZLength - 1));

		public int IndexOf(int xIndex, int yIndex, int zIndex) =>
			xIndex + this.XLength * yIndex + this.XLength * this.YLength * zIndex;

		public void CoolWorldPosition(Vector3 p0, float hitPoint, int range = 7)
		{
			var index = this.GetGridIndexOfLocalPosition(p0);
			for (var i = -range; i <= range; i++)
			{
				for (var j = -range; j <= range; j++)
				{
					for (var k = -range; k <= range; k++)
					{
						var newIndex = this.GetNeighbourIndex(index, i, j, k);
						if (newIndex < 0 || this.HitPoint.Length <= newIndex)
						{
							continue;
						}

						var fadeAmount = new Vector3(i, j, k).magnitude / (range * 2f) * hitPoint;
						this.HitPoint[newIndex] += hitPoint - fadeAmount;

						if (this.HitPoint[newIndex] > 50)
						{
							this.HitPoint[newIndex] = 50;
						}
					}
				}
			}
		}

		public void EndInitialization()
		{
			this.FlammableIndexes = Enumerable.Range(0, this.Flammable.Length).Where(i => this.Flammable[i]).ToArray();
			this.FlammableIndexesHashSet = new HashSet<int>(this.FlammableIndexes);

			foreach (var flammableIndex in this.FlammableIndexes)
			{
				if (this.HitPoint[flammableIndex] == 0)
				{
					this.HitPoint[flammableIndex] = 50;
					this.FireSpreaded[flammableIndex] = false;
				}
			}

			var borders = new List<int>();
			for (var i = 0; i < this.HitPoint.Length; i++)
			{
				this.GetDimensionIndexes(i, out var dx, out var dy, out var dz);

				// 'dy' ignored to prevent smokes to leave grid from ground
				if (dx == 0 || dx == this.XLength - 1 || dy == this.YLength - 1 || dz == 0 || dz == this.ZLength - 1)
				{
					borders.Add(i);
				}
			}

			this.BorderIndexes = borders.ToArray();
			this.BorderEmit = new bool[this.HitPoint.Length];
			this.MeshBurnAmount = new float[this.FlammableIndexes.Length];
		}

		public void FireLocalPosition(Vector3 p, int circle = 1)
		{
			var index = this.GetGridIndexOfLocalPosition(p);
			this.GetDimensionIndexes(index, out var dx, out var dy, out var dz);

			var minX = (dx - circle <= 0) ? -dx : -circle;
			var maxX = (dx >= this.XLength - 1) ? 0 : circle;

			var minY = (dy - circle <= 0) ? -dy : -circle;
			var maxY = (dy >= this.YLength - 1) ? 0 : circle;

			var minZ = (dz - circle <= 0) ? -dz : -circle;
			var maxZ = (dz >= this.ZLength - 1) ? 0 : circle;

			for (var i = minX; i <= maxX; i++)
			{
				for (var j = minY; j <= maxY; j++)
				{
					for (var k = minZ; k <= maxZ; k++)
					{
						this.HitPoint[this.GetNeighbourIndex(index, i, j, k)] = -25;
					}
				}
			}
		}

		public void GetDimensionIndexes(int index, out int x, out int y, out int z)
		{
			z = index / (this.XLength * this.YLength);
			index -= z * this.XLength * this.YLength;

			y = index / this.XLength;
			index -= y * this.XLength;

			x = index;
		}

		public void InitializeDimensions(int x, int y, int z)
		{
			this.XLength = x;
			this.YLength = y;
			this.ZLength = z;

			var totalDimension = this.XLength * this.YLength * this.ZLength;
			this.Flammable = new bool[totalDimension];
			this.HitPoint = new float[totalDimension];
			this.LifeTime = new float[totalDimension];
			this.Smoke = new float[totalDimension];
			this.FireSpreaded = new bool[totalDimension];
			this.Positions = new Vector3[totalDimension];
		}
		#endregion

		#region Internal Methods
		internal int GetNeighbourIndex(int index, int x, int y, int z) => index + this.IndexOf(x, y, z);

		internal Vector3 GetCachedPositionAt(int index) => this.Positions[index];

		internal Vector3 GetCachedRandomPositionAt(int index) =>
			this.Positions[index]
				+ new Vector3(
					Random.Range(0, this.CellSize.x),
					Random.Range(0, this.CellSize.y),
					Random.Range(0, this.CellSize.z));

		internal Vector3 GetPositionAt(int index)
		{
			this.GetDimensionIndexes(index, out int i, out int j, out int k);

			return new Vector3(
				this.MaxsMinsDif.x * i / this.XLength,
				this.MaxsMinsDif.y * j / this.YLength,
				this.MaxsMinsDif.z * k / this.ZLength)
					+ this.Mins;
		}

		internal Vector3 GetRandomPositionAt(int index)
		{
			this.GetDimensionIndexes(index, out int i, out int j, out int k);

			return this.GetPositionAt(index)
				+ new Vector3(
					Random.Range(0, this.CellSize.x),
					Random.Range(0, this.CellSize.y),
					Random.Range(0, this.CellSize.z));
		}

		internal void SetFlammable(int xIndex, int yIndex, int zIndex, bool flammable = true) =>
			this.Flammable[this.IndexOf(xIndex, yIndex, zIndex)] = flammable;
		#endregion

		#region Methods
		bool IsBetween(float value, float min, float max) => value > min && value < max;

		int GetIndex(float v, float min, float width, float dimension) =>
			(int)Mathf.Min(Convert.ToInt32((v - min) / width), dimension);
		#endregion

		#region Nested Types
		[Serializable]
		public class MeshMapping
		{
			#region Public Fields
			public int Key;

			public List<int> Value;
			#endregion

			#region Constructors
			public MeshMapping(int key, List<int> value)
			{
				this.Key = key;
				this.Value = value;
			}
			#endregion
		}
		#endregion
	}
}
