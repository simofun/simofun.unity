﻿//-----------------------------------------------------------------------
// <copyright file="ISimGridInfoFileInfo.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Serialization
{
	public interface ISimGridInfoFileInfo
	{
		#region Properties
		string DirectoryName { get; }

		string FileName { get; }

		string FileNameExtension { get; }

		string RootDirectoryName { get; }
		#endregion

		#region Methods
		string GetFullDirectoryPath();

		string GetFullPath();
		#endregion
	}
}
