﻿//-----------------------------------------------------------------------
// <copyright file="ISimGridInfoFileNameGenerator.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Serialization
{
	public interface ISimGridInfoFileNameGenerator
	{
		string Generate(string name);
	}
}
