﻿//-----------------------------------------------------------------------
// <copyright file="SimStringExt.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Serialization.Json.Extensions
{
	using Simofun.Unity.Systems.FireSystem.Model;

	/// <summary>
	///		<see cref="string"/>' s extension class
	/// </summary>
	public static class SimStringExt
	{
		public static SimGridInfo ReadJsonGridInfoByFileName(this string src) =>
			new SimGridInfoJsonReader(new SimPersistentGridInfoJsonFileInfo(src)).Read();
	}
}
