﻿//-----------------------------------------------------------------------
// <copyright file="SimGridInfoJsonReader.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Serialization.Json
{
	using Newtonsoft.Json;
	using Simofun.Unity.Systems.FireSystem.Model;
	using System.IO;

	public class SimGridInfoJsonReader : ISimGridInfoReader
	{
		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="SimGridInfoJsonReader"/> class.
		/// </summary>
		public SimGridInfoJsonReader()
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SimGridInfoJsonReader"/> class.
		/// </summary>
		/// <param name="fileInfo">The file information.</param>
		public SimGridInfoJsonReader(ISimGridInfoFileInfo fileInfo)
		{
			this.FileInfo = fileInfo;
		}
		#endregion

		#region Properties		
		/// <inheritdoc />
		public ISimGridInfoFileInfo FileInfo { get; }
		#endregion

		#region Public Methods
		/// <inheritdoc />
		public SimGridInfo Read()
		{
			if (this.FileInfo == null)
			{
				throw new InvalidDataException($"{this.FileInfo} cannot be 'null'");
			}

			var path = this.FileInfo.GetFullPath();

			return Directory.Exists(this.FileInfo.GetFullDirectoryPath()) && File.Exists(path)
				? JsonConvert.DeserializeObject<SimGridInfo>(File.ReadAllText(path))
				: null;
		}

		/// <inheritdoc />
		public SimGridInfo Read(string text) => JsonConvert.DeserializeObject<SimGridInfo>(text);
		#endregion
	}
}
