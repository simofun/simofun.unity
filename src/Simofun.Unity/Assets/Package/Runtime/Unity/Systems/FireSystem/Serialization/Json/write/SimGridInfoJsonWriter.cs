﻿//-----------------------------------------------------------------------
// <copyright file="SimGridInfoJsonWriter.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Serialization.Json
{
	using Newtonsoft.Json;
	using Simofun.Unity.Systems.FireSystem.Model;
	using Simofun.Unity.Systems.FireSystem.Serialization;
	using System.IO;

	public class SimGridInfoJsonWriter : ISimGridInfoWriter
	{
		#region Constructors		
		/// <summary>
		/// Initializes a new instance of the <see cref="SimGridInfoJsonWriter"/> class.
		/// </summary>
		/// <param name="fileInfo">The file information.</param>
		public SimGridInfoJsonWriter(ISimGridInfoFileInfo fileInfo)
		{
			this.FileInfo = fileInfo;
		}
		#endregion

		#region Properties
		/// <inheritdoc />
		public ISimGridInfoFileInfo FileInfo { get; }
		#endregion

		#region Public Methods
		/// <inheritdoc />
		public void Write(SimGridInfo gridInfo)
		{
			var dir = this.FileInfo.GetFullDirectoryPath();
			if (!Directory.Exists(dir))
			{
				Directory.CreateDirectory(dir);
			}

			File.WriteAllText(this.FileInfo.GetFullPath(), JsonConvert.SerializeObject(gridInfo));
		}
		#endregion
	}
}
