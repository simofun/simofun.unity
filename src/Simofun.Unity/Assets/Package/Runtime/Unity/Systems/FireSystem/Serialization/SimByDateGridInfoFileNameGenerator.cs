﻿//-----------------------------------------------------------------------
// <copyright file="SimByDateGridInfoFileNameGenerator.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Serialization
{
	using System;

	public class SimByDateGridInfoFileNameGenerator : ISimGridInfoFileNameGenerator
	{
		#region Public Methods
		/// <inheritdoc />
		public string Generate(string name) => $"{name}-{DateTime.Now:yyyy.MM.dd HH.mm}";
		#endregion
	}
}
