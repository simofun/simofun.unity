﻿//-----------------------------------------------------------------------
// <copyright file="SimDefaultGridInfoJsonFileInfo.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Serialization.Json
{
	using UnityEngine;

	public class SimDefaultGridInfoJsonFileInfo : ISimGridInfoFileInfo
	{
		#region Constructors		
		/// <summary>
		/// Initializes a new instance of the <see cref="SimDefaultGridInfoJsonFileInfo"/> class.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		public SimDefaultGridInfoJsonFileInfo(string fileName)
		{
			this.FileName = fileName;
		}
		#endregion

		#region Properties
		/// <inheritdoc />
		public virtual string DirectoryName { get; } = "GridInfos";

		/// <inheritdoc />
		public virtual string FileName { get; }

		/// <inheritdoc />
		public virtual string FileNameExtension { get; } = ".json";

		/// <inheritdoc />
		public virtual string RootDirectoryName { get; } = "Data";
		#endregion

		#region Public Methods
		/// <inheritdoc />
		public virtual string GetFullDirectoryPath() =>
			Application.persistentDataPath + $"/{this.RootDirectoryName}/{this.DirectoryName}";

		/// <inheritdoc />
		public virtual string GetFullPath() =>
			$"{this.GetFullDirectoryPath()}/{this.FileName}{this.FileNameExtension}";
		#endregion
	}
}
