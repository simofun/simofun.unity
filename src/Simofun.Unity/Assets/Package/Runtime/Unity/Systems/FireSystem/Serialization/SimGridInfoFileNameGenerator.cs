﻿//-----------------------------------------------------------------------
// <copyright file="SimGridInfoFileNameGenerator.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Serialization
{
	public class SimGridInfoFileNameGenerator : ISimGridInfoFileNameGenerator
	{
		#region Public Methods
		/// <inheritdoc />
		public string Generate(string name) => name;
		#endregion
	}
}
