﻿//-----------------------------------------------------------------------
// <copyright file="SimPersistentGridInfoJsonFileInfo.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Serialization.Json
{
	using UnityEngine;

	public class SimPersistentGridInfoJsonFileInfo : SimDefaultGridInfoJsonFileInfo
	{
		#region Constructors		
		/// <summary>
		/// Initializes a new instance of the <see cref="SimPersistentGridInfoJsonFileInfo"/> class.
		/// </summary>
		/// <param name="fileName">Name of theSimPersistentGridInfoJsonFileInfo file.</param>
		public SimPersistentGridInfoJsonFileInfo(string fileName) : base(fileName)
		{
		}
		#endregion

		#region Public Methods
		/// <inheritdoc />
		public override string GetFullDirectoryPath() =>
			Application.persistentDataPath + $"/{this.RootDirectoryName}/{this.DirectoryName}";
		#endregion
	}
}
