﻿//-----------------------------------------------------------------------
// <copyright file="SimResourcesGridInfoJsonFileInfo.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Serialization.Json
{
	public class SimResourcesGridInfoJsonFileInfo : SimDefaultGridInfoJsonFileInfo
	{
		#region Constructors		
		/// <summary>
		/// Initializes a new instance of the <see cref="SimResourcesGridInfoJsonFileInfo"/> class.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		public SimResourcesGridInfoJsonFileInfo(string fileName) : base(fileName)
		{
		}
		#endregion

		#region Public Methods
		/// <inheritdoc />
		public override string GetFullDirectoryPath() => $"{this.RootDirectoryName}/{this.DirectoryName}";

		/// <inheritdoc />
		public override string GetFullPath() => $"{this.GetFullDirectoryPath()}/{this.FileName}";
		#endregion
	}
}
