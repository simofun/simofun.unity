﻿//-----------------------------------------------------------------------
// <copyright file="ISimGridInfoReader.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Serialization
{
	using Simofun.Unity.Systems.FireSystem.Model;

	public interface ISimGridInfoReader
	{
		#region Properties		
		/// <summary>
		/// Gets the file information.
		/// </summary>
		/// <value>
		/// The file information.
		/// </value>
		ISimGridInfoFileInfo FileInfo { get; }
		#endregion

		#region Methods
		SimGridInfo Read();

		SimGridInfo Read(string text);
		#endregion
	}
}
