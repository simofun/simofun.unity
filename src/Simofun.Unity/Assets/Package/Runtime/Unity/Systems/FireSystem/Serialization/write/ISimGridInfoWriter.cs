﻿//-----------------------------------------------------------------------
// <copyright file="ISimGridInfoWriter.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Serialization
{
	using Simofun.Unity.Systems.FireSystem.Model;

	public interface ISimGridInfoWriter
	{
		#region Properties
		ISimGridInfoFileInfo FileInfo { get; }
		#endregion

		#region Methods
		void Write(SimGridInfo gridInfo);
		#endregion
	}
}
