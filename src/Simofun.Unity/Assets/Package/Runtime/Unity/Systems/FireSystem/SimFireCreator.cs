//-----------------------------------------------------------------------
// <copyright file="SimFireCreator.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

using UniRx;

namespace Simofun.Unity.Systems.FireSystem
{
	using Bayhaksam.Unity.Logging.DevelopmentBuild;
	using Simofun.UniRx.Unity;
	using Simofun.Unity.Data.Model;
	using Simofun.Unity.Systems.FireSystem.Extinguish.Events;
	using Simofun.Unity.Systems.FireSystem.Initialization;
	using Simofun.Unity.Systems.FireSystem.Model;
	using Simofun.Unity.Systems.FireSystem.Serialization;
	using Simofun.Unity.Systems.FireSystem.Serialization.Json;
	using Sirenix.OdinInspector;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;
	using UnityEngine.SceneManagement;

	public class SimFireCreator : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimFireCreator), "Settings")]
		SimGridInfo grid;

		[SerializeField]
		SimAppDataPath dataPath = SimAppDataPath.Resources;

		[SerializeField]
		Vector3 cellCount;

		[SerializeField]
		float fireSpreadPower = 0.4f;

		[SerializeField]
		int spreadInterval = 1000;

		[SerializeField]
		float coolerStrength = 0.15f;

		[SerializeField, Title("", "References")]
		Light fireLight;
		#endregion

		#region Fields
		const float fireEmitInterval = 250f;

		const float minDamageDistance = 5f;

		const float smokeEmitInterval = 1000f;

		AudioSource fireAudioSource;

		SimFireSpreader spreader;

		IDisposable audioTimerDisposable;

		IDisposable fireVisualizerTimerDisposable;

		IDisposable smokeVisualizerTimerDisposable;

		IDisposable spreaderTimerDisposable;

		MeshRenderer[] meshRenderers;

		ParticleSystem fireEffect;

		ParticleSystem psSmoke;

		ParticleSystem smokeEffect;

		Vector3 wind = Vector3.up * 0.75f;
		#endregion

		#region Events
		public event Action OnGridCreated;
		#endregion

		#region Properties
		public bool IsFireExtinguished => this.spreader.IsFireExtinguished;

		public SimGridInfo Grid { get => this.grid; protected set => this.grid = value; }
		#endregion

		#region Unity Methods		
		/// <inheritdoc />
		protected virtual void Awake()
		{
			this.fireAudioSource = Instantiate(
				Resources.Load<GameObject>("FireSystem/Prefabs/FireAudio")).GetComponent<AudioSource>();
		}

		/// <inheritdoc />
		protected virtual void Start()
		{
			SimMessageBus.OnEvent<SimFireExtinguishHitEvent>()
				.Subscribe(whe => this.CoolWorldPosition(whe.Position, whe.Strength * this.coolerStrength))
				.AddTo(this);

			Observable.Timer(TimeSpan.FromSeconds(1f))
				.Subscribe(_ =>
				{
					if (this.fireEffect != null)
					{
						var lightModule = this.fireEffect.lights;
						lightModule.enabled = true;
						lightModule.light = this.fireLight;
						lightModule.maxLights = 150;
						lightModule.ratio = 1f;
						lightModule.intensityMultiplier = 0.5f;
					}

					this.SimulateFire();
				})
				.AddTo(this);
		}

		/// <inheritdoc />
		protected virtual void OnDisable()
		{
			if (this.spreaderTimerDisposable != null)
			{
				this.spreaderTimerDisposable.Dispose();
				this.spreaderTimerDisposable = null;
			}

			if (this.fireVisualizerTimerDisposable != null)
			{
				this.fireVisualizerTimerDisposable.Dispose();
				this.fireVisualizerTimerDisposable = null;
			}

			if (this.smokeVisualizerTimerDisposable != null)
			{
				this.smokeVisualizerTimerDisposable.Dispose();
				this.smokeVisualizerTimerDisposable = null;
			}
		}

		/// <inheritdoc />
		protected virtual void OnDestroy()
		{
			if (this.fireEffect != null)
			{
				Destroy(this.fireEffect.gameObject);
				this.fireEffect = null;
			}

			this.OnGridCreated = null;
			this.audioTimerDisposable?.Dispose();
		}
		#endregion

		#region Public Methods
		/// <summary>
		/// Gets damage for certain given position
		/// </summary>
		/// <param name="position"></param>
		/// <returns></returns>
		public float GetDamage(Vector3 position)
		{
			var totalDamage = 0f;
			foreach (var index in this.Grid.FlammableIndexes)
			{
				var nodePosition = this.Grid.Positions[index] + this.transform.position;
				var checkDistance = (nodePosition - position).magnitude;
				var damage = this.Grid.HitPoint[index] * (minDamageDistance - checkDistance) / minDamageDistance;

				if (checkDistance < minDamageDistance && this.Grid.HitPoint[index] < 0)
				{
					totalDamage += damage;
				}
			}

			return Mathf.Abs(totalDamage);
		}

		public Vector3 ConvertWorldToLocalPosition(Vector3 pos)
		{
			var p = this.transform.InverseTransformPoint(pos);
			var q = Quaternion.AngleAxis(this.transform.localRotation.eulerAngles.y, Vector3.up);

			return q * p;
		}

		public void ChangeWindForce(Vector3 wind) => this.wind = wind;

		public void CoolWorldPosition(Vector3 p, float hitPoint)
		{
			if (this.Grid == null)
			{
				return;
			}

			p = this.transform.InverseTransformPoint(p);
			var q = Quaternion.AngleAxis(this.transform.localRotation.eulerAngles.y, Vector3.up);
			p = q * p;
			this.Grid.CoolWorldPosition(p, hitPoint);
		}

		public void StartFireAtWorldPosition(Vector3 p, int range = 1)
		{
			if (this.Grid == null)
			{
				debug.Log("Grid creation in progress wait ...");

				return;
			}

			this.Grid.FireLocalPosition(this.ConvertWorldToLocalPosition(p), range); // Convert world to local
			this.Grid.IsFireStarted = true;
		}

		public void StopFire()
		{
			for (var i = 0; i < this.Grid.HitPoint.Length; i++)
			{
				this.Grid.HitPoint[i] = 50f;
			}
		}
		#endregion

		#region Methods
		void AudioUpdate()
		{
			if (this.Grid == null)
			{
				if (this.audioTimerDisposable != null || this.fireAudioSource)
				{
					this.audioTimerDisposable?.Dispose();
				}

				this.audioTimerDisposable = Observable.Timer(TimeSpan.FromMilliseconds(1000))
					.Subscribe(_ => this.AudioUpdate());

				return;
			}

			var positionSum = Vector3.zero;
			var burningNodeCount = 0;
			for (var i = 0; i < this.Grid.FlammableIndexes.Length; i++)
			{
				var flammableIndex = this.Grid.FlammableIndexes[i];
				if (!(this.Grid.HitPoint[flammableIndex] <= 0))
				{
					continue;
				}

				positionSum += (this.transform.position + this.Grid.GetCachedPositionAt(flammableIndex));
				burningNodeCount++;
			}

			if (burningNodeCount == 0)
			{
				this.fireAudioSource.Stop();
			}
			else
			{
				this.fireAudioSource.transform.position = positionSum / burningNodeCount;
				this.fireAudioSource.volume = 0.5f + Mathf.Clamp(burningNodeCount / 500f, 0, 0.5f);
				this.fireAudioSource.pitch = 0.9f + Mathf.Clamp(burningNodeCount / 500f, 0, 0.3f);
			}

			this.audioTimerDisposable?.Dispose();
			this.audioTimerDisposable = Observable.Timer(TimeSpan.FromMilliseconds(1000))
				.Subscribe(_ => this.AudioUpdate());
		}

		ISimGridInfoFileInfo GetFileInfoByDataPath(string gridInfoFileName)
		{
			switch (this.dataPath)
			{
				case SimAppDataPath.Default:
					{
						return new SimDefaultGridInfoJsonFileInfo(gridInfoFileName);
					}
				case SimAppDataPath.PersistentDataPath:
					{
						return new SimPersistentGridInfoJsonFileInfo(gridInfoFileName);
					}

				case SimAppDataPath.Resources:
					{
						return new SimResourcesGridInfoJsonFileInfo(gridInfoFileName);
					}
				default:
					{
						return new SimDefaultGridInfoJsonFileInfo(gridInfoFileName);
					}
			}
		}

		void InitParticleSystems()
		{
			if (this.fireEffect != null)
			{
				return;
			}

			var fireEffectGo = Instantiate(Resources.Load<GameObject>("FireSystem/Prefabs/FireEffect"));
			if (fireEffectGo == null)
			{
				Debug.LogError("Could not found fire effect prefab ");

				return;
			}

			fireEffectGo.transform.SetParent(this.transform);
			this.fireEffect = fireEffectGo.GetComponent<ParticleSystem>();

			foreach (Transform child in this.fireEffect.transform)
			{
				this.psSmoke = child.GetComponent<ParticleSystem>();
				if (this.psSmoke == null)
				{
					break;
				}
			}

			var smokeEffectGo = Instantiate(Resources.Load<GameObject>("FireSystem/Prefabs/SmokeEffect"));
			if (smokeEffectGo == null)
			{
				Debug.LogError("Could not found smoke effect prefab ");

				return;
			}
			
			this.smokeEffect = smokeEffectGo.GetComponent<ParticleSystem>();
		}

		void OnGridInfoFound()
		{
			this.Grid.ReferencePoint = this.transform.position;
			this.Grid.PivotPoint = this.transform.TransformPoint(Vector3.zero);
			this.Grid.FlammableIndexesHashSet = new HashSet<int>(this.Grid.FlammableIndexes); // Hashset don't serialize

			this.StartFire();
		}

		void OnGridInfoNotFound(MeshFilter[] meshFilters)
		{
			var refPos = this.transform.position;
			var center = this.transform.TransformPoint(Vector3.zero);

			new SimMeshGridExtractor(meshFilters).GenerateGridAsync(this.cellCount, this.transform, (grid) =>
			{
				// Save newly generated 'GridInfo' to JSON file.
				var gridInfoFileName = new SimGridInfoFileNameGenerator().Generate(SceneManager.GetActiveScene().name);
				new SimGridInfoJsonWriter(this.GetFileInfoByDataPath(gridInfoFileName)).Write(grid);

				this.Grid = grid;
				this.Grid.ReferencePoint = refPos;
				this.Grid.PivotPoint = center;

				MainThreadDispatcher.Post((s) => this.StartFire(), null);
			});
		}

		void SetBurnMaterial(MeshFilter[] meshFilters)
		{
			var source = Resources.Load<Material>("FireSystem/Materials/Burn");
			for (var i = 0; i < meshFilters.Length; i++)
			{
				var r = meshFilters[i].GetComponent<MeshRenderer>();
				if (r == null)
				{
					continue;
				}

				this.meshRenderers[i] = r;

				var mat = new Material(source);
				var mats = r.materials;
				for (var j = 0; j < mats.Length; j++)
				{
					var m = mats[j];
					if (!m.HasProperty("_Mode"))
					{
						continue;
					}

					var mode = m.GetFloat("_Mode");
					if (mode >= 0.1f)
					{
						continue;
					}

					if (m.HasProperty("_MainTex"))
					{
						mat.SetTexture("_MainTex", m.GetTexture("_MainTex"));
						mat.SetTextureScale("_MainTex", m.GetTextureScale("_MainTex"));
					}

					if (m.HasProperty("_BumpMap"))
					{
						mat.SetTexture("_BumpMap", m.GetTexture("_BumpMap"));
						mat.SetTextureScale("_BumpMap", m.GetTextureScale("_BumpMap"));
					}

					mat.SetColor("_Color", m.color);

					mats[j] = mat;
				}

				r.materials = mats;
			}
		}

		void SimulateFire()
		{
			// Fire simulation
			var meshFilters = this.GetComponentsInChildren<MeshFilter>()
				.Where(mf =>
				{
					var mr = mf.GetComponent<MeshRenderer>();
					if (mr == null || mr.material == null || !mf.mesh.vertices.Any() || !mf.mesh.triangles.Any())
					{
						return false;
					}

					//mr.enabled = false;

					return true;
				})
				.ToArray();

			meshFilters
				.Where(mf => mf.GetComponent<Collider>() == null)
				.ToList()
				.ForEach(mf => mf.gameObject.AddComponent<MeshCollider>());
			if (!meshFilters.Any())
			{
				Debug.LogError(
					"Fire creator can't find meshRenderers for grid extraction."
						+ " MeshRenderer component can be disabled"
						+ " but gameobjects must be active to include mesh for grid extraction.");

				return;
			}

			this.meshRenderers = new MeshRenderer[meshFilters.Length];
			this.SetBurnMaterial(meshFilters); //Change material to burn material

			this.InitParticleSystems();

			var gridInfoFileName = SceneManager.GetActiveScene().name;
			SimGridInfo gridInfo = null;
			if (this.dataPath == SimAppDataPath.Resources)
			{
				gridInfo = new SimGridInfoJsonReader()
					.Read(Resources.Load<TextAsset>(this.GetFileInfoByDataPath(gridInfoFileName).GetFullPath()).text);
			}
			else
			{
				gridInfo = new SimGridInfoJsonReader(this.GetFileInfoByDataPath(gridInfoFileName)).Read();
			}

			if (gridInfo != null)
			{
				this.Grid = gridInfo;
			}
			else
			{
				this.Grid = null;

				debug.Log($"Didn't found grid info for named '{gridInfoFileName}'.");
			}

			// Load grid info if provided else create new one
			if (this.Grid != null)
			{
				this.OnGridInfoFound();

				return;
			}

			this.OnGridInfoNotFound(meshFilters);
		}

		void StartFire()
		{
			this.StartFireSpread();
			this.VisualizeFire();
			this.VisualizeSmoke();
			this.AudioUpdate();
		}

		void StartFireSpread()
		{
			this.spreader = new SimFireSpreader(this.Grid) { HitDelta = fireSpreadPower };
			this.spreader.Start();

			Observable.Interval(TimeSpan.FromMilliseconds(this.spreadInterval))
				.Subscribe(_ => this.spreader.Update(this.wind, this.spreadInterval))
				.AddTo(this);

			// Use fire start position setters
			var fireStartPosSetters = this.GetComponentsInChildren<ISimFireStartPositionSetter>();
			if (fireStartPosSetters.Any())
			{
				foreach (var fireStartPosSetter in fireStartPosSetters)
				{
					fireStartPosSetter.Execute();
					this.StartFireAtWorldPosition(fireStartPosSetter.FireStartPosition);
				}

				this.fireAudioSource.Play();
			}

			debug.Log("Grid Created");

			Observable.NextFrame().Subscribe(_ => OnGridCreated?.Invoke()).AddTo(this); // TODO: Fix later
		}

		void VisualizeFire()
		{
			if (this.Grid == null)
			{
				this.fireVisualizerTimerDisposable?.Dispose();
				this.fireVisualizerTimerDisposable = Observable.Timer(TimeSpan.FromMilliseconds(fireEmitInterval))
					.Subscribe(_ => this.VisualizeFire());

				return;
			}

			//Debug.Log("Fire Particle Count: " + this.psFire.particleCount);
			var parent = this.transform;
			while (parent.parent != null)
			{
				parent = parent.parent;
			}

			var flammableIndexCount = this.Grid.FlammableIndexes.Length;
			for (var i = 0; i < flammableIndexCount; i++)
			{
				var flammableIndex = this.Grid.FlammableIndexes[i];
				if (!(this.Grid.HitPoint[flammableIndex] <= 0))
				{
					continue;
				}

				var rand = this.Grid.GetCachedRandomPositionAt(flammableIndex); // Grid.GetRandomPositionAt(flammableIndex);
				var transformPos = this.transform.position;
				var position = transformPos + rand;

				if (this.fireEffect != null)
				{
					this.fireEffect.Emit(
						new ParticleSystem.EmitParams { position = position }, 1); // 1 emit per interval
				}
			}

			foreach (var pair in this.Grid.MeshToNodeMapping)
			{
				var materials = this.meshRenderers[pair.Key].materials;
				foreach (var material in materials.Where(material => material.HasProperty("_BurnAmount")))
				{
					material.SetFloat("_BurnAmount", this.Grid.MeshBurnAmount[pair.Key]);
				}

				this.meshRenderers[pair.Key].materials = materials;
			}

			this.fireVisualizerTimerDisposable?.Dispose();
			this.fireVisualizerTimerDisposable = Observable
				.Timer(TimeSpan.FromMilliseconds(fireEmitInterval))
				.Subscribe(_ => this.VisualizeFire());
		}

		void VisualizeSmoke()
		{
			if (this.Grid == null)
			{
				this.smokeVisualizerTimerDisposable?.Dispose();
				this.smokeVisualizerTimerDisposable = Observable.Timer(TimeSpan.FromMilliseconds(smokeEmitInterval))
					.Subscribe(_ => this.VisualizeSmoke());

				return;
			}

			for (var i = 0; i < this.Grid.Smoke.Length; i++)
			{
				var rand = this.Grid.GetCachedRandomPositionAt(i);
				var transformPos = this.transform.position;
				var position = (transformPos) + rand;

				if (this.Grid.BorderEmit[i])
				{
					this.smokeEffect.Emit(new ParticleSystem.EmitParams { position = position }, 1);
					this.Grid.BorderEmit[i] = false;
				}
				else if (this.Grid.Smoke[i] <= SimGridInfo.SmokeEmitTreshold)
				{
					continue;
				}

				if (this.psSmoke != null)
				{
					this.psSmoke.Emit(new ParticleSystem.EmitParams { position = position }, 1);
				}
			}

			this.smokeVisualizerTimerDisposable?.Dispose();
			this.smokeVisualizerTimerDisposable = Observable
				.Timer(TimeSpan.FromMilliseconds(smokeEmitInterval))
				.Subscribe(_ => this.VisualizeSmoke());
		}
		#endregion
	}
}
