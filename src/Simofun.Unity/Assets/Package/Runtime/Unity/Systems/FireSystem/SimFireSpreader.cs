﻿//-----------------------------------------------------------------------
// <copyright file="SimFireSpreader.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem
{
	using Simofun.Unity.Systems.FireSystem.Model;
	using System;
	using System.Collections.Generic;
	using System.ComponentModel;
	using System.Linq;
	using System.Threading;
	using UnityEngine;
	using Random = System.Random;

	public class SimFireSpreader : IDisposable
	{
		#region Fields
		const float EmitAmount = 0.25f;

		readonly AutoResetEvent ev = new AutoResetEvent(false);

		readonly bool[] processed;

		readonly SimGridInfo grid;

		readonly Random random = new Random();

		BackgroundWorker bgWorker;

		float deltaTime;

		Vector3 wind;
		#endregion

		#region Properties
		public bool IsFireExtinguished { get; private set; }

		public bool IsRunning { get => this.bgWorker != null && this.bgWorker.IsBusy; }

		public float HitDelta { get; set; }
		#endregion

		#region Constructors
		public SimFireSpreader(SimGridInfo grid)
		{
			this.grid = grid;
			this.HitDelta = 0.5f;
			this.processed = new bool[grid.Flammable.Length];
		}
		#endregion

		#region Public Methods
		#region IDisposable Methods
		public void Dispose() => throw new NotImplementedException();
		#endregion

		public void Start()
		{
			this.InitBw();
			this.bgWorker.RunWorkerAsync();
			this.bgWorker.WorkerSupportsCancellation = true;
		}

		public void Stop()
		{
			if (!this.IsRunning)
			{
				return;
			}

			this.bgWorker.CancelAsync();
			this.ev.Reset();
		}

		public void Update(Vector3 wind, float deltaTime)
		{
			this.wind = wind;
			this.deltaTime = deltaTime;
			this.ev.Set();
		}
		#endregion

		#region Methods
		#region Event Handlers
		void HandleOnBgWorkerDoWork(object sender, DoWorkEventArgs e)
		{
			while (!this.bgWorker.CancellationPending)
			{
				this.ev.WaitOne();
				this.SpreadFire();
				this.ClearProcessedIndexes();
			}

			this.ev.Set();
		}

		void HandleOnBgWorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			this.bgWorker.DoWork -= this.HandleOnBgWorkerDoWork;
			this.bgWorker.RunWorkerCompleted -= this.HandleOnBgWorkerRunWorkerCompleted;
		}
		#endregion

		void ApplySpread(int newIndex, float hitPoint)
		{
			// To prevent reignite of burned nodes
			var lifeTimeIndex = this.grid.LifeTime[newIndex];
			if (this.grid.HitPoint[newIndex] > -30f/* && lifeTimeIndex > 0f*/)
			{
				this.grid.HitPoint[newIndex] -= hitPoint;
			}

			if (lifeTimeIndex > 0)
			{
				this.grid.LifeTime[newIndex] = Mathf.Clamp(lifeTimeIndex - hitPoint, 0f, 100f);
			}

			this.processed[newIndex] = true;
		}

		void ClearProcessedIndexes()
		{
			for (var i = 0; i < this.processed.Length; i++)
			{
				this.processed[i] = false;
			}
		}

		void EmitSmoke(int emitterIndex, int emitableIndex)
		{
			// 'EmitAmount' => Mathf.Clamp(this.grid.HitPoint[emitterIndex] / -50f, 0f, 1f); // Test purpose
			this.grid.Smoke[emitableIndex] += EmitAmount;
		}

		void InitBw()
		{
			this.bgWorker = new BackgroundWorker();
			this.bgWorker.DoWork += this.HandleOnBgWorkerDoWork;
			this.bgWorker.RunWorkerCompleted += this.HandleOnBgWorkerRunWorkerCompleted;
		}

		void SpreadFire()
		{
			// TODO: Find better solution, this is workaround to prevent burn texture sync
			var isExtinguished = true;
			var spreadableNeighbourNodes = new Dictionary<int, float>();
			try
			{
				for (var i = 0; i < this.grid.HitPoint.Length; i++)
				{
					this.grid.GetDimensionIndexes(i, out var dx, out var dy, out var dz);

					var minX = (dx == 0) ? 0 : -1;
					var maxX = (dx == this.grid.XLength - 1) ? 0 : 1;

					var minY = (dy == 0) ? 0 : -1;
					var maxY = (dy == this.grid.YLength - 1) ? 0 : 1;

					var minZ = (dz == 0) ? 0 : -1;
					var maxZ = (dz == this.grid.ZLength - 1) ? 0 : 1;

					var isNodeOnFire = this.grid.HitPoint[i] < 5f;
					if (isNodeOnFire)
					{
						isExtinguished = false;
					}

					var smoke = this.grid.Smoke[i];
					spreadableNeighbourNodes.Clear();
					spreadableNeighbourNodes.Add(i, smoke);

					for (var j = minX; j <= maxX; j++)
					{
						for (var k = minY; k <= maxY; k++)
						{
							for (var l = minZ; l <= maxZ; l++)
							{
								var newIndex = this.grid.GetNeighbourIndex(i, j, k, l);
								if (isNodeOnFire && newIndex >= 0 && this.grid.HitPoint.Length > newIndex)
								{
									//var res = Vector3.Dot(new Vector3(j, k, l), this.wind); // 'Wind' effect disabled
									var delta = this.HitDelta/*+ res*/;

									if (this.grid.Flammable[newIndex] && !this.processed[newIndex])
									{
										this.ApplySpread(newIndex, delta * this.random.Next(0, 300) / 100f);
										this.EmitSmoke(i, newIndex);
									}
								}

								if (smoke > 0
										&& newIndex < this.grid.Smoke.Length
										&& !this.grid.FlammableIndexesHashSet.Contains(newIndex)
										&& !spreadableNeighbourNodes.ContainsKey(newIndex))
								{
									spreadableNeighbourNodes.Add(newIndex, this.grid.Smoke[newIndex]);
								}
							}
						}
					}

					if (spreadableNeighbourNodes.Count > 1)
					{
						var targetSmoke = spreadableNeighbourNodes.Values.Average();
						foreach (var pair in spreadableNeighbourNodes)
						{
							this.grid.Smoke[pair.Key] = targetSmoke;
						}
					}

					this.IsFireExtinguished = isExtinguished;
				}
			}
			catch (Exception)
			{
				// Ignored
			}

			foreach (var borderIndex in this.grid.BorderIndexes)
			{
				if (this.grid.Smoke[borderIndex] > 1)
				{
					this.grid.Smoke[borderIndex] -= EmitAmount;
					this.grid.BorderEmit[borderIndex] = true;
				}
			}

			foreach (var pair in this.grid.MeshToNodeMapping)
			{
				this.grid.MeshBurnAmount[pair.Key] = 1f
					- pair.Value.Sum(s => this.grid.LifeTime[s]) / (pair.Value.Count * 100);
			}
		}
		#endregion
	}
}
