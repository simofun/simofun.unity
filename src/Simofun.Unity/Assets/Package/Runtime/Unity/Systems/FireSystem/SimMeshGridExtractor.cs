﻿//-----------------------------------------------------------------------
// <copyright file="SimMeshGridExtractor.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem
{
	using Bayhaksam.Unity.Logging.DevelopmentBuild;
	using Simofun.Unity.Systems.FireSystem.Model;
	using System;
	using System.Collections.Generic;
	using System.ComponentModel;
	using System.Linq;
	using UnityEngine;

	public class SimMeshGridExtractor
	{
		#region Fields
		readonly MeshFilter[] meshFilters;
		#endregion

		#region Constructors		
		/// <summary>
		/// Initializes a new instance of the <see cref="SimMeshGridExtractor"/> class.
		/// </summary>
		/// <param name="meshFilters">The mesh filters.</param>
		public SimMeshGridExtractor(params MeshFilter[] meshFilters)
		{
			this.meshFilters = meshFilters;
		}
		#endregion

		#region Public Methods
		public SimGridInfo GenerateGrid(
			Vector3[][] vertices,
			int[][] triangles,
			float[] hitPoints,
			float[] lifeTime,
			Vector3 cellCount,
			float resolution)
		{
			var maxs = new Vector3(float.MinValue, float.MinValue, float.MinValue);
			var mins = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);

			foreach (var v in vertices)
			{
				mins.x = Mathf.Min(mins.x, v.Min(_ => _.x));
				mins.y = Mathf.Min(mins.y, v.Min(_ => _.y));
				mins.z = Mathf.Min(mins.z, v.Min(_ => _.z));

				maxs.x = Mathf.Max(maxs.x, v.Max(_ => _.x));
				maxs.y = Mathf.Max(maxs.y, v.Max(_ => _.y));
				maxs.z = Mathf.Max(maxs.z, v.Max(_ => _.z));
			}

			var cellWidthX = (maxs.x - mins.x + 1) / cellCount.x;
			var cellWidthY = (maxs.y - mins.y + 1) / cellCount.y;
			var cellWidthZ = (maxs.z - mins.z + 1) / cellCount.z;

			var xDimension = Convert.ToInt32((maxs.x - mins.x) / cellWidthX) + 1;
			var yDimension = Convert.ToInt32((maxs.y - mins.y) / cellWidthY) + 1;
			var zDimension = Convert.ToInt32((maxs.z - mins.z) / cellWidthZ) + 1;

			var info = new SimGridInfo();
			info.InitializeDimensions(xDimension, yDimension, zDimension);

			var indexes = new Vector3[vertices.Length][];
			for (var i = 0; i < vertices.Length; i++)
			{
				var vi = vertices[i];
				var index = new Vector3[vi.Length];
				indexes[i] = index;
				for (var j = 0; j < vi.Length; j++)
				{
					var vj = vi[j];
					var xIndex = this.GetIndex(vj.x, mins.x, cellWidthX, xDimension - 1);
					var yIndex = this.GetIndex(vj.y, mins.y, cellWidthY, yDimension - 1);
					var zIndex = this.GetIndex(vj.z, mins.z, cellWidthZ, zDimension - 1);

					try
					{
						var linearIndex = info.IndexOf(xIndex, yIndex, zIndex);

						//info.Flammable[linearIndex] = true; // Disabled because makes noises in grid
						index[j] = new Vector3(xIndex, yIndex, zIndex);
						//info.HitPoint[linearIndex] = hitPoints[i];
						info.LifeTime[linearIndex] = lifeTime[i];
					}
					catch (Exception)
					{
						Debug.LogError(xIndex + "-" + yIndex + "-" + zIndex);
					}
				}
			}

			for (var i = 0; i < info.HitPoint.Length; i++)
			{
				info.HitPoint[i] = SimGridInfo.HitPointTreshold;
				info.LifeTime[i] = 100;
				info.Smoke[i] = 0f;
			}

			for (var i = 0; i < triangles.Length; i++)
			{
				var index = indexes[i];
				var triangle = triangles[i];
				for (var j = 0; j < triangle.Length; j += 3)
				{
					// 'i' is meshFilter index
					FillGrid(info, index[triangle[j]], index[triangle[j + 1]], index[triangle[j + 2]], i);
				}
			}

			info.Mins = new Vector3(mins[0], mins[1], mins[2]);
			info.Maxs = new Vector3(maxs[0], maxs[1], maxs[2]);
			info.MaxsMinsDif = info.Maxs - info.Mins;
			info.CellSize = new Vector3(cellWidthX, cellWidthY, cellWidthZ);
			info.Resolution = resolution;

			for (var i = 0; i < info.XLength; i++)
			{
				for (var j = 0; j < info.YLength; j++)
				{
					for (var k = 0; k < info.ZLength; k++)
					{
						info.Positions[info.IndexOf(i, j, k)] = new Vector3(
							i * info.Resolution + info.Mins.x,
							j * info.Resolution + info.Mins.y,
							k * info.Resolution + info.Mins.z);
					}
				}
			}

			debug.Log("Flammable Count:" + cellCount + " | " + info.Flammable.Count(x => x));

			return info;
		}

		public void GenerateGridAsync(Vector3 cellCount, Transform parent, Action<SimGridInfo> callBack)
		{
			using (var bw = new BackgroundWorker())
			{
				var vertices = this.meshFilters
					.Select(m =>
						m.mesh.vertices.Select(i => m.transform.TransformPoint(i) - parent.position).ToArray())
					.ToArray();
				var triangles = this.meshFilters.Select(i => i.mesh.triangles).ToArray();

				var hitpoints = this.meshFilters.Select(_ => 50f).ToArray();
				var lifeTime = this.meshFilters.Select(_ => 3000f).ToArray();

				var maxX = vertices.Max(x => x.Max(x2 => x2.x));
				var maxY = vertices.Max(y => y.Max(y2 => y2.y));
				var maxZ = vertices.Max(z => z.Max(z2 => z2.z));

				var minX = vertices.Min(x => x.Min(x2 => x2.x));
				var minY = vertices.Min(y => y.Min(y2 => y2.y));
				var minZ = vertices.Min(z => z.Min(z2 => z2.z));

				const float resolution = 0.5f;
				cellCount = new Vector3(
					Mathf.CeilToInt(Mathf.Abs(maxX - minX) / resolution) + 1,
					Mathf.CeilToInt(Mathf.Abs(maxY - minY) / resolution) + 1,
					Mathf.CeilToInt(Mathf.Abs(maxZ - minZ) / resolution) + 1);
				debug.Log("Cell Count:" + cellCount + " | " + cellCount.x * cellCount.y * cellCount.z);

				bw.DoWork += (s, e) =>
				{
					var res = this.GenerateGrid(vertices, triangles, hitpoints, lifeTime, cellCount, resolution);
					res.EndInitialization();
					e.Result = res;
				};

				bw.RunWorkerCompleted += (s, e) =>
				{
					if (e.Error != null)
					{
						Debug.LogError(e.Error);
					}

					callBack?.Invoke((SimGridInfo)e.Result);
				};
				bw.RunWorkerAsync();
			}
		}
		#endregion

		#region Methods
		int GetIndex(float v, float min, float width, float dimension) =>
			(int)Mathf.Min(Convert.ToInt32((v - min) / width), dimension);

		void FillGrid(SimGridInfo info, Vector3 v1, Vector3 v2, Vector3 v3, int meshFilterIndex)
		{
			var refPos = info.IndexOf((int)v1.x, (int)v1.y, (int)v1.z);
			const float stepf = 0.01f; // 0.01 for more precise mesh, maybe use parameter for this
			for (var a1 = 0f; a1 <= 0.99f; a1 += stepf)
			{
				for (var a2 = 0f; (a1 + a2) <= 0.99f && a2 <= 0.99f; a2 += stepf)
				{
					var a3 = 0.99f - (a1 + a2);
					var pos = (v1 * a1 + v2 * a2 + v3 * a3);

					var linearIndex = info.IndexOf((int)pos.x, (int)pos.y, (int)pos.z);

					info.Flammable[linearIndex] = true;
					info.HitPoint[linearIndex] = info.HitPoint[refPos];
					info.LifeTime[linearIndex] = info.LifeTime[refPos];
					info.Smoke[linearIndex] = 0f;
					info.FireSpreaded[linearIndex] = false;

					if (!info.MeshToNodeMapping.ContainsKey(meshFilterIndex))
					{
						info.MeshToNodeMapping.Add(meshFilterIndex, new List<int> { linearIndex });
					}
					else if (!info.MeshToNodeMapping[meshFilterIndex].Contains(linearIndex))
					{
						info.MeshToNodeMapping[meshFilterIndex].Add(linearIndex);
					}
				}
			}
		}
		#endregion
	}
}
