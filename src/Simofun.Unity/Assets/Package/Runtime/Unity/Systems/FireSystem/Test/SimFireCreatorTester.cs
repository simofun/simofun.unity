﻿//-----------------------------------------------------------------------
// <copyright file="SimFireCreatorTester.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Test
{
	using Sirenix.OdinInspector;
	using UnityEngine;

	public class SimFireCreatorTester : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimFireCreatorTester), "Settings")]
		bool isEnabled;

		[SerializeField, Title("", "References")]
		SimFireCreator target;
		#endregion

		#region Properties
		public bool IsEnabled { get => this.isEnabled; set => this.isEnabled = value; }

		public virtual SimFireCreator Target
		{
			get => this.target != null ? this.target : this.GetComponent<SimFireCreator>();
			set => this.target = value;
		}
		#endregion

		#region Unity Methods
		protected virtual void Update()
		{
			if (!this.IsEnabled)
			{
				return;
			}

			if (Input.GetMouseButtonDown(0)
					&& Input.GetKey(KeyCode.LeftControl)
					&& Physics.Raycast(
							Camera.main.ScreenPointToRay(Input.mousePosition),
							out RaycastHit hit1,
							100))
			{
				this.Target.StartFireAtWorldPosition(hit1.point);
			}

			if (Input.GetMouseButtonDown(1)
					&& Input.GetKey(KeyCode.LeftControl)
					&& Physics.Raycast(
							Camera.main.ScreenPointToRay(Input.mousePosition),
							out RaycastHit hit2,
							100))
			{
				this.Target.CoolWorldPosition(hit2.point, 50f);
			}
		}
		#endregion
	}
}
