﻿//-----------------------------------------------------------------------
// <copyright file="SimFireDamageTester.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Systems.FireSystem.Test
{
	using UnityEngine;

	public class SimFireDamageTester : MonoBehaviour
	{
		#region Fields
		SimFireCreator fireCreator;
		#endregion

		#region Properties
		public SimFireCreator FireCreator
		{
			get => this.fireCreator ?? (this.fireCreator = FindAnyObjectByType<SimFireCreator>());
			set => this.fireCreator = value;
		}
		#endregion

		#region Unity Methods		
		/// <inheritdoc />
		protected virtual void Update()
		{
			var damage = this.FireCreator.GetDamage(this.transform.position);
			if (damage > 0)
			{
				Debug.Log("Damage: " + damage);
			}
		}
		#endregion
	}
}
