﻿//-----------------------------------------------------------------------
// <copyright file="SimTutorialController.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

#if UNITY_STANDALONE || UNITY_EDITOR
#define NOT_MOBILE_INPUT
#endif

namespace Simofun.Unity.Tutorial
{
	using Bayhaksam.Unity.Logging.DevelopmentBuild;
	using Sirenix.OdinInspector;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;
	using UnityEngine.UI;

	public class SimTutorialController : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimTutorialController), "Settings")]
		List<SimTutorialItemDefinition> tutorialItemDefinitions;

		[SerializeField, Title("", "References")]
		Transform pnlInfo;

		[SerializeField]
		Button btnOk;

		[SerializeField]
		Button btnQuit;

		[SerializeField]
		Text txtInfo;

		[SerializeField]
		Text txtTitle;

		[SerializeField]
		Transform indicator;
		#endregion

		#region Fields
		bool isWaitingForTouchEnd;

		int index = -1;

		string lastInfoText;

		Transform initialInfoPanelParent;

		SimTutorialItem currentItem;

		SimTutorialItemDefinition currentItemDefinition;
		#endregion

		#region Protected Methods
		protected RectTransform CurrentItemTarget
		{
			get
			{
				if (this.currentItemDefinition != null && this.currentItemDefinition.Target != null)
				{
					return this.currentItemDefinition.Target;
				}

				return this.currentItem?.Target;
			}
		}
		#endregion

		#region Unity Methods
		protected virtual void OnEnable() => this.Open();

		protected virtual void Update()
		{
			if (this.CurrentItemTarget == null
					|| this.currentItemDefinition == null
					|| !this.currentItemDefinition.IsAutoHandled)
			{
				return;
			}

			if (!this.isWaitingForTouchEnd && this.IsTouchBegan())
			{
				if (!RectTransformUtility.RectangleContainsScreenPoint(this.CurrentItemTarget, Input.mousePosition))
				{
					this.isWaitingForTouchEnd = false;
					this.btnQuit.onClick.Invoke();

					return;
				}

				this.isWaitingForTouchEnd = true;

				return;
			}

			if (this.isWaitingForTouchEnd && this.IsTouchEnd())
			{
				this.isWaitingForTouchEnd = false;
				this.currentItem.Complete();
			}
		}
		#endregion

		#region Public Methods
		public virtual void Close()
		{
			this.indicator.gameObject.SetActive(false);
			this.SetInfoPanelPosition(this.initialInfoPanelParent);
			this.btnOk.gameObject.SetActive(true);
			this.gameObject.SetActive(false);
		}

		public virtual void Open()
		{
			this.btnOk.onClick.RemoveAllListeners();
			this.btnQuit.onClick.RemoveAllListeners();
			this.currentItemDefinition = null;
			this.index = -1;
			this.initialInfoPanelParent = this.pnlInfo.transform.parent;

			this.btnOk.onClick.AddListener(this.NextTutorialItem);
			this.btnQuit.onClick.AddListener(this.Close);
		}

		public virtual void SetInitialInfoText(string value) => this.SetInfoText(value);

		public virtual void SetLastInfoText(string value) => this.lastInfoText = value;

		public virtual void SetTitleText(string value) => this.txtTitle.text = value;
		#endregion

		#region Methods
		#region Event Handlers
		void HandleOnCurrentItemCompleted() => this.NextTutorialItem();
		#endregion

		bool IsTouchBegan()
		{
#if NOT_MOBILE_INPUT
		return Input.GetMouseButtonDown(0);
#else
			if (Input.touchCount > 0)
			{
				Touch touch = Input.GetTouch(0);
				if (touch.phase == TouchPhase.Began)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
#endif
		}

		bool IsTouchEnd()
		{
#if NOT_MOBILE_INPUT
			return Input.GetMouseButtonUp(0);
#else
			if (Input.touchCount > 0)
			{
				Touch touch = Input.GetTouch(0);
				if (touch.phase == TouchPhase.Ended)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
#endif
		}

		SimTutorialItem GetNextTutorialItem(SimTutorialItemDefinition tutorialItemDefinition)
		{
			if (tutorialItemDefinition.TutorialItem != null)
			{
				return tutorialItemDefinition.TutorialItem;
			}

			SimTutorialItem tutorialItem;
			var itemIdentifiers = FindObjectsByType<SimTutorialItemIdentifier>(FindObjectsSortMode.None);
			if (itemIdentifiers == null || itemIdentifiers.Length < 1)
			{
				tutorialItem = this.GetNextTutorialItemByDefinition(tutorialItemDefinition);
				if (tutorialItem != null)
				{
					return tutorialItem;
				}

				debug.LogError(
					$"Not found any '{nameof(SimTutorialItemIdentifier)}' component to identify next tutorial item.",
					this);

				return null;
			}

			var itemIdentifier = itemIdentifiers.FirstOrDefault(i => i.Id == tutorialItemDefinition.Id);
			if (itemIdentifier == null)
			{
				tutorialItem = this.GetNextTutorialItemByDefinition(tutorialItemDefinition);
				if (tutorialItem != null)
				{
					return tutorialItem;
				}

				debug.LogError(
					$"Not found any '{nameof(SimTutorialItemIdentifier)}' by '{tutorialItemDefinition.Id}' id.", this);

				return null;
			}

			tutorialItem = itemIdentifier.GetComponent<SimTutorialItem>();
			if (tutorialItem != null)
			{
				return tutorialItem;
			}

			return this.GetNextTutorialItemByDefinition(tutorialItemDefinition);
		}

		SimTutorialItem GetNextTutorialItemByDefinition(SimTutorialItemDefinition tutorialItemDefinition)
		{
			if (tutorialItemDefinition.Target == null)
			{
				return null;
			}

			return tutorialItemDefinition.Target.GetComponent<SimTutorialItem>();
		}

		void NextTutorialItem()
		{
			if (this.currentItem != null)
			{
				this.currentItem.OnCompleted -= this.HandleOnCurrentItemCompleted;
			}

			this.index++;
			this.isWaitingForTouchEnd = false;

			if (this.index == this.tutorialItemDefinitions.Count)
			{
				this.btnOk.gameObject.SetActive(true);
				this.indicator.gameObject.SetActive(false);
				this.SetInfoText(this.lastInfoText);

				return;
			}

			if (this.index >= this.tutorialItemDefinitions.Count)
			{
				this.Close();

				return;
			}

			var itemDefinition = this.tutorialItemDefinitions[this.index];
			if (!itemDefinition.IsEnabled)
			{
				this.NextTutorialItem();

				return;
			}

			this.currentItem = this.GetNextTutorialItem(itemDefinition);
			if (this.currentItem == null)
			{
				debug.LogError(
					$"Current tutorial item by '{itemDefinition.Id}' id not found. Closing tutorial.",
					this);

				return;
			}

			if (!this.currentItem.IsEnabled)
			{
				debug.Log($"Skipped tutorial item by '{itemDefinition.Id}' id.", this.currentItem);

				this.NextTutorialItem();

				return;
			}

			this.currentItemDefinition = itemDefinition;
			this.btnOk.gameObject.SetActive(false);
			this.SetIndicatorPosition(this.currentItemDefinition.IndicatorPoint);
			this.SetInfoPanelPosition(this.currentItemDefinition.InfoPanelPoint);

			this.currentItem.OnCompleted += this.HandleOnCurrentItemCompleted;
			this.currentItem.Execute();
			this.SetInfoText(this.currentItem.Text);
		}

		void SetIndicatorPosition(Transform point)
		{
			this.indicator.gameObject.SetActive(true);
			this.indicator.SetParent(point);
			this.indicator.localPosition = Vector3.zero;
			this.indicator.localEulerAngles = Vector3.zero;
		}

		void SetInfoPanelPosition(Transform point)
		{
			this.pnlInfo.SetParent(point);
			this.pnlInfo.localPosition = Vector3.zero;
			this.pnlInfo.localEulerAngles = Vector3.zero;
		}

		void SetInfoText(string value) => this.txtInfo.text = value;
		#endregion
	}
}
