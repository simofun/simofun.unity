﻿//-----------------------------------------------------------------------
// <copyright file="SimTutorialItem.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Tutorial
{
	using Sirenix.OdinInspector;
	using System;
	using UnityEngine;

	public class SimTutorialItem : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField, Title(nameof(SimTutorialItem), "Settings")]
		bool isEnabled = true;

		[SerializeField, Title("", "References"), Tooltip("If not set, try to find by 'GetComponent'")]
		SimTutorialItemIdentifier identifier;

		[SerializeField]
		RectTransform target;
		#endregion

		#region Events
		public event Action OnCompleted;

		public event Action OnStarted;
		#endregion

		#region Properties
		public bool IsCompleted { get; protected set; }

		public bool IsEnabled => this.isEnabled;

		public bool IsRunning { get; protected set; }

		public RectTransform Target => this.target;

		public string Text { get; set; }

		public SimTutorialItemIdentifier Identifier =>
			this.identifier ?? (this.identifier = this.GetComponent<SimTutorialItemIdentifier>());
		#endregion

		#region Public Methods
		public virtual void Complete()
		{
			this.IsRunning = false;
			this.IsCompleted = true;
			this.InvokeOnCompleted();
		}

		public virtual void Execute()
		{
			this.IsCompleted = false;
			this.IsRunning = true;
			this.InvokeOnStarted();
		}
		#endregion

		#region Protected Methods
		protected virtual void InvokeOnCompleted() => this.OnCompleted?.Invoke();

		protected virtual void InvokeOnStarted() => this.OnStarted?.Invoke();
		#endregion
	}
}
