﻿//-----------------------------------------------------------------------
// <copyright file="SimTutorialItemDefinition.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Tutorial
{
	using System;
	using UnityEngine;

	[Serializable]
	public class SimTutorialItemDefinition
	{
		#region Unity Fields
		[SerializeField]
		string id;

		[SerializeField]
		SimTutorialItem tutorialItem;

		[SerializeField]
		bool isEnabled = true;

		[SerializeField]
		bool isAutoHandled = true;

		[SerializeField]
		RectTransform target;

		[SerializeField]
		RectTransform pnlInfoPoint;

		[SerializeField]
		RectTransform indicatorPoint;
		#endregion

		#region Properties
		public bool IsAutoHandled => this.isAutoHandled;

		public bool IsEnabled => this.isEnabled;

		public RectTransform IndicatorPoint => this.indicatorPoint;

		public RectTransform InfoPanelPoint => this.pnlInfoPoint;

		public RectTransform Target => this.target;

		public string Id => this.id;

		public SimTutorialItem TutorialItem => this.tutorialItem;
		#endregion
	}
}
