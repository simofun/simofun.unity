﻿//-----------------------------------------------------------------------
// <copyright file="SimTutorialItemIdentifier.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Tutorial
{
	using UnityEngine;

	public class SimTutorialItemIdentifier : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField]
		string id;
		#endregion

		#region Properties
		public string Id => this.id;
		#endregion
	}
}
