﻿//-----------------------------------------------------------------------
// <copyright file="SimTutorialItemHandlerBase.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Tutorial
{
	using Sirenix.OdinInspector;
	using UnityEngine;

	public abstract class SimTutorialItemHandlerBase : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField,
			Title(nameof(SimTutorialItemHandlerBase), "References"),
			Tooltip("If not set, try to find by 'GetComponent'")]
		SimTutorialItem tutorialItem;
		#endregion

		#region Properties
		public SimTutorialItem TutorialItem =>
			this.tutorialItem ?? (this.tutorialItem = this.GetComponent<SimTutorialItem>());
		#endregion

		#region Unity Methods
		protected virtual void Start()
		{
			this.TutorialItem.OnCompleted += this.WhenCompleted;
			this.TutorialItem.OnStarted += this.WhenStarted;
		}

		protected virtual void OnDestroy()
		{
			this.TutorialItem.OnCompleted -= this.WhenCompleted;
			this.TutorialItem.OnStarted -= this.WhenStarted;
		}
		#endregion

		#region Protected Methods
		protected virtual void WhenCompleted()
		{
		}

		protected virtual void WhenStarted()
		{
		}
		#endregion
	}
}
