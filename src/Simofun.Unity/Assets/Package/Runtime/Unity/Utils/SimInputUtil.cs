﻿//-----------------------------------------------------------------------
// <copyright file="SimInputUtil.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Utils
{
	using UnityEngine;

	/// <summary>
	/// Utility class for <see cref="Input"/>.
	/// </summary>
	public static class SimInputUtil
	{
		#region Public Methods
		public static float GetCrossPlatformHorizontalAxis(float steeringSpeedForMobile = 3)
		{
#if UNITY_EDITOR
			return Input.GetAxis("Horizontal");
#else
			if (Input.touchCount <= 0)
			{
				return 0;
			}

			var touch = Input.touches[0];
			if (touch.deltaPosition.x == 0 || touch.deltaPosition.magnitude == 0)
			{
				return 0;
			}

			return Mathf.Clamp(ConvertTouchToAxisX(touch, steeringSpeedForMobile), -1, 1);
#endif
		}

		public static float GetCrossPlatformVerticalAxis(float steeringSpeedForMobile = 3)
		{
#if UNITY_EDITOR
			return Input.GetAxis("Vertical");
#else
			if (Input.touchCount <= 0)
			{
				return 0;
			}

			var touch = Input.touches[0];
			if (touch.deltaPosition.y == 0 || touch.deltaPosition.magnitude == 0)
			{
				return 0;
			}

			return Mathf.Clamp(ConvertTouchToAxisY(touch, steeringSpeedForMobile), -1, 1);
#endif
		}
		#endregion

		#region Methods
		static float ConvertTouchToAxisX(Touch touch, float steeringSpeedForMobile = 3)
		{
			var ret = touch.deltaPosition.magnitude * touch.deltaTime;
			var multiplier = touch.deltaPosition.x >= 0 ? 1 : -1;
			ret *= multiplier;

			return ret * steeringSpeedForMobile;
		}

		static float ConvertTouchToAxisY(Touch touch, float steeringSpeedForMobile = 3)
		{
			var ret = touch.deltaPosition.magnitude * touch.deltaTime;
			var multiplier = touch.deltaPosition.y >= 0 ? 1 : -1;
			ret *= multiplier;

			return ret * steeringSpeedForMobile;
		}
		#endregion
	}
}
