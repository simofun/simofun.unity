﻿//-----------------------------------------------------------------------
// <copyright file="SimObjectUtil.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Utils
{
	using UnityEngine;

	/// <summary>
	/// Utility class for <see cref="Object"/>.
	/// </summary>
	public static class SimObjectUtil
	{
		#region Public Methods		
		/// <summary>
		/// Destroys the specified object. Calls <see cref="Object.Destroy"/>
		/// if <see cref="Application.isPlaying"/> is true, otherwise calls <see cref="Object.DestroyImmediate"/>.
		/// </summary>
		/// <param name="obj">The object to destroy</param>
		public static void Destroy(Object obj)
		{
			if (Application.isPlaying)
			{
				Object.Destroy(obj);

				return;
			}

			Object.DestroyImmediate(obj);
		}
		#endregion
	}
}
