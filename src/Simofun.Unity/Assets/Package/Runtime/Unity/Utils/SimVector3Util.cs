﻿//-----------------------------------------------------------------------
// <copyright file="SimVector3Util.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Utils
{
	using UnityEngine;

	/// <summary>
	/// Utility class for <see cref="Vector3"/>.
	/// </summary>
	public static class SimVector3Util
	{
		#region Public Methods
		/// <summary>
		/// Returns a random vector3 between min and max. (Inclusive)
		/// </summary>
		/// <returns>The <see cref="Vector3"/>.</returns>
		/// <param name="min">Minimum.</param>
		/// <param name="max">Max.</param>
		public static Vector3 GetRandomVector3Between(Vector3 min, Vector3 max) =>
			min + Random.Range(0, 1) * (max - min);

		/// <summary>
		/// Gets the random vector3 between the min and max points provided.
		/// Also uses minPadding and maxPadding (in metres).
		/// maxPadding is the padding amount to be added on the other Vector3's side.
		/// Setting minPadding and maxPadding to 0 will make it return inclusive values.
		/// </summary>
		/// <returns>The <see cref="Vector3"/>.</returns>
		/// <param name="min">Minimum.</param>
		/// <param name="max">Max.</param>
		/// <param name="minPadding">Minimum padding.</param>
		/// <param name="maxPadding">Max padding.</param>
		public static Vector3 GetRandomVector3Between(Vector3 min, Vector3 max, float minPadding, float maxPadding) =>
			GetRandomVector3Between(min + minPadding * (max - min), max + maxPadding * (min - max));
		#endregion
	}
}
