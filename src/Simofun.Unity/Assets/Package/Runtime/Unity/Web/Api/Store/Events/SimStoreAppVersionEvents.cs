﻿//-----------------------------------------------------------------------
// <copyright file="SimStoreAppVersionEvents.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Web.API.Store.Events
{
	using Simofun.Unity.Events;
	using UnityEngine;

	/// <summary>
	/// Publishing when store app version events are received.
	/// </summary>
	public sealed class SimStoreAppVersionEvents
	{
		/// <summary>
		/// Publishing when a store app version latest event is received.
		/// </summary>
		public class OnLatest : SimBaseUnityEvent
		{
			#region Fields
			string applicationIdentifier;
			
			System.Version version;
			#endregion

			#region Properties
			public string ApplicationIdentifier
			{
				get => this.applicationIdentifier ??= Application.identifier;
				set
				{
					if (value == null)
					{
						return;
					}

					this.applicationIdentifier = value;
				}
			}

			public System.Version Version
			{
				get => this.version ??= new System.Version(Application.version);
				set
				{
					if (value == null)
					{
						return;
					}

					this.version = value;
				}
			}
			#endregion
		}

		/// <summary>
		/// Publishing when a store app version update event is received.
		/// </summary>
		public class OnUpdate : SimBaseUnityEvent
		{
			#region Fields
			string applicationIdentifier;
			
			System.Version version;
			#endregion

			#region Properties
			public string ApplicationIdentifier
			{
				get => this.applicationIdentifier ??= Application.identifier;
				set
				{
					if (value == null)
					{
						return;
					}

					this.applicationIdentifier = value;
				}
			}

			public System.Version Version
			{
				get => this.version ??= new System.Version(Application.version);
				set
				{
					if (value == null)
					{
						return;
					}

					this.version = value;
				}
			}
			#endregion
		}
	}
}
