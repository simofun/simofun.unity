﻿//-----------------------------------------------------------------------
// <copyright file="ISimStoreAppVersionApi.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Web.Api.Store
{
	using Cysharp.Threading.Tasks;
	using Simofun.Unity.Web.API.Store.Model;

	public interface ISimStoreAppVersionApi
	{
		UniTask<SimStoreAppVersionResponse> Get();

		UniTask<SimStoreAppVersionResponse> Get(string applicationIdentifier);
	}
}
