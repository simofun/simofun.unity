﻿//-----------------------------------------------------------------------
// <copyright file="SimStoreAppVersionResponse.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Web.API.Store.Model
{
	using UnityEngine;

	[System.Serializable]
	public class SimStoreAppVersionResponse
	{
		#region Fields
		string applicationIdentifier;
		
		System.Version version;
		#endregion

		#region Properties
		public string ApplicationIdentifier
		{
			get => this.applicationIdentifier ??= Application.identifier;
			set
			{
				if (value == null)
				{
					return;
				}

				this.applicationIdentifier = value;
			}
		}
		
		public System.Version Version
		{
			get => this.version ??= new System.Version(Application.version);
			set
			{
				if (value == null)
				{
					return;
				}

				this.version = value;
			}
		}
		#endregion
	}
}
