﻿//-----------------------------------------------------------------------
// <copyright file="SimAndroidStoreAppVersionApi.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Web.Api.Store.Platform.Android
{
	using Cysharp.Threading.Tasks;
	using Simofun.Unity.Web.API.Store.Model;
	using System.Linq;
	using System.Text.RegularExpressions;
	using UnityEngine.Networking;

	public class SimAndroidStoreAppVersionApi : SimStoreAppVersionApiBase
	{
		#region Fields
		const string pattern = @"\[\[\""(\d+\.)(\d+\.)(\d+\.)?(\d+)\""\]\]";

		const string urlFormat = "https://play.google.com/store/apps/details?id={0}";
		#endregion

		#region Protected Methods
		/// <inheritdoc />
		protected async override UniTask<SimStoreAppVersionResponse> HandleOnGet(string applicationIdentifier)
		{
			string responseText;

			using (var request = UnityWebRequest.Get(string.Format(urlFormat, applicationIdentifier)))
			{
				request.timeout = 5;

				try
				{
					await request.SendWebRequest();
				}
				catch (System.Exception)
				{
				}

				if (request.result is UnityWebRequest.Result.ConnectionError
						or UnityWebRequest.Result.ProtocolError
						or UnityWebRequest.Result.DataProcessingError)
				{
					return await this.FakeApi.Get(applicationIdentifier);
				}

				responseText = request.downloadHandler.text;
			}

			if (string.IsNullOrEmpty(responseText))
			{
				return await this.FakeApi.Get(applicationIdentifier);
			}

			try
			{
				var match = Regex.Matches(responseText, pattern, RegexOptions.Multiline).FirstOrDefault();
				if (match == null)
				{
					return await this.FakeApi.Get(applicationIdentifier);
				}

				return await UniTask.FromResult(
					new SimStoreAppVersionResponse
					{
						ApplicationIdentifier = applicationIdentifier,
						Version = new System.Version(
							match.Value.Replace("\"", string.Empty)
								.Replace("[", string.Empty)
								.Replace("]", string.Empty))
					});
			}
			catch (System.Exception)
			{
				return await this.FakeApi.Get(applicationIdentifier);
			}
		}

		#endregion
	}
}
