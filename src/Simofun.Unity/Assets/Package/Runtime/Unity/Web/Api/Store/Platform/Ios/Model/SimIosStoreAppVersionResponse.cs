﻿//-----------------------------------------------------------------------
// <copyright file="SimIosStoreAppVersionResponse.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Web.Api.Store.Platform.Ios
{
	using Newtonsoft.Json;
	using System.Collections.Generic;

	public class SimIosStoreAppVersionResponse
	{
		#region Properties
		[JsonProperty("resultCount")]
		public int ResultCount { get; set; }

		[JsonProperty("results")]
		public List<SimIosStoreAppVersionResponseResult> Results { get; set; } = new();
		#endregion

		#region Nested Types
		public class SimIosStoreAppVersionResponseResult
		{
			[JsonProperty("version")]
			public string Version { get; set; }
		}
		#endregion
	}
}
