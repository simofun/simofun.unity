﻿//-----------------------------------------------------------------------
// <copyright file="SimIosStoreAppVersionApi.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Web.Api.Store.Platform.Ios
{
	using Cysharp.Threading.Tasks;
	using Newtonsoft.Json;
	using Simofun.Unity.Web.API.Store.Model;
	using System.Linq;
	using UnityEngine.Networking;

	public class SimIosStoreAppVersionApi : SimStoreAppVersionApiBase
	{
		#region Fields
		const string urlFormat = "https://itunes.apple.com/lookup?bundleId={0}";
		#endregion

		#region Protected Methods
		/// <inheritdoc />
		protected async override UniTask<SimStoreAppVersionResponse> HandleOnGet(string applicationIdentifier)
		{
			string responseText;

			using (var request = UnityWebRequest.Get(string.Format(urlFormat, applicationIdentifier)))
			{
				request.timeout = 5;

				try
				{
					await request.SendWebRequest();
				}
				catch (System.Exception)
				{
				}

				if (request.result is UnityWebRequest.Result.ConnectionError
						or UnityWebRequest.Result.ProtocolError
						or UnityWebRequest.Result.DataProcessingError)
				{
					return await this.FakeApi.Get(applicationIdentifier);
				}

				responseText = request.downloadHandler.text;
			}

			if (string.IsNullOrEmpty(responseText))
			{
				return await this.FakeApi.Get(applicationIdentifier);
			}

			try
			{
				var response = JsonConvert.DeserializeObject<SimIosStoreAppVersionResponse>(responseText);
				if (response.ResultCount <= 0)
				{
					return await this.FakeApi.Get(applicationIdentifier);
				}

				return await UniTask.FromResult(
					new SimStoreAppVersionResponse
					{
						ApplicationIdentifier = applicationIdentifier,
						Version = new System.Version(response.Results.First().Version)
					});
			}
			catch (System.Exception)
			{
				return await this.FakeApi.Get(applicationIdentifier);
			}
		}
		#endregion
	}
}
