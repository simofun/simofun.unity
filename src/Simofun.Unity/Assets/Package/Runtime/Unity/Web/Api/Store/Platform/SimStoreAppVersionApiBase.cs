﻿//-----------------------------------------------------------------------
// <copyright file="SimStoreAppVersionApiBase.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Web.Api.Store.Platform
{
	using Cysharp.Threading.Tasks;
	using Simofun.Unity.Web.API.Store.Model;
	using UnityEngine;

	public abstract class SimStoreAppVersionApiBase : ISimStoreAppVersionApi
	{
		#region Protected Properties
		protected virtual ISimStoreAppVersionApi FakeApi { get; set; } = new SimFakeStoreAppVersionApi();
		#endregion

		#region Public Methods
		/// <inheritdoc />
		public async UniTask<SimStoreAppVersionResponse> Get() => await this.Get(Application.identifier);

		/// <inheritdoc />
		public async UniTask<SimStoreAppVersionResponse> Get(string applicationIdentifier)
		{
			if (Application.internetReachability == NetworkReachability.NotReachable)
			{
				return await this.FakeApi.Get(applicationIdentifier);
			}

			return await this.HandleOnGet(applicationIdentifier);
		}
		#endregion

		#region Protected Methods
		protected abstract UniTask<SimStoreAppVersionResponse> HandleOnGet(string applicationIdentifier);
		#endregion
	}
}
