﻿//-----------------------------------------------------------------------
// <copyright file="SimFakeStoreAppVersionApi.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Web.Api.Store
{
	using Cysharp.Threading.Tasks;
	using Simofun.Unity.Web.API.Store.Model;
	using UnityEngine;

	public class SimFakeStoreAppVersionApi : ISimStoreAppVersionApi
	{
		#region Public Methods
		/// <inheritdoc />
		public async UniTask<SimStoreAppVersionResponse> Get() => await this.Get(Application.identifier);

		/// <inheritdoc />
		public async UniTask<SimStoreAppVersionResponse> Get(string applicationIdentifier) =>
			await UniTask.FromResult(new SimStoreAppVersionResponse { ApplicationIdentifier = applicationIdentifier });
		#endregion
	}
}
