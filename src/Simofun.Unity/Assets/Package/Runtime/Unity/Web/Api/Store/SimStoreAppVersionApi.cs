//-----------------------------------------------------------------------
// <copyright file="SimStoreAppVersionApi.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Web.Api.Store
{
	using Cysharp.Threading.Tasks;
	using Simofun.UniRx.Unity;
	using Simofun.Unity.DesignPatterns.Singleton;
	using Simofun.Unity.Web.API.Store.Events;
	using Simofun.Unity.Web.API.Store.Model;
	using Sirenix.OdinInspector;
	using UnityEngine;

	public class SimStoreAppVersionApi
		: SimGlobalSingletonBase<ISimStoreAppVersionApi, SimStoreAppVersionApi>, ISimStoreAppVersionApi
	{
		#region Unity Properties
		[field: SerializeField,
			Title(nameof(SimStoreAppVersionApi), "Settings"),
			Tooltip("Custom application identifier. Commonly, using for test purposes.")]
		protected virtual string ApplicationIdentifier { get; set; }
		#endregion

		#region Protected Properties
		protected virtual ISimStoreAppVersionApi FakeApi { get; set; } = new SimFakeStoreAppVersionApi();
		#endregion
		
		#region Unity Methods
		/// <inheritdoc />
		protected override void Awake()
		{
			base.Awake();

#if UNITY_EDITOR || !(UNITY_ANDROID || UNITY_IOS)
			return;
#endif
			if (string.IsNullOrEmpty(this.ApplicationIdentifier))
			{
				this.ApplicationIdentifier = Application.identifier;
			}

			var currentVersion = new System.Version(Application.version);

			UniTask.Create(async () =>
			{
				var response = await this.Get(this.ApplicationIdentifier);
				var version = response.Version;
				if (currentVersion == version)
				{
					SimMessageBus.Publish(new SimStoreAppVersionEvents.OnLatest
					{
						ApplicationIdentifier = response.ApplicationIdentifier,
						Version = version
					});

					return;
				}

				if (currentVersion < version)
				{
					SimMessageBus.Publish(new SimStoreAppVersionEvents.OnUpdate
					{
						ApplicationIdentifier = response.ApplicationIdentifier,
						Version = version
					});

					return;
				}
				
				Debug.Log($"Current version '{currentVersion}' is greater than latest version '{version}'.");
			});
		}
		#endregion

		#region Public Methods
		/// <inheritdoc />
		public async UniTask<SimStoreAppVersionResponse> Get() => await this.Get(Application.identifier);

		/// <inheritdoc />
		public async UniTask<SimStoreAppVersionResponse> Get(string applicationIdentifier)
		{
			if (Application.internetReachability == NetworkReachability.NotReachable)
			{
				return await this.FakeApi.Get(applicationIdentifier);
			}
			
#if UNITY_ANDROID
			return await UniTask.FromResult(
				(await new Platform.Android.SimAndroidStoreAppVersionApi().Get(applicationIdentifier)));
#elif UNITY_IOS
			return await UniTask.FromResult(
				(await new Platform.Ios.SimIosStoreAppVersionApi().Get(applicationIdentifier)));
#else
			return await this.FakeApi.Get(applicationIdentifier);
#endif
		}
		#endregion
	}
}
