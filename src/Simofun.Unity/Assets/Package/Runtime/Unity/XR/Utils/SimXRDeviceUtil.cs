//-----------------------------------------------------------------------
// <copyright file="SimXRDeviceUtil.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.XR.Utils
{
	using Cysharp.Threading.Tasks;
	using Simofun.CysharpUniTask.Unity.Utils;
	using System;
	using System.Collections.Generic;
	using System.Threading;
	using UnityEngine;
	using UnityEngine.XR;

	/// <summary>
	/// Utility class for <see cref="XRDevice"/>.
	/// </summary>
	public static class SimXRDeviceUtil
	{
		#region Properties
		public static bool IsPresent { get; private set; }
		#endregion

		#region Public Methods		
		/// <summary>
		/// Gets the any VR device is present. Continuously, checks value.
		/// To optimization use cached version <see cref="IsPresent"/>.
		/// </summary>
		/// <returns></returns>
		public static bool GetIsPresent()
		{
			var xrDisplaySubsystems = new List<XRDisplaySubsystem>();
			SubsystemManager.GetInstances(xrDisplaySubsystems);

			foreach (var xrDisplay in xrDisplaySubsystems)
			{
				if (xrDisplay.running)
				{
					return true;
				}
			}

			return false;
		}
		#endregion

		#region Methods
		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
		static void Init()
		{
			var cts = new CancellationTokenSource();
			cts.CancelAfterSlim(TimeSpan.FromSeconds(5)); // 5 sec timeout.

			SimUniTaskUtil.ExecuteOrWaitUntil(
				() => GetIsPresent(),
				() =>
				{ IsPresent = GetIsPresent(); },
				cancellationToken: cts.Token);
		}
		#endregion
	}
}
