﻿//-----------------------------------------------------------------------
// <copyright file="SimMinMaxSliderAttribute.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity
{
	using UnityEngine;

	/// <summary>
	/// <seealso cref="https://github.com/GucioDevs/SimpleMinMaxSlider"/>
	/// </summary>
	/// <seealso cref="PropertyAttribute" />
	public class SimMinMaxSliderAttribute : PropertyAttribute
	{
		#region Public Fields
		public float min;

		public float max;
		#endregion

		#region Constructors
		public SimMinMaxSliderAttribute(float min, float max)
		{
			this.min = min;
			this.max = max;
		}
		#endregion
	}
}
