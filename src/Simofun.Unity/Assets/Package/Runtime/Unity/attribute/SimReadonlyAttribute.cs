﻿//-----------------------------------------------------------------------
// <copyright file="SimReadonlyAttribute.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity
{
	using UnityEngine;

	/// <summary>
	/// Readonly attribute.
	/// Attribute is use only to mark Readonly properties.
	/// </summary>
	public class SimReadonlyAttribute : PropertyAttribute
	{
	}
}
