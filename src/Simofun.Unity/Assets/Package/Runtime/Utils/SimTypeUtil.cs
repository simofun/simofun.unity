﻿//-----------------------------------------------------------------------
// <copyright file="SimTypeUtil.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

namespace Simofun.Utils
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Utility class for <see cref="Type"/>.
    /// </summary>
    public static class SimTypeUtil
    {
        #region Fields
        static List<Type> types;
        #endregion

        #region Private Properties
        static List<Type> Types
        {
            get => types != null
                        ? types
                        : (types = GetAllTypes().Cast<Type>().ToList());
        }
        #endregion

        #region Methods
        static IEnumerable<Type> GetTypesUnderNamespaceInternal(IEnumerable<Type> types, string nameSpace) =>
            types.Where(t => t.Namespace != null && t.Namespace.StartsWith(nameSpace));

        static Type GetTypeWithNameInternal(IEnumerable<Type> types, string type) =>
            types.FirstOrDefault(t => t.Name.Equals(type));

        static Type GetTypeWithFullNameInternal(IEnumerable<Type> types, string type) =>
            types.FirstOrDefault(t => t.FullName.Equals(type));
        #endregion

        #region Public Methods
        public static IEnumerable<string> GetTypesAsString<T>() => GetTypesAsString<T>(AppDomain.CurrentDomain);

        public static IEnumerable<string> GetTypesAsString<T>(AppDomain domain)
        {
            foreach (var item in GetChildrenTypes<T>(domain).Select(x => x.FullName))
            {
                yield return item;
            }
        }

        public static IEnumerable<Type> GetChildrenTypes<T>()
        {
            foreach (var item in GetChildrenTypes<T>(AppDomain.CurrentDomain))
            {
                yield return item;
            }
        }

        public static IEnumerable<Type> GetChildrenTypes<T>(AppDomain domain)
        {
            var type = typeof(T);
            var query = (from domainAssembly in domain.GetAssemblies()
                         from assemblyType in domainAssembly.GetTypes()
                         where type.IsAssignableFrom(assemblyType) && assemblyType != type
                         select assemblyType);
            foreach (var item in query)
            {
                yield return item;
            }
        }

        public static IEnumerable GetAllTypes() =>
            AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes());

        public static IEnumerable GetFilteredClassList<T>() =>
            GetTypes<T>().Where(t => t.IsClass).Select(t => t.Name);

        public static IEnumerable GetFilteredClassListFull<T>() =>
            GetTypes<T>().Where(t => t.IsClass).Select(t => t.FullName);

        public static IEnumerable GetFilteredTypeList<T>() =>
            GetTypes<T>().Select(t => t.Name);

        public static IEnumerable GetFilteredTypeListFull<T>() =>
            GetTypes<T>().Select(t => t.FullName);

        public static IEnumerable<Type> GetTypes<T>() =>
            SimTypeUtil.GetChildrenTypes<T>();

        public static IEnumerable<Type> GetTypesUnderNamespace(Type type) =>
            GetTypesUnderNamespace(type.Namespace);

        public static IEnumerable<Type> GetTypesUnderNamespaceCached(Type type) =>
            GetTypesUnderNamespaceCached(type.Namespace);

        public static IEnumerable<Type> GetTypesUnderNamespace(string nameSpace) =>
            GetTypesUnderNamespaceInternal(GetAllTypes().Cast<Type>(), nameSpace);

        public static IEnumerable<Type> GetTypesUnderNamespaceCached(string nameSpace) =>
            GetTypesUnderNamespaceInternal(Types, nameSpace);

        public static Type GetType(string type) =>
            GetTypeWithFullNameInternal(GetAllTypes().Cast<Type>(), type);

        public static Type GetTypeCached(string type) =>
            GetTypeWithFullNameInternal(Types, type);

        public static Type GetTypeWithName(string type) =>
            GetTypeWithNameInternal(GetAllTypes().Cast<Type>(), type);

        public static Type GetTypeWithNameCached(string type) =>
            GetTypeWithNameInternal(Types, type);
        #endregion
    }
}
